/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.studentquizscores;

import com.sg.sudentquizscores.ui.UserIO;
import com.sg.sudentquizscores.ui.UserIOImpl;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * @author cjsm12
 */
public class StudentQuizScores {

    private static Map<Integer, Student> studentList = new HashMap<>();
    private static UserIO io = new UserIOImpl();
    private static int ID = 1;

    /*
    In this lab, you will write a program that stores quiz scores for each student in a 
    class and calculates the average quiz score for each student on request.  The user 
    should be able to do the following:

    View a list of students in the system
    Add a student to the system
    Remove a student from the system
    View a list of quiz scores for a given student
    View the average quiz score for a given student
    It is up to you to design and implement a reasonable UI menu system.  Design the UI 
    and program flow chart before coding — you must review both with the instructor 
    before proceeding.

    Your program must have the following features:

    This program will be a Java Console Application called StudentQuizGrades.
    The program must use the UserIO class created previously for all console input and 
    output.
    The program must store student quiz data in a HashMap that has the studentID as 
    the key and a Student as the value. 

    Extension:
    Calculate the average quiz score for the entire class.
     */
    
    public static void main(String[] args) {
        boolean loop = true;

        do {
            
            io.print("\n--- STUDENT QUIZ SCORES ---\n");
            io.print("===========================\n");
            io.print("1) View List of Students\n");
            io.print("2) Add Student\n");
            io.print("3) Remove Student\n");
            io.print("4) View list of quiz scores for a given student\n");
            io.print("5) View average quiz score for given student\n");
            io.print("6) See class average\n");
            io.print("7) Exit\n");
            int choice = io.readInt("\nWhat do you want to do: ", 1, 7);

            switch (choice) {
                case 1:
                    listAllStudents();
                    break;
                case 2:
                    addStudent();
                    break;
                case 3:
                    removeStudent();
                    break;
                case 4:
                    viewGradesForStudent();
                    break;
                case 5:
                    viewAverageQuizScoreForStudent();
                    break;
                case 6:
                    calculateClassAverage();
                    break;
                case 7:
                    loop = false;
                    break;
                default:
                    break;
            }

        } while (loop);

        exit();
    }

    private static void addStudent() {
        io.print("---ADD STUDENT---\n");
        io.print("=================\n");
        List<Double> quizScores = new ArrayList<>();
        String firstName = io.readString("What is the students first name: ");
        String lastName = io.readString("What is the students last name: ");
        int gradesToEnter = io.readInt("Do you have grades for " + firstName + " to enter: [1) Yes 2) No]", 1, 2);
        if (gradesToEnter == 1) {
            boolean loop = true;
            while (loop) {
                double grade = io.readDouble("Enter grade(or press 101 to exit): ", 0, 101);
                if (grade == 101) {
                    break;
                }
                quizScores.add(grade);
                loop = true;
            }
        }
        Student student = new Student();
        if (student.getStudentId() < ID) {
            student.setStudentId(ID);
            ID++;
        }
        student.setFirstName(firstName);
        student.setLastName(lastName);
        student.setQuizScores(quizScores);

        studentList.put(student.getStudentId(), student);
        
        io.print("Student successfully added\n");
    }

    private static void removeStudent() {
        io.print("---REMOVE STUDENT---\n");
        io.print("====================\n");
        int studentID = io.readInt("What is the students ID(Press -1 if you don't know): ");
        if (studentID == -1) {
            String firstName = io.readString("What is the students first name: ");
            List<Student> studentsWithFirstName
                    = studentList.values().stream()
                            .filter(s -> s.getFirstName().equalsIgnoreCase(firstName))
                            .collect(Collectors.toList());

            io.print("All students matching first name and their ID\n");
            for (Student currentStudent : studentsWithFirstName) {
                io.print("First name: " + currentStudent.getFirstName()
                        + " | Last name: " + currentStudent.getLastName()
                        + " | ID: " + currentStudent.getStudentId() + "\n");
            }
        }
        int matchingID = io.readInt("What is the studentID: ");
        studentList.remove(matchingID);
        io.print("Student with " + matchingID + " removed!\n");
    }

    private static void listAllStudents() {
        io.print("---LIST ALL STUDENTS---\n");
        io.print("=======================\n");
        Collection<Student> allStudents = studentList.values();
        for (Student currentStudent : allStudents) {
            io.print("StudentID " + currentStudent.getStudentId()
                    + " | First name: "
                    + currentStudent.getFirstName()
                    + " | Last name: " + currentStudent.getLastName() + "\n");
        }
    }

    private static void viewGradesForStudent() {
        io.print("---VIEW STUDENT GRADES---\n");
        io.print("=========================\n");
        int studentID = io.readInt("What is the students ID(Press -1 if you don't know): ");
        if (studentID == -1) {
            String firstName = io.readString("What is the students first name: ");
            List<Student> studentsWithFirstName
                    = studentList.values().stream()
                            .filter(s -> s.getFirstName().equalsIgnoreCase(firstName))
                            .collect(Collectors.toList());

            io.print("All students matching first name and their ID ");
            for (Student currentStudent : studentsWithFirstName) {
                io.print("First name: " + currentStudent.getFirstName()
                        + " | Last name: " + currentStudent.getLastName()
                        + " | ID: " + currentStudent.getStudentId() + "\n");
            }
        }
        int matchingID = io.readInt("What is the studentID: ");
        Student matchingStudent = studentList.get(matchingID);
        List<Double> studentsGrades = matchingStudent.getQuizScores();
        io.print("---ALL GRADES---\n");
        for(Double currentGrade : studentsGrades){
            io.print(currentGrade.toString() + "\n");
        }
    }

    private static void viewAverageQuizScoreForStudent() {
        io.print("---AVERAGE GRADE FOR STUDENT---\n");
        io.print("===============================\n");
        int studentID = io.readInt("What is the students ID(Press -1 if you don't know): ");
        if (studentID == -1) {
            String firstName = io.readString("What is the students first name: ");
            List<Student> studentsWithFirstName
                    = studentList.values().stream()
                            .filter(s -> s.getFirstName().equalsIgnoreCase(firstName))
                            .collect(Collectors.toList());

            io.print("All students matching first name and their ID ");
            for (Student currentStudent : studentsWithFirstName) {
                io.print("First name: " + currentStudent.getFirstName()
                        + " | Last name: " + currentStudent.getLastName()
                        + " | ID: " + currentStudent.getStudentId() + "\n");
            }
        }
        int matchingID = io.readInt("What is the studentID: ");
        Student matchingStudent = studentList.get(matchingID);
        List<Double> studentsGrades = matchingStudent.getQuizScores();
        int gradesSum = 0;
        for(Double currentNumber : studentsGrades){
            gradesSum += currentNumber;
        }
        double average = gradesSum / studentsGrades.size();
        io.print("The average for this student = " + average);
    }
    
    //Additional challenge 
    private static void calculateClassAverage(){
        io.print("---CALCULATE CLASS AVERAGE---\n");
        io.print("=============================\n");
        Collection<Student> allStudents = studentList.values();
        List<Double> allStudentsGrades = new ArrayList<>();
        int allGradesAdded = 0;
        int gradesInClass = 0;
        for(Student currentStudent : allStudents){
            allStudentsGrades = currentStudent.getQuizScores();
            for(Double currentGrade : allStudentsGrades){
                allGradesAdded += currentGrade;
                gradesInClass++;
            }
        }
        double classAverage = allGradesAdded / gradesInClass;
        
        io.print("The class average = " + classAverage);
    }

    private static void exit() {
        io.print("Goodbye!");
    }

}
