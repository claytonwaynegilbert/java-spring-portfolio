/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.studentquizscores;

import java.util.List;

/**
 *
 * @author cjsm12
 */
public class Student {
    
    private int studentId;
    private String firstName;
    private String lastName;
    private List<Double> quizScores;

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<Double> getQuizScores() {
        return quizScores;
    }

    public void setQuizScores(List<Double> quizScores) {
        this.quizScores = quizScores;
    }
    
}
