/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.sudentquizscores.ui;

import java.util.Scanner;

/**
 *
 * @author cjsm12
 */
public class UserIOImpl implements UserIO {

    /*
    Design a class (no main method) that has methods to ask for and retrieve keyboard 
    input from the user and to print information out to the console. Your class must 
    implement the following interface:
    
    The methods that you implement must behave in the following manner:

    Display a prompt to the user and read in an integer. The prompt value should be 
    passed in as a parameter and the value that is read in should be the return value 
    of the method.
    
    Display a prompt to the user and read in an integer between a max value and a min 
    value. The prompt value, min, and max should be passed in as parameters. The value 
    read in from the console should be the return value of the method. The method must 
    keep asking the user for input until the value is in range.
    
    Display a prompt to the user and read in a string. The prompt value should be passed 
    in as a parameter and the value that is read in should be the return value of the 
    method.
    
    Display a prompt to the user and read in a float. The prompt value should be passed in
    as a parameter and the value that is read in should be the return value of the method.
    
    Display a prompt to the user and read in a float between a max value and a min value. 
    The prompt value, min, and max should be passed in as parameters. The value read in 
    from the console should be the return value of the method. The method must keep asking 
    the user for input until the value is in range.
    
    Display a prompt to the user and read in a double. The prompt value should be passed in 
    as a parameter and the value that is read in should be the return value of the method.
    
    Display a prompt to the user and read in a double between a max value and a min value. 
    The prompt value, min, and max should be passed in as parameters. The value read in from
    the console should be the return value of the method. The method must keep asking the 
    user for input until the value is in range.
    Print a given string to the console. The prompt value should be passed in as a parameter.
    
    After completing this class, refactor your Simple Calculator program to use this object 
    for all of its console input and output.
     */
    
    Scanner sc = new Scanner(System.in);

    @Override
    public void print(String message) {
        System.out.print(message);
    }

    @Override
    public double readDouble(String prompt) {
        System.out.print(prompt);
        return sc.nextDouble();
    }

    @Override
    public double readDouble(String prompt, double min, double max) {
        boolean loop = false;
        do {
            System.out.print(prompt);
            double value = sc.nextDouble();
            if (value >= min && value <= max) {
                loop = false;
                return value;
            } else {
                System.out.print("Value must be between " + min + " and " + max);
                loop = true;
            }
        } while (loop);

        return 0;
    }

    @Override
    public float readFloat(String prompt) {
        System.out.print(prompt);
        return sc.nextFloat();

    }

    @Override
    public float readFloat(String prompt, float min, float max) {
        boolean loop = false;
        do {
            System.out.print(prompt);
            float value = sc.nextFloat();
            if (value >= min && value <= max) {
                loop = false;
                return value;
            } else {
                System.out.print("Value must be between " + min + " and " + max);
                loop = true;
            }
        } while (loop);

        return 0;
    }

    @Override
    public int readInt(String prompt) {
        System.out.print(prompt);
        return sc.nextInt();
    }

    @Override
    public int readInt(String prompt, int min, int max) {
        boolean loop = false;
        do {
            System.out.print(prompt);
            int value = sc.nextInt();
            if (value >= min && value <= max) {
                loop = false;
                return value;
            } else {
                System.out.print("Value must be between " + min + " and " + max);
                loop = true;
            }
        } while (loop);

        return 0;
    }

    @Override
    public long readLong(String prompt) {
        System.out.print(prompt);
        return sc.nextLong();
    }

    @Override
    public long readLong(String prompt, long min, long max) {
        boolean loop = false;
        do {
            System.out.print(prompt);
            long value = sc.nextLong();
            if (value >= min && value <= max) {
                loop = false;
                return value;
            } else {
                System.out.print("Value must be between " + min + " and " + max);
                loop = true;
            }
        } while (loop);

        return 0;
    }

    @Override
    public String readString(String prompt) {
        System.out.print(prompt);
        return sc.next();
    }

}
