/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.classroster.service;

import com.sg.classroster.dto.Student;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author savannahg
 */
public class ClassRosterServiceLayerTest {
    
    ClassRosterServiceLayer service;
    
    public ClassRosterServiceLayerTest() {
//        ClassRosterDao dao = new ClassRosterDaoStubImpl();
//        ClassRosterAuditDao audit = new ClassRosterAuditDaoStubImpl();
//        service = new ClassRosterServiceLayerImpl(dao, audit);
    ApplicationContext ctx = 
            new ClassPathXmlApplicationContext("applicationContext.xml");
    service = ctx.getBean("service", ClassRosterServiceLayer.class);
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of createStudent method, of class ClassRosterServiceLayer.
     * @throws java.lang.Exception
     */
    @Test
    public void testCreateStudent() throws Exception {
        Student newStudent = new Student("02");
        newStudent.setFirstName("Destinee");
        newStudent.setLastName("Rhoades");
        newStudent.setCohort("C#-Akron");
        service.createStudent(newStudent);
    }
    
    @Test
    public void testCreateStudentDuplicateId() throws Exception{
        Student duplicateIdStudent = new Student("01");
        duplicateIdStudent.setFirstName("John");
        duplicateIdStudent.setLastName("Jacobs");
        duplicateIdStudent.setCohort("Java");
        try {
            service.createStudent(duplicateIdStudent);
            fail("Expected Duplicate ID exception was not thrown");
        } catch (ClassRosterDuplicateIdException ex) {
            return;
        }
    }
    
    @Test
    public void testStudentInvalidData() throws Exception{
        Student invalidStudent = new Student("03");
        invalidStudent.setFirstName("");
        invalidStudent.setLastName("Smith");
        invalidStudent.setCohort("Java-Jan-2015");
        
        try{
        service.createStudent(invalidStudent);
        fail("Expected data validation exception was not thrown.");
        }catch(ClassRosterDataValidationException ex){
            return;
        }
    }

    /**
     * Test of removeStudent method, of class ClassRosterServiceLayer.
     */
    @Test
    public void testRemoveStudent() throws Exception {
        Student student = service.removeStudent("01");
        assertNotNull(student);
        student = service.removeStudent("99");
        assertNull(student);
    }

    /**
     * Test of getAllStudents method, of class ClassRosterServiceLayer.
     */
    @Test
    public void testGetAllStudents() throws Exception {
        assertEquals(1, service.getAllStudents().size());
    }

    /**
     * Test of getStudentById method, of class ClassRosterServiceLayer.
     */
    @Test
    public void testGetStudentById() throws Exception {
        Student student = service.getStudentById("01");
        assertNotNull(student);
        student = service.getStudentById("99");
        assertNull(student);
    }    
}
