/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.classroster.controller;

import com.sg.classroster.dao.ClassRosterPersistenceException;
import com.sg.classroster.dto.Student;
import com.sg.classroster.service.ClassRosterDataValidationException;
import com.sg.classroster.service.ClassRosterDuplicateIdException;
import com.sg.classroster.service.ClassRosterServiceLayer;
import com.sg.classroster.view.ClassRosterView;
import java.util.List;

/**
 *
 * @author savannahg
 */
public class ClassRosterController {

    ClassRosterView view;
    ClassRosterServiceLayer service;

    public ClassRosterController(ClassRosterView view, ClassRosterServiceLayer service) {
        this.view = view;
        this.service = service;

    }

    public void run() {
        boolean keepGoing = true;
        int usersChoice = 0;

        try {
            while (keepGoing) {

                usersChoice = view.getUsersMenuChoice();

                switch (usersChoice) {
                    case 1:
                        viewAllStudentIDs();
                        break;
                    case 2:
                        createStudent();
                        break;
                    case 3:
                        viewStudent();
                        break;
                    case 4:
                        removeStudent();
                        break;
                    case 5:
                        keepGoing = false;
                        break;
                    default:
                        unknownCommand();
                        break;
                }
            }
            exitMessage();
        } catch (ClassRosterPersistenceException e) {
            view.displayExceptionError(e.getMessage());
        }
    }

    private void viewAllStudentIDs() throws ClassRosterPersistenceException {
        view.displayViewAllStudentsBanner();
        List<Student> allStudents = service.getAllStudents();
        view.displayAllStudents(allStudents);
    }

    private void createStudent() throws ClassRosterPersistenceException {
        view.displayCreateStudentBanner();
        boolean loop = false;
        do {
            Student createdStudent = view.createStudent();
            try {
                service.createStudent(createdStudent);
                loop = false;
            } catch (ClassRosterDuplicateIdException | ClassRosterDataValidationException e) {
                view.displayExceptionError(e.getMessage());
                loop = true;
            }
        } while (loop);
        
        

        view.displayCreateSuccessBanner();
    }

    private void viewStudent() throws ClassRosterPersistenceException {
        view.displayViewStudentBanner();
        String studentID = view.getStudentIDFromUser();
        Student foundStudent = service.getStudentById(studentID);
        view.displayStudent(foundStudent);
    }

    private void removeStudent() throws ClassRosterPersistenceException {
        view.displayRemoveStudentBanner();
        String studentID = view.getStudentIDFromUser();
        Student removedStudent = service.removeStudent(studentID);
        view.displayStudentRemovedSuccessfullyBanner(removedStudent);
    }

    private void unknownCommand() {
        view.printUnknownCommand();
    }

    private void exitMessage() {
        view.printGoodbyeMessage();
    }
}
