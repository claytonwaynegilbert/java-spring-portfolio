/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.classroster.advice;

import com.sg.classroster.dao.ClassRosterAuditDao;
import com.sg.classroster.dao.ClassRosterPersistenceException;
import org.aspectj.lang.JoinPoint;

/**
 *
 * @author savannahg
 */
public class Advice {
    
    ClassRosterAuditDao audit;

    public Advice(ClassRosterAuditDao audit) {
        this.audit = audit;
    }
    
    public void createAuditEntry(JoinPoint jp){
        Object[] args = jp.getArgs();
        String auditLine = jp.getSignature().getName() + ":";
        for(Object currentArg : args){
            auditLine += currentArg;
        }
        
        try {
            audit.writeAuditEntry(auditLine);
        } catch (ClassRosterPersistenceException ex) {
            System.err.println("ERROR: Could not create audit entry for logging advice");
        }
    }
    
}
