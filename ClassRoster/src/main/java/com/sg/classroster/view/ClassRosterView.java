/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.classroster.view;

import com.sg.classroster.dto.Student;
import java.util.List;

/**
 *
 * @author savannahg
 */
public class ClassRosterView {

    UserIO io;
    
    public ClassRosterView(UserIO io){
        this.io = io;
    }

    public int getUsersMenuChoice() {
        io.print("---Main Menu---");
        io.print("===============");
        io.print("1) List All Student ID's");
        io.print("2) Create A Student");
        io.print("3) View A Student");
        io.print("4) Remove A Student");
        io.print("5) Exit");

        return io.readInt("What choice do you want to do: ", 1, 5);
    }

    public void printUnknownCommand() {
        io.print("UNKNOWN COMMAND!");
    }

    public void printGoodbyeMessage() {
        io.print("Goodbye!");
    }

    public Student createStudent() {
        String studentID = io.readString("Please enter the student's ID: ");
        String firstName = io.readString("Please enter the student's first name: ");
        String lastName = io.readString("Please enter the student's last name: ");
        String cohort = io.readString("Please enter student's cohort: ");

        Student newStudent = new Student(studentID);
        newStudent.setFirstName(firstName);
        newStudent.setLastName(lastName);
        newStudent.setCohort(cohort);

        return newStudent;
    }

    public void displayCreateStudentBanner() {
        io.print("---CREATE STUDENT---");
    }

    public void displayCreateSuccessBanner() {
        io.readString("Student created successfully! Press ENTER to continue");
    }

    public String getStudentIDFromUser() {
        return io.readString("What is the student's ID: ");
    }

    public void displayStudent(Student student) {
        if (student != null) {
            io.print("ID: " + student.getStudentID());
            io.print("First name: " + student.getFirstName());
            io.print("Last name: " + student.getLastName());
            io.print("Cohort: " + student.getCohort());
        } else {
            io.print("Student does not exist!");
        }

        io.readString("Press ENTER to continue: ");
    }

    public void displayViewStudentBanner() {
        io.print("---VIEW STUDENT---");
    }

    public void displayViewAllStudentsBanner() {
        io.print("---VIEW ALL STUDENTS---");
    }

    public void displayAllStudents(List<Student> allStudents) {
        for (Student currentStudent : allStudents) {
            io.print("Student ID: " + currentStudent.getStudentID());
            io.print("First name: " + currentStudent.getFirstName());
            io.print("Last name: " + currentStudent.getLastName());
            io.print("Cohort: " + currentStudent.getCohort());
        }
        io.readString("Press ENTER to continue: ");
    }

    public void displayRemoveStudentBanner() {
        io.print("---REMOVE STUDENT---");
    }

    public void displayStudentRemovedSuccessfullyBanner(Student student) {
        if (student != null) {
            io.print("Student with ID of " + student.getStudentID() + " removed successfully!");
        }else{
            io.print("No student with that ID in system.");
        }
        io.readString("Press ENTER to continue: ");
    }

    public void displayExceptionError(String errMessage){
        io.print("---ERROR MESSAGE---");
        io.print(errMessage);
    }
}
