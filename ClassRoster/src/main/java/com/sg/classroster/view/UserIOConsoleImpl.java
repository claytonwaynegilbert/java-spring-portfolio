/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.classroster.view;

import java.util.Scanner;

/**
 *
 * @author savannahg
 */
public class UserIOConsoleImpl implements UserIO {

    Scanner sc = new Scanner(System.in);

    @Override
    public void print(String message) {
        System.out.println(message);
    }

    @Override
    public double readDouble(String prompt) {
        System.out.print(prompt);
        return sc.nextDouble();
    }

    @Override
    public double readDouble(String prompt, double min, double max) {
        boolean loop = false;
        do {
            System.out.print(prompt);
            double value = sc.nextDouble();
            if (value >= min && value <= max) {
                loop = false;
                return value;
            } else {
                System.out.print("Value must be between " + min + " and " + max);
                loop = true;
            }
        } while (loop);

        return 0;
    }

    @Override
    public float readFloat(String prompt) {
        System.out.print(prompt);
        return sc.nextFloat();

    }

    @Override
    public float readFloat(String prompt, float min, float max) {
        boolean loop = false;
        do {
            System.out.print(prompt);
            float value = sc.nextFloat();
            if (value >= min && value <= max) {
                loop = false;
                return value;
            } else {
                System.out.print("Value must be between " + min + " and " + max);
                loop = true;
            }
        } while (loop);

        return 0;
    }

    @Override
    public int readInt(String prompt) {
        System.out.print(prompt);
        return sc.nextInt();
    }

    @Override
    public int readInt(String prompt, int min, int max) {
        boolean loop = false;
        do {
            System.out.print(prompt);
            int value = sc.nextInt();
            if (value >= min && value <= max) {
                loop = false;
                return value;
            } else {
                System.out.print("Value must be between " + min + " and " + max);
                loop = true;
            }
        } while (loop);

        return 0;
    }

    @Override
    public long readLong(String prompt) {
        System.out.print(prompt);
        return sc.nextLong();
    }

    @Override
    public long readLong(String prompt, long min, long max) {
        boolean loop = false;
        do {
            System.out.print(prompt);
            long value = sc.nextLong();
            if (value >= min && value <= max) {
                loop = false;
                return value;
            } else {
                System.out.print("Value must be between " + min + " and " + max);
                loop = true;
            }
        } while (loop);

        return 0;
    }

    @Override
    public String readString(String prompt) {
        System.out.print(prompt);
        return sc.next();
    }
}
