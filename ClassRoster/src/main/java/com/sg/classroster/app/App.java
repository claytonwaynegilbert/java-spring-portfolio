/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.classroster.app;

import com.sg.classroster.controller.ClassRosterController;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author savannahg
 */
public class App {
    
    public static void main(String[] args) {
        
//        ClassRosterDao dao = new ClassRosterDaoFileImpl();
//        ClassRosterAuditDao audit = new ClassRosterAuditDaoImpl();
//        ClassRosterServiceLayer service = new ClassRosterServiceLayerImpl(dao, audit);
//        UserIO io = new UserIOConsoleImpl();
//        ClassRosterView view = new ClassRosterView(io);
//        ClassRosterController controller = new ClassRosterController(view, service);
//        
//        controller.run();

    //Using Spring Dependency Injection
    ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
    ClassRosterController controller = ctx.getBean("controller", ClassRosterController.class);
    controller.run();
    
    
    }
    
}
