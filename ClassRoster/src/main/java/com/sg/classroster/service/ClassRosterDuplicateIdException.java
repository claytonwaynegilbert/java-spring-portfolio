/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.classroster.service;

/**
 *
 * @author savannahg
 */
public class ClassRosterDuplicateIdException extends Exception {

    /**
     * Creates a new instance of <code>ClassRosterDuplicateIdException</code>
     * without detail message.
     */
    public ClassRosterDuplicateIdException() {
    }

    /**
     * Constructs an instance of <code>ClassRosterDuplicateIdException</code>
     * with the specified detail message.
     *
     * @param msg the detail message.
     */
    public ClassRosterDuplicateIdException(String msg) {
        super(msg);
    }

    public ClassRosterDuplicateIdException(String message, Throwable cause) {
        super(message, cause);
    }
    
    
}
