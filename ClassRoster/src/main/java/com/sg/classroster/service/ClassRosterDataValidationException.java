/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.classroster.service;

/**
 *
 * @author savannahg
 */
public class ClassRosterDataValidationException extends Exception {

    /**
     * Creates a new instance of <code>ClassRosterDataValidationException</code>
     * without detail message.
     */
    public ClassRosterDataValidationException() {
    }

    /**
     * Constructs an instance of <code>ClassRosterDataValidationException</code>
     * with the specified detail message.
     *
     * @param msg the detail message.
     */
    public ClassRosterDataValidationException(String msg) {
        super(msg);
    }

    public ClassRosterDataValidationException(String message, Throwable cause) {
        super(message, cause);
    }
    
    
}
