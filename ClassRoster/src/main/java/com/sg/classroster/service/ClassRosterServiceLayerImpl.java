/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.classroster.service;

import com.sg.classroster.dao.ClassRosterAuditDao;
import com.sg.classroster.dao.ClassRosterDao;
import com.sg.classroster.dao.ClassRosterPersistenceException;
import com.sg.classroster.dto.Student;
import java.util.List;

/**
 *
 * @author savannahg
 */
public class ClassRosterServiceLayerImpl implements ClassRosterServiceLayer {
    
    ClassRosterDao dao;
    ClassRosterAuditDao audit;
    
    public ClassRosterServiceLayerImpl(ClassRosterDao dao, ClassRosterAuditDao audit){
        this.dao = dao;
        this.audit = audit;
    }

    @Override
    public Student createStudent(Student student) throws ClassRosterPersistenceException,
                                                      ClassRosterDuplicateIdException,
                                                      ClassRosterDataValidationException{
        Student testStudent = dao.getStudentById(student.getStudentID());
        if(testStudent != null){
            throw new ClassRosterDuplicateIdException("Student with ID of " + student.getStudentID() + " already exists.");
        }
        validateStudentData(student);
        
        dao.addStudent(student.getStudentID(), student);
        //audit.writeAuditEntry("Student with ID of " + student.getStudentID() + " CREATED!");
        return student;
    }

    @Override
    public Student removeStudent(String studentId) throws ClassRosterPersistenceException {
        Student removedStudent = dao.removeStudent(studentId);
        //audit.writeAuditEntry("Student with ID of " + studentId + " REMOVED!");
        return removedStudent;
    }

    @Override
    public List<Student> getAllStudents() throws ClassRosterPersistenceException {
        return dao.getAllStudents();
    }

    @Override
    public Student getStudentById(String studentId) throws ClassRosterPersistenceException {
        return dao.getStudentById(studentId);
    }
    
    private void validateStudentData(Student student) throws ClassRosterDataValidationException{
        if(student.getFirstName() == null || student.getFirstName().trim().length() == 0
        || student.getLastName() == null || student.getLastName().trim().length() == 0
        || student.getCohort() == null || student.getCohort().trim().length() == 0){
            
            throw new ClassRosterDataValidationException("All fields are required!");
        }
    }
    
}
