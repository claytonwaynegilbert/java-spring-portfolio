/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.classroster.service;

import com.sg.classroster.dao.ClassRosterPersistenceException;
import com.sg.classroster.dto.Student;
import java.util.List;

/**
 *
 * @author savannahg
 */
public interface ClassRosterServiceLayer {
    
    Student createStudent(Student student) throws ClassRosterPersistenceException,
                                               ClassRosterDuplicateIdException,
                                               ClassRosterDataValidationException;

    Student removeStudent(String studentId) throws ClassRosterPersistenceException;

    List<Student> getAllStudents() throws ClassRosterPersistenceException;
    
    Student getStudentById(String studentId) throws ClassRosterPersistenceException;
   
}
