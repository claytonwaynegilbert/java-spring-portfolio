/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.classroster.dao;

/**
 *
 * @author savannahg
 */
public class ClassRosterPersistenceException extends Exception {

    /**
     * Creates a new instance of <code>ClassRosterPersistenceException</code>
     * without detail message.
     */
    public ClassRosterPersistenceException() {
    }

    /**
     * Constructs an instance of <code>ClassRosterPersistenceException</code>
     * with the specified detail message.
     *
     * @param msg the detail message.
     */
    public ClassRosterPersistenceException(String msg) {
        super(msg);
    }

    public ClassRosterPersistenceException(String message, Throwable cause) {
        super(message, cause);
    }
    
    
}
