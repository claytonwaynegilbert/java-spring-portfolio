/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.classroster.dao;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;

/**
 *
 * @author savannahg
 */
public class ClassRosterAuditDaoImpl implements ClassRosterAuditDao{

    public static final String AUDIT_LOG = "auditLog.txt";

    @Override
    public void writeAuditEntry(String entry) throws ClassRosterPersistenceException {

        PrintWriter out;

        try {
            out = new PrintWriter(new FileWriter(AUDIT_LOG, true));
        } catch  (IOException ex) {
            throw new ClassRosterPersistenceException("Something went wrong writing data to audit log file.", ex);
        }
        
        LocalDate timeStamp = LocalDate.now();
        out.println(timeStamp.toString() + " : " + entry);
        out.flush();
    }
}
