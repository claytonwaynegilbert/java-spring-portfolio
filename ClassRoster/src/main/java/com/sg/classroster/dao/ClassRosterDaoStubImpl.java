/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.classroster.dao;

import com.sg.classroster.dto.Student;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author savannahg
 */
public class ClassRosterDaoStubImpl implements ClassRosterDao {

    Student onlyStudent;
    List<Student> studentList = new ArrayList<>();

    public ClassRosterDaoStubImpl() {
        onlyStudent = new Student("01");
        onlyStudent.setFirstName("Clayton");
        onlyStudent.setLastName("Gilbert");
        onlyStudent.setCohort("Java-Akron");
        studentList.add(onlyStudent);
    }

    @Override
    public Student addStudent(String studentID, Student student) throws ClassRosterPersistenceException {
        if (student.getStudentID().equalsIgnoreCase(onlyStudent.getStudentID())) {
            return onlyStudent;
        } else {
            return null;
        }
    }

    @Override
    public Student removeStudent(String studentId) throws ClassRosterPersistenceException {
        if (onlyStudent.getStudentID().equalsIgnoreCase(studentId)) {
            return onlyStudent;
        } else {
            return null;
        }
    }

    @Override
    public List<Student> getAllStudents() throws ClassRosterPersistenceException {
        return studentList;
    }

    @Override
    public Student getStudentById(String studentId) throws ClassRosterPersistenceException {
        if (onlyStudent.getStudentID().equalsIgnoreCase(studentId)) {
            return onlyStudent;
        } else {
            return null;
        }
    }

}
