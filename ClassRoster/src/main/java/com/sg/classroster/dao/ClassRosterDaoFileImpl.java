/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.classroster.dao;

import com.sg.classroster.dto.Student;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author savannahg
 */
public class ClassRosterDaoFileImpl implements ClassRosterDao {

    public static final String ROSTER_FILE = "ClassRoster.txt";
    public static final String DELIMITER = "::";
    private Map<String, Student> studentMap = new HashMap<>();

    @Override
    public Student addStudent(String studentID, Student student) throws ClassRosterPersistenceException {
        Student newStudent = studentMap.put(studentID, student);
        writeToRoster();
        return newStudent;
    }

    @Override
    public Student removeStudent(String studentId) throws ClassRosterPersistenceException {
        loadFromRoster();
        Student removedStudent = studentMap.remove(studentId);
        writeToRoster();
        return removedStudent;
    }

    @Override
    public List<Student> getAllStudents() throws ClassRosterPersistenceException{
        loadFromRoster();
        return new ArrayList<>(studentMap.values());
    }

    @Override
    public Student getStudentById(String studentId) throws ClassRosterPersistenceException{
        loadFromRoster();
        return studentMap.get(studentId);
    }

    public void loadFromRoster() throws ClassRosterPersistenceException {
        Scanner sc;

        try {
            sc = new Scanner(
                    new BufferedReader(
                            new FileReader("ClassRoster.txt")));
        } catch (FileNotFoundException ex) {
            throw new ClassRosterPersistenceException(
                    "There was a problem finding file holding the roster of students", ex);
        }

        String currentLine;

        String[] currentLineSplit;

        while (sc.hasNextLine()) {

            currentLine = sc.nextLine();

            currentLineSplit = currentLine.split(DELIMITER);

            Student student = new Student(currentLineSplit[0]);
            student.setFirstName(currentLineSplit[1]);
            student.setLastName(currentLineSplit[2]);
            student.setCohort(currentLineSplit[3]);

            studentMap.put(student.getStudentID(), student);
        }
        
        sc.close();
    }

    public void writeToRoster() throws ClassRosterPersistenceException {
        PrintWriter out;
        
        try {
            out = new PrintWriter(
                                  new FileWriter("ClassRoster.txt"));
                    } catch (IOException ex) {
                        throw new ClassRosterPersistenceException("Problem writing Student data to file", ex);
                    }
        
        List<Student> allStudents = this.getAllStudents();
        
        for(Student currentStudent : allStudents){
            out.println(currentStudent.getStudentID() + DELIMITER 
                      + currentStudent.getFirstName() + DELIMITER 
                      + currentStudent.getLastName() + DELIMITER 
                      + currentStudent.getCohort());
            out.flush();
        }
        out.close();
    }

}
