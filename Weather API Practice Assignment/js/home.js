$(document).ready(function () {
    $('#get-weather-button').on('click', function () {
        clearForms();
        var zipCode = $('#add-zipcode').val();
        var imperial = 'Imperial';
        var metric = 'Metric';
        var units = $('#units option:selected').text();
        
        var hasValidationErrors = checkAndDisplayValidationErrors($('#zipcode-input-form').find('#add-zipcode'));
        
        if(hasValidationErrors){
            return false;
        }

        if (units === imperial) {
            $.ajax({
                type: 'GET',
                url: 'http://api.openweathermap.org/data/2.5/weather?zip=' + zipCode + ',us&units=imperial&APPID=536d64a34584fafc92a2ab243dee4e10',
                success: function (data, status) {
                    var city = data.name;
                    var description = '<p>' + data.weather[0].description + '</p>';
                    var icon = data.weather[0].icon;
                    var image = '<img src="http://openweathermap.org/img/w/' + icon + '.png"/>';
                    var temperature = '<p>Temperature: ' + data.main.temp + 'F</p>';
                    var humidity = '<p>Humidity: ' + data.main.humidity + '%</p>';
                    var wind = '<p>Wind: ' + data.wind.speed + 'miles/hour</p>';

                    $('#current-forecast').show();
                    $('#city-header').append(city);
                    $('#image').append(image)
                    $('#description').append(description);
                    $('#temperature-humidity-wind').append(temperature)
                        .append(humidity)
                        .append(wind);
                },
                error: function () {
                    $('#errorMessages').append($('<li>')
                        .attr({
                            class: 'list-group-item list-group-item-danger'
                        })
                        .text('Error calling web service.  Please try again later.'));
                }
            });

            $.ajax({
                type: 'GET',
                url: 'http://api.openweathermap.org/data/2.5/forecast/daily?zip=' + zipCode + ',us&units=imperial&APPID=536d64a34584fafc92a2ab243dee4e10',
                success: function (data, status) {
                    $('#five-day-forecast').show();
                    var d = new Date();
                    var month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                    var month = month[d.getMonth()];
                    for (var i = 0; i < 5; i++) {
                        var day = d.getDate() + i;
                        var completeDay = '<p>' + day + ' ' + month + '</p>';
                        var description = data.list[i].weather[0].description;
                        var icon = data.list[i].weather[0].icon;
                        var image = '<img src="http://openweathermap.org/img/w/' + icon + '.png"/>';
                        var highTemp = '<p>H ' + data.list[i].temp.max + 'F</p>';
                        var lowTemp = '<p>L ' + data.list[i].temp.min + 'F</p>';

                        $('#date' + i).append(completeDay);
                        $('#image' + i).append(image);
                        $('#description' + i).append(description);
                        $('#high' + i).append(highTemp);
                        $('#low' + i).append(lowTemp);
                    }
                },
                error: function () {
                    $('#errorMessages').append($('<li>')
                        .attr({
                            class: 'list-group-item list-group-item-danger'
                        })
                        .text('Error calling web service.  Please try again later.'));
                }
            });

        } else {
            $.ajax({
                type: 'GET',
                url: 'http://api.openweathermap.org/data/2.5/weather?zip=' + zipCode + ',us&units=metric&APPID=536d64a34584fafc92a2ab243dee4e10',
                success: function (data, status) {
                    var city = data.name;
                    var description = '<p>' + data.weather[0].description + '</p>';
                    var icon = data.weather[0].icon;
                    var image = '<img src="http://openweathermap.org/img/w/' + icon + '.png"/>';
                    var temperature = '<p>Temperature: ' + data.main.temp + 'C</p>';
                    console.log(temperature);
                    var humidity = '<p>Humidity: ' + data.main.humidity + '%</p>';
                    var wind = '<p>Wind :' + data.wind.speed + 'kilometers/hour</p>';
                    console.log(wind);

                    $('#current-forecast').show();
                    $('#city-header').append(city);
                    $('#image').append(image)
                    $('#description').append(description);
                    $('#temperature-humidity-wind').append(temperature)
                        .append(humidity)
                        .append(wind);

                },
                error: function () {
                    $('#errorMessages').append($('<li>')
                        .attr({
                            class: 'list-group-item list-group-item-danger'
                        })
                        .text('Error calling web service.  Please try again later.'));
                }
            });

            $.ajax({
                type: 'GET',
                url: 'http://api.openweathermap.org/data/2.5/forecast/daily?zip=' + zipCode + ',us&units=metric&APPID=536d64a34584fafc92a2ab243dee4e10',
                success: function (data, status) {
                    $('#five-day-forecast').show();
                    var d = new Date();
                    var month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                    var month = month[d.getMonth()];
                    for (var i = 0; i < 5; i++) {
                        var day = d.getDate() + i;
                        var completeDay = '<p>' + day + ' ' + month + '</p>';
                        var description = data.list[i].weather[0].description;
                        var icon = data.list[i].weather[0].icon;
                        var image = '<img src="http://openweathermap.org/img/w/' + icon + '.png"/>';
                        var highTemp = '<p>H ' + data.list[i].temp.max + 'C</p>';
                        var lowTemp = '<p>L ' + data.list[i].temp.min + 'C</p>';

                        $('#date' + i).append(completeDay);
                        $('#image' + i).append(image);
                        $('#description' + i).append(description);
                        $('#high' + i).append(highTemp);
                        $('#low' + i).append(lowTemp);
                    }
                },
                error: function () {
                    $('#errorMessages').append($('<li>')
                        .attr({
                            class: 'list-group-item list-group-item-danger'
                        })
                        .text('Error calling web service.  Please try again later.'));
                }
            });
        }
    })

    function clearForms() {
        $('#city-header').empty();
        $('#image').empty();
        $('#description').empty();
        $('#temperature-humidity-wind').empty();

        for(var i = 0; i <=4; i++){
            $('#date' + i).empty();
            $('#image' + i).empty();
            $('#description' + i).empty();
            $('#high' + i).empty();
            $('#low' + i).empty();
        }
    }

    // processes validation errors for the given input.  returns true if there
    // are validation errors, false otherwise
    function checkAndDisplayValidationErrors(input) {
        // clear displayed error message if there are any
        $('#errorMessages').empty();
        // check for HTML5 validation errors and process/display appropriately
        // a place to hold error messages
        var errorMessages = [];

        // loop through each input and check for validation errors
        input.each(function () {
            // Use the HTML5 validation API to find the validation errors
            if (!this.validity.valid) {
                var errorField = $('label[for=' + this.id + ']').text();
                errorMessages.push(errorField + ' ' + this.validationMessage);
            }
        });

        // put any error messages in the errorMessages div
        if (errorMessages.length > 0) {
            $.each(errorMessages, function (index, message) {
                $('#errorMessages').append($('<li>').attr({
                    class: 'list-group-item list-group-item-danger'
                }).text(message));
            });
            // return true, indicating that there were errors
            return true;
        } else {
            // return false, indicating that there were no errors
            return false;
        }
    }
});
