<%-- 
    Document   : results
    Created on : Sep 18, 2017, 5:00:19 AM
    Author     : cjsm12
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Interest Calculator Results</title>
    </head>
    <body>
        <h1>Results:</h1>
        <p class="results">
            <strong>Year:</strong><br>
            <c:forEach var ="year" items="${years}">
                <c:out value="${year}"/>
                <br/>
            </c:forEach>
        </p>
        <p>
            <strong>Principal beginning of year:</strong><br>
            <c:forEach var ="beginningPrincipal" items="${principalBeginningYear}">
                <c:out value="$${beginningPrincipal}"/>
                <br/>
            </c:forEach>
        </p>
        <p>
            <strong>Interest earned for year:</strong><br>
            <c:forEach var ="interestEarned" items="${interestEarned}">
                <c:out value="$${interestEarned}"/>
                <br/>
            </c:forEach>
        </p>
        <p>
            <strong>Principal end of year:</strong><br>
            <c:forEach var ="endPrincipal" items="${principalEndYear}">
                <c:out value="$${endPrincipal}"/>
                <br/>
            </c:forEach>
        </p>
        
        <a href="index.jsp"><strong>Do another calculation</strong></a>
    </body>
</html>
