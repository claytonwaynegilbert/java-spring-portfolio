/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.interestcalculatorspringmvcapplication;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author cjsm12
 */
@Controller
public class InterestCalculatorController {
    
    @RequestMapping(value="/calculateInterest", method=RequestMethod.POST)
    public String calculateInterest(HttpServletRequest request, Map<String, Object> model){
        List<Integer> yearList = new ArrayList<>();
        List<Double> principalBeginningYear = new ArrayList<>();
        List<Double> principalEndYear = new ArrayList<>();
        List<Double> interestEarnedForYear = new ArrayList<>();
        
        BigDecimal annualInterestRate = new BigDecimal(request.getParameter("annualInterest"));
        //double interestRate = Double.parseDouble(annualInterestRate);
        BigDecimal quarterlyInterest = annualInterestRate.divide(new BigDecimal("4"), 2, RoundingMode.HALF_UP);
        //double quarterlyInterest = interestRate / 4;
        BigDecimal principal = new BigDecimal(request.getParameter("principal"));
        //double currentBalance = Double.parseDouble(principal);
        BigDecimal years = new BigDecimal(request.getParameter("years"));
        //int numberOfYears = Integer.parseInt(years);
        BigDecimal endOfYearBalance = principal;
        

        for (int yearsToInvest = 0; yearsToInvest < years.intValue(); yearsToInvest++) {
            BigDecimal initialPrincipal = principal;
            principalBeginningYear.add(initialPrincipal.doubleValue());
            for (int compPeriods = 0; compPeriods < 4; compPeriods++) {
                BigDecimal hundred = new BigDecimal("100");
                //double hundred = 100;
                BigDecimal one = new BigDecimal("1");
                //double one = 1;
                BigDecimal formulaHalf = quarterlyInterest.divide(new BigDecimal("100"), 2, RoundingMode.HALF_UP);
                BigDecimal formulaComplete = formulaHalf.add(new BigDecimal("1"));
                //double formulaComplete = 1 + formulaHalf;
                principal = principal.multiply(formulaComplete).setScale(2, RoundingMode.HALF_UP);
                endOfYearBalance = principal;
                //double endOfYearBalance = currentBalance;
            }
            
            int year = yearsToInvest + 1;
            yearList.add(year);
            BigDecimal interestEarned = principal.subtract(initialPrincipal);
            //double interestEarned = currentBalance - initialPrincipal;
            interestEarnedForYear.add(interestEarned.doubleValue());
            principalEndYear.add(endOfYearBalance.doubleValue());
        }
        
        model.put("years", yearList);
        model.put("principalBeginningYear", principalBeginningYear);
        model.put("principalEndYear", principalEndYear);
        model.put("interestEarned", interestEarnedForYear);

        return "results";
    }
}
