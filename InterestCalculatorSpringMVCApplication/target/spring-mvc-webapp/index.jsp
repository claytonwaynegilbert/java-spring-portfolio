<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Interest Calculator</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">        
        <style>
            #instructions{
                font-style: italic
            }
            #formInputs{
                font-weight: bold;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <h1>Interest Calculator</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="active">
                        <a href="${pageContext.request.contextPath}/index.jsp">
                            Home</a></li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/hello/sayhi">
                            Hello Controller</a></li>
                </ul>    
            </div>
            <h2>Instructions:</h2>
            <p id="instructions">
                John has $500 to invest. Sue knows of a mutual fund plan that pays 
                10% interest annually, compounded quarterly. That is, every three months, 
                the principal is multiplied by 2.5% <br> (the 10% annual rate divided by 4 
                because it is compounded 4 times per year) and the result is added to the 
                principal.<br>

                Your assignment is to write a program that will tell John how much money 
                will be in the fund after a specified number of years for a given initial 
                amount of principle.
            </p>
            <br>
            <form action="calculateInterest" method="POST">
                <p id="formInputs">
                Annual Interest: <input type="text" name="annualInterest"/><br><br>
                Initial Principal: <input type="text" name="principal"/><br><br>
                Years In Fund: <input type="text" name="years"/><br><br>
                <input type="submit" value="Calculate"/>
                </p>
            </form>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>

