/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.luckysevens;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author cjsm12
 */
public class LuckySevens {

    public static void main(String[] args) {

        /*
        Each round, the program rolls a virtual pair of dice for the user
        If the sum of the 2 dice is equal to 7, the player wins $4; otherwise, the player 
        loses $1 and your job is to write a program that plays this game, which will also
        demonstrate the futility of playing Lucky Sevens.

        Your program must have the following features:

        This program will be a Java Console Application called LuckySevens .
        The program first asks the user how many dollars they have to bet.
        The program then rolls the dice repeatedly until all the money is gone.
        Hint: use a loop construct to keep playing until the money is gone.
        The program keeps track of how many rolls were taken before the money ran out.
        The program keeps track of the maximum amount of money held by the player.
        The program keeps track of how many rolls were taken at the point when the user 
        held the most money.
        Hint: for steps 4, 5, and 6, declare some variables.
         */
        
        //Welcome user
        System.out.println("Welcome to Lucky Sevens!");
        System.out.println("------------------------");

        //Declare type Scanner and Random for user input and generating dice rolls
        Scanner userInput = new Scanner(System.in);
        Random diceRoll = new Random();

        //Declare variables
        int dice1;
        int dice2;
        int diceSum;
        int rolls = 0;
        int rollCountAtHighestMoneyHeld = 0;
        int winningRoll = 7;
        int losingRoll = 2;
        int winningMoney = 4;
        int losingMoney = -1;
        int moneyWon = 0;
        int maxMoneyHeld = 0;

        //Ask user for amount of money to bet
        System.out.print("How much money are you willing to bet(whole dollar amounts only): ");
        int moneyToBet = userInput.nextInt();
        //Assign initial amount to another variable for keeping track of highest money total
        maxMoneyHeld = moneyToBet;

        //Keep performing everything within loop until money is gone
        while (moneyToBet > 0) {
            //Genreate random numbers for dice to simulate roll
            dice1 = diceRoll.nextInt(6) + 1;
            dice2 = diceRoll.nextInt(6) + 1;
            //Add two dice together for total
            diceSum = dice1 + dice2;

            //Check to see if total matches either the win or lose condition
            if (diceSum == winningRoll) {
                //If won, increase winnings and roll count
                moneyWon += winningMoney;
                rolls++;
                //Check to see after winning money if that total is higher than
                //what the user previously had to begin with
                if (moneyWon > maxMoneyHeld) {
                    //If true, update highest money held total to new total
                    maxMoneyHeld = moneyWon;
                    //Keep track of roll count
                    rollCountAtHighestMoneyHeld = rolls;
                }
                //If user rolled a losing roll
            } else if (diceSum == losingRoll) {
                //Decrease users money
                moneyWon += losingMoney;
                //Add another roll
                rolls++;
            } else {
                //Otherwise just incrament roll and keep playing
                rolls++;
            }
        }

        //Display game info to user after losing all money
        System.out.println("You are broke after " + rolls + "\n"
                + "You should have quit after roll " + rollCountAtHighestMoneyHeld
                + " when you had " + maxMoneyHeld);

    }

}
