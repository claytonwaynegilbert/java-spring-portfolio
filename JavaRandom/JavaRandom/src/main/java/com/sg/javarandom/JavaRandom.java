/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.javarandom;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author cjsm12
 */
public class JavaRandom {

    public static void main(String[] args) {

        /*
        Filename: FortuneCookie.java
        Write a program that randomly prints out one of the following to the screen.

        “Those aren’t the droids you’re looking for.”
        “Never go in against a Sicilian when death is on the line!”<
        “Goonies never say die.”
        “With great power there must also come — great responsibility.”
        “Never argue with the data.”
        “Try not. Do, or do not. There is no try.”
        “You are a leaf on the wind, watch how you soar.”
        “Do absolutely nothing, and it will be everything that you thought it could be.”
        “Kneel before Zod.”
        “Make it so.” 
         */
        
        //Welcoming user
        System.out.println("Welcome to Fortune Cookie!");
        System.out.println("---------------------------");

        //Declare type Random
        Random rn = new Random();
        //Setup random to choose a random number between 0 and 10 and store number in variable.
        int randomChoice = rn.nextInt(11);

        //Used for illusion of picking a fortune cookie.
        System.out.println("Choosing cookie...");

        //Switch statement for testing value in stored random variable
        switch (randomChoice) {
            case 0:
                System.out.println("Those aren’t the droids you’re looking for.");
                break;
            case 1:
                System.out.println("Never go in against a Sicilian when death is on the line!");
                break;
            case 2:
                System.out.println("Goonies never say die.");
                break;
            case 3:
                System.out.println("With great power there must also come — great responsibility.");
                break;
            case 4:
                System.out.println("Never argue with the data.");
                break;
            case 5:
                System.out.println("Try not. Do, or do not. There is no try.");
                break;
            case 6:
                System.out.println("You are a leaf on the wind, watch how you soar.");
                break;
            case 7:
                System.out.println("Do absolutely nothing, and it will be everything that you thought it could be.");
                break;
            case 8:
                System.out.println("Kneel before Zod.");
                break;
            case 9:
                System.out.println("Make it so.");
                break;
            default:
                System.out.println("");
                break;
        }

        /*
        Filename: CoinFlipper.java
        Write a program that simulates a random coin toss.
        Don't use .nextInt()
         */
        
        //Welcome user and explain you are flippin the coin
        System.out.println("Welcome to CoinFlipper!");
        System.out.println("------------------------");

        System.out.println("Ready, Set, FLIP!!!");
        System.out.println("Flipping...");

        //Generating a random boolean value of either true or false.
        boolean headsOrTails = rn.nextBoolean();

        //If random boolean is true, then it is heads, otherwise it is tails
        if (headsOrTails == true) {
            System.out.println("You got HEADS!");
        } else {
            System.out.println("You got TAILS!");
        }

        /*   
        Filename: GuessMeMore.java
        Take your original number guessing program and improve it!

        Make the number chosen, a random number between -100 and 100
        If the user gets it wrong, give them another chance at guessing.
         */
        
        System.out.println("Welcome to GuessMeMore!");
        System.out.println("------------------------");
        System.out.print("I'm thinking of a number between -100 and 100...Want to guess it? [Y or N] : ");

        Scanner sc = new Scanner(System.in);
        int randomNumber = rn.nextInt(100) - 100;
        int guessAgain = 0;

        String playOrNot = sc.nextLine();
        do {
            if (playOrNot.equalsIgnoreCase("Y")) {
                System.out.print("What is your guess: ");
                int usersGuess = sc.nextInt();
                if (usersGuess == randomNumber) {
                    System.out.println("You got it!");
                    guessAgain = 0;
                } else if (usersGuess < randomNumber) {
                    System.out.println("Sorry, you need to guess higher!");
                    guessAgain = 1;
                } else if (usersGuess > randomNumber) {
                    System.out.println("Sorry, you need to guess lower!");
                    guessAgain = 1;
                }
            } else {
                guessAgain = 0;
            }

        } while (guessAgain == 1);

        System.out.println("Goodbye!");

        /*
        Filename: DogGenetics.java
        Write a program that asks the user for the name of their dog, and then generates 
        a fake DNA background report on the pet dog.
        It should assign a random percentage to 5 dog breeds (that should add up to 100%!)
         */
        
        //Welcome user
        System.out.println("Welcome to DogGenetics!");
        System.out.println("-----------------------");

        //Declaring boolean variable for loop
        boolean keepRolling = true;

        //Ask user for users dog name
        System.out.println("What is the name of your dog: ");
        String usersDogName = sc.nextLine();
        System.out.println("Calculating...");

        //Generate random numbers until all numbers add up to 100
        while (keepRolling) {
            int randomOne = rn.nextInt(100) + 1;
            int randomTwo = rn.nextInt(100) + 1;
            int randomThree = rn.nextInt(100) + 1;
            int randomFour = rn.nextInt(100) + 1;
            int randomFive = rn.nextInt(100) + 1;

            //When numbers add up to 100% then display random numbers with a random dog breed.
            if (randomOne + randomTwo + randomThree + randomFour + randomFive == 100) {
                System.out.println("Here is a completely accurate DNA analysis of your dog " + usersDogName + ".\n");
                System.out.println("Your dog is " + randomOne + "% German Shephard.");
                System.out.println("Your dog is " + randomTwo + "% Doberman Pincher.");
                System.out.println("Your dog is " + randomThree + "% Bulldog.");
                System.out.println("Your dog is " + randomFour + "% Rotweiler.");
                System.out.println("Your dog is " + randomFive + "% Beagle.");
                //Set keepRolling variable to false to exit loop.
                keepRolling = false;
            }
        }

        

    }

}
