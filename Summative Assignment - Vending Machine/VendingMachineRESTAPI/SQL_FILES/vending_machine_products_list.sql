DROP DATABASE IF EXISTS vending_machine;

CREATE DATABASE vending_machine;

USE vending_machine;

CREATE TABLE IF NOT EXISTS products
(
product_id int NOT NULL auto_increment,
product_name varchar(30) NOT NULL,
product_price DECIMAL(3,2) NOT NULL,
product_quantity varchar(2) NOT NULL,
PRIMARY KEY(product_id)
);

INSERT INTO products(product_id, product_name, product_price, product_quantity) VALUES
(1, 'Snickers', 1.5, 5),
(2, 'M&M''s', 2.25, 6),
(3, 'Almond Joy', 1.5, 4),
(4, 'Milky Way', 1.5, 8),
(5, 'PayDay', 1.75, 7),
(6, 'Reese''s', 0.99, 5),
(7, 'Pringles', 3.5, 5),
(8, 'Cheezits', 3.2, 10),
(9, 'Doritos', 2.75, 5);




