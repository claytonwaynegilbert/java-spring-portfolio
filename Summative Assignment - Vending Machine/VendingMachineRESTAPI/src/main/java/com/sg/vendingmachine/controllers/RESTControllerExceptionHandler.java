/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachine.controllers;

import com.sg.vendingmachine.dao.InsufficientFundsException;
import com.sg.vendingmachine.dao.InvalidItemSelectionException;
import com.sg.vendingmachine.dao.NoInventoryException;
import com.sg.vendingmachine.model.ErrorMessage;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author cjsm12
 */
@ControllerAdvice
public class RESTControllerExceptionHandler {

    @ExceptionHandler(InsufficientFundsException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    public ErrorMessage handleInsufficientFundsException(InsufficientFundsException ex) {
        ErrorMessage error = new ErrorMessage();
        error.setErrorMessage(ex.getMessage());

        return error;
    }

    @ExceptionHandler(NoInventoryException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    public ErrorMessage handleNoInventoryException(NoInventoryException ex) {
        ErrorMessage error = new ErrorMessage();
        error.setErrorMessage(ex.getMessage());

        return error;
    }

    @ExceptionHandler(InvalidItemSelectionException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    public ErrorMessage handleNoInventoryException(InvalidItemSelectionException ex) {
        ErrorMessage error = new ErrorMessage();
        error.setErrorMessage(ex.getMessage());

        return error;
    }
}
