/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachine.model;

/**
 *
 * @author cjsm12
 */
public class Change {

    private int quarters;
    private int dimes;
    private int nickels;
    private int pennies;

    public Change(int numberOfPennies) {        
        quarters = numberOfPennies / 25;
        dimes = ((numberOfPennies) - (quarters * 25)) / 10;
        nickels = ((numberOfPennies) - (quarters * 25) - (dimes * 10)) / 5;
        pennies = ((numberOfPennies) - (quarters * 25) - (dimes * 10) - (nickels * 5)) / 1;
    }

    public int getQuarters() {
        return quarters;
    }

    public void setQuarters(int quarters) {
        this.quarters = quarters;
    }

    public int getDimes() {
        return dimes;
    }

    public void setDimes(int dimes) {
        this.dimes = dimes;
    }

    public int getNickels() {
        return nickels;
    }

    public void setNickels(int nickels) {
        this.nickels = nickels;
    }

    public int getPennies() {
        return pennies;
    }

    public void setPennies(int pennies) {
        this.pennies = pennies;
    }

}
