/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachine.dao;

/**
 *
 * @author cjsm12
 */
public class InvalidItemSelectionException extends Exception {

    public InvalidItemSelectionException() {
    }

    public InvalidItemSelectionException(String message) {
        super(message);
    }

    public InvalidItemSelectionException(String message, Throwable cause) {
        super(message, cause);
    }
    
    
    
}
