/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachine.dao;

/**
 *
 * @author cjsm12
 */
public class NoInventoryException extends Exception {

    /**
     * Creates a new instance of <code>NoInventoryException</code> without
     * detail message.
     */
    public NoInventoryException() {
    }

    /**
     * Constructs an instance of <code>NoInventoryException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public NoInventoryException(String msg) {
        super(msg);
    }

    public NoInventoryException(String message, Throwable cause) {
        super(message, cause);
    }
    
    
}
