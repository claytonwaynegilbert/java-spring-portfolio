package com.sg.vendingmachine.controllers;

import com.sg.vendingmachine.dao.InsufficientFundsException;
import com.sg.vendingmachine.dao.InvalidItemSelectionException;
import com.sg.vendingmachine.dao.NoInventoryException;
import com.sg.vendingmachine.model.Change;
import com.sg.vendingmachine.model.Product;
import com.sg.vendingmachine.service.VendingMachineService;
import java.math.BigDecimal;
import java.util.List;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@CrossOrigin
public class VendingMachineRESTController {

    private VendingMachineService service;

    @Inject
    public VendingMachineRESTController(VendingMachineService service) {
        this.service = service;
    }

    @RequestMapping(value = "/items", method = RequestMethod.GET)
    @ResponseBody
    public List<Product> getAllProducts() {
        return service.getAllProducts();
    }

    @RequestMapping(value = "/money/{amount}/item/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Change purchaseProduct(
            @PathVariable("amount") BigDecimal amount, 
            @PathVariable("id") int id) throws InsufficientFundsException, NoInventoryException, InvalidItemSelectionException {

        Change change = service.purchaseProducts(amount, id);
        
        return change;
    }

}
