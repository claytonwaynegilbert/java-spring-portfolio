/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachine.service;

import com.sg.vendingmachine.dao.InsufficientFundsException;
import com.sg.vendingmachine.dao.InvalidItemSelectionException;
import com.sg.vendingmachine.dao.NoInventoryException;
import com.sg.vendingmachine.dao.ProductDao;
import com.sg.vendingmachine.model.Change;
import com.sg.vendingmachine.model.Product;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author cjsm12
 */
public class VendingMachineServiceDatabaseImpl implements VendingMachineService {

    private ProductDao dao;

    public VendingMachineServiceDatabaseImpl(ProductDao dao) {
        this.dao = dao;
    }

    @Override
    public Change purchaseProducts(BigDecimal userMoney, int productId) throws NoInventoryException,
            InsufficientFundsException,
            InvalidItemSelectionException {
        Product matchingProduct = dao.getProductById(productId);

        if (matchingProduct == null) {
            throw new InvalidItemSelectionException("No item associated with that ID");
        }

        if (matchingProduct.getQuantity() <= 0) {
            throw new NoInventoryException("SOLD OUT!");
        }

        if (userMoney.compareTo(matchingProduct.getPrice()) < 0) {
            BigDecimal priceDiff = matchingProduct.getPrice().subtract(userMoney);
            throw new InsufficientFundsException("INSUFFICIENT FUNDS - Please Deposit $" + priceDiff.toString());
        }

        BigDecimal changeLeftOver = matchingProduct.getPrice().subtract(userMoney);
        BigDecimal pennyCount = changeLeftOver.multiply(new BigDecimal("100"));
        Change changeObject = new Change(pennyCount.intValue());

        return changeObject;
    }

    @Override
    public void updateProduct(Product updatedProduct) {
        dao.updateProduct(updatedProduct);
    }

    @Override
    public Product getProductById(int productId) {
        return dao.getProductById(productId);
    }

    @Override
    public Product getProductByName(String productName) {
        return dao.getProductByName(productName);
    }

    @Override
    public List<Product> getAllProducts() {
        return dao.getAllProducts();
    }

}
