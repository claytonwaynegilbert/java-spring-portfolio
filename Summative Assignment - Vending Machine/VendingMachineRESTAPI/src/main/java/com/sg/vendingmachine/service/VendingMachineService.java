/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachine.service;

import com.sg.vendingmachine.dao.InsufficientFundsException;
import com.sg.vendingmachine.dao.InvalidItemSelectionException;
import com.sg.vendingmachine.dao.NoInventoryException;
import com.sg.vendingmachine.model.Change;
import com.sg.vendingmachine.model.Product;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author cjsm12
 */
public interface VendingMachineService {

    Change purchaseProducts(BigDecimal userMoney, int productId)
            throws NoInventoryException, InsufficientFundsException, InvalidItemSelectionException;

    void updateProduct(Product updatedProduct);

    Product getProductById(int productId);

    Product getProductByName(String productName);

    List<Product> getAllProducts();
}
