/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.vendingmachine.dao;

import com.sg.vendingmachine.model.Product;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author cjsm12
 */
public class ProductDaoDatabaseImpl implements ProductDao {

    private static final String SQL_INSERT_INTO_PRODUCTS
            = "insert into products(product_name, product_price, product_quantity) "
            + "VALUES(?, ?, ?)";

    private static final String SQL_REMOVE_FROM_PRODUCTS
            = "remove from products where product_id = ?";

    private static final String SQL_UPDATE_PRODUCTS
            = "update products set "
            + "product_name = ?, "
            + "product_price = ?, "
            + "product_quantity = ? "
            + "where product_id = ?";

    private static final String SQL_SELECT_ALL_PRODUCTS
            = "select * from products";

    private static final String SQL_SELECT_PRODUCT_BY_ID
            = "select * from products where product_id = ?";

    private static final String SQL_SELECT_PRODUCT_BY_NAME
            = "select * from products where product_name = ?";

    private JdbcTemplate jdbcTemplate;

    public ProductDaoDatabaseImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Product addProduct(Product product) {
        jdbcTemplate.update(SQL_INSERT_INTO_PRODUCTS, product.getProduct_name(),
                product.getPrice(),
                product.getQuantity());
        int productId = jdbcTemplate.queryForObject("select LAST_INSERT_ID()", Integer.class);
        product.setProduct_id(productId);

        return product;
    }

    @Override
    public void removeProduct(int productId) {
        jdbcTemplate.update(SQL_REMOVE_FROM_PRODUCTS, productId);
    }

    @Override
    public void updateProduct(Product updatedProduct) {
        jdbcTemplate.update(SQL_UPDATE_PRODUCTS,
                updatedProduct.getProduct_name(),
                updatedProduct.getPrice(),
                updatedProduct.getQuantity(),
                updatedProduct.getProduct_id());
    }

    @Override
    public Product getProductById(int productId) {
        Product product;
        try {
            product = jdbcTemplate.queryForObject(SQL_SELECT_PRODUCT_BY_ID, new ProductMapper(), productId);
            return product;
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    @Override
    public Product getProductByName(String productName) {
        Product product;
        try {
            product = jdbcTemplate.queryForObject(SQL_SELECT_PRODUCT_BY_NAME, new ProductMapper(), productName);
            return product;
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    @Override
    public List<Product> getAllProducts() {
        return jdbcTemplate.query(SQL_SELECT_ALL_PRODUCTS, new ProductMapper());
    }

    private static class ProductMapper implements RowMapper<Product> {

        @Override
        public Product mapRow(ResultSet rs, int i) throws SQLException {
            Product product = new Product();
            product.setProduct_id(rs.getInt("product_id"));
            product.setProduct_name(rs.getString("product_name"));
            product.setPrice(rs.getBigDecimal("product_price"));
            product.setQuantity(rs.getInt("product_quantity"));

            return product;
        }

    }

}
