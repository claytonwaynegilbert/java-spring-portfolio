/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.serverinventory.app;

import com.sg.serverinventory.dao.ServerDao;
import com.sg.serverinventory.dao.ServerDaoImpl;
import com.sg.serverinventory.dto.Server;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author savannahg
 */
public class App {

    public static void main(String[] args) {

        //Add a couple Servers to the DAO from a couple different manufacturers
        ServerDao dao = new ServerDaoImpl();

        Server myServer = new Server();
        myServer.setName("eng10");
        myServer.setManufacturer("HP");
        myServer.setIp("192.928.12.19");
        myServer.setRam(5);
        myServer.setProcessors(2);
        myServer.setPurchaseDate(LocalDate.parse("2015-03-19", DateTimeFormatter.ISO_DATE));

        dao.addServer(myServer);

        myServer = new Server();
        myServer.setName("eng45");
        myServer.setManufacturer("Dell");
        myServer.setIp("192.128.23.89");
        myServer.setRam(3);
        myServer.setProcessors(2);
        myServer.setPurchaseDate(LocalDate.parse("2004-11-08", DateTimeFormatter.ISO_DATE));

        dao.addServer(myServer);

        myServer = new Server();
        myServer.setName("hlr22");
        myServer.setManufacturer("Lenovo");
        myServer.setIp("102.225.42.99");
        myServer.setRam(4);
        myServer.setProcessors(1);
        myServer.setPurchaseDate(LocalDate.parse("2017-01-10", DateTimeFormatter.ISO_DATE));

        dao.addServer(myServer);

        myServer = new Server();
        myServer.setName("eng45");
        myServer.setManufacturer("HP");
        myServer.setIp("162.228.92.13");
        myServer.setRam(2);
        myServer.setProcessors(1);
        myServer.setPurchaseDate(LocalDate.parse("2000-11-22", DateTimeFormatter.ISO_DATE));

        dao.addServer(myServer);

        myServer = new Server();
        myServer.setName("llk30");
        myServer.setManufacturer("Toshiba");
        myServer.setIp("192.438.12.19");
        myServer.setRam(5);
        myServer.setProcessors(1);
        myServer.setPurchaseDate(LocalDate.parse("2008-12-19", DateTimeFormatter.ISO_DATE));

        dao.addServer(myServer);
        
        //Get all the servers that correlate to a passed in String which is the name of the manufacturer.
        List<Server> hps = dao.getAllServersByManucaturer("HP");
        //Used the lambda forEach stream operation to cycle through each Server and print out the name 
        hps.stream()
           .forEach(s -> System.out.println(s.getName()));
        
        //Get all servers from the dao, which gets stored in a Map where the manufacturer is the key
        //and a list of Servers made by that company is the value.
        Map<String, List<Server>> serverMap = dao.getAllServersGroupByManufacturer();
        //Get all the keys which is the manufacturer of the server
        Set<String> manufacturers = serverMap.keySet();
        //forEach key(manufacturer) print out the name
        manufacturers.stream()
                     .forEach(name -> {
                     System.out.println("===========================");
                     System.out.println("Manufacturer: " + name);
                     //Then using that key, get the corresponding List of servers and do another forEach
                     //stream method on that List so that the name of the actual server gets displayed to the user
                     serverMap.get(name).stream()
                                        .forEach(s -> System.out.println(s.getName()));
                     });
        
        
    }

}
