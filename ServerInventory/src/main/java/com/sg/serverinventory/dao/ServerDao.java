/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.serverinventory.dao;

import com.sg.serverinventory.dto.Server;
import java.util.List;
import java.util.Map;

/**
 *
 * @author savannahg
 */
public interface ServerDao {
    
    void addServer(Server server);
    
    void removeServer(String name);
    
    Server getServerByName(String name);
    
    List<Server> getAllServers();
    
    Map<String, List<Server>> getAllServersGroupByManufacturer();
    
    List<Server> getAllServersByManucaturer(String manufacturer);
    
    List<Server> getServersOlderThan(int ageInYear);
    
    Map<String, List<Server>> getServersOlderThanGroupByManufacturer(int ageInYears);
    
    double getAverageServerAge();
    
}
