<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Create A User</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/create-user-form.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="active">
                        <a href="${pageContext.request.contextPath}/displayHomePage">Home
                        </a>
                    </li>
                    <sec:authorize access="hasRole('ROLE_USER')">
                        <li role="presentation">
                            <a href="${pageContext.request.contextPath}/main/manager">Manager
                            </a>
                        </li>
                    </sec:authorize>
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <li role="presentation">
                            <a href="${pageContext.request.contextPath}/users/viewUsers">Users
                            </a>
                        </li>
                    </sec:authorize>
                </ul>    
            </div>

            <h2>Create A User</h2>
            <hr>
            <form class="form-horizontal"
                  method="POST"
                  action="createUser">
                <div class="form-group">
                    <label for="username"
                           class="control-label col-md-4">Username:</label>
                    <div class="col-md-4">
                        <input type="text" 
                               id="first-name" 
                               class="form-control"
                               name="username"/>
                    </div>
                </div>

                <div class="form-group">
                    <label for="password"
                           class="control-label col-md-4">Password:</label>
                    <div class="col-md-4">
                        <input type="text" 
                               id="first-name" 
                               class="form-control"
                               name="password"/>
                    </div>
                </div>

                <div class="form-group">
                    <label for="password"
                           class="control-label col-md-4">isAdmin:</label>
                    <div class="col-md-4">
                        <input type="checkbox" 
                               id="first-name" 
                               class="form-control"
                               name="isAdmin"
                               value="yes"/>
                    </div>
                </div>

                <div id="submit" class="form-group">
                    <input type="submit" 
                           id="submit-button"
                           class="btn btn-primary col-md-offset-4"
                           value="Create"/>

                    <a href="${pageContext.request.contextPath}/displayHomePage">
                        <button type="button"
                                id="cancel-button" 
                                class="btn btn-primary">Cancel</button>
                    </a>
                </div>
            </form>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>
