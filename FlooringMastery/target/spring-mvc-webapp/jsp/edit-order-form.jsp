<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Edit Order</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/displayHomePage">Home
                        </a>
                    </li>
                    <sec:authorize access="hasRole('ROLE_USER')">
                        <li role="presentation">
                            <a href="${pageContext.request.contextPath}/main/manager">Manager
                            </a>
                        </li>
                    </sec:authorize>
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <li role="presentation">
                            <a href="${pageContext.request.contextPath}/users/viewUsers">Users
                            </a>
                        </li>
                    </sec:authorize>
                </ul>    
            </div>

            <h1>Edit Order</h1>
            <hr>
            <sf:form id="create-order-form"
                     role="form"
                     class="form-horizontal"
                     modelAttribute="order"
                     method="POST"
                     action="editOrder">
                <div class="form-group">
                    <label for="first-name"
                           class="col-md-4 control-label">First Name:
                    </label>
                    <div class="col-md-4">
                        <sf:input type="text"
                                  id="first-name"
                                  class="form-control"
                                  path="firstName"
                                  placeholder="John"/>
                        <sf:errors path="firstName" class="cssError"/>
                        <sf:hidden path="orderId"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="last-name"
                           class="col-md-4 control-label">Last Name:
                    </label>
                    <div class="col-md-4">
                        <sf:input type="text"
                                  id="last-name"
                                  class="form-control"
                                  path="lastName"
                                  placeholder="Smith"/>
                        <sf:errors path="lastName" class="cssError"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="product-name"
                           class="col-md-4 control-label">Material:
                    </label>
                    <div class="col-md-4">
                        <sf:select id="product-name"
                                   class="form-control"
                                   path="product.productName"
                                   items="${products}"/>
                        <sf:errors path="product.productName" class="cssError"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="state-name"
                           class="col-md-4 control-label">State:
                    </label>
                    <div class="col-md-4">
                        <sf:select id="state-name"
                                   class="form-control"
                                   path="tax.stateName"
                                   items="${states}"/>
                        <sf:errors path="tax.stateName" class="cssError"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="area-length"
                           class="col-md-4 control-label">Area Length(Feet):
                    </label>
                    <div class="col-md-4">
                        <sf:input type="text"
                                  id="area-length"
                                  class="form-control"
                                  path="areaLength"
                                  placeholder="30"/>
                        <sf:errors path="areaLength" class="cssError"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="area-width"
                           class="col-md-4 control-label">Area Width(Feet):
                    </label>
                    <div class="col-md-4">
                        <sf:input type="text"
                                  id="area-width"
                                  class="form-control"
                                  path="areaWidth"
                                  placeholder="17.5"/> 
                        <sf:errors path="areaWidth" class="cssError"/>
                    </div>
                </div>
                <div class="form-group">
                    <input type="submit"
                           id="submit-order"
                           class="btn btn-primary col-md-offset-4"
                           value="Edit"/>
                    <a href="${pageContext.request.contextPath}/main/manager">
                        <button type="button"
                                id="cancel-order"
                                class="btn btn-primary">Cancel</button>
                    </a>
                </div>
            </sf:form>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>
