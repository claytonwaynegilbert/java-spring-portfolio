<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Order Review</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/displayHomePage">Home
                        </a>
                    </li>
                    <sec:authorize access="hasRole('ROLE_USER')">
                        <li role="presentation">
                            <a href="${pageContext.request.contextPath}/main/manager">Manager
                            </a>
                        </li>
                    </sec:authorize>
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <li role="presentation">
                            <a href="${pageContext.request.contextPath}/users/viewUsers">Users
                            </a>
                        </li>
                    </sec:authorize>
                </ul>    
            </div>
                        
            <h2>Review Your Order</h2>
            <hr>
            <p>Name: <c:out value="${order.firstName} "/><c:out value=" ${order.lastName}"/></p>
            <br><br>
            <p>Date: <c:out value="${date}"/></p>
            <br><br>
            <p>Product: <c:out value="${order.product.productName}"/></p>
            <br><br>
            <p>State: <c:out value="${order.tax.stateName}"/></p>
            
            <div class="form-group">
                <a href="${pageContext.request.contextPath}/create/createOrder">
                <button id="order-review-search-button"
                        class="btn btn-primary">Submit</button>
                </a>
                <a href="${pageContext.request.contextPath}/main/manager">
                <button id="order-review-cancel-button"
                        class="btn btn-primary">Cancel</button>
                </a>
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>
