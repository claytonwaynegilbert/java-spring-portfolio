/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dao;

import com.sg.flooringmastery.dto.Tax;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author cjsm12
 */
public class TaxDaoDatabaseImpl implements TaxDao {

    private static final String SQL_INSERT_INTO_TAXES = "insert into "
            + "taxes(state_name, tax_rate) values(?, ?)";

    private static final String SQL_REMOVE_FROM_TAXES = "delete from taxes where state_name = ?";

    private static final String SQL_SELECT_FROM_TAXES_BY_ID = "select * from taxes where tax_id = ?";

    private static final String SQL_SELECT_FROM_TAXES_BY_NAME = "select * from taxes where state_name = ?";

    private static final String SQL_UPDATE_TAXES = "update taxes set "
            + "state_name = ?, "
            + "tax_rate = ? "
            + "where tax_id = ?";

    private static final String SQL_SELECT_ALL_TAXES = "select * from taxes";

    
    private JdbcTemplate jdbcTemplate;

    public TaxDaoDatabaseImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Tax addTax(Tax tax) {
        jdbcTemplate.update(SQL_INSERT_INTO_TAXES, tax.getStateName(),
                tax.getStateTax());

        int tax_id = jdbcTemplate.queryForObject("select LAST_INSERT_ID()", Integer.class);
        tax.setTaxId(tax_id);

        return tax;
    }

    @Override
    public void removeTax(String stateName) {
        jdbcTemplate.update(SQL_REMOVE_FROM_TAXES, stateName);
    }

    @Override
    public void updateTax(Tax updatedTax) {
        jdbcTemplate.update(SQL_UPDATE_TAXES, 
                updatedTax.getStateName(),
                updatedTax.getStateTax(),
                updatedTax.getTaxId());
    }

    @Override
    public Tax getTaxById(int taxId) {
        try {
            return jdbcTemplate.queryForObject(SQL_SELECT_FROM_TAXES_BY_ID, new TaxMapper(), taxId);
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    @Override
    public Tax getTaxByName(String taxName) {
        try {
            return jdbcTemplate.queryForObject(SQL_SELECT_FROM_TAXES_BY_NAME, new TaxMapper(), taxName);
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    @Override
    public List<Tax> getAllTaxes() {
        return jdbcTemplate.query(SQL_SELECT_ALL_TAXES, new TaxMapper());
    }

    private static final class TaxMapper implements RowMapper<Tax> {

        @Override
        public Tax mapRow(ResultSet rs, int i) throws SQLException {
            Tax tax = new Tax();
            tax.setTaxId(rs.getInt("tax_id"));
            tax.setStateName(rs.getString("state_name"));
            tax.setStateTax(rs.getBigDecimal("tax_rate"));
            
            return tax;
        }

    }

}
