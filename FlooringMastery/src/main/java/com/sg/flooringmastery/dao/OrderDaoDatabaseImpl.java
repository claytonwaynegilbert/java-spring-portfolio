/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dao;

import com.sg.flooringmastery.dto.Order;
import com.sg.flooringmastery.dto.Product;
import com.sg.flooringmastery.dto.Tax;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author cjsm12
 */
public class OrderDaoDatabaseImpl implements OrderDao {

    private static final String SQL_INSERT_INTO_ORDERS = "insert into "
            + "orders(first_name, last_name, order_date, product_id, tax_id, area_length, area_width, total_area, total_material_cost, total_labor_cost, total_tax, total_cost) "
            + "values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

    private static final String SQL_REMOVE_FROM_ORDERS = "delete from orders where order_id = ?";

    private static final String SQL_UPDATE_ORDERS = "update orders set "
            + "first_name = ?, "
            + "last_name = ?, "
            + "order_date = ?, "
            + "product_id = ?, "
            + "tax_id = ?, "
            + "area_length = ?, "
            + "area_width = ?, "
            + "total_area = ?, "
            + "total_material_cost = ?, "
            + "total_labor_cost = ?, "
            + "total_tax = ?, "
            + "total_cost = ? "
            + "where order_id = ?";

    private static final String SQL_SELECT_ORDER_BY_ID = "select * from orders where order_id = ?";

    private static final String SQL_SELECT_ALL_ORDERS = "select * from orders";

    private static final String SQL_SELECT_ALL_ORDERS_BY_LAST_NAME = "select * from orders where last_name = ?";

    private static final String SQL_SELECT_ALL_ORDERS_BY_DATE = "select * from orders where order_date = ?";

    private static final String SQL_SELECT_PRODUCT_BY_ORDER_ID = "select * from products p join orders o on p.product_id = o.product_id where order_id = ?";
    
    private static final String SQL_SELECT_TAX_BY_ORDER_ID = "select * from taxes t join orders o on t.tax_id = o.tax_id where order_id = ?";
    
    
    private JdbcTemplate jdbcTemplate;
    
    public OrderDaoDatabaseImpl(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Order addOrder(Order order) {
        jdbcTemplate.update(SQL_INSERT_INTO_ORDERS, order.getFirstName(),
                order.getLastName(),
                order.getOrderDate().toString(),
                order.getProduct().getProductId(),
                order.getTax().getTaxId(),
                order.getAreaLength(),
                order.getAreaWidth(),
                order.getTotalArea(),
                order.getTotalMaterialCost(),
                order.getTotalLaborCost(),
                order.getTotalTax(),
                order.getTotalCost());
        int order_id = jdbcTemplate.queryForObject("select LAST_INSERT_ID()", Integer.class);
        order.setOrderId(order_id);

        return order;
    }

    @Override
    public void removeOrder(int orderId) {
        jdbcTemplate.update(SQL_REMOVE_FROM_ORDERS, orderId);
    }

    @Override
    public void updateOrder(Order updatedOrder) {
        jdbcTemplate.update(SQL_UPDATE_ORDERS,
                updatedOrder.getFirstName(),
                updatedOrder.getLastName(),
                updatedOrder.getOrderDate().toString(),
                updatedOrder.getProduct().getProductId(),
                updatedOrder.getTax().getTaxId(),
                updatedOrder.getAreaLength(),
                updatedOrder.getAreaWidth(),
                updatedOrder.getTotalArea(),
                updatedOrder.getTotalMaterialCost(),
                updatedOrder.getTotalLaborCost(),
                updatedOrder.getTotalTax(),
                updatedOrder.getTotalCost(),
                updatedOrder.getOrderId());

    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public Order getOrderById(int orderId) {
        Order order = null;
        try {
            order = jdbcTemplate.queryForObject(SQL_SELECT_ORDER_BY_ID, new OrderMapper(), orderId);
            setProductToOrder(order);
            setTaxToOrder(order);
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
        return order;
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<Order> getAllOrders() {
        List<Order> allOrders = jdbcTemplate.query(SQL_SELECT_ALL_ORDERS, new OrderMapper());
        assignProductAndTaxToCollection(allOrders);

        return allOrders;
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<Order> getAllOrdersByName(String customerName) {
        List<Order> allOrders = jdbcTemplate.query(SQL_SELECT_ALL_ORDERS_BY_LAST_NAME, new OrderMapper(), customerName);
        assignProductAndTaxToCollection(allOrders);

        return allOrders;
    }

    @Override
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)
    public List<Order> getAllOrdersByDate(LocalDate orderDate) {
        List<Order> allOrders = jdbcTemplate.query(SQL_SELECT_ALL_ORDERS_BY_DATE, new OrderMapper(), orderDate.toString());
        assignProductAndTaxToCollection(allOrders);

        return allOrders;
    }

    private void setProductToOrder(Order order) {
        Product product = jdbcTemplate.queryForObject(SQL_SELECT_PRODUCT_BY_ORDER_ID,
                new ProductMapper(),
                order.getOrderId());
        order.setProduct(product);
    }

    private void setTaxToOrder(Order order) {
        Tax tax = jdbcTemplate.queryForObject(SQL_SELECT_TAX_BY_ORDER_ID,
                new TaxMapper(),
                order.getOrderId());
        
        order.setTax(tax);
    }

    private void assignProductAndTaxToCollection(List<Order> allOrders) {
        for(Order order : allOrders){
            setProductToOrder(order);
            setTaxToOrder(order);
        }
    }

    private static final class OrderMapper implements RowMapper<Order> {

        @Override
        public Order mapRow(ResultSet rs, int i) throws SQLException {
            Order order = new Order();
            order.setOrderId(rs.getInt("order_id"));
            order.setFirstName(rs.getString("first_name"));
            order.setLastName(rs.getString("last_name"));
            order.setOrderDate(rs.getTimestamp("order_date").toLocalDateTime().toLocalDate());
            order.setAreaLength(rs.getBigDecimal("area_length"));
            order.setAreaWidth(rs.getBigDecimal("area_width"));
            order.setTotalArea(rs.getBigDecimal("total_area"));
            order.setTotalMaterialCost(rs.getBigDecimal("total_material_cost"));
            order.setTotalLaborCost(rs.getBigDecimal("total_labor_cost"));
            order.setTotalTax(rs.getBigDecimal("total_tax"));
            order.setTotalCost(rs.getBigDecimal("total_cost"));

            return order;
        }
    }
    
    private static final class ProductMapper implements RowMapper<Product>{

        @Override
        public Product mapRow(ResultSet rs, int i) throws SQLException {
            Product product = new Product();
            product.setProductId(rs.getInt("product_id"));
            product.setProductName(rs.getString("product_name"));
            product.setCostPerSquareFoot(rs.getBigDecimal("cost_per_sq_ft"));
            product.setLaborCostPerSquareFoot(rs.getBigDecimal("labor_cost_per_sq_ft"));
            
            return product;
        }
        
    }
    
    private static final class TaxMapper implements RowMapper<Tax>{

        @Override
        public Tax mapRow(ResultSet rs, int i) throws SQLException {
            Tax tax = new Tax();
            tax.setTaxId(rs.getInt("tax_id"));
            tax.setStateName(rs.getString("state_name"));
            tax.setStateTax(rs.getBigDecimal("tax_rate"));
            
            return tax;
        }
        
    }
}
