/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dao;

import com.sg.flooringmastery.dto.Order;
import java.time.LocalDate;
import java.util.List;

/**
 *
 * @author cjsm12
 */
public interface OrderDao {
    
    Order addOrder(Order order);
    
    void removeOrder(int orderId);
    
    void updateOrder(Order updatedOrder);
    
    Order getOrderById(int orderId);
    
    List<Order> getAllOrders();
    
    List<Order> getAllOrdersByName(String customerName);
    
    List<Order> getAllOrdersByDate(LocalDate orderDate);
    
}
