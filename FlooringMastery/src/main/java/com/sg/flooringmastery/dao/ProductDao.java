/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dao;

import com.sg.flooringmastery.dto.Product;
import java.util.List;

/**
 *
 * @author cjsm12
 */
public interface ProductDao {
    
    Product addProduct(Product product);
    
    void deleteProduct(String productName);
    
    void updateProduct(Product updatedProduct);
    
    Product getProductById(int productId);
    
    Product getProductByName(String productName);
    
    List<Product> getAllProducts();
    
}
