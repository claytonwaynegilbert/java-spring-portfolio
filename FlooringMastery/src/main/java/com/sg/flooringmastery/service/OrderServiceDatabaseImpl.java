/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.service;

import com.sg.flooringmastery.dao.OrderDao;
import com.sg.flooringmastery.dao.ProductDao;
import com.sg.flooringmastery.dao.TaxDao;
import com.sg.flooringmastery.dto.Order;
import com.sg.flooringmastery.dto.Product;
import com.sg.flooringmastery.dto.Tax;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.List;

/**
 *
 * @author cjsm12
 */
public class OrderServiceDatabaseImpl implements OrderService {

    private OrderDao orderDao;
    private ProductDao productDao;
    private TaxDao taxDao;

    public OrderServiceDatabaseImpl(OrderDao orderDao, ProductDao productDao, TaxDao taxDao) {
        this.orderDao = orderDao;
        this.productDao = productDao;
        this.taxDao = taxDao;
    }

    @Override
    public void createOrder(Order order) {
        order.setOrderDate(LocalDate.now());
        calculateLaborMaterialTaxTotalCosts(order);
        orderDao.addOrder(order);
    }

    @Override
    public void removeOrder(int orderId) {
        orderDao.removeOrder(orderId);
    }

    @Override
    public void updateOrder(Order order) {
        calculateLaborMaterialTaxTotalCosts(order);
        orderDao.updateOrder(order);
    }

    @Override
    public Order getOrderById(int orderId) {
        return orderDao.getOrderById(orderId);
    }

    @Override
    public List<Order> getAllOrders() {
        return orderDao.getAllOrders();
    }

    @Override
    public List<Order> getAllOrdersByName(String customerName) {
        return orderDao.getAllOrdersByName(customerName);
    }

    @Override
    public List<Order> getAllOrdersByDate(LocalDate orderDate) {
        return orderDao.getAllOrdersByDate(orderDate);
    }

    private void calculateLaborMaterialTaxTotalCosts(Order order) {
        Product matchingProduct = productDao.getProductByName(order.getProduct().getProductName());
        Tax matchingTax = taxDao.getTaxByName(order.getTax().getStateName());
        order.setProduct(matchingProduct);
        order.setTax(matchingTax);

        BigDecimal totalArea = order.getAreaLength().multiply(order.getAreaWidth());
        BigDecimal totalMaterialCost = matchingProduct.getCostPerSquareFoot().multiply(totalArea);
        BigDecimal totalLaborCost = matchingProduct.getLaborCostPerSquareFoot().multiply(totalArea);
        BigDecimal taxRateFinal = matchingTax.getStateTax().divide(new BigDecimal("100.00"), 2, RoundingMode.HALF_UP);
        BigDecimal laborAndMaterialCost = totalMaterialCost.add(totalLaborCost);
        BigDecimal totalTax = laborAndMaterialCost.multiply(taxRateFinal);
        BigDecimal totalCost = laborAndMaterialCost.add(totalTax).setScale(2, RoundingMode.HALF_UP);

        order.setTotalArea(totalArea);
        order.setTotalMaterialCost(totalMaterialCost);
        order.setTotalLaborCost(totalLaborCost);
        order.setTotalTax(totalTax);
        order.setTotalCost(totalCost);
        order.setOrderDate(LocalDate.now());
    }

}
