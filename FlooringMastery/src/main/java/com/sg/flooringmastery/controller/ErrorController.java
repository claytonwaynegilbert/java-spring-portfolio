/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author cjsm12
 */
@Controller
public class ErrorController {
    
    @RequestMapping(value="/error", method=RequestMethod.GET)
    public String displayErrorPage(HttpServletRequest request){
        
        return "error-page";
    }
}
