/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.controller;

import com.sg.flooringmastery.dao.ProductDao;
import com.sg.flooringmastery.dao.TaxDao;
import com.sg.flooringmastery.dto.Order;
import com.sg.flooringmastery.dto.Product;
import com.sg.flooringmastery.dto.Tax;
import com.sg.flooringmastery.service.OrderService;
import java.time.LocalDate;
import java.util.List;
import javax.inject.Inject;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

/**
 *
 * @author cjsm12
 */
@Controller
@RequestMapping("/create")
@SessionAttributes("order")
public class CreateController {

    private OrderService service;
    private ProductDao productDao;
    private TaxDao taxDao;

    @Inject
    public CreateController(OrderService service, ProductDao productDao, TaxDao taxDao) {
        this.service = service;
        this.productDao = productDao;
        this.taxDao = taxDao;
    }

    @RequestMapping(value = "/displayCreateOrderForm", method = RequestMethod.GET)
    public String displayCreateOrderForm(Model model) {
        Order order = new Order();
        List<Product> allProducts = productDao.getAllProducts();
        List<Tax> allTaxes = taxDao.getAllTaxes();
        model.addAttribute("order", order);
        model.addAttribute("products", allProducts);
        model.addAttribute("states", allTaxes);

        return "create-order-form";
    }

    @RequestMapping(value = "/reviewCreateOrder", method = RequestMethod.POST)
    public String displayCreateOrderReview(@Valid @ModelAttribute("order") Order order, 
                                                                 BindingResult results, 
                                                                           Model model) {
        if (results.hasErrors()) {
            return "create-order-form";
        }
        LocalDate todaysDate = LocalDate.now();
        model.addAttribute("date", todaysDate.toString());

        return "create-order-review";
    }

    @RequestMapping(value = "/createOrder", method = RequestMethod.GET)
    public String createOrder(@ModelAttribute("order") Order order) {
        service.createOrder(order);

        return "redirect:/main/manager";
    }
}
