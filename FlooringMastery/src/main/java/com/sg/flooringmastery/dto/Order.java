/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dto;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

/**
 *
 * @author cjsm12
 */
public class Order {

    private int orderId;
    private String firstName;
    private String lastName;
    private LocalDate orderDate;
    private Product product;
    private Tax tax;
    private BigDecimal areaLength;
    private BigDecimal areaWidth;
    private BigDecimal totalArea;
    private BigDecimal totalLaborCost;
    private BigDecimal totalMaterialCost;
    private BigDecimal totalTax;
    private BigDecimal totalCost;

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDate orderDate) {
        this.orderDate = orderDate;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Tax getTax() {
        return tax;
    }

    public void setTax(Tax tax) {
        this.tax = tax;
    }

    public BigDecimal getAreaLength() {
        return areaLength;
    }

    public void setAreaLength(BigDecimal areaLength) {
        this.areaLength = areaLength;
    }

    public BigDecimal getAreaWidth() {
        return areaWidth;
    }

    public void setAreaWidth(BigDecimal areaWidth) {
        this.areaWidth = areaWidth;
    }

    public BigDecimal getTotalArea() {
        return totalArea;
    }

    public void setTotalArea(BigDecimal totalArea) {
        this.totalArea = totalArea;
    }

    public BigDecimal getTotalLaborCost() {
        return totalLaborCost;
    }

    public void setTotalLaborCost(BigDecimal totalLaborCost) {
        this.totalLaborCost = totalLaborCost;
    }

    public BigDecimal getTotalMaterialCost() {
        return totalMaterialCost;
    }

    public void setTotalMaterialCost(BigDecimal totalMaterialCost) {
        this.totalMaterialCost = totalMaterialCost;
    }

    public BigDecimal getTotalTax() {
        return totalTax;
    }

    public void setTotalTax(BigDecimal totalTax) {
        this.totalTax = totalTax;
    }

    public BigDecimal getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(BigDecimal totalCost) {
        this.totalCost = totalCost;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + this.orderId;
        hash = 41 * hash + Objects.hashCode(this.firstName);
        hash = 41 * hash + Objects.hashCode(this.lastName);
        hash = 41 * hash + Objects.hashCode(this.orderDate);
        hash = 41 * hash + Objects.hashCode(this.product);
        hash = 41 * hash + Objects.hashCode(this.tax);
        hash = 41 * hash + Objects.hashCode(this.totalArea);
        hash = 41 * hash + Objects.hashCode(this.totalLaborCost);
        hash = 41 * hash + Objects.hashCode(this.totalMaterialCost);
        hash = 41 * hash + Objects.hashCode(this.totalTax);
        hash = 41 * hash + Objects.hashCode(this.totalCost);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Order other = (Order) obj;
        if (this.orderId != other.orderId) {
            return false;
        }
        if (!Objects.equals(this.firstName, other.firstName)) {
            return false;
        }
        if (!Objects.equals(this.lastName, other.lastName)) {
            return false;
        }
        if (!Objects.equals(this.orderDate, other.orderDate)) {
            return false;
        }
        if (!Objects.equals(this.product, other.product)) {
            return false;
        }
        if (!Objects.equals(this.tax, other.tax)) {
            return false;
        }
        if (!Objects.equals(this.totalArea, other.totalArea)) {
            return false;
        }
        if (!Objects.equals(this.totalLaborCost, other.totalLaborCost)) {
            return false;
        }
        if (!Objects.equals(this.totalMaterialCost, other.totalMaterialCost)) {
            return false;
        }
        if (!Objects.equals(this.totalTax, other.totalTax)) {
            return false;
        }
        if (!Objects.equals(this.totalCost, other.totalCost)) {
            return false;
        }
        return true;
    }



    @Override
    public String toString() {
        return "Order{" + "orderId=" + orderId + ", firstName=" + firstName + ", lastName=" + lastName + ", orderDate=" + orderDate + ", product=" + product + ", tax=" + tax + ", areaLength=" + areaLength + ", areaWidth=" + areaWidth + ", totalArea=" + totalArea + ", totalLaborCost=" + totalLaborCost + ", totalMaterialCost=" + totalMaterialCost + ", totalTax=" + totalTax + ", totalCost=" + totalCost + '}';
    }

}
