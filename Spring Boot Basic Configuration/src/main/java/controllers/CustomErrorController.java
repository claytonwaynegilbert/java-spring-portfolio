/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.request.WebRequest;

/**
 *
 * @author cjsm12
 */
//This controller is to handle errors in our application, like 404 and 500 server errors...
//We implement the errorcontroller so that we can specify our error url which we provide in the overriden method below...
@Controller
public class CustomErrorController implements ErrorController {
    
    private static final String ERROR_PATH = "/error"; //This is the url Spring Boot goes to when an error occurs...
    private static final String ERROR_PAGE = "custom_error"; //This is our standard error page...
    
    private final ErrorAttributes errorAttributes;

    @Autowired
    public CustomErrorController(ErrorAttributes errorAttributes) {
        this.errorAttributes = errorAttributes;
    }
    
    //When we hit our error url, Spring Boot goes here...
    @RequestMapping(ERROR_PATH)
    public String error(Model model, HttpServletRequest request){
         
        Map<String, Object> errors = getErrorAttributes(request, true); //Getting all the errors off of our request using the private utility method below...
        
        model.addAttribute("time", errors.get("timestamp"));
        model.addAttribute("status", errors.get("status"));
        model.addAttribute("error", errors.get("error"));
        model.addAttribute("message", errors.get("message"));
        model.addAttribute("path", errors.get("path"));
        
        return ERROR_PAGE; //We return our error page we have in our templates folder...
    }
    
    //This request mapping handles a specific error, which we also setup a bean in the main class to add this error page to the Spring container so that
    //Spring can find our custom error page for this url mapping...
    @RequestMapping("/404")
    public String pageNotFoundError(Model model, HttpServletRequest request){
        model.addAttribute("error", getErrorAttributes(request, true));
        
        return "404";
    }

    //Method that had to be overriden to specify to Spring Boot what our error url is...
    @Override
    public String getErrorPath() {
        return ERROR_PATH;
    }
    
    //This utility method retrieves all of our error messages from our request...
    private Map<String, Object> getErrorAttributes(HttpServletRequest request, boolean includeStackTrace){
        RequestAttributes requestAttributes = new ServletRequestAttributes(request);
        return this.errorAttributes.getErrorAttributes((WebRequest) requestAttributes, includeStackTrace);
    }
    
    
}
