/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import domain.ApplicationExceptionOne;
import domain.ApplicationExceptionTwo;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 *
 * @author cjsm12
 */
//Controller advice annotation tells Spring that we want this controller to handle all exceptions specified below when thrown in any of our other controllers...
@ControllerAdvice
public class CustomExceptionHandlerController {
    
    //Specifying the exceptions we want to handle when encountered and what exception page we want to be routed to to display the exception...
    @ExceptionHandler(ApplicationExceptionOne.class)
    public String handleApplicationExceptionOne(Model model, ApplicationExceptionOne exceptionOne){
        model.addAttribute("exception", exceptionOne);
        
        return "customApplicationExceptionOnePage";
    }
    
    @ExceptionHandler(ApplicationExceptionTwo.class)
    public String handleApplicationExceptionTwo(Model model, ApplicationExceptionTwo exceptionTwo){
        model.addAttribute("exception", exceptionTwo);
        
        return "customApplicationExceptionTwoPage";
    }
}
