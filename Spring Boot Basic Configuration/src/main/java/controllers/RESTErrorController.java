/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import domain.RestExceptionExample;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 *
 * @author cjsm12
 */
//This controller is for handling rest type exceptions thrown...
@ControllerAdvice
public class RESTErrorController {
    
    //We are catching this specific exception...
    @ExceptionHandler(RestExceptionExample.class)
    public void handleRestExceptionOne(RestExceptionExample exception, HttpServletResponse response) throws IOException{
        response.sendError(HttpStatus.UNPROCESSABLE_ENTITY.value(), exception.getMessage());
    }
}
