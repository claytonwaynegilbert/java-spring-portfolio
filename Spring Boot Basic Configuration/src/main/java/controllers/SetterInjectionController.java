/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import services.GreetingService;

/**
 *
 * @author cjsm12
 */
@Controller
public class SetterInjectionController {

    private GreetingService greeting;

    @Autowired
    public void setGreetingService(@Qualifier("setterBasedGreetingServiceImpl") GreetingService greeting) {
        this.greeting = greeting;
    }
}
