/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import services.GreetingService;

/**
 *
 * @author cjsm12
 */
@Controller
public class ConstructorInjectionController {

    private GreetingService greeting;

    @Autowired
    //For constructor Injection, you NEED to use the @Qualifier NOT on method level, but on property level.
    public ConstructorInjectionController(@Qualifier("constructorBasedGreetingServiceImpl") GreetingService greeting) {
        this.greeting = greeting;
    }

}
