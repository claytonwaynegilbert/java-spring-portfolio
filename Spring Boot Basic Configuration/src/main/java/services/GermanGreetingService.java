/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import org.springframework.beans.factory.annotation.Autowired;
import repositories.GreetingRepository;

/**
 *
 * @author cjsm12
 */
public class GermanGreetingService implements GreetingService {
    
    private GreetingRepository repository;

    @Autowired
    public GermanGreetingService(GreetingRepository repository) {
        this.repository = repository;
    }

    @Override
    public String sayHello() {
        return repository.getGermanGreeting();
    }
    
}
