/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import org.springframework.beans.factory.annotation.Autowired;
import repositories.GreetingRepository;

/**
 *
 * @author cjsm12
 */
public class GreetingServiceFactory {

    private GreetingRepository repository;

    @Autowired
    public GreetingServiceFactory(GreetingRepository repository) {
        this.repository = repository;
    }

    //Factory Method used to return a service dependent on what language is passed in... common practice
    public GreetingService createGreetingService(String lang) {
        switch (lang) {
            case "en":
                return new EnglishGreetingService(repository);
            case "es":
                return new SpanishGreetingService(repository);
            case "ge":
                return new GermanGreetingService(repository);
            default:
                return new EnglishGreetingService(repository);
        }
    }

}
