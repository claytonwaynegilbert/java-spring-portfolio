/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import repositories.GreetingRepository;

/**
 *
 * @author cjsm12
 */
public class SpanishGreetingService implements GreetingService {

    private GreetingRepository repository;

    @Autowired
    public SpanishGreetingService(GreetingRepository repository) {
        this.repository = repository;
    }

    @Override
    public String sayHello() {
        return repository.getSpanishGreeting();
    }

}
