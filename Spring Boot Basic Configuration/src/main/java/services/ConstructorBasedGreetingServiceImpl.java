/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import org.springframework.stereotype.Service;

/**
 *
 * @author cjsm12
 */
@Service
public class ConstructorBasedGreetingServiceImpl implements GreetingService {
    
    @Override
    public String sayHello() {
        return "Hello - I was injected via constructor injection!";
    }
    
}
