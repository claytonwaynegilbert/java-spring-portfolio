package app;

import controllers.MyController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.web.server.ErrorPage;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.http.HttpStatus;

@SpringBootApplication //This Annotation also does ComponentScan by default and it starts in this package and then everything DOWN! It also gives us a dispatcher servlet by default
@ComponentScan(basePackages={"app","controllers"})//If you wanted to specify locations instead of the default to scan, you can use this method of doing so
@ImportResource("classpath:application-context.xml")//We use this annotation if we want to import an xml based configuration into the spring application                                                   //to use either as the entirity of the configuration or to mix it with Java based configuration.
public class SpringapplicationApplication {
    

        //This bean is so we can customize the Spring Boot container so that we can add our custom error pages for our application, we created the bean
        //here just becasue its not as crowded over here, this could have been created in any of the config files.
        @Bean
        public EmbeddedServletContainerCustomizer containerCustomizer(){
            return ( container -> {
               ErrorPage custom404ErrorPage = new ErrorPage(HttpStatus.NOT_FOUND, "404"); //Creating a new error page and specifying what url needs to be hit..
               container.addErrorPages(custom404ErrorPage); //Adding the custom error page to the container for use in our custom error controller...
            });
        }

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(SpringapplicationApplication.class, args);
                
                MyController controller = ctx.getBean("myController", MyController.class);
                
                controller.hello();
	}
}
