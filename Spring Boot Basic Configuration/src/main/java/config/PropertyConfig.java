/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package config;

import domain.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;

/**
 *
 * @author cjsm12
 */
@Configuration
@PropertySource("classpath:datasource.properties") //Tells Spring where the properties file is we want to read from
//We can use this annotation for when you have multiple property files you need to being in...
@PropertySources({
        @PropertySource("classpath:datasource.properties"),
        @PropertySource("classpath:jms.broker.properties")
})
//REMEMBER: If we are using the default application.properties file that is given to us by Spring Boot, that file weill automatically be loaded in
//and there is no need to use the @PropertySource annotation OR the property bean we configured below.
public class PropertyConfig {
    
    @Autowired
    Environment env; //Getting access to the environment object where we have properties defined we can use instead of the ones defined in the properties file

    @Value("${guru.user}") //Telling Spring the values in the properties file we want the property bean to read
    String user;

    @Value("${guru.password}") //Telling Spring the values in the properties file we want the property bean to read
    String password;

    @Value("${guru.dburl}") //Telling Spring the values in the properties file we want the property bean to read
    String url;

    @Bean
    public DataSource dataSource() {
        DataSource ds = new DataSource();
        ds.setUser(env.getProperty("USERNAME"));//If we wanted to use an environment property instead of using the data on the properties file itself,
                                                //we can use the environment variable we wired in above and get the property off of it like so.
        ds.setPassword(password);
        ds.setUrl(url);

        return ds;
    }

    //This bean is what Spring uses to go out and find the specific properties file we specify in the @PropertySource annotation above and read
    //the expression language to get the values from the properties file
    @Bean
    public static PropertySourcesPlaceholderConfigurer properties() {
        PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer = new PropertySourcesPlaceholderConfigurer();

        return propertySourcesPlaceholderConfigurer;
    }

}
