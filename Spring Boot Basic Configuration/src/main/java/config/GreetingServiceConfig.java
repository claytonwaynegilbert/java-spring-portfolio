/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import repositories.GreetingRepository;
import services.GreetingService;
import services.GreetingServiceFactory;

/**
 *
 * @author cjsm12
 */
@Configuration
public class GreetingServiceConfig {

    @Bean
    GreetingServiceFactory getGreetingServiceFactory(GreetingRepository repository) {
        return new GreetingServiceFactory(repository);
    }

    @Bean
    @Primary//Primary means this bean is the default bean to use if no qualifier statement is used
    @Profile({"en", "default"})//This is the English Profile, meaning if English profile is set in the properties file, this bean is what would be used if needing a 
                               //GreetingService bean and no qualifier was specified, also this is set to default, meaning is no profile is set at all, this also
                               //would be the bean used since it's the default.
    GreetingService getEnglishGreetingService(GreetingServiceFactory factory) {
        return factory.createGreetingService("en");
    }

    @Bean
    @Primary//Primary means this bean is the default bean to use if no qualifier statement is used
    @Profile("es")//This is the Spanish Profile, meaning if Spanish profile is set in the properties file, this bean is what would be used if needing a 
                 //GreetingService bean and no qualifier was specified.
    GreetingService getSpanishGreetingService(GreetingServiceFactory factory) {
        return factory.createGreetingService("es");
    }

    @Bean
    @Primary//Primary means this bean is the default bean to use if no qualifier statement is used
    @Profile("ge")//This is the German Profile, meaning if German profile is set in the properties file, this bean is what would be used if needing a 
                  //GreetingService bean and no qualifier was specified
    GreetingService getGermanGreetingService(GreetingServiceFactory factory) {
        return factory.createGreetingService("ge");
    }

}
