/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

/**
 *
 * @author cjsm12
 */
public class GreetingRepositoryImpl implements GreetingRepository {
    
    @Override
    public String getEnglishGreeting() {
        return "Hello - This is the english greeting...";
    }

    @Override
    public String getSpanishGreeting() {
        return "Hello - This is the spanish greeting...";
    }

    @Override
    public String getGermanGreeting() {
        return "Hello - This is the german greeting...";
    }
    
}
