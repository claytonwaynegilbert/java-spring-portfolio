/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import controllers.*;

/**
 *
 * @author cjsm12
 */
public class RestExceptionExample extends Exception {

    /**
     * Creates a new instance of <code>RestExceptionExample</code> without
     * detail message.
     */
    public RestExceptionExample() {
    }

    /**
     * Constructs an instance of <code>RestExceptionExample</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public RestExceptionExample(String msg) {
        super(msg);
    }
}
