/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import controllers.*;

/**
 *
 * @author cjsm12
 */
public class ApplicationExceptionTwo extends Exception {

    /**
     * Creates a new instance of <code>ServerErrorException</code> without
     * detail message.
     */
    public ApplicationExceptionTwo() {
    }

    /**
     * Constructs an instance of <code>ServerErrorException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public ApplicationExceptionTwo(String msg) {
        super(msg);
    }
}
