/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringcalculatorspringmvcapplication;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author cjsm12
 */
@Controller
public class FlooringCalculatorController {

    @RequestMapping(value = "/calculateCost", method = RequestMethod.POST)
    public String calculateCost(HttpServletRequest request, Map<String, Object> model) {
        BigDecimal floorWidth = new BigDecimal(request.getParameter("width"));
        //double width = Double.parseDouble(floorWidth);
        BigDecimal floorLength = new BigDecimal(request.getParameter("length"));
        //double length = Double.parseDouble(floorLength);
        BigDecimal materialCostPerSquareInch = new BigDecimal(request.getParameter("costPerSquareInch"));
        //double costPerSquareInch = Double.parseDouble(materialCostPerSquareInch);

        BigDecimal feetLayedPerHour = new BigDecimal("20");
        //int feetLayedPerHour = 20;
        BigDecimal area = floorWidth.multiply(floorLength);
        //double area = width * length;
        BigDecimal totalMaterialCost = area.multiply(materialCostPerSquareInch).setScale(2, RoundingMode.HALF_UP);
        //double totalMaterialCost = area * costPerSquareInch;
        BigDecimal laborCostPerHour = new BigDecimal("86.00");
        //double laborCostPerHour = 86;
        BigDecimal timeToLayFloor = area.divide(feetLayedPerHour, 2, RoundingMode.HALF_UP);
        BigDecimal totalLaborCost = laborCostPerHour.multiply(timeToLayFloor).setScale(2, RoundingMode.HALF_UP);
        //double totalLaborCost = laborCostPerHour * timeToLayFloor;
        BigDecimal totalCost = totalMaterialCost.add(totalLaborCost);
        //double totalCost = totalMaterialCost + totalLaborCost;

        model.put("materialCost", totalMaterialCost);
        model.put("laborCost", totalLaborCost);
        model.put("timeToLayFloor", timeToLayFloor);
        model.put("totalCost", totalCost);

        return "results";
    }
}
