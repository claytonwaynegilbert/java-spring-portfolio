<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Flooring Calculator</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">        
        <style>
            #inputFields{
                font-weight: bold;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <h1>Flooring Calculator</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="active">
                        <a href="${pageContext.request.contextPath}/index.jsp">
                            Home</a>
                    </li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/hello/sayhi">
                            Hello Controller</a>
                    </li>
                </ul>    
            </div>
            <h2>Instructions:</h2>
            <p id="instructions">
                This is a flooring calculator program that takes inputs from the
                user(you):<br>
                1) Width(feet)<br>
                2) Length(feet)<br>
                3) Cost per 1 square foot of flooring material<br>
                The calculator then uses these inputs to calculate the cost of the materials
                for the specified area of the floor inputted.
            </p>
            <br>
            <form method="POST" action="calculateCost">
                <p id="inputFields">
                    Floor width: <input type="text" name="width"/><br><br>
                
                    Floor length: <input type="text" name="length"/><br><br>
                
                    Cost per square foot of flooring material used: <input type="text" name="costPerSquareInch"/><br><br>
                </p>
                <input type="submit" value="Calculate"/>
            </form>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>

