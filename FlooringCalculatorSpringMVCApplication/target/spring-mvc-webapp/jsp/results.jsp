<%-- 
    Document   : Results
    Created on : Sep 18, 2017, 4:13:18 AM
    Author     : cjsm12
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Flooring Calculator Results</title>
    </head>
    <body>
        <h1>Results</h1>
        <p>
            <strong>Here is the breakdown:</strong>
        </p>
        <p>
            <strong>Time to lay floor:</strong> ${timeToLayFloor} hours<br>
            <strong>Material cost:</strong> $ ${materialCost}<br>
            <strong>Labor cost:</strong> $ ${laborCost}<br>
            <strong>Total cost:</strong> $ ${totalCost}<br>
        </p>

        <a href="index.jsp"><strong>Do another area</strong></a>
        
    </body>
</html>
