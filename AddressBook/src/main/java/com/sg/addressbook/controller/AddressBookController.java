/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.addressbook.controller;

import com.sg.addressbook.dao.AddressBookDao;
import com.sg.addressbook.dao.AddressBookPersistenceException;
import com.sg.addressbook.dto.Address;
import com.sg.addressbook.view.AddressBookView;
import java.util.List;

/**
 *
 * @author savannahg
 */
public class AddressBookController {

    AddressBookView view;
    AddressBookDao dao;

    public AddressBookController(AddressBookView view, AddressBookDao dao) {
        this.view = view;
        this.dao = dao;
    }

    public void run() {

        boolean menuLoop = true;
        int userMenuChoice;

        while (menuLoop) {

            userMenuChoice = view.askUserForMenuChoice();
            try {
                switch (userMenuChoice) {
                    case 1:
                        listAllAddresses();
                        break;
                    case 2:
                        addAddress();
                        break;
                    case 3:
                        removeAddress();
                        break;
                    case 4:
                        findAddressByLastName();
                        break;
                    case 5:
                        getCurrentAddressCount();
                        break;
                    case 6:
                        editAddress();
                        break;
                    case 7:
                        menuLoop = false;
                        break;
                    default:
                        unknownCommandMessage();
                        break;
                }
            } catch (AddressBookPersistenceException e) {
                view.displayErrorMessage(e.toString());
            }
        }
        exitMessage();
    }

    private void addAddress() throws AddressBookPersistenceException {
        view.displayAddAddressBanner();
        Address createdAddress = view.askForAddressInfo();
        dao.addAddress(createdAddress);
        view.displayAddressAddedSuccessfullyBanner();
    }

    private void removeAddress() throws AddressBookPersistenceException {
        view.displayRemoveAddressBanner();
        String lastName = view.askUserForLastNameOfAddress();
        List<Address> matchingAddresses = dao.findAddressByLastName(lastName);
        int addressId = view.askUserForAddressId(lastName, matchingAddresses);
        dao.removeAddress(addressId);
        view.displayRemoveAddressSuccessfulBanner();
    }

    private void listAllAddresses() throws AddressBookPersistenceException {
        view.displayAllAddressesBanner();
        List<Address> allAddresses = dao.getAllAddresses();
        view.displayAllAddresses(allAddresses);
    }

    private void findAddressByLastName() throws AddressBookPersistenceException {
        view.displaySearchByLastNameBanner();
        String lastName = view.askUserForLastNameOfAddress();
        List<Address> matchingAddresses = dao.findAddressByLastName(lastName);
        view.displayAllAddresses(matchingAddresses);
    }

    private void getCurrentAddressCount() throws AddressBookPersistenceException {
        view.displayAddressCountBanner();
        long addressCount = dao.countAllAddresses();
        view.displayAddressCount(addressCount);
    }

    private void editAddress() throws AddressBookPersistenceException {
        view.displayEditAddressBanner();
        String lastName = view.askUserForLastNameOfAddress();
        List<Address> matchingAddresses = dao.findAddressByLastName(lastName);
        int addressId = view.askUserForAddressId(lastName, matchingAddresses);
        Address specificAddress = dao.getAddressById(addressId);
        int addressPartToEdit = view.getAddressPartToEdit(specificAddress);
        Address editedAddress = view.editAddressPart(specificAddress, addressPartToEdit);
        dao.editAddress(editedAddress, addressId);
        view.displayAddressEditedSuccessfullyBanner();
    }

    private void unknownCommandMessage() {
        view.displayUnknownCommand();
    }

    private void exitMessage() {
        view.displayExitMessage();
    }

}
