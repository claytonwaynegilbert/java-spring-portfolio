/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.addressbook.dao;

import com.sg.addressbook.dto.Address;
import java.util.List;

/**
 *
 * @author savannahg
 */
public interface AddressBookDao {
    
    Address addAddress(Address address) throws AddressBookPersistenceException;
    
    Address removeAddress(int id) throws AddressBookPersistenceException;
    
    List<Address> getAllAddresses() throws AddressBookPersistenceException;
    
    List<Address> findAddressByLastName(String lastName) throws AddressBookPersistenceException;
    
    Address getAddressById(int id) throws AddressBookPersistenceException;
    
    Long countAllAddresses() throws AddressBookPersistenceException;
    
    Address editAddress(Address newAddress, int oldAddressId) throws AddressBookPersistenceException;
}
