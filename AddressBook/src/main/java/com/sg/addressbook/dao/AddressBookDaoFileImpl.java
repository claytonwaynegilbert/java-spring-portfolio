/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.addressbook.dao;

import com.sg.addressbook.dto.Address;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 *
 * @author savannahg
 */
public class AddressBookDaoFileImpl implements AddressBookDao {
    
    private Map<Integer, Address> addressMap = new HashMap<>();
    public static final String ADDRESS_LIST = "addressList.txt";
    public static final String DELIMITER = ",";
    public static int ID = 1;

    @Override
    public Address addAddress(Address address) throws AddressBookPersistenceException {
        address.setAddressId(ID);
        ID++;
        Address addedAddress = addressMap.put(address.getAddressId(), address);
        write();
        return addedAddress;
    }

    @Override
    public Address removeAddress(int id) throws AddressBookPersistenceException {
        load();
        Address removedAddress = addressMap.remove(id);
        write();
        return removedAddress;
    }

    @Override
    public List<Address> getAllAddresses() throws AddressBookPersistenceException {
        load();
        return new ArrayList<>(addressMap.values());
    }

    @Override
    public List<Address> findAddressByLastName(String lastName) throws AddressBookPersistenceException {
        load();
        return addressMap.values().stream()
                                  .filter(a -> a.getLastName().equalsIgnoreCase(lastName))
                                  .collect(Collectors.toList());
    }
    
    @Override
    public Address getAddressById(int id) throws AddressBookPersistenceException{
        load();
        return addressMap.get(id);
    }

    @Override
    public Long countAllAddresses() throws AddressBookPersistenceException{
        load();
        return addressMap.values().stream()
                                  .count();
    }

    @Override
    public Address editAddress(Address newAddress, int oldAddressId) throws AddressBookPersistenceException {
        load();
        Address newEditedAddress = addressMap.put(oldAddressId, newAddress);
        write();
        return newEditedAddress;
    }
    
    private void load() throws AddressBookPersistenceException{
        Scanner sc = null;
        
        try {
            sc = new Scanner(
                    new BufferedReader(
                            new FileReader(ADDRESS_LIST)));
        } catch (FileNotFoundException ex) {
            throw new AddressBookPersistenceException(ex.toString());
        }
        
        String currentLine;
        
        String[] currentLineChunks;
        
        while(sc.hasNextLine()){
            currentLine = sc.nextLine();
            currentLineChunks = currentLine.split(DELIMITER);
            
            Address address = new Address();
            
            if(currentLineChunks == null || currentLineChunks[0].trim().equalsIgnoreCase("")){
                throw new AddressBookPersistenceException("There are no addresses in the system.");
            }
            
            int addressID = Integer.parseInt(currentLineChunks[0]);
            address.setAddressId(addressID);
            address.setFirstName(currentLineChunks[1]);
            address.setLastName(currentLineChunks[2]);
            address.setStreetAddress(currentLineChunks[3]);
            address.setCity(currentLineChunks[4]);
            address.setState(currentLineChunks[5]);
            address.setZipCode(currentLineChunks[6]);
            
            addressMap.put(address.getAddressId(), address);
        }
        sc.close();
    }
    
    private void write() throws AddressBookPersistenceException{
        PrintWriter out = null;
        
        try {
            out = new PrintWriter(
                                  new FileWriter(ADDRESS_LIST));
        } catch (IOException ex) {
            throw new AddressBookPersistenceException("Problem writing data to file", ex);
        }
        
        List<Address> allAddresses = this.getAllAddresses();
        for(Address currentAddress : allAddresses){
            out.println(currentAddress.getAddressId() + DELIMITER
                      + currentAddress.getFirstName() + DELIMITER
                      + currentAddress.getLastName() + DELIMITER
                      + currentAddress.getStreetAddress() + DELIMITER
                      + currentAddress.getCity() + DELIMITER
                      + currentAddress.getState() + DELIMITER
                      + currentAddress.getZipCode());
            out.flush();
        }
        out.close();
    }
}
