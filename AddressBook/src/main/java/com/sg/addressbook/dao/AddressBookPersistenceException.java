/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.addressbook.dao;

/**
 *
 * @author savannahg
 */
public class AddressBookPersistenceException extends Exception {

    /**
     * Creates a new instance of <code>AddressBookPersistenceException</code>
     * without detail message.
     */
    public AddressBookPersistenceException() {
    }

    /**
     * Constructs an instance of <code>AddressBookPersistenceException</code>
     * with the specified detail message.
     *
     * @param msg the detail message.
     */
    public AddressBookPersistenceException(String msg) {
        super(msg);
    }

    public AddressBookPersistenceException(String message, Throwable cause) {
        super(message, cause);
    }
    
    
    
    
}
