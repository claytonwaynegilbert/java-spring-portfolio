/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.addressbook.advice;

import com.sg.addressbook.dao.AddressBookAuditDao;
import com.sg.addressbook.dao.AddressBookPersistenceException;
import org.aspectj.lang.JoinPoint;

/**
 *
 * @author savannahg
 */
public class LoggingAdvice {
    
    AddressBookAuditDao audit;
    
    public LoggingAdvice(AddressBookAuditDao audit){
        this.audit = audit;
    }
    
    public void createAuditEntry(JoinPoint jp){
        Object[] args = jp.getArgs();
        String auditEntry = jp.getSignature().getName() + ": ";
        for(Object currentArg : args){
            auditEntry += currentArg;
        }
        try {
            audit.writeAuditEntry(auditEntry);
        } catch (AddressBookPersistenceException ex) {
            System.err.println("ERROR: Problem creating audit entry.");
        }
    }
    
}
