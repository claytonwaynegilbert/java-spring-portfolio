/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.addressbook.view;

import com.sg.addressbook.dto.Address;
import java.util.List;

/**
 *
 * @author savannahg
 */
public class AddressBookView {

    UserIO io;

    public AddressBookView(UserIO io) {
        this.io = io;
    }

    public int askUserForMenuChoice() {
        io.print("---Main Menu---");
        io.print("===============");
        io.print("1) List all Addresses");
        io.print("2) Add an Address");
        io.print("3) Remove an Address");
        io.print("4) Find Address By last name");
        io.print("5) Get current Address count");
        io.print("6) Edit an Address");
        io.print("7) Exit");

        return io.readInt("Enter the number of your choice: ", 1, 7);
    }

    public void displayAddAddressBanner() {
        io.print("---ADD ADDRESS---");
    }

    public Address askForAddressInfo() {
        String firstName = io.readString("Enter the first name: ");
        String lastName = io.readString("Enter the last name: ");
        String streetAddress = io.readString("Enter the street address only[No city, state, or zip]: ");
        String state = io.readString("Enter the state: ");
        String city = io.readString("Enter the city: ");
        String zipCode = io.readString("Enter the zip code: ");

        Address newAddress = new Address();
        newAddress.setFirstName(firstName);
        newAddress.setLastName(lastName);
        newAddress.setStreetAddress(streetAddress);
        newAddress.setState(state);
        newAddress.setCity(city);
        newAddress.setZipCode(zipCode);

        return newAddress;
    }

    public void displayAddressAddedSuccessfullyBanner() {
        io.print("---ADDRESS CREATED SUCCESSFULLY---");
    }

    public void displayRemoveAddressBanner() {
        io.print("---REMOVE ADDRESS---");
    }

    public String askUserForLastNameOfAddress() {
        String lastName = io.readString("What is the last name: ");
        return lastName;
    }

    public int askUserForAddressId(String lastName, List<Address> matchingAddresses) {
        if (matchingAddresses != null) {
            io.print("We found these addresses related to the last name " + lastName);
            for (Address currentAddress : matchingAddresses) {
                io.print("ID: " + currentAddress.getAddressId());
                io.print("Name: " + currentAddress.getFirstName() + " " + currentAddress.getLastName());
                io.print("Street Address: " + currentAddress.getStreetAddress());
                io.print("City: " + currentAddress.getCity());
                io.print("State: " + currentAddress.getState());
                io.print("Zip: " + currentAddress.getZipCode());
            }
            int addressId = io.readInt("What is the ID of the address: ");
            return addressId;
        }else{
            io.print("There are no addresses matching the last name " + lastName);
            io.readString("Type ENTER to continue: ");
        }
        return 0;
    }

    public void displayRemoveAddressSuccessfulBanner() {
        io.print("---ADDRESS REMOVED SUCCESSFULLY---");
    }

    public void displayAllAddressesBanner() {
        io.print("---ALL ADDRESSES---");
    }

    public void displayAllAddresses(List<Address> allAddresses) {
        if (allAddresses != null) {
            for (Address currentAddress : allAddresses) {
                io.print("ID: " + currentAddress.getAddressId());
                io.print("Name: " + currentAddress.getFirstName() + " " + currentAddress.getLastName());
                io.print("Street Address: " + currentAddress.getStreetAddress());
                io.print("City: " + currentAddress.getCity());
                io.print("State: " + currentAddress.getState());
                io.print("Zip: " + currentAddress.getZipCode());
            }
        } else {
            io.print("There are no addresses in system.");
        }
        io.readString("Type ENTER to continue: ");
        return;
    }

    public void displaySearchByLastNameBanner() {
        io.print("---SEARCH BY LAST NAME---");
    }

    public void displayAddressCountBanner() {
        io.print("--- ADDRESS COUNT---");
    }

    public void displayAddressCount(long addressCount) {
        io.print("Current address count = " + addressCount + " addresses");
    }

    public void displayEditAddressBanner() {
        io.print("---EDIT ADDRESS---");
    }

    public int getAddressPartToEdit(Address specificAddress) {
        io.print("1) First name: " + specificAddress.getFirstName());
        io.print("2) Last name: " + specificAddress.getLastName());
        io.print("3) Street Address: " + specificAddress.getStreetAddress());
        io.print("4) City: " + specificAddress.getCity());
        io.print("5) State: " + specificAddress.getState());
        io.print("6) Zip Code: " + specificAddress.getZipCode());
        io.print("7) Edit all");
        int partToEdit = io.readInt("Which part do you want to edit: ", 1, 7);
        return partToEdit;
    }

    public Address editAddressPart(Address oldAddress, int addressPartToEdit) {
        Address newAddress = oldAddress;
        switch (addressPartToEdit) {
            case 1:
                String newFirstName = io.readString("Enter new first name: ");
                newAddress.setFirstName(newFirstName);
                return newAddress;
            case 2:
                String lastName = io.readString("Enter new last name: ");
                newAddress.setLastName(lastName);
                return newAddress;
            case 3:
                String newStreet = io.readString("Enter new street address: ");
                newAddress.setStreetAddress(newStreet);
                return newAddress;
            case 4:
                String newCity = io.readString("Enter new city: ");
                newAddress.setCity(newCity);
                return newAddress;
            case 5:
                String newState = io.readString("Enter new state: ");
                newAddress.setState(newState);
                return newAddress;
            case 6:
                String newZip = io.readString("Enter new zip code: ");
                newAddress.setZipCode(newZip);
                return newAddress;
            case 7:
                String newFirst = io.readString("Enter new first name: ");
                String newLast = io.readString("Enter new last name: ");
                String newStreetAddress = io.readString("Enter new street address: ");
                String newCityName = io.readString("Enter new city: ");
                String newStateName = io.readString("Enter new state: ");
                String newZipCode = io.readString("Enter new zip code: ");

                newAddress.setFirstName(newFirst);
                newAddress.setLastName(newLast);
                newAddress.setStreetAddress(newStreetAddress);
                newAddress.setCity(newCityName);
                newAddress.setState(newStateName);
                newAddress.setZipCode(newZipCode);

                return newAddress;
            default:
                io.print("ENTER BETWEEN VALID RANGE");
                break;
        }
        return null;
    }

    public void displayAddressEditedSuccessfullyBanner() {
        io.print("---ADDRESS SUCCESSFULLY EDITED---");
    }

    public void displayUnknownCommand() {
        io.print("UNKNOWN COMMAND");
    }

    public void displayExitMessage() {
        io.print("Goodbye");
    }

    public void displayErrorMessage(String error) {
        io.print(error);
    }

}
