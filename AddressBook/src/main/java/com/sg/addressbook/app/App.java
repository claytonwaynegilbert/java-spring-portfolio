/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.addressbook.app;

import com.sg.addressbook.controller.AddressBookController;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author savannahg
 */
public class App {
    
    public static void main(String[] args) {
        
//        UserIO io = new UserIOConsoleImpl();
//        AddressBookView view = new AddressBookView(io);
//        AddressBookDao dao = new AddressBookDaoFileImpl();
//        AddressBookController controller = new AddressBookController(view, dao);
//        
//        controller.run();

          ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
          AddressBookController controller = ctx.getBean("addressBookController", AddressBookController.class);
          controller.run();
        
    }
    
}
