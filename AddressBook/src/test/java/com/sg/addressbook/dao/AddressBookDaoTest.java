/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.addressbook.dao;

import com.sg.addressbook.dto.Address;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author savannahg
 */
public class AddressBookDaoTest {

    AddressBookDao dao;

    public AddressBookDaoTest() {
        dao = new AddressBookDaoFileImpl();
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() throws AddressBookPersistenceException {
        List<Address> allAddresses = dao.getAllAddresses();
        for (Address currentAddress : allAddresses) {
            dao.removeAddress(currentAddress.getAddressId());
        }
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of addAddress method, of class AddressBookDao.
     */
    @Test
    public void testAddAddress() throws Exception {
        Address address = new Address();
        address.setFirstName("Clayton");
        address.setLastName("Gilbert");
        address.setStreetAddress("1787 State Route 772");
        address.setState("Ohio");
        address.setCity("Bainbridge");
        address.setZipCode("45612");

        dao.addAddress(address);
        
        Address onlyAddress = dao.getAddressById(1);
        assertNotNull(onlyAddress);
    }

    /**
     * Test of removeAddress method, of class AddressBookDao.
     */
    @Test
    public void testRemoveAddress() throws Exception {
        Address address = new Address();
        address.setFirstName("Clayton");
        address.setLastName("Gilbert");
        address.setStreetAddress("1787 State Route 772");
        address.setState("Ohio");
        address.setCity("Bainbridge");
        address.setZipCode("45612");

        dao.addAddress(address);
        
        Address onlyAddress = dao.getAddressById(1);
        assertNotNull(onlyAddress);
        
        dao.removeAddress(1);
        
        Address removedAddress = dao.getAddressById(1);
        assertNull(removedAddress);
    }

    /**
     * Test of getAllAddresses method, of class AddressBookDao.
     */
    @Test
    public void testGetAllAddresses() throws Exception {
        Address address = new Address();
        address.setFirstName("Clayton");
        address.setLastName("Gilbert");
        address.setStreetAddress("1787 State Route 772");
        address.setState("Ohio");
        address.setCity("Bainbridge");
        address.setZipCode("45612");

        dao.addAddress(address);
        
        assertEquals(1, dao.getAllAddresses().size());
    }

    /**
     * Test of findAddressByLastName method, of class AddressBookDao.
     */
    @Test
    public void testFindAddressByLastName() throws Exception {
        Address address = new Address();
        address.setFirstName("Clayton");
        address.setLastName("Gilbert");
        address.setStreetAddress("1787 State Route 772");
        address.setState("Ohio");
        address.setCity("Bainbridge");
        address.setZipCode("45612");

        dao.addAddress(address);
        
        List<Address> foundAddress = dao.findAddressByLastName("Gilbert");
        assertNotNull(foundAddress);
        
        List<Address> nullAddress = dao.findAddressByLastName("Jenkins");
        assertNull(nullAddress);
    }

    /**
     * Test of getAddressById method, of class AddressBookDao.
     */
    @Test
    public void testGetAddressById() throws Exception {
        
        Address nullAddress = dao.getAddressById(1);
        assertNull(nullAddress);
        
        Address address = new Address();
        address.setFirstName("Clayton");
        address.setLastName("Gilbert");
        address.setStreetAddress("1787 State Route 772");
        address.setState("Ohio");
        address.setCity("Bainbridge");
        address.setZipCode("45612");

        dao.addAddress(address);
        
        Address onlyAddress = dao.getAddressById(1);
        assertNotNull(onlyAddress);
    }

    /**
     * Test of countAllAddresses method, of class AddressBookDao.
     */
    @Test
    public void testCountAllAddresses() throws Exception {
        Address address = new Address();
        address.setFirstName("Clayton");
        address.setLastName("Gilbert");
        address.setStreetAddress("1787 State Route 772");
        address.setState("Ohio");
        address.setCity("Bainbridge");
        address.setZipCode("45612");

        dao.addAddress(address);
        
        Address address2 = new Address();
        address2.setFirstName("Destinee");
        address2.setLastName("Rhoades");
        address2.setStreetAddress("3323 Black Run Road");
        address2.setState("Ohio");
        address2.setCity("Chillicothe");
        address2.setZipCode("45601");

        dao.addAddress(address2);
        
        assertEquals(2, dao.countAllAddresses().longValue());
    }

    /**
     * Test of editAddress method, of class AddressBookDao.
     */
    @Test
    public void testEditAddress() throws Exception {
        Address address = new Address();
        address.setFirstName("Clayton");
        address.setLastName("Gilbert");
        address.setStreetAddress("1787 State Route 772");
        address.setState("Ohio");
        address.setCity("Bainbridge");
        address.setZipCode("45612");

        dao.addAddress(address);
        
        Address address2 = address;
        address2.setFirstName("Jacob");

        Address editedAddress = dao.editAddress(address2, 1);
        assertEquals(address, editedAddress);
        
        
    }
    
}
