While in process of reorganization of these projects, I went ahead and made a small list here of the projects in the mean time to save you some time as to what I feel would best suite you to look at as far as seeing current skill level is: 

*Keep in mind these projects were done a year+ ago so attempting to run them may cause issues, but the source code will usually be heavily
documented so that you can see my thought process and the tools used when building the applications.

1) SuperHeroSightingsSpringMVCApplication (Final project completed at school attending, full stack application with full Spring Security and database access)

2) Summative Assignment - Vending Machine (Simulation of a Vending Machine)

3) SpringMVC Configuration (Showing complete configuration of Spring project through XML)

4) Spring Security Heavily Documented (Heavily documented Spring Security setup through Spring MVC and XML)

5) Spring Boot Security Demo (Same as Spring MVC project above but configured through Spring Boot)

6) Spring Boot Example Project Detailed (Self explanatory)

7) DvdLibrarySpringMVCApplication (Spring MVC application, accessing data from database, Dvd Library CRUD application simulation)

8) Flooring Mastery or FlooringMastery(Same application, Mock simulation software to simulate a product management system for a company similar to a Menards or Lowes, CRUD features)

9) Database DAO Creation/Library Database DAO/ SQL...(Showing off creations of DAO objects and sql scripts that go along with the DAO's)

10) ContactListSpringMVCApplication (Contact List CRUD application)

