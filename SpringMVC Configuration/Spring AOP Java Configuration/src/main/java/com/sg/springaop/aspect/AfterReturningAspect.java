/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springaop.aspect;

import com.sg.springaop.dto.Account;
import java.util.List;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 *
 * @author cjsm12
 */

@Component("afterAspect")
@Aspect
@Order(1)
public class AfterReturningAspect {
    
    
    //@AfterReturning runs the aspect after the method successfully finishes and returns,
    //You specify the pointcut, which is the location of the method.
    //The returning keyword specifies the variable name you want to store the return 
    //data the method returns, this case, we are storing a list of accounts into a
    //variable named values
    @AfterReturning(
                    pointcut="execution(* com.sg.springaop.dao.AccountDaoImpl.findAccounts(..))",
                    returning="values"
                   )
    //We add the JoinPoint and a List, which is what we expect to be returned from 
    //the method call
    public void afterFindAccountsMethod(JoinPoint joinPoint, List<Account> values){
        
        //Display the method signature
        MethodSignature methodSig = (MethodSignature) joinPoint.getSignature();
        
        System.out.println("Method Signature " + methodSig);
        
        //Display the contents of the list that was returned from the method call
        for(Account temp : values){
            System.out.println(temp);
        }
        
        
        
    }
    
}
