/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springaop.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 *
 * @author cjsm12
 */
//Tell Spring this is a bean to be created in the container
@Component("beforeAspect")
//Tell Spring this is class should be treated as an aspect
@Aspect
@Order(3)
public class BeforeAspect {

    //Tells Spring when to run this aspect - public modifier is optional
    @Before("execution(public void addAccount())")
    public void runBeforeAddAccountMethod() {

        System.out.println("Executing @Before advice on addAccount method of DAO class");

    }

    //Pointcut expression language
    
    //          optional(public)   void, boolean, *     optional(package name)  addAccount(paramaters if needed)   optional(exception name)
    //execution(modifiers-pattern? return-type-pattern declaring-type-pattern? method-name-pattern(param-pattern) throws-pattern?)
    
    //* means matches everything
    //Ex: execution(public void add*()) will run the aspect on any method that starts 
    //with the word add in any class
    
    //Matching method paramaters - any ONE paramater *
    //
    //execution(* addAccount(*))
    
    //Matching method paramaters - any number of paramaters ..
    //
    //execution(* addAccount(..))
    
    //Running an aspect on any method within a package - declare package name 
    //along with asterisk for any class type and a second asterisk for any method name,
    //along with a paramater list consisting of two dots to specify any number of args.
    //
    //execution(* com.sg.springaop.dao.*.*(..))
    
    
    //--------------------------------------------------------------------------
    
    //Reusing a pointcut declaration on multiple advices
    
    //@Pointcut tells Spring whatever is in parenthesis is to be treated as a reusable 
    //pointcut location
    @Pointcut("execution(* com.sg.springaop.dao.*.*(..))")
    private void pointcutForDaoPackageMethods() {}
    
    //You specify the @Pointcut method name in the parenthesis of the @Before annotation
    //You can reference that pointcut method name for any advice now..completely reusable
    @Before("pointcutForDaoPackageMethods()")
    public void runBeforeAllMethodsInDaoPackage(){
        
        System.out.println("Will run before all methods in dao package...");
    }
    
    //--------------------------------------------------------------------------
    
    //Controlling the order of advices when you have multiple of them running at 
    
    //the same pointcut(location)
    
    //1) Keep advices in separate aspects
    
    //2) Use the @Order annotation on the class to determine order of aspects
    
    //3) Input the number of the order in the @Order annotation ex: @Order(1), @Order(2)
    //means you want the aspect Order(1) to run first, then @Order(2).
    
    //--------------------------------------------------------------------------
    
    //Getting method signature and paramaters from method you are running advice on
    
    //1) Add JoinPoint object to your advice method paramaters
    
    //2) JoinPoint stores all information about method you are running advice on
    //including paramater information
    @Before("pointcutForDaoPackageMethods()")
    public void runBeforeCertainMethod(JoinPoint joinPoint){
        
        //Cast joinPoint method getSignature() to of type MethodSignature
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        
        //Display method signature
        System.out.println(signature);
        
        //Getting paramater information from method
        Object[] args = joinPoint.getArgs();
        
        //Displaying each argument
        for(Object tempArg : args){
            System.out.println(tempArg);
        }
        
        
    }

    
    
    
}
