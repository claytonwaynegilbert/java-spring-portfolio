/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springaop.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

/**
 *
 * @author cjsm12
 */
@Component("aroundAdvice")
@Aspect
public class AroundAspect {

    //ProceedingJoinPoint is an object available to you when using the @Around aspect,
    //It lets you have control on when to execute the method the pointcut is pointing towards
    //by using the proceed method. In this case we are seeing how long it takes to execute
    //that method.
    @Around("execution(* com.sg.springaop.dao.*.returnFortune(..)")
    public Object adviceToBeRanBeforeAndAfterTargetMethod(ProceedingJoinPoint joinPoint) throws Throwable {

        long startTime = System.currentTimeMillis();

        Object method = joinPoint.proceed();

        long endTime = System.currentTimeMillis();
        long duration = endTime - startTime;

        System.out.println("Time to execute method " + duration);

        //We want to return method because that is what the target method is returning,
        //in this case, it is a fortune String and we don't want it to stop here, we want
        //to return it to the calling code. If we didn't return it, the result of that
        //method would have gotten swollowed up by this aspect, which is usually something
        //we don't want to do unless it is an exception.  

        //return method;
        

        //----------------------------------------------------------------------
        

        //Handling and rethrowing exceptions
        //If the method of the pointcut throws an exception, we can either handle 
        //the exception or rethrow it back to the calling code. 
        

        //Handling exception
        //Variable below is what the result of the method pointcut will be stored in
        Object methodCausingException = null;

        //Start timer
        long start = System.currentTimeMillis();

        try {
            //Attempt to execute the method in pointcut
            methodCausingException = joinPoint.proceed();

            //Catch exception
        } catch (Exception e) {
            //Assign returning object the custom error message
            methodCausingException = "Sorry, you've encountered an error: " + e.getMessage();
        }

        //If no exceptions, do normal execution
        long end = System.currentTimeMillis();
        long time = end - start;

        System.out.println("Time to execute method " + time);

        //return methodCausingException;

        

        //Rethrowing Exception
        //Variable below is what the result of the method pointcut will be stored in
        Object methodCausingExc = null;

        //Start timer
        long started = System.currentTimeMillis();

        try {
            //Attempt to execute the method in pointcut
            methodCausingExc = joinPoint.proceed();

            //Catch exception
        } catch (Exception e) {
            //Warn calling code there was a problem
            System.out.println("Sorry, you've encountered an error: " + e.getMessage());
            
            //Throw exception again
            throw e;
            
            //OR encapsulate exception into custom exception
            
            //throw new CustomCreatedException(e);
        }

        //If no exceptions, do normal execution
        long ended = System.currentTimeMillis();
        long timed = ended - started;

        System.out.println("Time to execute method " + time);

        return methodCausingExc;

    }

}
