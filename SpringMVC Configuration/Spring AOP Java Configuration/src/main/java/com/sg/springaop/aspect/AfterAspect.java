/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springaop.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

/**
 *
 * @author cjsm12
 */
@Component("afterAspect")
@Aspect
public class AfterAspect {
    
    //After is the exact same as the @Before aspect, except the logic gets ran
    //after the method returns instead of before
    
}
