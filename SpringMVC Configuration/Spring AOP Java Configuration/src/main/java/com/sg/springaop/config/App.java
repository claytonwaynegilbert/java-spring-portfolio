/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springaop.config;

import com.sg.springaop.dao.AccountDao;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 *
 * @author cjsm12
 */
public class App {

    public static void main(String[] args) {

        //Create the bean factory not using xml, but using pure java configuration
        AnnotationConfigApplicationContext context
                = new AnnotationConfigApplicationContext(DemoConfig.class);

        //Grab the account object from the container
        AccountDao dao = context.getBean("accountDao", AccountDao.class);

        //Run the addAccount method from the object and which the aspect is applied to.
        dao.addAccount();

        context.close();

    }
}
