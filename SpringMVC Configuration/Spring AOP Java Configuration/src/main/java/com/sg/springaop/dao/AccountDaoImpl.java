/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springaop.dao;

import com.sg.springaop.dto.Account;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

/**
 *
 * @author cjsm12
 */
@Component("accountDao")
public class AccountDaoImpl implements AccountDao {

    @Override
    public void addAccount() {
        System.out.println("Adding an account...");
    }

    @Override
    public List<Account> findAccounts(){
        List<Account> allAccounts = new ArrayList<>();

        if(allAccounts == null){
            throw new RuntimeException("We've encountered an exception");
        }
        
        allAccounts.add(new Account());
        allAccounts.add(new Account());
        allAccounts.add(new Account());

        return allAccounts;
    }

}
