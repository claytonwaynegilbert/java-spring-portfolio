/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springaop.dao;

import org.springframework.stereotype.Component;

/**
 *
 * @author cjsm12
 */

@Component("happyFortune")
public class HappyFortuneService implements FortuneService {

    @Override
    public String returnFortune() {
        return "A good fortune is upon you!";
    }
    
}
