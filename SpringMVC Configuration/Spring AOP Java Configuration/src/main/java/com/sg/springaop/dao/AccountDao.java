/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springaop.dao;

import com.sg.springaop.dto.Account;
import java.util.List;

/**
 *
 * @author cjsm12
 */
public interface AccountDao {
    
    void addAccount();
    
    List<Account> findAccounts();
    
}
