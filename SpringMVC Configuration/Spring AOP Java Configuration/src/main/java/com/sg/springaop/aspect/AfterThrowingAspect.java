/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springaop.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 *
 * @author cjsm12
 */
@Component
@Aspect
@Order(2)
public class AfterThrowingAspect {

    //@AfterThrowing runs the aspect after the method finishes and throws an exception,
    //You specify the pointcut, which is the location of the method.
    //The throwing keyword specifies the exception name you want to store the exception 
    //the method returns.
    @AfterThrowing(
            pointcut = "execution(* com.sg.springaop.dao.AccountDaoImpl.findAccounts(..)",
            throwing = "exception"
    )
    //We add the JoinPoint and the generic Throwable object, which is what we expect to 
    //be returned from the method call
    public void afterThrowingExceptionHandler(JoinPoint joinPoint, Throwable exception) {

        //Print out the exception
        System.out.println("The exception that we ran into was " + exception);
    }

}
