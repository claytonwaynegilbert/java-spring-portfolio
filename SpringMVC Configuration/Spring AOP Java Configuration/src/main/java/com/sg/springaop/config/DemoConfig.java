/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springaop.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 *
 * @author cjsm12
 */

//Tell Spring you want to use this class for configuration instead of xml
@Configuration
//Enable AspectJ functionality for Spring AOP
@EnableAspectJAutoProxy
//Tell Spring where to begin looking for components(beans) in the application
@ComponentScan("com.sg.springaop.config")
public class DemoConfig {
    
 
    
}
