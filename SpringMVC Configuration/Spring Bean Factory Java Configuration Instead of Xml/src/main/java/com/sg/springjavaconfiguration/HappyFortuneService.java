/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springjavaconfiguration;

import org.springframework.stereotype.Component;

/**
 *
 * @author cjsm12
 */
@Component("happyFortune")
public class HappyFortuneService implements FortuneTeller {

    @Override
    public String getFortune() {
        return "Your life will fall into place in more ways than one";
    }
    
}
