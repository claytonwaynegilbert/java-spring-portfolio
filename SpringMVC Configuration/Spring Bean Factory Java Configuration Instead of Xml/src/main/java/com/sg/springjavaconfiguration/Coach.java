/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springjavaconfiguration;

/**
 *
 * @author cjsm12
 */
public interface Coach {
    
    String getWorkout();
    
    String getFortune();
    
}
