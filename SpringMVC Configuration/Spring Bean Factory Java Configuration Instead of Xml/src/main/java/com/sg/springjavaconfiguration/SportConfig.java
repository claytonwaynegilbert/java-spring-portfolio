/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springjavaconfiguration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author cjsm12
 */

//This tells Spring we are using this Java class as our configuration file instead of
//using our applicationContext.xml. THis file will act as the container
@Configuration
//Just like in our applicationContext.xml file, this tells Spring what package to start
//looking in for annotation driven components. Component scan isn't needed though if you
//plan on doing all configuration solely using java configuration.
@ComponentScan("com.sg.springjavaconfiguration")
public class SportConfig {
    
    //Creates a specific implementation of FortuneTeller named SadFortuneService
    @Bean
    public FortuneTeller sadFortuneService(){
        return new SadFortuneService();
    }
    
    //Creating a Coach bean with it's dependencies injected into the method.
    //Same as creating a bean in the xml file
    //In the constructor of the specific implementation of the Coach interface goes the 
    //name of the method above, which is the service we want to inject into our coach
    //object before using
    @Bean
    public Coach exampleCoach(){
        return new JavaConfigExampleCoach(sadFortuneService());
    }
    
    
}
