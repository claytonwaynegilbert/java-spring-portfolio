/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springjavaconfiguration;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author cjsm12
 */
@Component("badFortune")
@Scope("singleton") //This is default. Not really needed
public class BadFortuneService implements FortuneTeller{

    @Override
    public String getFortune() {
        return "An evil presence is lurking behind your back";
    }
    
}
