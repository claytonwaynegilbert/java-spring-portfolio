/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springjavaconfiguration;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 *
 * @author cjsm12
 */
@Component("basketballCoach")
public class BasketballCoach implements Coach {

    @Autowired
    @Qualifier("badFortune")
    private FortuneTeller teller;

    @Override
    public String getWorkout() {
        return "Run 50 suicides and make 200 jumpers before going home";
    }

    @Override
    public String getFortune() {
        return teller.getFortune();
    }

    @PostConstruct
    public void doWorkAfterCreation() {
        System.out.println("Doing work after creation...");
    }

    @PreDestroy
    public void doWorkRightBeforeBeingDestroyed() {
        System.out.println("Doing work right before being destoryed...");
    }

}
