/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springjavaconfiguration;

/**
 *
 * @author cjsm12
 */
//This class is used with the Java configuration file, so it is not annotated as it will
//be created in the configuration file class SportConfig
public class SadFortuneService implements FortuneTeller {

    @Override
    public String getFortune() {
        return "Heartbreak and sorrow may be upon you sooner than you think";
    }
    
}
