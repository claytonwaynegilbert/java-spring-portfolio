/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springjavaconfiguration;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 *
 * @author cjsm12
 */
@Component("tennisCoach")
public class TennisCoach implements Coach {
    
    @Autowired
    @Qualifier("badFortune")
    private FortuneTeller teller;

    @Override
    public String getWorkout() {
        return "Hit 300 balls with your backhand before going home today";
    }

    @Override
    public String getFortune() {
        return teller.getFortune();
    }
    
    @PostConstruct
    public void doWorkAfterCreation(){
        System.out.println("Doing work after creation...");
    }
    
    @PreDestroy
    public void doWorkRightBeforeBeingDestroyed(){
        System.out.println("Doing work right before being destoryed...");
    }
    
}
