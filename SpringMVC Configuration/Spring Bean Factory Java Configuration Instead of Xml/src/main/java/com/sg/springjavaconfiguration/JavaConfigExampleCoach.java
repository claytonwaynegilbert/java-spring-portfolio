/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springjavaconfiguration;

/**
 *
 * @author cjsm12
 */
//This class will be our exmaple used in the Java Configuration class
//Notice how no annotations are used what so ever. Just a plain java class
public class JavaConfigExampleCoach implements Coach {

    private FortuneTeller teller;
    
    //This constructor will be used in our java configuration class to inject 
    //a FortuneTeller implementation into this Coach object
    public JavaConfigExampleCoach(FortuneTeller teller){
        this.teller = teller;
    }
    
    @Override
    public String getWorkout() {
        return "Example workout";
    }

    @Override
    public String getFortune() {
        return this.getFortune();
    }
    
}
