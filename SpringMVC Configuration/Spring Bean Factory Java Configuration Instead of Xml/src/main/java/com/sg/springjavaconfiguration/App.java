/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springjavaconfiguration;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 *
 * @author cjsm12
 */
public class App {
    
    public static void main(String[] args) {
        
        //Testing the Java Configured Beans 
        ApplicationContext context = 
                            new AnnotationConfigApplicationContext(SportConfig.class);
        
        //The name of the coach entered is the name of the method name in the java
        //configuration file class named SportConfig
        Coach myCoach = context.getBean("exampleCoach", Coach.class);
        
        
        
    }
    
}
