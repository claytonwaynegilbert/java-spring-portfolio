/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.annotationsdemo;

import org.springframework.stereotype.Component;

/**
 *
 * @author cjsm12
 */
@Component("happyFortune")
public class HappyFortuneService implements FortuneService{

    @Override
    public String getFortune() {
        return "Your luck will be at an all time high today!";
    }
    
}
