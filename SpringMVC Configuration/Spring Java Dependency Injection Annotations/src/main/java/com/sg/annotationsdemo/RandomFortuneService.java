/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.annotationsdemo;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.springframework.stereotype.Component;

/**
 *
 * @author cjsm12
 */
@Component("randomFortune")
public class RandomFortuneService implements FortuneService {
    
    private Random rn = new Random();
    private List<String> randomFortune = new ArrayList<>();
    
    public RandomFortuneService(){
      this.randomFortune.add("Good fortune");
      this.randomFortune.add("Bad Fortune");
      this.randomFortune.add("Meh Fortune");
    }
    
    @Override
    public String getFortune() {
        return randomFortune.get(rn.nextInt(randomFortune.size()));
    }
    
}
