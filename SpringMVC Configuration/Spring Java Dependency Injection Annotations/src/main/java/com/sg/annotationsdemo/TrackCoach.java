/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.annotationsdemo;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author cjsm12
 */
//Annotation to create bean instead of doing it in xml file. Spring will scan all classes
//and look for these annotations to create beans with the id you specify here
@Component("trackCoach")
//The default scope is Singleton, meaning when Spring creates your bean(Object) it only
//creates one and that one bean is shared by everyone who requests to use that bean. 
//Prototype scope means when Spring creates a bean, it creates a new instance each and
//every time that bean is requested
@Scope("prototype")
public class TrackCoach implements Coach {

    //Instead of using constructor or setter injection, we can use the autowired 
    //annotation directly on the field itself. Spring does the same again as the others,
    //it looks at all classes with the @Component annotation(beans) and sees if any of
    //them implement the FortuneService interface, when it finds the class(es) that do
    //implement the interface, it takes the implemented class and assigns is to this 
    //field, so the field below at runtime will look like: 
    //FortuneService fortune = new RandomFortuneService();
    @Autowired
    @Qualifier("randomFortune")
    private FortuneService fortune;

    @Override
    public String getDailyWorkout() {
        return "Run 15 100m Sprints";
    }

    @Override
    public String getDailyFortune() {
        return fortune.getFortune();
    }
    
    //This annotation tells Spring to run this method as soon as the object is created.
    //So after the constructor of this object is ran, this method will run.
    @PostConstruct
    public void doSomeWorkUponCreation(){
        System.out.println("Work being done right after creation...");
    }
    
    //This annotation tells Spring to run this method right before this object is 
    //destroyed.
    @PreDestroy
    public void doSomeWorkBeforeBeingDestroyed(){
        System.out.println("Work being done before destroyed...");
    }

}
