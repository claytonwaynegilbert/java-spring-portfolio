/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.annotationsdemo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author cjsm12
 */
public class App {
    
    public static void main(String[] args) {
        
        //Read the application context file 
        ApplicationContext ctx = 
                    new ClassPathXmlApplicationContext("spring-persistence.xml");
        
        //Retrieving a bean that was created with Java annotations
        Coach theCoach = ctx.getBean("swimCoach", Coach.class);
        
        
        System.out.println("The coach said to: " + theCoach.getDailyWorkout());
        
        System.out.println("The coach has given a daily fotune to you: " 
                                                    + theCoach.getDailyFortune());
        
        
    }
    
}
