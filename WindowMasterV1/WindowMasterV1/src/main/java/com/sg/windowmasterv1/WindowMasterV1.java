/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.windowmasterv1;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Scanner;

/**
 *
 * @author cjsm12
 */
public class WindowMasterV1 {

    public static void main(String[] args) {

        /*
        WINDOW MASTER V1:
        It must prompt the user for the height of the window (in feet).
        It must prompt the user for the width of the window (in feet).
        It must calculate and display the area of the window.
        It must calculate and display the perimeter of the window.
        Based on the area and perimeter, it must calculate the total cost of the window.
        The glass for the windows cost $3.50 per square foot.
        The trim for the windows cost $2.25 per linear foot.
         */
        //Declare type Scanner for getting user input from console.
        Scanner sc = new Scanner(System.in);

        //Showing welcome screen for program to user.
        System.out.println("Welcome to WINDOW MASTER V1");
        System.out.println("----------------------------");

        //Declaring BigDecimal variables for cost of glass and trim to be used later in program.
        BigDecimal windowGlassCostPerSquareFootBD = new BigDecimal("3.50");
        BigDecimal windowTrimCostPerSquareFootBD = new BigDecimal("2.25");

        //Ask user for his/her's window height
        System.out.print("Enter height of window(feet): ");
        //Using Scanner to grab what the user types by calling nextLine() function
        //and storing that String in a variable.
        String userWindowHeight = sc.nextLine();
        //Parsing user input from String to of type double by using a static method
        //on Double class called parseDouble() and store in another variable of type double.
        BigDecimal userWindowHeightBD = new BigDecimal(userWindowHeight);

        //Same procedure above for width of window instead of height.
        System.out.print("Enter width of window(feet): ");
        String userWindowWidth = sc.nextLine();
        BigDecimal userWindowWidthBD = new BigDecimal(userWindowWidth);

        //Calculate are of window based on user's input and store in variable.
        BigDecimal userWindowAreaBD = userWindowHeightBD.multiply(userWindowWidthBD).setScale(2, RoundingMode.HALF_UP);

        //Calculating perimeter of user window.
        BigDecimal userWindowHeightTotalBD = (userWindowHeightBD.multiply(new BigDecimal("2")).setScale(2, RoundingMode.HALF_UP));
        BigDecimal userWindowWidthTotalBD = (userWindowWidthBD.multiply(new BigDecimal("2")).setScale(2, RoundingMode.HALF_UP));
        BigDecimal userWindowPerimeterBD = userWindowHeightTotalBD.add(userWindowWidthTotalBD);

        //Displaying calculated area and perimeter to user.
        System.out.println("\nArea of your window = " + userWindowAreaBD + " feet.");
        System.out.println("Perimiter of your window = " + userWindowPerimeterBD + " feet. \n");

        //Informing user of how to continue running program.
        System.out.print("Please press Enter to calculate total cost of glass and trim for your window...[Enter] ");
        sc.nextLine();

        //Calculate both glass and trim cost for user window and store in variables.
        BigDecimal totalGlassCostBD = windowGlassCostPerSquareFootBD.multiply(userWindowAreaBD).setScale(2, RoundingMode.HALF_UP);
        BigDecimal totalTrimCostBD = windowTrimCostPerSquareFootBD.multiply(userWindowPerimeterBD).setScale(2, RoundingMode.HALF_UP);
        //Calculate total cost of both glass and trim for user's window.
        BigDecimal totalCostBD = totalGlassCostBD.add(totalTrimCostBD);

        //Displaying total cost to user.
        System.out.println("\nTotal cost for glass and trim for your window = $" + totalCostBD);

    }

}
