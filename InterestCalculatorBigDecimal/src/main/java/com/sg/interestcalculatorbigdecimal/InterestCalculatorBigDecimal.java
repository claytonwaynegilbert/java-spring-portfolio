/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.interestcalculatorbigdecimal;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Scanner;

/**
 *
 * @author savannahg
 */
public class InterestCalculatorBigDecimal {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        //Welcome user
        System.out.println("Welcome to Interest Calculator!!!");
        System.out.println("---------------------------------");

        //Ask user for interest rate
        System.out.print("What is the annual interest rate: ");
        BigDecimal annualInterestRateBD = new BigDecimal(sc.next());

        //Ask user for principal
        System.out.print("What is the initial principal:  ");
        BigDecimal initialPrincipalBD = new BigDecimal(sc.next());

        //Ask user how long they want money to stay in fund
        System.out.print("How many years do you want principal to stay in fund: ");
        BigDecimal yearsToStayInFundBD = new BigDecimal(sc.next());

        //Declare other variables for calculations below
        BigDecimal currentBalanceBD = initialPrincipalBD;
        BigDecimal quarterlyInterestBD = annualInterestRateBD.divide(new BigDecimal("4"), 2, RoundingMode.HALF_UP);

        System.out.println("Okay here is the breakdown: ");

        //For loop to calculate quarterly interest rate, end of year balance, and interest
        //earned each year.
        for (int i = 1; i <= yearsToStayInFundBD.intValue(); i++) {
            //Variable used for displaying principal at beginning of each year
            BigDecimal principalBD = currentBalanceBD;
            //Nested for loop used for calculating new balance each quarter and assigning
            //new value to itself which after 4 revolutions, is the new balance at the
            //end of the year
            for (int j = 1; j <= 4; j++) {
                BigDecimal equation = quarterlyInterestBD.divide(new BigDecimal("100"), 2, RoundingMode.HALF_UP).add(new BigDecimal("1"));
                currentBalanceBD = currentBalanceBD.multiply(equation);
            }
            //Variable used for keeping track of year
            BigDecimal yearBD = new BigDecimal(i);
            //Calculating interest earned for the year
            BigDecimal interestEarnedBD = currentBalanceBD.subtract(principalBD);
            //Displaying all details to user for each year
            System.out.println("Year " + yearBD.toString() + "\n"
                    + "Principal at beginning of year " + principalBD.setScale(2, RoundingMode.HALF_UP).toString() + "\n"
                    + "Interest earned for the year " + interestEarnedBD.setScale(2, RoundingMode.HALF_UP).toString() + "\n"
                    + "Principal at end of year " + currentBalanceBD.setScale(2, RoundingMode.HALF_UP));
        }
    }

}
