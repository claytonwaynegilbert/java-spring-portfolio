/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.javaifstatements;

import java.util.Scanner;

/**
 *
 * @author cjsm12
 */
public class IfStatements {

    public static void main(String[] args) {

        /*
        Write a program that that has a picked integer stored in a variable, and asks the user to guess it.
        Ask the user to pick a number.
        If their choice is equal to the number print out: "Wow, nice guess! That was it!"
        If their choice is less to the number print out: "Ha, nice try - too low! I chose #"
        If their choice is greater to the number print out: "Too bad, way too high. I chose #"        
         */
        
        //Declare type Scanner for user input.
        Scanner sc = new Scanner(System.in);
        //Declare number user has to guess.
        int numberToPick = 13;

        //Print welcome screen
        System.out.println("Welcome to GuessMe!");
        System.out.println("-------------------\n");

        //Ask user to play.
        System.out.print("I've got a number I bet you can't guess! Wanna try? [Y or N]: ");
        String choiceToPlay = sc.nextLine();
        
        //Check to see if user wants to play
        if (choiceToPlay.equalsIgnoreCase("Y")) {
            //Ask user to guess number
            System.out.print("\nTry to guess the number...hint(It's between 1 and 100): ");
            String userGuess = sc.nextLine();
            //Parse user's input from a String to an int for easy comparison
            int userGuessNumber = Integer.parseInt(userGuess);
            //Check to see if user's number is too high, low, or just right and print an
            //appropriate statement for each condition.
            if (userGuessNumber == numberToPick) {
                System.out.print("WOW, nice guess! That was it!\n");
            } else if (userGuessNumber < numberToPick) {
                System.out.print("Ha, nice try - too low! I chose " + numberToPick + "\n");
            } else {
                System.out.print("Too bad, way too high. I chose " + numberToPick + "\n");
            }
        //Print statement for if user doesn't want to play.
        } else {
            System.out.print("Goodbye!\n");
        }

        
        /*
        YourLifeInMovies.java
        Write a program that displays a different message based on the year someone was born to make them feel old.
        If they were born before 2005, print out that Pixar's 'Up' came out half a decade ago.
        If they were born before 1995, print out that the first Harry Potter came out over 15 years ago.
        If they were born before 1985, print out Space Jam came out not last decade, but the one before THAT.
        If they were born before 1975, print out that the original Jurassic Park release is closer to the lunar landing, than today.
        If they were born before 1965, print out that the MASH has been around for almost half a century!
        Note: A person who is born in 1980 should display three messages, one for being born before 2005, one for 1995, and finally for 1985.
        */
        
        //Not commenting this code as the concepts are almost identical to one above.
        System.out.println("Welcome to your life in movies!");
        System.out.println("-------------------------------\n");
        
        System.out.print("What year were you born: ");
        String userBirthDate = sc.nextLine();
        int userBirthDateNumber = Integer.parseInt(userBirthDate);
        
        if(userBirthDateNumber < 2005){
            System.out.print("FACT 1: Pixar's 'Up' came out half a decade ago!\n");
        }else if(userBirthDateNumber < 1995){
            System.out.print("FACT 1: Pixar's 'Up' came out half a decade ago!\n");
            System.out.print("FACT 2: The first Harry Potter came out over 15 years ago!\n");
        }else if(userBirthDateNumber < 1985){
            System.out.print("FACT 1: Pixar's 'Up' came out half a decade ago!\n");
            System.out.print("FACT 2: The first Harry Potter came out over 15 years ago!\n");
            System.out.print("FACT 3: Space Jam came out not last decade, but the one before THAT!\n");
        }else if(userBirthDateNumber < 1975){
            System.out.print("FACT 1: Pixar's 'Up' came out half a decade ago!\n");
            System.out.print("FACT 2: The first Harry Potter came out over 15 years ago!\n");
            System.out.print("FACT 3: Space Jam came out not last decade, but the one before THAT!\n");
            System.out.print("FACT 4: The original Jurassic Park release is closer to the lunar landing, than today!\n");
        }else{
            System.out.print("FACT 1: Pixar's 'Up' came out half a decade ago!\n");
            System.out.print("FACT 2: The first Harry Potter came out over 15 years ago!\n");
            System.out.print("FACT 3: Space Jam came out not last decade, but the one before THAT!\n");
            System.out.print("FACT 4: The original Jurassic Park release is closer to the lunar landing, than today!\n");
            System.out.print("FACT 5: MASH has been around for almost half a century!\n");
        }
        
        
        /*
           BirthStones.java
           Write a program that asks for a number, match that number against the month number and printout the name of the month and it’s corresponding birth stone.
           Use Ifs and Elses to complete it.
        */

        //Also the same as the two above. No need to comment
        System.out.println("Welcome to BirthStones!");
        System.out.println("-----------------------\n");
        
        System.out.println("Enter the month you were born and I will tell you your birthstone!(1-12): ");
        String userBirthMonth = sc.nextLine();
        int userBirthMonthNumber = Integer.parseInt(userBirthMonth);
        
        if(userBirthMonthNumber == 1){
            System.out.println("January birth stone is Garnet");
        }else if(userBirthMonthNumber == 2){
            System.out.println("February birth stone is Amethyst");
        }else if(userBirthMonthNumber == 3){
            System.out.println("March birth stone is Aquamarine");
        }else if(userBirthMonthNumber == 4){
            System.out.println("April birth stone is Diamond");
        }else if(userBirthMonthNumber == 5){
            System.out.println("May birth stone is Emerald");
        }else if(userBirthMonthNumber == 6){
            System.out.println("June birth stone is Pearl");
        }else if(userBirthMonthNumber == 7){
            System.out.println("July birth stone is Ruby");
        }else if(userBirthMonthNumber == 8){
            System.out.println("August birth stone is Peridot");
        }else if(userBirthMonthNumber == 9){
            System.out.println("September birth stone is Sapphire");
        }else if(userBirthMonthNumber == 10){
            System.out.println("October birth stone is Opal");
        }else if(userBirthMonthNumber == 11){
            System.out.println("November birth stone is Topaz");
        }else if(userBirthMonthNumber == 12){
            System.out.println("December birth stone is Turquoise");
        }else{
            System.out.println("That number is not a month. Please choose between 1-12");
        }
        
        /*
        Filename: TriviaNight.java
        Write a program that prints out a series of multiple choice questions (minimum 3!) and asks the user for the answer to each. Your program should tally the number of correct answers, and print out the results at the end!

        Bonus: Have another print out if they get all or none of the questions correct.
        */
        
        //Display welcome text.
        System.out.println("Welcome to TriviaNight!");
        System.out.println("------------------------\n");
        
        //Declare variable for keeping track of correct guesses
        int numberGuessedCorrect = 0;
        
        //Listing out question with choices.
        System.out.println("1) What is the color of the sun?");
        System.out.print  ("A) Blue               B) Green\n"
                         + "C) Yellow             D) Black\n"
                         + "\n"
                         + "Your pick: ");
        //Store users answer in variable
        String questionOneAnswer = sc.nextLine();
        //Conditional statement to check for correct answer
        if(questionOneAnswer.equalsIgnoreCase("C")){
            //If answer correct, increment variable stored above by one.
            numberGuessedCorrect++;
        }
        
        //Same principles as question 1
        System.out.println("2) Which movie actor played the lead role in Die Hard?");
        System.out.print  ("A) Mel Gibson         B) Robert DeNiro\n"
                         + "C) Bruce Willis       D) Jason Stathem\n"
                         + "\n"
                         + "Your pick: ");
        String questionTwoAnswer = sc.nextLine();
        if(questionTwoAnswer.equalsIgnoreCase("C")){
            numberGuessedCorrect++;
        }
        
        //Same principles as question 1 & 2
        System.out.println("3) How many days are in a year?");
        System.out.print  ("A) 365                B) 280\n"
                         + "C) 185                D) 423\n"
                         + "\n"
                         + "Your pick: ");
        String questionThreeAnswer = sc.nextLine();
        if(questionThreeAnswer.equalsIgnoreCase("A")){
            numberGuessedCorrect++;
        }
        
        //Checking if total number of correct guesses is either all right, none right, or some right
        if(numberGuessedCorrect == 3){
            System.out.println("WOW You got all 3 correct! Good job.");
        }else if(numberGuessedCorrect == 0){
            System.out.println("Sorry you got 0 correct...better luck next time.");
        }else{
           System.out.println("You correctly answered " + numberGuessedCorrect + " correct!");
        }
        
        /*
        Filename: FieldDay.java
        Your company has organized a morale event! They're hosting picnic and field day 
        in the park, and of course they want to play games! Team games! Team building games!
        To do that they want to assign all the people who show up to certain teams based 
        on their last name - they've figured out the distribution break down - all they 
        need YOU to do is to write the program that can sort them! 
        (ie take a last name as input, output the team name!).
        
        Here’s the specs:

        If a person's name falls before Baggins then they are on the team "Red Dragons"
        If it falls after Baggins, but before Dresden, they are on the team "Dark Wizards"
        If it falls after Dresden, but before Howl, they are on the team "Moving Castles"
        If it falls after Howl, but before Potter, they are on the team "Golden Snitches"
        If it falls after Potter, but before Vimes, they are on the team "Night Guards"
        If it falls after Vimes, they are on the team “Black Holes”
        */
        
        System.out.println("Welcome to FieldDay!");
        System.out.println("--------------------\n");
        
        //Declaring variables for conditional testing
        String Baggins = "Baggins";
        String Dresden = "Dresden";
        String Howl = "Howl";
        String Potter = "Potter";
        String Vimes = "Vimes";
        
        //Get users last name and store in variable
        System.out.println("Welcome to the event! Please give me your last name so that I can decide which team you are on: ");
        String userName = sc.nextLine();
        
        //Using the compareTo() function to compare users last name with each
        //declared variable above to determine if users name is either before
        //or after names declared above and printing appropriate response as needed.
        if(userName.compareToIgnoreCase(Baggins) < 0){
            System.out.println("Go slay them as you are on team Red Dragons!");
        }else if((userName.compareToIgnoreCase(Baggins) > 0) && (userName.compareToIgnoreCase(Dresden) < 0)){
            System.out.println("Cast your spells mightily as you join the team of the Dark Wizards!");
        }else if((userName.compareToIgnoreCase(Dresden) > 0) && (userName.compareToIgnoreCase(Howl) < 0)){
            System.out.println("Watch your step as you join the team Moving Castles!");
        }else if((userName.compareToIgnoreCase(Howl) > 0) && (userName.compareToIgnoreCase(Potter) < 0)){
            System.out.println("Watch your back at all times as you are on the team Golden Snitches!");
        }else if((userName.compareToIgnoreCase(Potter) > 0) && userName.compareToIgnoreCase(Vimes) < 0){
            System.out.println("Time to be on duty as you join the team Night Guards!");
        }else if(userName.compareToIgnoreCase(Vimes) > 0){
            System.out.println("What rhymes with dimes you may ask? Well look no further as you are now on the team Vimes!");
        }else{
            System.out.println("Please enter a valid name");
        }
        
    }
}
