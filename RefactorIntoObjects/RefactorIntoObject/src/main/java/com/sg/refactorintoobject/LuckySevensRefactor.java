/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.refactorintoobject;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author cjsm12
 */
public class LuckySevensRefactor {

    public void playLuckySevens() {
        //Welcome user
        System.out.println("Welcome to Lucky Sevens!");
        System.out.println("------------------------");

        //Declare type Scanner and Random for user input and generating dice rolls
        Scanner userInput = new Scanner(System.in);
        Random diceRoll = new Random();

        //Declare variables
        int dice1;
        int dice2;
        int diceSum;
        int rolls = 0;
        int rollCountAtHighestMoneyHeld = 0;
        int winningRoll = 7;
        int losingRoll = 2;
        int winningMoney = 4;
        int losingMoney = -1;
        int moneyWon = 0;
        int maxMoneyHeld = 0;

        //Ask user for amount of money to bet
        System.out.print("How much money are you willing to bet(whole dollar amounts only): ");
        int moneyToBet = userInput.nextInt();
        //Assign initial amount to another variable for keeping track of highest money total
        maxMoneyHeld = moneyToBet;

        //Keep performing everything within loop until money is gone
        while (moneyToBet > 0) {
            //Genreate random numbers for dice to simulate roll
            dice1 = diceRoll.nextInt(6) + 1;
            dice2 = diceRoll.nextInt(6) + 1;
            //Add two dice together for total
            diceSum = dice1 + dice2;

            //Check to see if total matches either the win or lose condition
            if (diceSum == winningRoll) {
                //If won, increase winnings and roll count
                moneyWon += winningMoney;
                rolls++;
                //Check to see after winning money if that total is higher than
                //what the user previously had to begin with
                if (moneyWon > maxMoneyHeld) {
                    //If true, update highest money held total to new total
                    maxMoneyHeld = moneyWon;
                    //Keep track of roll count
                    rollCountAtHighestMoneyHeld = rolls;
                }
                //If user rolled a losing roll
            } else if (diceSum == losingRoll) {
                //Decrease users money
                moneyWon += losingMoney;
                //Add another roll
                rolls++;
            } else {
                //Otherwise just incrament roll and keep playing
                rolls++;
            }
        }

        //Display game info to user after losing all money
        System.out.println("You are broke after " + rolls + "\n"
                + "You should have quit after roll " + rollCountAtHighestMoneyHeld
                + " when you had " + maxMoneyHeld);
    }
}
