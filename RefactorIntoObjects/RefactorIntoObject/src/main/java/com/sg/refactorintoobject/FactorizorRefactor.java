/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.refactorintoobject;

import java.util.Scanner;

/**
 *
 * @author cjsm12
 */
public class FactorizorRefactor {

    public void runFactorizor() {

        Scanner sc = new Scanner(System.in);

        //Welcome user
        System.out.println("Welcome to Facotrizor!!!");
        System.out.println("------------------------\n");

        //Ask user for a number to factor
        System.out.print("Give me a number to factorize : ");
        int numberToFactor = sc.nextInt();

        //Declare variables
        int factorSum = 0;
        boolean primeCheck;
        boolean perfectCheck;

        System.out.println("You asked to factor " + numberToFactor);
        System.out.println("These are the factors of " + numberToFactor + ": ");

        //for loop to check for factors of user number and adding each factor found 
        //to a variable for checking if number is perfect later
        for (int i = 1; i < numberToFactor; i++) {
            if (numberToFactor % i == 0) {
                factorSum += i;
                //Print out factor
                System.out.println(i + " is a factor of " + numberToFactor);
            }
        }

        //Checking if number is prime and or perfect by method calls
        primeCheck = isPrime(numberToFactor);
        perfectCheck = isPerfect(numberToFactor, factorSum);

        //Informing user if number is perfect or prime
        System.out.println(numberToFactor + " is prime = " + primeCheck);
        System.out.println(numberToFactor + " is perfect = " + perfectCheck);

    }

    static boolean isPrime(int n) {
        //check if n is a multiple of 2
        if (n % 2 == 0) {
            return false;
        }
        //if not, then just check the odds
        for (int i = 3; i * i <= n; i += 2) {
            if (n % i == 0) {
                return false;
            }
        }
        //Is passes all conditions above, number is prime
        return true;
    }

    static boolean isPerfect(int numberToFactor, int factorSum) {
        //Checking to see if factors total sum added up is equal to the original number
        //because by definition that is what a perfect number is
        if (factorSum == numberToFactor) {
            return true;
        } else {
            return false;
        }
    }
}
