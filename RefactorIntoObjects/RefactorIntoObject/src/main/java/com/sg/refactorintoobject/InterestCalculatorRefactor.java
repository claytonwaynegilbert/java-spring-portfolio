/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.refactorintoobject;

import java.util.Scanner;

/**
 *
 * @author cjsm12
 */
public class InterestCalculatorRefactor {

    public void runInterestCalculator() {
        Scanner sc = new Scanner(System.in);

        //Welcome user
        System.out.println("Welcome to Interest Calculator!!!");
        System.out.println("---------------------------------");

        //Ask user for interest rate
        System.out.print("What is the annual interest rate: ");
        double annualInterestRate = sc.nextDouble();

        //Ask user for principal
        System.out.print("What is the initial principal:  ");
        double initialPrincipal = sc.nextDouble();

        //Ask user how long they want money to stay in fund
        System.out.print("How many years do you want principal to stay in fund: ");
        double yearsToStayInFund = sc.nextDouble();

        //Declare other variables for calculations below
        double currentBalance = initialPrincipal;
        double quarterlyInterest = annualInterestRate / 4;

        System.out.println("Okay here is the breakdown: ");

        //For loop to calculate quarterly interest rate, end of year balance, and interest
        //earned each year.
        for (int i = 1; i <= yearsToStayInFund; i++) {
            //Variable used for displaying principal at beginning of each year
            double principal = currentBalance;
            //Nested for loop used for calculating new balance each quarter and assigning
            //new value to itself which after 4 revolutions, is the new balance at the
            //end of the year
            for (int j = 1; j <= 4; j++) {
                currentBalance *= ((1 + (quarterlyInterest / 100)));
            }
            //Variable used for keeping track of year
            int year = i;
            //Calculating interest earned for the year
            double interestEarned = currentBalance - principal;
            //Displaying all details to user for each year
            System.out.println("Year " + year + "\n"
                    + "Principal at beginning of year " + principal + "\n"
                    + "Interest earned for the year " + interestEarned + "\n"
                    + "Principal at end of year " + currentBalance);
        }
    }

}
