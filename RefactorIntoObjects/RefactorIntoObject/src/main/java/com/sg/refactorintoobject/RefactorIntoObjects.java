/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.refactorintoobject;

/**
 *
 * @author cjsm12
 */
public class RefactorIntoObjects {
    
    public static void main(String[] args) {
        
        //Create instances of all classes
        FactorizorRefactor factorizor = new FactorizorRefactor();
        InterestCalculatorRefactor calculator = new InterestCalculatorRefactor();
        LuckySevensRefactor luckySevens = new LuckySevensRefactor();
        
        //Run each object's one method
        factorizor.runFactorizor();
        calculator.runInterestCalculator();
        luckySevens.playLuckySevens();
        
    }
    
}
