<%-- 
    Document   : result
    Created on : Sep 18, 2017, 12:18:14 AM
    Author     : cjsm12
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Results</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" 
              rel="stylesheet">   
    </head>
    <body>
        <h1>Result</h1>
        <p class="results">
            You asked to factor: ${numberToFactor}
        </p>
        <p>
            <strong>Your factors are:</strong>
            <c:forEach var="factor" items="${factorList}">
                <c:out value="${factor}"/>
            </c:forEach>
        </p>
        <p>
            <c:choose>
                <c:when test="${isPrime}">
                    <c:out value="The number is prime!"/>
                </c:when>
                <c:otherwise>
                    <c:out value="The number is not prime!"/>
                </c:otherwise>
            </c:choose>
        </p>
        <p>
            <c:choose>
                <c:when test="${isPerfect}">
                    <c:out value="The number is perfect!"/>
                </c:when>
                <c:otherwise>
                    <c:out value="The number is not perfect!"/>
                </c:otherwise>
            </c:choose>
        </p>
        <p>
            <a href="index.jsp"><strong>Factor Another One!</strong></a>
        </p>
    </body>
</html>
