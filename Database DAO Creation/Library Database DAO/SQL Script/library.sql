DROP DATABASE IF EXISTS library_final;

CREATE DATABASE library_final;

USE library_final;

CREATE TABLE IF NOT EXISTS authors
(
author_id int(11) NOT NULL auto_increment,
first_name varchar(20) NOT NULL,
last_name varchar(30) NOT NULL,
street varchar(20) NOT NULL,
city varchar(20) NOT NULL,
state varchar(15) NOT NULL,
zip varchar(10) NOT NULL,
phone varchar(20) NOT NULL,
PRIMARY KEY(author_id)
);

CREATE TABLE IF NOT EXISTS publishers
(
publisher_id int(11) NOT NULL auto_increment,
`name` varchar(20) NOT NULL,
street varchar(20) NOT NULL,
city varchar(20) NOT NULL,
state varchar(20) NOT NULL,
zip varchar(10) NOT NULL,
phone varchar(20) NOT NULL,
PRIMARY KEY(publisher_id)
);

CREATE TABLE IF NOT EXISTS books
(
book_id int(11) NOT NULL auto_increment,
isbn varchar(20) NOT NULL,
title varchar(50) NOT NULL,
publisher_id int(11) NOT NULL,
price DECIMAL(5,2) NOT NULL,
publish_date DATETIME NOT NULL,
PRIMARY KEY(book_id)
);

CREATE TABLE IF NOT EXISTS books_authors
(
book_id int(11) NOT NULL,
author_id int(11) NOT NULL,
PRIMARY KEY(book_id, author_id)
);

ALTER TABLE books ADD CONSTRAINT books_ibfk_1 
FOREIGN KEY (publisher_id) REFERENCES publishers(publisher_id) ON DELETE NO ACTION;

ALTER TABLE books_authors ADD CONSTRAINT books_authors_ibfk_1 
FOREIGN KEY (book_id) REFERENCES books(book_id) ON DELETE NO ACTION;

ALTER TABLE books_authors ADD CONSTRAINT books_authors_ibfk_2 
FOREIGN KEY (author_id) REFERENCES authors(author_id) ON DELETE NO ACTION;



