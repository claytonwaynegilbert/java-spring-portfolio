/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.library.dao;

import com.sg.library.model.Author;
import com.sg.library.model.Book;
import com.sg.library.model.Publisher;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author cjsm12
 */
public class LibraryDaoJdbcTemplateDbImpl implements LibraryDao {

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private static final String SQL_INSERT_AUTHOR
            = "insert into authors "
            + "(first_name, last_name, street, city, state, zip, phone) "
            + "values(?,?,?,?,?,?,?)";

    private static final String SQL_REMOVE_AUTHOR
            = "delete from authors where author_Id = ?";

    private static final String SQL_UPDATE_AUTHOR
            = "update authors set "
            + "first_name = ?, "
            + "last_name = ?, "
            + "street = ?, "
            + "city = ?, "
            + "state = ?, "
            + "zip = ?, "
            + "phone = ?, "
            + "where author_id = ?";

    private static final String SQL_SELECT_AUTHOR_BY_ID
            = "select * from authors where author_id = ?";

    private static final String SQL_SELECT_AUTHORS_BY_BOOK_ID
            = "select au.author_id, "
            + "au.first_name,"
            + "au.last_name,"
            + "au.street,"
            + "au.city,"
            + "au.state,"
            + "au.zip,"
            + "au.phone from authors au join books_authors ba on "
            + "au.author_id = ba.author_id where ba.book_id = ?";

    private static final String SQL_SELECT_ALL_AUTHORS
            = "select * from authors";

    private static final String SQL_INSERT_BOOK
            = "insert into books "
            + "(isbn, title, publisher_id, price, publish_date) "
            + "values (?, ?, ?, ?, ?)";

    private static final String SQL_INSERT_BOOKS_AUTHORS
            = "insert into books_authors (book_id, author_id) values(?, ?)";

    private static final String SQL_DELETE_BOOK
            = "delete from books where book_id = ?";

    private static final String SQL_DELETE_BOOKS_AUTHORS
            = "delete from books_authors where book_id = ?";

    private static final String SQL_UPDATE_BOOK
            = "update books set"
            + "isbn = ?,"
            + "title = ?,"
            + "publisher_id = ?,"
            + "price = ?,"
            + "publish_date = ?,"
            + "where book_id = ?";

    private static final String SQL_SELECT_BOOK_BY_ID
            = "select * from books where book_id = ?";

    private static final String SQL_SELECT_BOOKS_AUTHORS_AUTHOR_ID_BY_BOOK_ID
            = "select author_id from books_authors where book_id = ?";

    private static final String SQL_SELECT_BOOKS_BY_AUTHOR_ID
            = "select * from books b join books_authors ba on b.bookId = ba.book_id "
            + "where ba.author_id = ? ";

    private static final String SQL_SELECT_BOOKS_BY_PUBLISHER_ID
            = "select * from books where publisher_id = ?";

    private static final String SQL_SELECT_ALL_BOOKS
            = "select * from books";

    private static final String SQL_INSERT_PUBLISHER
            = "insert into publishers (name, street, city, state, zip, phone) "
            + "values(?, ?, ?, ?, ?, ?)";
    private static final String SQL_DELETE_PUBLISHER
            = "delete from publishers where publisher_id = ?";
    private static final String SQL_UPDATE_PUBLISHER
            = "update publishers set name = ?, street = ?, city = ?, "
            + "state = ?, zip = ?, phone = ? where publisher_id  =  ?";
    private static final String SQL_SELECT_PUBLISHER_BY_ID
            = "select * from publishers where publisher_id = ?";
    private static final String SQL_SELECT_PUBLISHER_BY_BOOK_ID
            = "select pub.publisher_id, pub.name, pub.street, pub.city, "
            + "pub.state, pub.zip, pub.phone from publishers pub "
            + "join books on pub.publisher_id = books.publisher_id where "
            + "books.book_id = ?";
    private static final String SQL_SELECT_ALL_PUBLISHERS
            = "select * from publishers";

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void addAuthor(Author author) {
        jdbcTemplate.update(SQL_INSERT_AUTHOR, author.getFirstName(),
                author.getLastName(),
                author.getStreet(),
                author.getCity(),
                author.getState(),
                author.getZip(),
                author.getPhone());

        int authorId = jdbcTemplate.queryForObject("select LAST_INSERT_ID()", Integer.class);
        author.setAuthorId(authorId);
    }

    @Override
    public void deleteAuthor(int authorId) {
        jdbcTemplate.update(SQL_REMOVE_AUTHOR, authorId);
    }

    @Override
    public void updateAuthor(Author author) {
        jdbcTemplate.update(SQL_UPDATE_AUTHOR,
                author.getFirstName(),
                author.getLastName(),
                author.getStreet(),
                author.getCity(),
                author.getState(),
                author.getZip(),
                author.getPhone(),
                author.getAuthorId());
    }

    @Override
    public Author getAuthorById(int id) {
        try {
            return jdbcTemplate.queryForObject(SQL_SELECT_AUTHOR_BY_ID, new AuthorMapper(), id);
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    @Override
    public List<Author> getAllAuthors() {
        return jdbcTemplate.query(SQL_SELECT_ALL_AUTHORS, new AuthorMapper());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void addBook(Book book) {
        jdbcTemplate.update(SQL_INSERT_BOOK,
                book.getIsbn(),
                book.getTitle(),
                book.getPublisher().getPublisherId(),
                book.getPrice(),
                book.getPublishDate().toString());

        int bookId = jdbcTemplate.queryForObject("select LAST_INSERT_ID()", Integer.class);
        book.setBookId(bookId);

        insertAuthorsIntoBook(book);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void deleteBook(int bookId) {
        jdbcTemplate.update(SQL_DELETE_BOOKS_AUTHORS, bookId);

        jdbcTemplate.update(SQL_DELETE_BOOK, bookId);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void updateBook(Book book) {
        jdbcTemplate.update(SQL_UPDATE_BOOK,
                book.getIsbn(),
                book.getTitle(),
                book.getPublisher().getPublisherId(),
                book.getPrice(),
                book.getPublishDate().toString(),
                book.getBookId());

        jdbcTemplate.update(SQL_DELETE_BOOKS_AUTHORS, book.getBookId());
        insertAuthorsIntoBook(book);
    }

    @Override
    public Book getBookById(int id) {
        try {
            Book book = jdbcTemplate.queryForObject(SQL_SELECT_BOOK_BY_ID,
                    new BookMapper(),
                    id);
            book.setAuthors(findListOfAuthorsForGivenBook(book));
            book.setPublisher(findPublisherForGivenBook(book));

            return book;
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    @Override
    public List<Book> getBooksByAuthorId(int authorId) {
        List<Book> matchingBooks = jdbcTemplate.query(SQL_SELECT_BOOKS_BY_AUTHOR_ID,
                new BookMapper(), authorId);

        return assignAuthorsAndPublishersToListOfBooks(matchingBooks);
    }

    @Override
    public List<Book> getBooksByPublisherId(int publisherId) {
        List<Book> matchingBooks = jdbcTemplate.query(SQL_SELECT_BOOKS_BY_PUBLISHER_ID,
                new BookMapper(), publisherId);

        return assignAuthorsAndPublishersToListOfBooks(matchingBooks);
    }

    @Override
    public List<Book> getAllBooks() {
        List<Book> matchingBooks = jdbcTemplate.query(SQL_SELECT_ALL_BOOKS,
                new BookMapper());

        return assignAuthorsAndPublishersToListOfBooks(matchingBooks);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void addPublisher(Publisher publisher) {
        jdbcTemplate.update(SQL_INSERT_PUBLISHER,
                publisher.getName(),
                publisher.getStreet(),
                publisher.getCity(),
                publisher.getState(),
                publisher.getZip(),
                publisher.getPhone());

        int publisherId = jdbcTemplate.queryForObject("select LAST_INSERT_ID()", Integer.class);
        publisher.setPublisherId(publisherId);
    }

    @Override
    public void deletePublisher(int publisherId) {
        jdbcTemplate.update(SQL_DELETE_PUBLISHER, publisherId);
    }

    @Override
    public void updatePublisher(Publisher publisher) {
        jdbcTemplate.update(SQL_UPDATE_PUBLISHER,
                publisher.getName(),
                publisher.getStreet(),
                publisher.getCity(),
                publisher.getState(),
                publisher.getZip(),
                publisher.getPhone(),
                publisher.getPublisherId());
    }

    @Override
    public Publisher getPublisherById(int id) {
        try {
            return jdbcTemplate.queryForObject(SQL_SELECT_PUBLISHER_BY_ID,
                    new PublisherMapper(), id);
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    @Override
    public List<Publisher> getAllPublishers() {
        return jdbcTemplate.query(SQL_SELECT_ALL_PUBLISHERS, new PublisherMapper());
    }

    private void insertAuthorsIntoBook(Book book) {
        final int bookId = book.getBookId();
        final List<Author> booksAuthors = book.getAuthors();

        for (Author currentAuthor : booksAuthors) {
            jdbcTemplate.update(SQL_INSERT_BOOKS_AUTHORS,
                    bookId, currentAuthor.getAuthorId());
        }
    }

    private List<Author> findListOfAuthorsForGivenBook(Book book) {
        return jdbcTemplate.query(SQL_SELECT_AUTHORS_BY_BOOK_ID,
                new AuthorMapper(),
                book.getBookId());
    }

    private Publisher findPublisherForGivenBook(Book book) {
        return jdbcTemplate.queryForObject(SQL_SELECT_PUBLISHER_BY_BOOK_ID,
                new PublisherMapper(),
                book.getBookId());
    }

    private List<Book> assignAuthorsAndPublishersToListOfBooks(List<Book> bookList) {

        for (Book currentBook : bookList) {

            currentBook.setAuthors(findListOfAuthorsForGivenBook(currentBook));

            currentBook.setPublisher(findPublisherForGivenBook(currentBook));
        }
        return bookList;
    }

    private static final class AuthorMapper implements RowMapper<Author> {

        @Override
        public Author mapRow(ResultSet rs, int i) throws SQLException {
            Author au = new Author();
            au.setFirstName(rs.getString("first_name"));
            au.setLastName(rs.getString("last_name"));
            au.setStreet(rs.getString("street"));
            au.setCity(rs.getString("city"));
            au.setState(rs.getString("state"));
            au.setZip(rs.getString("zip"));
            au.setPhone(rs.getString("phone"));
            au.setAuthorId(rs.getInt("author_id"));
            return au;
        }
    }

    private static final class PublisherMapper implements RowMapper<Publisher> {

        @Override
        public Publisher mapRow(ResultSet rs, int i) throws SQLException {
            Publisher pb = new Publisher();
            pb.setPublisherId(rs.getInt("publisher_id"));
            pb.setName(rs.getString("name"));
            pb.setStreet(rs.getString("street"));
            pb.setCity(rs.getString("city"));
            pb.setState(rs.getString("state"));
            pb.setZip(rs.getString("zip"));
            pb.setPhone(rs.getString("phone"));
            return pb;
        }

    }

    private static final class BookMapper implements RowMapper<Book> {

        @Override
        public Book mapRow(ResultSet rs, int i) throws SQLException {
            Book b = new Book();
            b.setBookId(rs.getInt("book_id"));
            b.setIsbn(rs.getString("isbn"));
            b.setTitle(rs.getString("title"));
            b.setPrice(rs.getBigDecimal("price"));
            b.setPublishDate(rs.getTimestamp("publish_date").
                    toLocalDateTime().toLocalDate());
            return b;
        }
    }

}
