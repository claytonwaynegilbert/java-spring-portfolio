/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.javaarrays;

import java.util.Arrays;

/**
 *
 * @author cjsm12
 */
public class NewClass {

    public static void main(String[] args) {

        String[] fruitSalad = new String[11];
         String[] fruits = {
            "Kiwi Fruit", "Gala Apple", "Granny Smith Apple",
            "Cherry Tomato", "Gooseberry", "Beefsteak Tomato", "Braeburn Apple",
            "Blueberry", "Strawberry", "Navel Orange", "Pink Pearl Apple",
            "Raspberry", "Blood Orange", "Sungold Tomato", "Fuji Apple", "Blackberry",
            "Banana", "Pineapple", "Florida Orange", "Kiku Apple", "Mango",
            "Satsuma Orange", "Watermelon", "Snozzberry"
        };

        for (int i = 0; i < fruits.length; i++) {
            String currentFruit = fruits[i];
            switch (currentFruit) {
                case "Gala Apple":
                    fruitSalad[0] = currentFruit;
                    break;
                case "Granny Smith Apple":
                    fruitSalad[1] = currentFruit;
                    break;
                case "Gooseberry":
                    fruitSalad[2] = currentFruit;
                    break;
                case "Braeburn Apple":
                    fruitSalad[3] = currentFruit;
                    break;
                case "Blueberry":
                    fruitSalad[4] = currentFruit;
                    break;
                case "Strawberry":
                    fruitSalad[5] = currentFruit;
                    break;
                case "Navel Orange":
                    fruitSalad[6] = currentFruit;
                    break;
                case "Raspberry":
                    fruitSalad[7] = currentFruit;
                    break;
                case "Blood Orange":
                    fruitSalad[8] = currentFruit;
                    break;
                case "Blackberry":
                    fruitSalad[9] = currentFruit;
                    break;
                case "Snozzberry":
                    fruitSalad[10] = currentFruit;
                    break;
                default:
                    break;
            }
        }

        for (int i = 0; i < fruitSalad.length; i++) {
            System.out.println(fruitSalad[i] + ", ");
        }

    }

}
