/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.javaarrays;

import java.util.Arrays;
import java.util.Random;

/**
 *
 * @author cjsm12
 */
public class JavaArrays {

    public static void main(String[] args) {

        /*
        Filename: StillPositive.java
        Print out all positive numbers of the following array:

        int[] numbers = { 389, -447, 26, -485, 712, -884, 94, -64, 868, -776, 227, -744, 
        422, -109, 259, -500, 278, -219, 799, -311};
         */
        
        int[] numbers = {389, -447, 26, -485, 712, -884, 94, -64, 868, -776, 227, -744,
            422, -109, 259, -500, 278, -219, 799, -311};

        System.out.println("Gotta stay positive... ");
        for (int i = 0; i < numbers.length; i++) {
            int currentNumber = numbers[i];
            if (currentNumber % 2 == 0) {
                System.out.print(currentNumber + " ");
            }
        }

        /*
        Filename: HiddenNuts.java
        Squirrels like to hide their nuts - but they're not always very good about 
        finding them again.

        Using the code snippet below as a base, iterate through the hiding spaces and 
        find out where the squirrel put his nut and print it to the screen!
         */
        
        String[] hidingSpots = new String[100];
        Random squirrel = new Random();
        hidingSpots[squirrel.nextInt(hidingSpots.length)] = "Nut";
        System.out.println("The nut has been hidden ...");

        for (int i = 0; i < hidingSpots.length; i++) {
            if (hidingSpots[i] == null) {
                i = i + 1;
            } else {
                System.out.println("Found it! It is in spot " + i);
            }
        }

        /*
        Filename: FruitsBasket.java
        Iterate over the below array and determine how many oranges and how many apples 
        it contains, and print it out.

        Then make 2 new arrays, one for each type, and sort the fruit into those new 
        arrays. (I.e. move the String value from the first array into the appropriate 
        sorting array).

        Finally, once they are sorted, print out how many fruit you have total, and then 
        how many oranges are in the orange array, and how many apples are in the apple 
        array.
         */
        
        String[] fruit = {"Orange", "Apple", "Orange", "Apple", "Orange", "Apple",
            "Orange", "Apple", "Orange", "Orange", "Orange", "Apple", "Orange", "Orange",
            "Apple", "Orange", "Orange", "Apple", "Apple", "Orange", "Apple", "Apple",
            "Orange", "Orange", "Apple", "Apple", "Apple", "Apple", "Orange", "Orange",
            "Apple", "Apple", "Orange", "Orange", "Orange", "Orange", "Apple", "Apple",
            "Apple", "Apple", "Orange", "Orange", "Apple", "Orange", "Orange", "Apple",
            "Orange", "Orange", "Apple", "Apple", "Orange", "Orange", "Apple", "Orange",
            "Apple", "Orange", "Apple", "Orange", "Apple", "Orange", "Orange"};

        int appleCounter = 0;
        int orangeCounter = 0;
        int totalCount = 0;
        String[] appleArray = new String[fruit.length];
        String[] orangeArray = new String[fruit.length];

        for (int i = 0; i < fruit.length; i++) {
            String currentFruit = fruit[i];
            if (currentFruit.equalsIgnoreCase("Apple")) {
                appleArray[i] = currentFruit;
                appleCounter++;
            } else {
                orangeArray[i] = currentFruit;
                orangeCounter++;
            }
        }

        totalCount = appleCounter + orangeCounter;
        System.out.println("There are " + appleCounter + " apples in the apple array and " + orangeCounter + " oranges in the orange array.");
        System.out.println("There are " + totalCount + " fruit all together!");

        /*
        Filename: SimpleSort.java
        Combine the following 2 arrays into one large array that has all the numbers sorted 
        in increasing order, then print them out!
         */
        
        int[] firstHalf = {3, 7, 9, 10, 16, 19, 20, 34, 55, 67, 88, 99};
        int[] secondHalf = {1, 4, 8, 11, 15, 18, 21, 44, 54, 79, 89, 100};

        int[] wholeNumbers = new int[24];

        System.arraycopy(firstHalf, 0, wholeNumbers, 0, firstHalf.length);
        System.arraycopy(secondHalf, 0, wholeNumbers, firstHalf.length, secondHalf.length);

        Arrays.sort(wholeNumbers);

        System.out.println("Okay here we go, all nice and ordered!");
        for (int d = 0; d < wholeNumbers.length; d++) {
            System.out.println(wholeNumbers[d] + " ");
        }

        /*
        Filename: FruitSalad.java
        After seeing you mucking about with all the apples & oranges from before - your 
        friends decided to throw a digital potluck, and tasked you with making the fruit 
        salad! However, while you have a lot of different fruit on hand, you know that 
        just throwing them all together isn't the best idea - so you went and found 
        a recipe.

        Apparently the best kind of fruit salad has:

        As many berries as possible
        No more than 3 kinds of apples
        No more than 2 kinds of orange
        Definitely no tomatoes
        More than 12 kinds of fruit isn't recommended

        Write some code to sort through fruits below and put all the proper fruits in 
        the fruitSalad array. Afterwards, print out the total number and types of 
        fruit in your fruit salad!
         */
        
        String[] fruitSalad = new String[11];
        String[] fruits = {
            "Kiwi Fruit", "Gala Apple", "Granny Smith Apple",
            "Cherry Tomato", "Gooseberry", "Beefsteak Tomato", "Braeburn Apple",
            "Blueberry", "Strawberry", "Navel Orange", "Pink Pearl Apple",
            "Raspberry", "Blood Orange", "Sungold Tomato", "Fuji Apple", "Blackberry",
            "Banana", "Pineapple", "Florida Orange", "Kiku Apple", "Mango",
            "Satsuma Orange", "Watermelon", "Snozzberry"
        };

        for (int i = 0; i < fruits.length; i++) {
            String currentFruit = fruits[i];
            switch (currentFruit) {
                case "Gala Apple":
                    fruitSalad[0] = currentFruit;
                    break;
                case "Granny Smith Apple":
                    fruitSalad[1] = currentFruit;
                    break;
                case "Gooseberry":
                    fruitSalad[2] = currentFruit;
                    break;
                case "Braeburn Apple":
                    fruitSalad[3] = currentFruit;
                    break;
                case "Blueberry":
                    fruitSalad[4] = currentFruit;
                    break;
                case "Strawberry":
                    fruitSalad[5] = currentFruit;
                    break;
                case "Navel Orange":
                    fruitSalad[6] = currentFruit;
                    break;
                case "Raspberry":
                    fruitSalad[7] = currentFruit;
                    break;
                case "Blood Orange":
                    fruitSalad[8] = currentFruit;
                    break;
                case "Blackberry":
                    fruitSalad[9] = currentFruit;
                    break;
                case "Snozzberry":
                    fruitSalad[10] = currentFruit;
                    break;
                default:
                    break;
            }
        }

        System.out.println("There are 3 different types of fruit in my fruit salad"
                + "and 11 pieces of fruit all together. Below is a list of all fruit:");
        for (int i = 0; i < fruitSalad.length; i++) {
            System.out.print(fruitSalad[i] + ", ");
        }
    }

}
