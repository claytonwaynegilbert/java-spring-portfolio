/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.functionalunittests;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author savannahg
 */
public class StringTimesTest {
    
    StringTimes times = new StringTimes();
    
    public StringTimesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of stringTimes method, of class StringTimes.
     */
    @Test
    public void testStringHello3Times() {
        String expectedString = "HelloHelloHello";
        String result = times.stringTimes("Hello", 3);
        assertEquals(expectedString, result);
    }
    
    @Test
    public void testStringAirplane1Times(){
        String expectedString = "Airplane";
        String result = times.stringTimes("Airplane", 1);
        assertEquals(expectedString, result);
    }
    
    @Test
    public void testStringCobra8Times(){
        String expectedString = "CobraCobraCobraCobraCobraCobraCobraCobra";
        String result = times.stringTimes("Cobra", 8);
        assertEquals(expectedString, result);
    }
    
    @Test
    public void testStringJuice0Times(){
        String expectedString = "";
        String result = times.stringTimes("Juice", 0);
        assertEquals(expectedString, result);
    }
    
}
