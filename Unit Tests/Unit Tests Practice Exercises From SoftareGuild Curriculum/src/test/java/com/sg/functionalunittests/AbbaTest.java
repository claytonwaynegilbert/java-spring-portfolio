/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.functionalunittests;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author savannahg
 */
public class AbbaTest {
    
    Abba mixUp = new Abba();
    
    public AbbaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    /**
     * Test of abba method, of class Abba.
     */
    @Test
    public void testHeyYall() {
        String expected = "HeyYallYallHey";
        String result = mixUp.abba("Hey", "Yall");
        assertEquals(expected, result);
    }
    
    @Test
    public void testCoolBeans(){
        String expected = "CoolBeansBeansCool";
        String result = mixUp.abba("Cool", "Beans");
        assertEquals(expected, result);
    }
    
}
