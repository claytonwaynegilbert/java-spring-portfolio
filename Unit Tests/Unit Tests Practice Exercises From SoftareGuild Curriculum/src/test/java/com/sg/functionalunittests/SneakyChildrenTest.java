/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.functionalunittests;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author savannahg
 */
public class SneakyChildrenTest {

    SneakyChildren children = new SneakyChildren();

    public SneakyChildrenTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testASmileBNoSmile() {
        boolean trouble = children.areWeInTrouble(true, false);
        assertFalse(trouble);
    }

    @Test
    public void testANoSmileBSmile() {
        boolean trouble = children.areWeInTrouble(false, true);
        assertFalse(trouble);
    }

    @Test
    public void testASmileBSmile() {
        assertTrue(children.areWeInTrouble(true, true));
    }

    @Test
    public void testBNoSmileANoSmile() {
        assertTrue(children.areWeInTrouble(false, false));
    }

}
