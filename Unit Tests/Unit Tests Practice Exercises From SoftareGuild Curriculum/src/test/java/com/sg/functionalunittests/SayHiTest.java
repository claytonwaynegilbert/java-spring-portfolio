/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.functionalunittests;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author savannahg
 */
public class SayHiTest {
    
    SayHi hello = new SayHi();
    
    public SayHiTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testHelloClayton() {
        String expectedName = "Hello Clayton";
        String returnedName = hello.sayHi("Clayton");
        assertEquals(returnedName, expectedName);
    }
    
    @Test
    public void testHelloX(){
        String expectedName = "Hello X";
        String returnedName = hello.sayHi("X");
        assertEquals(returnedName, expectedName);
    }
    
    @Test
    public void testHelloHappyGilmore(){
        String expectedName = "Hello Happy Gilmore";
        String returnedName = hello.sayHi("Happy Gilmore");
        assertEquals(expectedName, returnedName);
    }
    
}
