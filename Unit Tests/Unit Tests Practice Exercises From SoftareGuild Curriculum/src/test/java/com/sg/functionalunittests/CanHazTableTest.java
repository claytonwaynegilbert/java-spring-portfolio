/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.functionalunittests;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author savannahg
 */
public class CanHazTableTest {

    CanHazTable reservation = new CanHazTable();

    public CanHazTableTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of canHazTable method, of class CanHazTable.
     */
    @Test
    public void test2And7() {
        assertEquals(0, reservation.canHazTable(2, 7));
    }

    @Test
    public void test1And2() {
        assertEquals(0, reservation.canHazTable(1, 2));
    }

    @Test
    public void test3And8() {
        assertEquals(2, reservation.canHazTable(3, 8));

    }

    @Test
    public void test10And8() {
        assertEquals(2, reservation.canHazTable(10, 8));

    }

    @Test
    public void test7And7() {
        assertEquals(1, reservation.canHazTable(7, 7));

    }

}
