/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.functionalunittests;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author savannahg
 */
public class FrontTimesTest {
    
    FrontTimes ft = new FrontTimes();
    
    public FrontTimesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of frontTimes method, of class FrontTimes.
     */
    @Test
    public void testChocolate4Times() {
        String expected = "ChoChoChoCho";
        String result = ft.frontTimes("Chocolate", 4);
        assertEquals(expected, result);
    }
    
    @Test
    public void testWoohoo1Times() {
        String expected = "Woo";
        String result = ft.frontTimes("Woohoo", 1);
        assertEquals(expected, result);
    }
    
    @Test
    public void testYa3Times() {
        String expected = "YaYaYa";
        String result = ft.frontTimes("Ya", 3);
        assertEquals(expected, result);
    }
    
}
