/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.localdatepractice;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author savannahg
 */
public class LocalDatePractice {
    
    public static void main(String[] args) {
        
        //Creates a localdate object with todays date in default localdate format
        LocalDate now = LocalDate.now();
        System.out.println(now.toString());
        
        //Using the parse function to input custom date along with proper format, otherwise error
        LocalDate customDate = LocalDate.parse("2017-08-24");
        System.out.println(customDate);
        
        //Same as the one above but also specifiying a certain pattern we want the date to be displayed by
        //passing in a type DateTimeFormatter to specify the pattern, then calling the static method formate
        //on the new localdate object which returns a String of the new formatted date to be displayed.
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        LocalDate customDateFormatted = LocalDate.parse("08/24/2017", formatter);
        String formattedDate = customDateFormatted.format(formatter);
        System.out.println(formattedDate);
        
        //Calculating 40 days previous todays date by getting reference to todays LocalDate object above
        //and calling the static minusDays method and inputting specified days wanting to go back which gets 
        //stored in another LocalDate object
        LocalDate pastDate = now.minusDays(40);
        System.out.println(pastDate);
        
        //Calculating time betwen two LocalDate dates by calling the until() method on one of the LocalDate objects
        //and passing in the other as the paramater to the method. This results in a Period object being created
        //which has getters for the months, days, and years that the time between the two dats is.
        Period timeBetween = pastDate.until(now);
        System.out.println("Days: " + timeBetween.getDays() + " Months: " + timeBetween.getMonths() + " Years: " + timeBetween.getYears());
        
        
    }
    
}
