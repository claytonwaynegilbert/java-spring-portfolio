<%-- 
    Document   : results
    Created on : Sep 18, 2017, 4:49:30 PM
    Author     : cjsm12
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tip Calculator Results</title>
    </head>
    <body>
        <h1>Breakdown:</h1>
        <p>
            <strong>Bill Amount:</strong> $${bill}<br>
            <strong>Tip %:</strong> ${tipPercent}<br>
            <strong>Tip Amount:</strong> $${totalTip}<br>
            <strong>Total Bill:</strong> $${finalBill}
        </p>
        <br>
        <a href="index.jsp"><strong>Calculate another tip</strong></a>
    </body>
</html>
