/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.tipcalculatorspringmvcapplication;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author cjsm12
 */
@Controller
public class TipCalculatorController {

    @RequestMapping(value = "/calculateTip", method = RequestMethod.POST)
    public String calculateTip(HttpServletRequest request, Map<String, Object> model) {
        BigDecimal billAmount = new BigDecimal(request.getParameter("billAmount"));
        BigDecimal tipPercent = new BigDecimal(request.getParameter("tipPercent"));
        BigDecimal totalTip = null;
        BigDecimal finalBill = null;

        switch (tipPercent.intValue()) {
            case 5:
                totalTip = billAmount.multiply(new BigDecimal(".05")).setScale(2, RoundingMode.HALF_UP);
                break;
            case 10:
                totalTip = billAmount.multiply(new BigDecimal(".10")).setScale(2, RoundingMode.HALF_UP);
                break;
            case 15:
                totalTip = billAmount.multiply(new BigDecimal(".15")).setScale(2, RoundingMode.HALF_UP);
                break;
            case 20:
                totalTip = billAmount.multiply(new BigDecimal(".20")).setScale(2, RoundingMode.HALF_UP);
                break;
            case 25:
                totalTip = billAmount.multiply(new BigDecimal(".25")).setScale(2, RoundingMode.HALF_UP);
                break;
            default:
                break;
        }

        finalBill = billAmount.add(totalTip);

        model.put("bill", billAmount);
        model.put("tipPercent", tipPercent);
        model.put("totalTip", totalTip);
        model.put("finalBill", finalBill);

        return "results";
    }

}
