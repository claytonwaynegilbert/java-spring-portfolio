<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Tip Calculator</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">        
        <style>
            #instructions{
                font-style: italic;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <h1>Tip Calculator</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="active">
                        <a href="${pageContext.request.contextPath}/index.jsp">
                            Home</a></li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/hello/sayhi">
                            Hello Controller</a></li>
                </ul>    
            </div>
            <h2>Instructions:</h2>
            <p id="instructions">
                    Prompt the user for:<br>
                    The amount of the bill<br>
                    The percentage tip to leave (e.g., 15%)<br>
                    Calculates and displays the tip amount<br>
                    Calculates and displays the total cost (amount of bill + tip amount)<br>
            </p>
            <br>
            <form method="POST" action="calculateTip">
                <strong>Bill Amount:</strong> <input type="text" name="billAmount"/><br><br>
                <strong>Tip Percent:</strong>
                <select name="tipPercent">
                    <option value="5">5%</option>
                    <option value="10">10%</option>
                    <option value="15">15%</option>
                    <option value="20">20%</option>
                    <option value="25">25%</option>
                </select>
                <br><br>
                <input type="submit" value="Calculate"/>
            </form>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>

