/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvdlibraryspringmvcapplication;

import com.sg.dvdlibraryspringmvcapplication.dao.ErrorMessage;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author cjsm12
 */
public class ControllerExceptionHandler {
    
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ResponseBody
    public ErrorMessage processMethodArgumentNotValidException(MethodArgumentNotValidException e){
        BindingResult result = e.getBindingResult();
        List<FieldError> fieldErrors = result.getFieldErrors();
        StringBuilder message = new StringBuilder();
        message.append("You have the following errors: ");
        
        for(FieldError currentError: fieldErrors){
            message.append("[");
            message.append(currentError.getField());
            message.append(" : ");
            message.append(currentError.getDefaultMessage());
            message.append("]");
        }
        
        ErrorMessage errorMessage = new ErrorMessage();
        errorMessage.writeErrorMessage(message.toString());
        
        return errorMessage;
        
    }
    
    @ExceptionHandler(UpdateIntegrityException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ResponseBody
    public ErrorMessage processUpdateIntegrityException(UpdateIntegrityException e){
        ErrorMessage error = new ErrorMessage();
        error.writeErrorMessage(e.getMessage());
        return error;
    }
    
}
