/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvdlibraryspringmvcapplication;

import com.sg.dvdlibraryspringmvcapplication.dao.DvdLibraryDao;
import com.sg.dvdlibraryspringmvcapplication.model.Dvd;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author cjsm12
 */
@Controller
public class DvdController {
    
    DvdLibraryDao dao;
    
    @Inject
    public DvdController(DvdLibraryDao dao){
        this.dao = dao;
    }
    
    
    @RequestMapping(value="", method=RequestMethod.GET)
    public String displayHomePage(){
        
        return "redirect: homePage";
    }
    
    @RequestMapping(value="/homePage", method=RequestMethod.GET)
    public String displayDvdTable(Model model){
        List<Dvd> allDvds = dao.getAllDvds();
        
        model.addAttribute("dvds", allDvds);
        
        return "DvdLibraryHomePage";
    }
    
    @RequestMapping(value="/displayCreateDvdPage", method=RequestMethod.GET)
    public String displayCreateContactPage(){
         
        return "createDvdForm";
    }
    
    @RequestMapping(value="/displayEditDvdPage", method=RequestMethod.GET)
    public String displayEditDvdPage(HttpServletRequest request, Model model){
        String idString = request.getParameter("dvdId");
        int id = Integer.parseInt(idString);
        
        Dvd matchingDvd = dao.getDvd(id);
        
        model.addAttribute("dvd", matchingDvd);
        
        return "editDvdForm";
    }
    
    @RequestMapping(value="/createDvd", method=RequestMethod.POST)
    public String createDvd(HttpServletRequest request, Model model){
        
        Dvd dvd = new Dvd();
        dvd.setTitle(request.getParameter("add-title"));
        dvd.setReleaseDate(request.getParameter("add-year"));
        dvd.setDirector(request.getParameter("add-director"));
        dvd.setRating(request.getParameter("add-rating"));
        dvd.setNote(request.getParameter("add-notes"));

        dao.addDvd(dvd);
        
        return "redirect: homePage";
    }
    
    @RequestMapping(value="/editDvd", method=RequestMethod.POST)
    public String editDvd(@Valid @ModelAttribute("dvd") Dvd dvd, BindingResult result){
        
        if(result.hasErrors()){
            return "editDvdForm";
        }
        dao.updateDvd(dvd);
        
        return "redirect: homePage";
    }
    
    @RequestMapping(value="/deleteDvd", method=RequestMethod.GET)
    public String deleteDvd(HttpServletRequest request){
        String idString = request.getParameter("dvdId");
        int id = Integer.parseInt(idString);
        
        dao.removeDvd(id);
        
        return "redirect: homePage";
    }
    
    @RequestMapping(value="/cancel", method=RequestMethod.GET)
    public String cancelButton(){
        return "redirect: homePage";
    }
    
    @RequestMapping(value="/displayDvdDetails", method=RequestMethod.GET)
    public String displayDvdDetailsPage(HttpServletRequest request, Model model){
        String idString = request.getParameter("dvdId");
        int id = Integer.parseInt(idString);
        
        Dvd matchingDvd = dao.getDvd(id);
        model.addAttribute("dvd", matchingDvd);
        
        return "DvdDetails";
        
    }
    
}
