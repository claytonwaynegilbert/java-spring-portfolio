/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvdlibraryspringmvcapplication.dao;

import com.sg.dvdlibraryspringmvcapplication.model.Dvd;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author cjsm12
 */
public class DvdLibraryDaoDbImpl implements DvdLibraryDao {

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private static final String SQL_INSERT_DVD
            = "insert into dvds "
            + "(title, release_date, director, rating, dvd_note) "
            + "values (?, ?, ?, ?, ?)";

    private static final String SQL_REMOVE_DVD
            = "delete from dvds where dvd_id = ?";

    private static final String SQL_SELECT_DVD_BY_ID
            = "select * from dvds where dvd_id = ?";

    private static final String SQL_SELECT_ALL_DVD
            = "select * from dvds";

    private static final String SQL_UPDATE_DVD
            = "update dvds set "
            + "title = ?, "
            + "release_date = ?, "
            + "director = ?, "
            + "rating = ?, "
            + "dvd_note = ? "
            + "where dvd_id = ?";

    //Search Queries
    private static final String SQL_SELECT_DVD_BY_TITLE
            = "select * from dvd where title = ?";

    private static final String SQL_SELECT_DVD_BY_RELEASE_DATE
            = "select * from dvds where release_date = ?";

    private static final String SQL_SELECT_DVD_BY_DIRECTOR
            = "select * from dvds where director = ?";

    private static final String SQL_SELECT_DVD_BY_RATING
            = "select * from dvds where rating = ?";

    private static final String SQL_SELECT_DVD_BY_NOTE
            = "select * from dvds where dvd_note = ?";

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Dvd addDvd(Dvd dvd) {
        jdbcTemplate.update(SQL_INSERT_DVD,
                dvd.getTitle(),
                dvd.getReleaseDate(),
                dvd.getDirector(),
                dvd.getRating(),
                dvd.getNote());

        int dvdId = jdbcTemplate.queryForObject("select LAST_INSERT_ID()", Integer.class);
        dvd.setId(dvdId);
        return dvd;
    }

    @Override
    public void removeDvd(int id) {
        jdbcTemplate.update(SQL_REMOVE_DVD, id);
    }

    @Override
    public Dvd getDvd(int id) {
        try {
            return jdbcTemplate.queryForObject(SQL_SELECT_DVD_BY_ID, new DvdMapper(), id);
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    @Override
    public List<Dvd> getAllDvds() {
        try {
            return jdbcTemplate.query(SQL_SELECT_ALL_DVD, new DvdMapper());
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    @Override
    public void updateDvd(Dvd dvd) {
        jdbcTemplate.update(SQL_UPDATE_DVD,
                dvd.getTitle(),
                dvd.getReleaseDate(),
                dvd.getDirector(),
                dvd.getRating(),
                dvd.getNote(),
                dvd.getId());
    }

    @Override
    public List<Dvd> searchDvd(Map<SearchCategory, String> searchCriteria) {

        String title = searchCriteria.get(SearchCategory.TITLE);
        String releaseYear = searchCriteria.get(SearchCategory.RELEASE_YEAR);
        String director = searchCriteria.get(SearchCategory.DIRECTOR);
        String rating = searchCriteria.get(SearchCategory.RATING);
        String note = searchCriteria.get(SearchCategory.NOTES);

        if (title != null) {
            try {
                return jdbcTemplate.query(SQL_SELECT_DVD_BY_TITLE, new DvdMapper(), title);
            } catch (EmptyResultDataAccessException ex) {
                return null;
            }
        }

        if (releaseYear != null) {
            try {
                return jdbcTemplate.query(SQL_SELECT_DVD_BY_RELEASE_DATE, new DvdMapper(), releaseYear);
            } catch (EmptyResultDataAccessException ex) {
                return null;
            }
        }

        if (director != null) {
            try {
                return jdbcTemplate.query(SQL_SELECT_DVD_BY_DIRECTOR, new DvdMapper(), director);
            } catch (EmptyResultDataAccessException ex) {
                return null;
            }
        }

        if (rating != null) {
            try {
                return jdbcTemplate.query(SQL_SELECT_DVD_BY_RATING, new DvdMapper(), rating);
            } catch (EmptyResultDataAccessException ex) {
                return null;
            }
        }

        if (note != null) {
            try {
                return jdbcTemplate.query(SQL_SELECT_DVD_BY_NOTE, new DvdMapper(), note);
            } catch (EmptyResultDataAccessException ex) {
                return null;
            }
        }
        return null;
    }

    private static final class DvdMapper implements RowMapper<Dvd> {

        @Override
        public Dvd mapRow(ResultSet rs, int i) throws SQLException {
            Dvd dvd = new Dvd();
            dvd.setId(rs.getInt("dvd_id"));
            dvd.setTitle(rs.getString("title"));
            dvd.setReleaseDate(rs.getString("release_date"));
            dvd.setDirector(rs.getString("director"));
            dvd.setRating(rs.getString("rating"));
            dvd.setReleaseDate(rs.getString("release_date"));
            dvd.setNote(rs.getString("dvd_note"));

            return dvd;
        }
    }

}
