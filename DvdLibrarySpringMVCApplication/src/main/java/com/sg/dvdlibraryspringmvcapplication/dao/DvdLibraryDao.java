/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvdlibraryspringmvcapplication.dao;

import com.sg.dvdlibraryspringmvcapplication.model.Dvd;
import java.util.List;
import java.util.Map;

/**
 *
 * @author cjsm12
 */
public interface DvdLibraryDao {
    
    Dvd addDvd(Dvd dvd);
    
    void removeDvd(int id);
    
    Dvd getDvd(int id);
    
    List<Dvd> getAllDvds();
    
    void updateDvd(Dvd dvd);
    
    List<Dvd> searchDvd(Map<SearchCategory, String> searchCriteria);
    
}
