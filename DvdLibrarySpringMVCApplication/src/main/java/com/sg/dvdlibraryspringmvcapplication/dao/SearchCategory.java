/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvdlibraryspringmvcapplication.dao;

/**
 *
 * @author cjsm12
 */
public enum SearchCategory {
    
    TITLE,RELEASE_YEAR,DIRECTOR,RATING,NOTES;
    
}
