/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvdlibraryspringmvcapplication.dao;

/**
 *
 * @author cjsm12
 */
public class ErrorMessage {
    
    private String errorMessage;
    
    public void writeErrorMessage(String errorMessage){
        this.errorMessage = errorMessage;
    }
    
    public String getErrorMessage(){
        return this.errorMessage;
    }
    
}
