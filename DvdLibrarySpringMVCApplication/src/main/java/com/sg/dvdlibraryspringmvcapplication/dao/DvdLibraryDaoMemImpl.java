/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvdlibraryspringmvcapplication.dao;

import com.sg.dvdlibraryspringmvcapplication.model.Dvd;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 *
 * @author cjsm12
 */
public class DvdLibraryDaoMemImpl implements DvdLibraryDao {

    private Map<Integer, Dvd> dvdMap = new HashMap<>();

    public static int dvdId = 0;

    @Override
    public Dvd addDvd(Dvd dvd) {
        dvd.setId(dvdId);

        dvdId++;

        return dvdMap.put(dvd.getId(), dvd);
    }

    @Override
    public void removeDvd(int id) {
        dvdMap.remove(id);
    }

    @Override
    public Dvd getDvd(int id) {
        return dvdMap.get(id);
    }

    @Override
    public List<Dvd> getAllDvds() {
        return new ArrayList<>(dvdMap.values());
    }

    @Override
    public void updateDvd(Dvd dvd) {
        dvdMap.put(dvd.getId(), dvd);
    }

    @Override
    public List<Dvd> searchDvd(Map<SearchCategory, String> searchCriteria) {
        String title = searchCriteria.get(SearchCategory.TITLE);
        String releaseYear = searchCriteria.get(SearchCategory.RELEASE_YEAR);
        String director = searchCriteria.get(SearchCategory.DIRECTOR);
        String rating = searchCriteria.get(SearchCategory.RATING);
        String notes = searchCriteria.get(SearchCategory.NOTES);

        Predicate<Dvd> titlePredicate;
        Predicate<Dvd> releaseYearPredicate;
        Predicate<Dvd> directorPredicate;
        Predicate<Dvd> ratingPredicate;
        Predicate<Dvd> notesPredicate;

        Predicate<Dvd> falsePredicate = d -> {
            return false;
        };

        if (title == null) {
            titlePredicate = falsePredicate;
        } else {
            titlePredicate = d -> d.getTitle().equalsIgnoreCase(title);
            return dvdMap.values().stream()
                    .filter(titlePredicate)
                    .collect(Collectors.toList());
        }

        if (releaseYear == null) {
            releaseYearPredicate = falsePredicate;
        } else {
            releaseYearPredicate = d -> d.getReleaseDate().equalsIgnoreCase(releaseYear);
            return dvdMap.values().stream()
                    .filter(releaseYearPredicate)
                    .collect(Collectors.toList());
        }

        if (director == null) {
            directorPredicate = falsePredicate;
        } else {
            directorPredicate = d -> d.getDirector().equalsIgnoreCase(director);
            return dvdMap.values().stream()
                    .filter(directorPredicate)
                    .collect(Collectors.toList());
        }

        if (rating == null) {
            ratingPredicate = falsePredicate;
        } else {
            ratingPredicate = d -> d.getRating().equalsIgnoreCase(rating);
            return dvdMap.values().stream()
                    .filter(ratingPredicate)
                    .collect(Collectors.toList());
        }

        if (notes == null) {
            notesPredicate = falsePredicate;
        } else {
            notesPredicate = d -> d.getNote().equalsIgnoreCase(notes);
            return dvdMap.values().stream()
                    .filter(notesPredicate)
                    .collect(Collectors.toList());
        }
        return null;
    }
    
}
