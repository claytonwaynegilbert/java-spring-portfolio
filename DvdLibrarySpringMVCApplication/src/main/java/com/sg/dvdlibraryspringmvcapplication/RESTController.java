/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dvdlibraryspringmvcapplication;

import com.sg.dvdlibraryspringmvcapplication.dao.DvdLibraryDao;
import com.sg.dvdlibraryspringmvcapplication.dao.SearchCategory;
import com.sg.dvdlibraryspringmvcapplication.model.Dvd;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author cjsm12
 */
@Controller
public class RESTController {
    
    DvdLibraryDao dao;
    
    @Inject
    public RESTController(DvdLibraryDao dao){
        this.dao = dao;
    }
    
    @RequestMapping(value="/dvd", method=RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public Dvd createDvd(@RequestBody Dvd dvd){
            return dao.addDvd(dvd);
    }
    
    @RequestMapping(value="/dvd/{id}", method=RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteDvd(@PathVariable int id){
        dao.removeDvd(id);
    }
    
    @RequestMapping(value="/dvd/{id}", method=RequestMethod.GET)
    @ResponseBody
    public Dvd getDvd(@PathVariable int id){
        return dao.getDvd(id);
    }
    
    @RequestMapping(value="/dvds", method=RequestMethod.GET)
    @ResponseBody
    public List<Dvd> getAllDvds(){
        return dao.getAllDvds();
    }
    
    @RequestMapping(value="/dvd/{id}", method=RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateDvd(@PathVariable int id, @RequestBody Dvd dvd) throws UpdateIntegrityException{
        if(dvd.getId() != id){
            throw new UpdateIntegrityException("The ID's of the incoming Dvd must match the id on the path.");
        }
        
        dao.updateDvd(dvd);
    }
    
    @RequestMapping(value="/search/dvds", method=RequestMethod.POST)
    @ResponseBody
    public List<Dvd> searchForDvd(@RequestBody Map<String, String> searchCriteria){
        
        Map<SearchCategory, String> searchMap = new HashMap<>();
        
        String searchField = searchCriteria.get("searchField");
        String searchCategory = searchCriteria.get("searchCategory");
        
        if(searchCategory.equalsIgnoreCase("title")){
            searchMap.put(SearchCategory.TITLE, searchField);
        }
        
        if(searchCategory.equalsIgnoreCase("releaseYear")){
            searchMap.put(SearchCategory.RELEASE_YEAR, searchField);
        }
        
        if(searchCategory.equalsIgnoreCase("director")){
            searchMap.put(SearchCategory.DIRECTOR, searchField);
        }
        
        if(searchCategory.equalsIgnoreCase("rating")){
            searchMap.put(SearchCategory.RATING, searchField);
        }
        
        if(searchCategory.equalsIgnoreCase("notes")){
            searchMap.put(SearchCategory.NOTES, searchField);
        }
        
        return dao.searchDvd(searchMap);
        
    }
}
