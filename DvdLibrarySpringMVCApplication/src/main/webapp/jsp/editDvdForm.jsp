<%-- 
    Document   : contactPage
    Created on : Jun 7, 2017, 4:32:37 AM
    Author     : cjsm12
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!-- Directive for Spring Form tag libraries -->
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">        
        <title>Create Contact</title>
        <style>
            #create-contact-form{
                margin-left: 50px;
            }
            
            .create-dvd-forms-spacing{
                margin-bottom: 20px;
            }
            
            #buttons{
                margin-left: 80px;
            }
        </style>
    </head>
    <body>
        
        <div class="container">
            <div class="row" id="create-contact-form">
                <div>
                    <h1>Edit DVD</h1>
                    <hr>
                </div>
                <sf:form id="createContactForm" 
                         class="form form-horizontal"
                         modelAttribute="dvd"
                         method="POST"
                         action="editDvd">
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="add-title" class="col-md-4 control-label">Title:</label>
                        <div class="col-md-8">
                            <sf:input type="input" path="title" class="form-control create-dvd-forms-spacing" id="add-title" placeholder="Add Title"/>
                            <sf:errors path="title" cssClass="error"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-year" class="col-md-4 control-label">Release Year:</label>
                        <div class="col-md-8">
                            <sf:input type="input" path="releaseDate" class="form-control create-dvd-forms-spacing" id="add-year" placeholder="Add Release Year"/>
                            <sf:errors path="releaseDate" cssClass="error"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-director" class="col-md-4 control-label">Director:</label>
                        <div class="col-md-8">
                            <sf:input type="input" path="director" class="form-control create-dvd-forms-spacing" id="add-director" placeholder="Add Director"/>
                            <sf:errors path="director" cssClass="error"></sf:errors>            
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-rating" class="col-md-4 control-label">Rating:</label>
                        <div class="col-md-8">
                            <sf:input type="input" path="rating" class="form-control create-dvd-forms-spacing" id="add-rating" placeholder="Add Rating"/>
                            <sf:errors path="rating" cssClass="error"></sf:errors>                   
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-notes" class="col-md-4 control-label">Notes:</label>
                        <div class="col-md-8">
                            <sf:textarea rows="5" cols="25" path="note" class="form-control create-dvd-forms-spacing" id="add-notes" placeholder="Add Notes Here"></sf:textarea>
                            <sf:errors path="note" cssClass="error"></sf:errors>   
                            <sf:hidden path="id"/>
                        </div>
                    </div>
                    <div class="form-group" id="buttons">
                        <input type="submit" class="btn btn-default" id="createContactButton" value="Edit Contact"/>
                        <a href="${pageContext.request.contextPath}/cancel">
                        <button class="btn btn-default" id="cancelButton">Cancel</button>
                        </a>
                    </div>
                </div>
                </sf:form>
            </div>
        </div>
        
        
            <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
            <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    </body>
</html>
