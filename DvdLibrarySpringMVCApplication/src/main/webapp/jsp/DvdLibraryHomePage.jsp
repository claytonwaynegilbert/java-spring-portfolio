<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!-- Directive for Spring Form tag libraries -->
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>DVD Library</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">        
        
        <style>
            #top-nav-bar{
                margin-top: 30px;
            }

            #dvdTable{
                margin-left: 50px;
            }

            th{
                text-align: center;
            }

        </style>
    </head>
    <body>
        <div class="container">
            <div class="row col-md-offset-1" id="top-nav-bar">
                <div class="col-md-5">
                    <a href="${pageContext.request.contextPath}/displayCreateDvdPage">
                        <button class="btn btn-default" id="createDvdButton">Create Dvd</button>
                    </a> 
                </div>
                <form id="searchForm" role="form">
                    
                    <div class="col-md-1">
                        <input type="button" class="btn btn-default" id="searchDvdButton" value="Search"/>
                    </div>

                    <div class="col-md-2">
                        <select class="form-control" id="selectCategory" name="searchCategory">
                            <option selected disabled="disabled">Select Category</option>
                            <option value="title">Title</option>
                            <option value="releaseYear">Release Year</option>
                            <option value="director">Director</option>
                            <option value="rating">Rating</option>
                            <option value="notes">Notes</option>
                        </select>
                    </div>

                    <div class="col-md-3">
                        <input type="text" id="searchTerm" class="form-control" placeholder="Search Term"/>
                    </div> 
                    
                </form>
            </div>
            <hr/>

            <ul id="errorMessages" class="list-group-item-danger"></ul>
            <div class="row">
                <div class="col-md-11">
                    <table id="dvdTable" class="table table-hover table-bordered">
                        <tr>
                            <th width="15%">Title</th>
                            <th width="10%">Release Date</th>
                            <th width="15%">Director</th>
                            <th width="10%">Rating</th>
                            <th width="25%">Notes</th>
                            <th width="15%"></th>
                        </tr> 
                        <tbody id="dvdBody">
                            <c:forEach var="dvd" items="${dvds}">
                                <tr>
                                    <td>
                                        <a href="${pageContext.request.contextPath}/displayDvdDetails?dvdId=${dvd.id}">
                                            <c:out value="${dvd.title}"/>
                                        </a>
                                    </td>
                                    <td>
                                        <c:out value="${dvd.releaseDate}"/>
                                    </td>
                                    <td>
                                        <c:out value="${dvd.director}"/>
                                    </td>
                                    <td>
                                        <c:out value="${dvd.rating}"/>
                                    </td>
                                     <td>
                                        <c:out value="${dvd.note}"/>
                                    </td>
                                    <td>
                                        <a href="${pageContext.request.contextPath}/displayEditDvdPage?dvdId=${dvd.id}">Edit</a> | <a href="${pageContext.request.contextPath}/deleteDvd?dvdId=${dvd.id}">Delete</a>
                                    </td>
                                </tr>
                            </c:forEach> 
                        </tbody>
                    </table> 
                </div>
            </div>


        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/home.js"></script>

    </body>
</html>

