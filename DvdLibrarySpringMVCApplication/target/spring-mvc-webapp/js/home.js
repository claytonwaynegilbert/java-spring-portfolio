/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {

    $('#searchDvdButton').click(function (event) {
        var searchField = $('#searchTerm').val();
        var searchCategory = $('#selectCategory option:selected').val();

            $.ajax({
                type: 'POST',
                url: 'search/dvds',
                data: JSON.stringify({
                    searchCategory: searchCategory,
                    searchField: searchField
                }),
                headers: {
                    'Accept': "application/json",
                    'Content-Type': "application/json"
                },
                'dataType': "json",
                success: function (data) {
                    clearTable();
                    $.each(data, function(index, dvd) {
                        console.log(dvd);
                        var tableRow = "<tr>";
                            tableRow += "<td>" + dvd.title + "</td>";
                            tableRow += "<td>" + dvd.releaseDate + "</td>";
                            tableRow += "<td>" + dvd.director + "</td>";
                            tableRow += "<td>" + dvd.rating + "</td>";
                            tableRow += "<td>" + dvd.note + "</td>";
                            tableRow += "</tr>";
                            
                    $('#dvdBody').append(tableRow);
                            
                    });
                },
                error: function () {
                    $('#errorMessages')
                        .append($('<li>')
                                .attr({class: 'list-group-item list-group-item-danger'})
                                .text('Error calling web service.  Please try again later.'));
                },
         });
    });

    function clearErrorMessages() {
        $('#errorMessages').empty();
    }
    
    function clearTable(){
        $('#dvdBody').empty();
    }
    
});

