<%-- 
    Document   : DvdDetails
    Created on : Jun 7, 2017, 6:43:02 PM
    Author     : cjsm12
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>DVD Details</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">        
        <style>
            span{
                margin-left: 20px;
            }
            
            p{
                margin-left: 30px;
            }
            
            #backButton{
                margin-left: 30px;
                margin-top: 30px;
            }
        </style>
    </head>
    <body>
        <h1>${dvd.title}</h1>
        <hr>
        <p>
            Release Year: <span><c:out value="${dvd.releaseDate}"></c:out></span>
        </p>
        <p>
            Director: <span><c:out value="${dvd.director}"></c:out></span>
        </p>
        <p>
            Rating: <span><c:out value="${dvd.rating}"></c:out></span>
        </p>
        <p>
            Notes: <span><c:out value="${dvd.note}"></c:out></span>
        </p>
        
        <a href="${pageContext.request.contextPath}/cancel">
        <button id="backButton" class="btn btn-default">Back</button>
        </a>
    </body>
</html>
