DROP DATABASE IF EXISTS dvd_library;

CREATE DATABASE IF NOT EXISTS dvd_library;

USE dvd_library;

CREATE TABLE IF NOT EXISTS dvds (
dvd_id int(11) NOT NULL auto_increment,
title varchar(50) NOT NULL,
release_date varchar(4) NOT NULL,
director varchar(15) NOT NULL,
rating varchar(5) NOT NULL,
dvd_note varchar(100) NULL,
PRIMARY KEY(dvd_id)
);



