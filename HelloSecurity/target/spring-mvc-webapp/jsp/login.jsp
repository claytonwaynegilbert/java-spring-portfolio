<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">        
        <title>Login</title>
    </head>
    <body>
        <div class="container">
            <h1>Hello Security</h1>
            <hr>
            <h2>Login Page</h2>

            <c:if test="${param.login_error == 1}">
                <h3>Wrong ID or Password</h3>
            </c:if>

            <form id="login-form"
                  class="form-horizontal"
                  action="j_spring_security_check"
                  method="POST">

                <div class="from-group">
                    <label for="j_username" 
                           class="control-label col-md-4">Username:</label>
                    <div class="col-md-5">
                        <input type="text" 
                               id="j_username" 
                               class="form-control"
                               name="j_username"
                               placeholder="Username"/>
                    </div>
                </div>

                <div class="from-group">
                    <label for="j_username" 
                           class="control-label col-md-4">Password:</label>
                    <div class="col-md-5">
                        <input type="text" 
                               id="j_password" 
                               class="form-control"
                               name="j_password"
                               placeholder="Password"/>
                    </div>
                </div>

                <div class="from-group">
                    <div class="col-md-offset-4 col-md-8">
                        <input type="submit" 
                               id="login-button" 
                               class="btn btn-primary"
                               value="Sign In"/>
                    </div>
                </div>
            </form>
        </div>
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    </body>
</html>
