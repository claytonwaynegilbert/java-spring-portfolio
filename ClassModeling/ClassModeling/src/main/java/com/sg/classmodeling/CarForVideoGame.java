/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.classmodeling;

/**
 *
 * @author cjsm12
 */
public class CarForVideoGame {
    
    private String model;
    private String makel;
    private String cylinderCount;
    private int releaseYear;
    private double topSpeed;
    private double zeroToSixtyTime;
    private double horsePower;
    private double torquePerSquareInch;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getMakel() {
        return makel;
    }

    public void setMakel(String makel) {
        this.makel = makel;
    }

    public String getCylinderCount() {
        return cylinderCount;
    }

    public void setCylinderCount(String cylinderCount) {
        this.cylinderCount = cylinderCount;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
    }

    public double getTopSpeed() {
        return topSpeed;
    }

    public void setTopSpeed(double topSpeed) {
        this.topSpeed = topSpeed;
    }

    public double getZeroToSixtyTime() {
        return zeroToSixtyTime;
    }

    public void setZeroToSixtyTime(double zeroToSixtyTime) {
        this.zeroToSixtyTime = zeroToSixtyTime;
    }

    public double getHorsePower() {
        return horsePower;
    }

    public void setHorsePower(double horsePower) {
        this.horsePower = horsePower;
    }

    public double getTorquePerSquareInch() {
        return torquePerSquareInch;
    }

    public void setTorquePerSquareInch(double torquePerSquareInch) {
        this.torquePerSquareInch = torquePerSquareInch;
    }
    
}
