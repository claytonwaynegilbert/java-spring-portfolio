/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.classmodeling;

/**
 *
 * @author cjsm12
 */
public class IceCreamStoreInventory {
    
    private String skuNumber;
    private String manufacturer;
    private String name;
    private double price;
    private double wholesalePrice;
    private int quanity;
    private int quanityOnBackOrder;

    public String getSkuNumber() {
        return skuNumber;
    }

    public void setSkuNumber(String skuNumber) {
        this.skuNumber = skuNumber;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getWholesalePrice() {
        return wholesalePrice;
    }

    public void setWholesalePrice(double wholesalePrice) {
        this.wholesalePrice = wholesalePrice;
    }

    public int getQuanity() {
        return quanity;
    }

    public void setQuanity(int quanity) {
        this.quanity = quanity;
    }

    public int getQuanityOnBackOrder() {
        return quanityOnBackOrder;
    }

    public void setQuanityOnBackOrder(int quanityOnBackOrder) {
        this.quanityOnBackOrder = quanityOnBackOrder;
    }
     
}
