/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.classmodeling;

/**
 *
 * @author cjsm12
 */
public class IceCreamDairyStore {
    
    private String manufacturer;
    private String flavor;
    private double gallonsAvailable;
    private double pricePerServing;

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getFlavor() {
        return flavor;
    }

    public void setFlavor(String flavor) {
        this.flavor = flavor;
    }

    public double getGallonsAvailable() {
        return gallonsAvailable;
    }

    public void setGallonsAvailable(double gallonsAvailable) {
        this.gallonsAvailable = gallonsAvailable;
    }

    public double getPricePerServing() {
        return pricePerServing;
    }

    public void setPricePerServing(double pricePerServing) {
        this.pricePerServing = pricePerServing;
    }
    
}
