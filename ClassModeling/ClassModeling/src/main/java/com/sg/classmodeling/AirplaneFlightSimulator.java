/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.classmodeling;

/**
 *
 * @author cjsm12
 */
public class AirplaneFlightSimulator {
    
    private double topSpeed;
    private int engineCount;
    private String engineType;
    private double weight;
    private String productionYear;
    private double maxElevation;

    public double getTopSpeed() {
        return topSpeed;
    }

    public void setTopSpeed(double topSpeed) {
        this.topSpeed = topSpeed;
    }

    public int getEngineCount() {
        return engineCount;
    }

    public void setEngineCount(int engineCount) {
        this.engineCount = engineCount;
    }

    public String getEngineType() {
        return engineType;
    }

    public void setEngineType(String engineType) {
        this.engineType = engineType;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getProductionYear() {
        return productionYear;
    }

    public void setProductionYear(String productionYear) {
        this.productionYear = productionYear;
    }

    public double getMaxElevation() {
        return maxElevation;
    }

    public void setMaxElevation(double maxElevation) {
        this.maxElevation = maxElevation;
    }
    
    public void startEngine(){
        System.out.println("Engine started");
    }
    
    public void turnEngineOff(){
        System.out.println("Engine off");
    }
    
    public void accelerate(){
        System.out.println("Accelerating...");
    }
    
    public void slowDown(){
        System.out.println("Slowing down...");
    }
    
    public void enableCopilot(){
        System.out.println("Copilot anabled");
    }
    
    public void dropLandingGear(){
        System.out.println("Landing gear dropped");
    }
    
    public void raiseLandingGear(){
        System.out.println("Landing gear raised");
    }
    
}
