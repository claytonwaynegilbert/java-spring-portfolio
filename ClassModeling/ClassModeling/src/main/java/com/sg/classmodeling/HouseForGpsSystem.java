/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.classmodeling;

/**
 *
 * @author cjsm12
 */
public class HouseForGpsSystem {
    
    private String latitudeCoordinate;
    private String longiudeCoordinate;
    private String state;
    private String city;
    private int zipCode;
    private String address;

    public String getLatitudeCoordinate() {
        return latitudeCoordinate;
    }

    public void setLatitudeCoordinate(String latitudeCoordinate) {
        this.latitudeCoordinate = latitudeCoordinate;
    }

    public String getLongiudeCoordinate() {
        return longiudeCoordinate;
    }

    public void setLongiudeCoordinate(String longiudeCoordinate) {
        this.longiudeCoordinate = longiudeCoordinate;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    
}
