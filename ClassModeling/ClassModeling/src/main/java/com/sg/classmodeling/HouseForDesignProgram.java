/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.classmodeling;

/**
 *
 * @author cjsm12
 */
public class HouseForDesignProgram {
    
    private int bedroomCount;
    private int windowCount;
    private int doorCount;
    private int bathroomCount;
    private int floorCount;
    private int totalSquareFootage;
    private boolean hasBasement;
    private boolean hasAttic;
    private boolean hasChimney;

    public int getBedroomCount() {
        return bedroomCount;
    }

    public void setBedroomCount(int bedroomCount) {
        this.bedroomCount = bedroomCount;
    }

    public int getWindowCount() {
        return windowCount;
    }

    public void setWindowCount(int windowCount) {
        this.windowCount = windowCount;
    }

    public int getDoorCount() {
        return doorCount;
    }

    public void setDoorCount(int doorCount) {
        this.doorCount = doorCount;
    }

    public int getBathroomCount() {
        return bathroomCount;
    }

    public void setBathroomCount(int bathroomCount) {
        this.bathroomCount = bathroomCount;
    }

    public int getFloorCount() {
        return floorCount;
    }

    public void setFloorCount(int floorCount) {
        this.floorCount = floorCount;
    }

    public int getTotalSquareFootage() {
        return totalSquareFootage;
    }

    public void setTotalSquareFootage(int totalSquareFootage) {
        this.totalSquareFootage = totalSquareFootage;
    }

    public boolean isHasBasement() {
        return hasBasement;
    }

    public void setHasBasement(boolean hasBasement) {
        this.hasBasement = hasBasement;
    }

    public boolean isHasAttic() {
        return hasAttic;
    }

    public void setHasAttic(boolean hasAttic) {
        this.hasAttic = hasAttic;
    }

    public boolean isHasChimney() {
        return hasChimney;
    }

    public void setHasChimney(boolean hasChimney) {
        this.hasChimney = hasChimney;
    }
    
    
    
    
    
}
