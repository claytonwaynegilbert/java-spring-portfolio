/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.statecapitals;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author cjsm12
 */
public class StateCapitals {

    public static void main(String[] args) {
        
        /*
        This program will be a Java Console Application called StateCapitals.
        Create a HashMap to hold the names of all the states and their corresponding 
        capital names.  (State name is the key, capital name is the value.)
        Load the HashMap with each state/capital pair.  (This should be hard-coded.)
        Print all of the state names to the screen.  (Hint: Get the keys from the map and
        then print each state name one by one.)
        Print all of the capital names to the screen.  (Hint: Get the values from the map 
        and then print each capital name to the screen one by one.)
        Print each state along with its capital to the screen.  (Hint: Use the key set to 
        get each value from the map one by one, printing both the key and value as you go.)
        */

        //Create HashMap instance 
        Map<String, String> stateCapitals = new HashMap<>();

        //Populate map with States and Capitals
        stateCapitals.put("Alabama", "Montgomery");
        stateCapitals.put("Alaska", "Juneau");
        stateCapitals.put("Arizona", "Phoenix");
        stateCapitals.put("Arkansas", "Little Rock");
        stateCapitals.put("California", "Sacremento");
        stateCapitals.put("Colorado", "Denver");
        stateCapitals.put("Conneticut", "Hartford");
        stateCapitals.put("Deleware", "Dover");
        stateCapitals.put("Florida", "Tallahassee");
        stateCapitals.put("Georgia", "Atlanta");
        stateCapitals.put("Hawaii", "Honolulu");
        stateCapitals.put("Idaho", "Boise");
        stateCapitals.put("Illinois", "Springfield");
        stateCapitals.put("Indiana", "Indianapolis");
        stateCapitals.put("Iowa", "Desmoines");
        stateCapitals.put("Kansas", "Topeka");
        stateCapitals.put("Kentucky", "Frankfort");
        stateCapitals.put("Louisiana", "Baton Rouge");
        stateCapitals.put("Maine", "Augusta");
        stateCapitals.put("Maryland", "Annapolis");
        stateCapitals.put("Massachusettes", "Boston");
        stateCapitals.put("Michigan", "Lansing");
        stateCapitals.put("Minnesota", "St. Paul");
        stateCapitals.put("Mississippi", "Jackson");
        stateCapitals.put("Missouri", "Jefferson City");
        stateCapitals.put("Montana", "Helena");
        stateCapitals.put("Nebraska", "Lincoln");
        stateCapitals.put("Nevada", "Carson City");
        stateCapitals.put("New Hamphsire", "Concord");
        stateCapitals.put("New Jersey", "Trenton");
        stateCapitals.put("New Mexico", "Santa Fe");
        stateCapitals.put("New York", "Albany");
        stateCapitals.put("North Carolina", "Raleigh");
        stateCapitals.put("North Dakota", "Bismark");
        stateCapitals.put("Oklahoma", "Oklahoma City");
        stateCapitals.put("Ohio", "Columbus");
        stateCapitals.put("Oregon", "Salem");
        stateCapitals.put("Pennsylvania", "Harrisburg");
        stateCapitals.put("Rhode Island", "Providence");
        stateCapitals.put("South Carolina", "Columbia");
        stateCapitals.put("South Dakota", "Pierre");
        stateCapitals.put("Tennessee", "Nashville");
        stateCapitals.put("Texas", "Austin");
        stateCapitals.put("Utah", "Salt Lake City");
        stateCapitals.put("Vermont", "Montpieler");
        stateCapitals.put("Virginia", "Richmond");
        stateCapitals.put("Washington", "Olympia");
        stateCapitals.put("West Virgina", "Charleston");
        stateCapitals.put("Wisconsin", "Madison");
        stateCapitals.put("Wyoming", "Cheyenne");

        //Printing all States header
        System.out.println("---STATES---");
        System.out.println("============");
        
        //1st way of solving problem
        //Store all keys(States) in a Set collection
        Set<String> states = stateCapitals.keySet();
        
        //Enhance for loop through Set and displaying them
        for(String currentState : states){
            System.out.println(currentState);
        }
        
        //2nd solution 
        //Just use an enhanced for loop on the original collection and call
        //keySet method inside loop and print out states that are stored in the 
        //currentState variable
        for(String currentState : stateCapitals.keySet()){
            System.out.println(currentState);
        }
        
        //Printing out Capitals of States
        System.out.println("---CAPITALS---");
        System.out.println("==============");
        
        //1st method
        Collection<String> capitals = stateCapitals.values();
        
        for(String currentCapital : capitals){
            System.out.println(currentCapital);
        }
        
        //2nd method
        for(String currentCapital : stateCapitals.values()){
            System.out.println(currentCapital);
        }
        
        //Printing States and Capitals
        System.out.println("---STATE and CAPITAL---");
        System.out.println("=======================");
        
        //For each loop to go through all keys of a Map and then using each
        //key that gets stored as a way of getting the current capital associated
        //with the key inside the loop
        for(String currentState : stateCapitals.keySet()){
            System.out.println(currentState + " | " + stateCapitals.get(currentState));
        }
        
    }

}
