<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Home</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/home.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <h2>Welcome to Flooring Mastery</h2>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="active">
                        <a href="${pageContext.request.contextPath}/displayHomePage">Home
                        </a>
                    </li>
                    <sec:authorize access="hasRole('ROLE_USER')">
                        <li role="presentation">
                            <a href="${pageContext.request.contextPath}/main/manager">Manager
                            </a>
                        </li>
                    </sec:authorize>
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <li role="presentation">
                            <a href="${pageContext.request.contextPath}/users/viewUsers">Users
                            </a>
                        </li>
                    </sec:authorize>
                </ul>    
            </div>

            <c:if test="${param.login_error == 1}">
                <h3>Wrong id or password!</h3>
            </c:if>

            <c:if test="${pageContext.request.userPrincipal.name != null}">
                <p>Hello : ${pageContext.request.userPrincipal.name}
                    | <a href="<c:url value="/j_spring_security_logout" />" > Logout</a>
                </p>
            </c:if>

            <div id="login-form" class="col-md-6">
                <h2>Login</h2>
                <form class="form-horizontal" 
                      role="form" 
                      method="POST" 
                      action="j_spring_security_check">
                    <div class="form-group">
                        <label for="username" 
                               class="col-md-4 control-label">Username:</label>
                        <div class="col-md-8">
                            <input type="text"
                                   id="username"
                                   class="form-control" 
                                   name="j_username" 
                                   placeholder="Username"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" 
                               class="col-md-4 control-label">Password:</label>
                        <div class="col-md-8">
                            <input type="password" 
                                   id="password"
                                   class="form-control" 
                                   name="j_password" 
                                   placeholder="Password"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-4 col-md-8">
                            <input type="submit" 
                                   class="btn btn-primary" 
                                   id="search-button" 
                                   value="Sign In"/>
                        </div>
                    </div>
                </form>  
            </div>

            <div class="col-md-6">
                <div id="home-summary-box">
                    <p id="home-description">
                        Welcome to Flooring Mastery! A CRUD manager mock application
                        for a fictitious lumber business. It is used to manage orders
                        put into the system by an employee. It allows the creation, deletion,
                        retrieving, and editing of orders. Also available is the ability of
                        creating users if you are logged into the system as an Admin. This
                        is a full stack Spring application connected to a MySQL database along
                        with Spring Security.
                    </p>
                </div>
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>

