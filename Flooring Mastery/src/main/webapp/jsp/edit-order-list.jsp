<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Order Review</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/displayHomePage">Home
                        </a>
                    </li>
                    <sec:authorize access="hasRole('ROLE_USER')">
                        <li role="presentation">
                            <a href="${pageContext.request.contextPath}/main/manager">Manager
                            </a>
                        </li>
                    </sec:authorize>
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <li role="presentation">
                            <a href="${pageContext.request.contextPath}/users/viewUsers">Users
                            </a>
                        </li>
                    </sec:authorize>
                </ul>    
            </div>      

            <h1>Orders</h1>
            <hr>
            <div class="col-md-8">
                <c:choose>
                    <c:when test="${not empty orders}">
                        <table id="view-orders-table"
                               class="table-hover">
                            <tr>
                                <th width="5%">ID</th>
                                <th width="15%">First</th>
                                <th width="15%">Last</th>
                                <th width="20%">Date</th>
                                <th width="20%">Product</th>
                                <th width="15%">Price</th>
                                <th width="10%"></th>
                            </tr>
                            <c:forEach var="order" items="${orders}">
                                <tr>
                                    <td><c:out value="${order.orderId}"/></td>
                                    <td><c:out value="${order.firstName}"/></td>
                                    <td><c:out value="${order.lastName}"/></td>
                                    <td><c:out value="${order.orderDate}"/></td>
                                    <td><c:out value="${order.product.productName}"/></td>
                                    <td><c:out value="$${order.totalCost}"/></td>
                                    <td>
                                        <a href="${pageContext.request.contextPath}/edit/displayEditOrderForm?orderId=${order.orderId}">
                                            Edit
                                        </a>
                                    </td>

                                </tr>
                            </c:forEach>
                        </table>
                    </c:when>
                    <c:otherwise>
                        <c:out value="There are no orders associated with that name"/>
                    </c:otherwise>
                </c:choose>

                <div class="form-group">
                    <a href="${pageContext.request.contextPath}/main/manager">
                        <button id="delete-order-list-button"
                                class="btn btn-primary">Cancel
                        </button>
                    </a>
                </div>
            </div>

        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>


