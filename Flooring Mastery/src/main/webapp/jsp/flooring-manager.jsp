<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Manager</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/flooring-manager.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/displayHomePage">Home
                        </a>
                    </li>
                    <sec:authorize access="hasRole('ROLE_USER')">
                        <li role="presentation">
                            <a href="${pageContext.request.contextPath}/main/manager">Manager
                            </a>
                        </li>
                    </sec:authorize>
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <li role="presentation">
                            <a href="${pageContext.request.contextPath}/users/viewUsers">Users
                            </a>
                        </li>
                    </sec:authorize>
                </ul>    
            </div>
                        
                <h1 id="manager-summary-header">Flooring Manager</h1>

                <div id="manager-summary-box">
                    <p id="manager-summary-text">
                        This is where you manage all your orders, such as creating, 
                        deleting, editing, and viewing all the orders you have placed.
                    </p>
                </div>

                <div id="manager-options">
                    <div id="manager-options-box">
                        <a href="${pageContext.request.contextPath}/create/displayCreateOrderForm">
                            <button id="creat-order-button"
                                    class="btn btn-primary btn-lg">Create Order
                            </button>
                        </a>
                        <a href="${pageContext.request.contextPath}/edit/displaySearchOrderForm">
                            <button id="edit-order-button"
                                    class="btn btn-primary btn-lg">Edit Order
                            </button>
                        </a>
                        <br><br>
                        <a href="${pageContext.request.contextPath}/delete/displaySearchOrderForm">
                            <button id="delete-order-button"
                                    class="btn btn-primary btn-lg">Delete Order
                            </button>
                        </a>
                        <a href="${pageContext.request.contextPath}/view/viewAllOrders">
                            <button id="view-order-button"
                                    class="btn btn-primary btn-lg">View Orders
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>
