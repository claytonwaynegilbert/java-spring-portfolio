/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.controller;

import com.sg.flooringmastery.dto.Order;
import com.sg.flooringmastery.service.OrderService;
import java.util.List;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author cjsm12
 */
@Controller
@RequestMapping("/view")
public class DisplayController {

    private OrderService service;

    @Inject
    public DisplayController(OrderService service) {
        this.service = service;
    }
    
    @RequestMapping(value = "/viewAllOrders", method = RequestMethod.GET)
    public String viewAllOrders(Model model) {
        List<Order> allOrders = service.getAllOrders();
        model.addAttribute("orders", allOrders);
        
        return "orders-view";
    }
}
