/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.controller;

import com.sg.flooringmastery.dao.ProductDao;
import com.sg.flooringmastery.dao.TaxDao;
import com.sg.flooringmastery.dto.Order;
import com.sg.flooringmastery.dto.Product;
import com.sg.flooringmastery.dto.Tax;
import com.sg.flooringmastery.service.OrderService;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author cjsm12
 */
@Controller
@RequestMapping("/edit")
public class EditController {
    
    private OrderService service;
    private ProductDao productDao;
    private TaxDao taxDao;
    
    @Inject
    public EditController(OrderService service, ProductDao productDao, TaxDao taxDao){
        this.service = service;
        this.productDao = productDao;
        this.taxDao = taxDao;
    }

    @RequestMapping(value = "/displaySearchOrderForm", method = RequestMethod.GET)
    public String displaySearchOrderForm(Model model) {

        return "edit-search-order-form";
    }
    
    @RequestMapping(value = "/displayEditOrderList", method=RequestMethod.POST)
    public String displayEditOrderList(HttpServletRequest request, Model model){
        String lastName = request.getParameter("lastName");
        List<Order> matchingOrders = service.getAllOrdersByName(lastName);
        model.addAttribute("orders", matchingOrders);
        
        return "edit-order-list";
    }

    @RequestMapping(value = "/displayEditOrderForm", method = RequestMethod.GET)
    public String displayEditOrderForm(HttpServletRequest request, Model model) {
        String id = request.getParameter("orderId");
        int orderId = Integer.parseInt(id);
        Order matchingOrder = service.getOrderById(orderId);
        List<Product> allProducts = productDao.getAllProducts();
        List<Tax> allTaxes = taxDao.getAllTaxes();
        model.addAttribute("order", matchingOrder);
        model.addAttribute("products", allProducts);
        model.addAttribute("states", allTaxes);
        
        return "edit-order-form";
    }
    
    @RequestMapping(value="/editOrder", method=RequestMethod.POST)
    public String editOrder(@Valid @ModelAttribute("order") Order order, BindingResult results){
        if(results.hasErrors()){
            return "edit-order-form";
        }
        service.updateOrder(order);
        
        return "redirect:/main/manager";
    }
}
