/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.controller;

import com.sg.flooringmastery.dao.UserDao;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author cjsm12
 */
@Controller
@RequestMapping("/users")
public class UserController {
    
    private UserDao dao;
    
    @Inject
    public UserController(UserDao dao){
        this.dao = dao;
    }
    
    @RequestMapping(value="/viewUsers", method=RequestMethod.GET)
    public String displayUserList(){
        
        return "users";
    }
    
    @RequestMapping(value="/displayCreateUserForm", method=RequestMethod.GET)
    public String displayUserForm(){
        
        return "create-user-form";
    }
    
    @RequestMapping(value="/createUser", method=RequestMethod.POST)
    public String createUser(){
        
        return "redirect:/displayHomePage";
    }
    
}
