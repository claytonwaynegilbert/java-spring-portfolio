/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.controller;

import com.sg.flooringmastery.dto.Order;
import com.sg.flooringmastery.service.OrderService;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author cjsm12
 */

@Controller
@RequestMapping("/delete")
public class DeleteController {
    
    private OrderService service;
    
    @Inject
    public DeleteController(OrderService service){
        this.service = service;
    }

    @RequestMapping(value = "/displaySearchOrderForm", method = RequestMethod.GET)
    public String displayDeleteOrderForm() {
        return "delete-search-order-form";
    }
    
    @RequestMapping(value="/deleteOrderList", method=RequestMethod.POST)
    public String displayDeleteOrderList(HttpServletRequest request, Model model){
        String lastName = request.getParameter("lastName");
        List<Order> matchingOrders = service.getAllOrdersByName(lastName);
        model.addAttribute("orders", matchingOrders);
        
        return "delete-order-list";
    }
    
    @RequestMapping(value="/deleteOrder", method=RequestMethod.DELETE)
    public String deleteOrder(HttpServletRequest request){
        String id = request.getParameter("orderId");
        int orderId = Integer.parseInt(id);
        service.removeOrder(orderId);
        
        return "redirect:/delete/deleteOrderList";
    }
}
