/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dao;

import com.sg.flooringmastery.dto.Product;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author cjsm12
 */
public class ProductDaoDatabaseImpl implements ProductDao {

    private static final String SQL_INSERT_INTO_PRODUCTS = "insert into "
            + "products(product_name, cost_per_sq_ft, labor_cost_per_sq_ft) "
            + "values(?, ?, ?)";

    private static final String SQL_REMOVE_FROM_PRODUCTS_BY_NAME = "delete from products "
            + "where product_name = ?";

    private static final String SQL_UPDATE_PRODUCTS = "update products set "
            + "product_name = ?, "
            + "cost_per_sq_ft = ?, "
            + "labor_cost_per_sq_ft = ? "
            + "where product_id = ?";

    private static final String SQL_SELECT_ALL_PRODUCTS = "select * from products";

    private static final String SQL_SELECT_PRODUCT_BY_ID = "select * from products where product_id = ?";

    private static final String SQL_SELECT_PRODUCT_BY_NAME = "select * from products where product_name = ?";

    private JdbcTemplate jdbcTemplate;

    public ProductDaoDatabaseImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Product addProduct(Product product) {
        jdbcTemplate.update(SQL_INSERT_INTO_PRODUCTS, product.getProductName(),
                product.getCostPerSquareFoot(),
                product.getLaborCostPerSquareFoot());

        int product_id = jdbcTemplate.queryForObject("select LAST_INSERT_ID()", Integer.class);
        product.setProductId(product_id);

        return product;
    }

    @Override
    public void deleteProduct(String productName) {
        jdbcTemplate.update(SQL_REMOVE_FROM_PRODUCTS_BY_NAME, productName);
    }

    @Override
    public void updateProduct(Product updatedProduct) {
        jdbcTemplate.update(SQL_UPDATE_PRODUCTS, updatedProduct.getProductName(),
                updatedProduct.getCostPerSquareFoot(),
                updatedProduct.getLaborCostPerSquareFoot(),
                updatedProduct.getProductId());
    }

    @Override
    public Product getProductById(int productId) {
        Product product = null;
        try {
            product = jdbcTemplate.queryForObject(SQL_SELECT_PRODUCT_BY_ID, new ProductMapper(), productId);
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
        return product;
    }

    @Override
    public Product getProductByName(String productName) {
        Product product = null;
        try {
            product = jdbcTemplate.queryForObject(SQL_SELECT_PRODUCT_BY_NAME, new ProductMapper(), productName);
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
        return product;
    }

    @Override
    public List<Product> getAllProducts() {
        return jdbcTemplate.query(SQL_SELECT_ALL_PRODUCTS, new ProductMapper());
    }

    private static final class ProductMapper implements RowMapper<Product> {

        @Override
        public Product mapRow(ResultSet rs, int i) throws SQLException {
            Product product = new Product();
            product.setProductId(rs.getInt("product_id"));
            product.setProductName(rs.getString("product_name"));
            product.setCostPerSquareFoot(rs.getBigDecimal("cost_per_sq_ft"));
            product.setLaborCostPerSquareFoot(rs.getBigDecimal("labor_cost_per_sq_ft"));

            return product;
        }

    }
}
