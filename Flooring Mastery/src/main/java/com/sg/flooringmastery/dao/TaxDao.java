/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dao;

import com.sg.flooringmastery.dto.Tax;
import java.util.List;

/**
 *
 * @author cjsm12
 */
public interface TaxDao {
    
    Tax addTax(Tax tax);
    
    void removeTax(String stateName);
    
    void updateTax(Tax updatedTax);
    
    Tax getTaxById(int taxId);
    
    Tax getTaxByName(String taxName);
    
    List<Tax> getAllTaxes();
    
}
