/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.dao;

import com.sg.flooringmastery.dto.User;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author cjsm12
 */
public class UserDaoDatabaseImpl implements UserDao {

    private static final String SQL_INSERT_INTO_USERS
            = "insert into users(username, user_password, enabled) "
            + "values(?, ?, 1)";

    private static final String SQL_REMOVE_FROM_USERS
            = "delete from users where username = ?";

    private static final String SQL_SELECT_ALL_USERS = "select * from users";

    private static final String SQL_INSERT_INTO_AUTHORITIES = "insert into authorities(username, authority) "
            + "values(?, ?)";

    private static final String SQL_REMOVE_FROM_AUTHORITIES = "delete from authorities where username = ?";

    JdbcTemplate jdbcTemplate;

    public UserDaoDatabaseImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public User addUser(User user) {
        jdbcTemplate.update(SQL_INSERT_INTO_USERS, user.getUsername(),
                user.getPassword());

        int id = jdbcTemplate.queryForObject("select LAST_INSERT_ID()", Integer.class);
        user.setUserId(id);

        insertAuthorities(user);

        return user;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void removeUser(String userName) {
        jdbcTemplate.update(SQL_REMOVE_FROM_AUTHORITIES, userName);
        jdbcTemplate.update(SQL_REMOVE_FROM_USERS, userName);
    }

    @Override
    public List<User> getAllUsers() {
        List<User> users = jdbcTemplate.query(SQL_SELECT_ALL_USERS, new UserMapper());

        return users;
    }

    private void insertAuthorities(User user) {
        List<String> authorities = user.getAuthorities();

        for (String authority : authorities) {
            jdbcTemplate.update(SQL_INSERT_INTO_AUTHORITIES, user.getUsername(), authority);
        }
    }

    private static final class UserMapper implements RowMapper<User> {
        @Override
        public User mapRow(ResultSet rs, int i) throws SQLException {
            User user = new User();
            user.setUserId(rs.getInt("user_id"));
            user.setUsername(rs.getString("username"));
            user.setPassword(rs.getString("user_password"));

            return user;
        }

    }

}
