/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.unitTests;

import com.sg.flooringmastery.dao.ProductDao;
import com.sg.flooringmastery.dto.Product;
import java.math.BigDecimal;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author cjsm12
 */
public class ProductDaoUnitTests {

    ProductDao productDao;

    public ProductDaoUnitTests() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        ApplicationContext ctx
                = new ClassPathXmlApplicationContext("test-applicationContext.xml");

        productDao = ctx.getBean("productDao", ProductDao.class);

        List<Product> allProducts = productDao.getAllProducts();
        for (Product product : allProducts) {
            productDao.deleteProduct(product.getProductName());
        }

    }

    @After
    public void tearDown() {
    }

    @Test
    public void testAddGetProduct() {
        Product product = new Product();
        product.setProductName("Wood");
        product.setCostPerSquareFoot(new BigDecimal("2.75"));
        product.setLaborCostPerSquareFoot(new BigDecimal("1.99"));

        productDao.addProduct(product);

        Product matchingProduct = productDao.getProductByName("Wood");

        assertEquals(matchingProduct, product);
    }

    @Test
    public void testRemoveProduct() {
        Product product = new Product();
        product.setProductName("Wood");
        product.setCostPerSquareFoot(new BigDecimal("2.75"));
        product.setLaborCostPerSquareFoot(new BigDecimal("1.99"));

        productDao.addProduct(product);

        Product matchingProduct = productDao.getProductByName("Wood");

        assertEquals(matchingProduct, product);

        productDao.deleteProduct(product.getProductName());

        assertEquals(0, productDao.getAllProducts().size());
    }

    @Test
    public void testGetAllProduct() {
        Product product = new Product();
        product.setProductName("Wood");
        product.setCostPerSquareFoot(new BigDecimal("2.75"));
        product.setLaborCostPerSquareFoot(new BigDecimal("1.99"));

        productDao.addProduct(product);

        Product product2 = new Product();
        product2.setProductName("Tile");
        product2.setCostPerSquareFoot(new BigDecimal("4.35"));
        product2.setLaborCostPerSquareFoot(new BigDecimal("2.55"));

        productDao.addProduct(product2);

        List<Product> allProducts = productDao.getAllProducts();

        assertEquals(2, allProducts.size());
    }

    @Test
    public void testUpdateProduct() {
        Product product = new Product();
        product.setProductName("Wood");
        product.setCostPerSquareFoot(new BigDecimal("2.75"));
        product.setLaborCostPerSquareFoot(new BigDecimal("1.99"));

        productDao.addProduct(product);

        Product matchingProduct = productDao.getProductByName("Wood");

        assertEquals(matchingProduct, product);
        
        product.setProductName("Old Grain Wood");
        productDao.updateProduct(product);
        
        assertEquals("Old Grain Wood", product.getProductName());
    }

}
