/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.unitTests;

import com.sg.flooringmastery.dao.OrderDao;
import com.sg.flooringmastery.dao.ProductDao;
import com.sg.flooringmastery.dao.TaxDao;
import com.sg.flooringmastery.dto.Order;
import com.sg.flooringmastery.dto.Product;
import com.sg.flooringmastery.dto.Tax;
import com.sg.flooringmastery.service.OrderService;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author cjsm12
 */
public class OrderServiceDaoUnitTests {

    OrderService service;
    ProductDao productDao;
    TaxDao taxDao;

    public OrderServiceDaoUnitTests() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        ApplicationContext ctx
                = new ClassPathXmlApplicationContext("test-applicationContext.xml");

        service = ctx.getBean("service", OrderService.class);
        productDao = ctx.getBean("productDao", ProductDao.class);
        taxDao = ctx.getBean("taxDao", TaxDao.class);

        List<Order> allOrders = service.getAllOrders();
        for (Order order : allOrders) {
            service.removeOrder(order.getOrderId());
        }

        List<Product> allProducts = productDao.getAllProducts();
        for (Product product : allProducts) {
            productDao.deleteProduct(product.getProductName());
        }

        List<Tax> allTaxes = taxDao.getAllTaxes();
        for (Tax tax : allTaxes) {
            taxDao.removeTax(tax.getStateName());
        }
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testAddGetOrder() {
        Product product = new Product();
        product.setProductName("Wood");
        product.setCostPerSquareFoot(new BigDecimal("2.75"));
        product.setLaborCostPerSquareFoot(new BigDecimal("1.99"));

        productDao.addProduct(product);

        Tax tax = new Tax();
        tax.setStateName("Ohio");
        tax.setStateTax(new BigDecimal("1.75"));

        taxDao.addTax(tax);

        Order order = new Order();
        order.setFirstName("Clayton");
        order.setLastName("Gilbert");
        order.setOrderDate(LocalDate.now());
        order.setProduct(product);
        order.setTax(tax);
        order.setAreaLength(new BigDecimal("3.5"));
        order.setAreaWidth(new BigDecimal("8.0"));
        order.setTotalArea(new BigDecimal("24.5"));
        order.setTotalLaborCost(new BigDecimal("254.56"));
        order.setTotalMaterialCost(new BigDecimal("450.99"));
        order.setTotalTax(new BigDecimal("125.20"));
        order.setTotalCost(new BigDecimal("1023.73"));

        service.createOrder(order);

        Order fromDb = service.getOrderById(order.getOrderId());

        assertEquals(fromDb, order);
    }

    @Test
    public void testRemoveOrder() {
        Product product = new Product();
        product.setProductName("Wood");
        product.setCostPerSquareFoot(new BigDecimal("2.75"));
        product.setLaborCostPerSquareFoot(new BigDecimal("1.99"));

        productDao.addProduct(product);

        Tax tax = new Tax();
        tax.setStateName("Ohio");
        tax.setStateTax(new BigDecimal("1.75"));

        taxDao.addTax(tax);

        Order order = new Order();
        order.setFirstName("Clayton");
        order.setLastName("Gilbert");
        order.setOrderDate(LocalDate.now());
        order.setProduct(product);
        order.setTax(tax);
        order.setAreaLength(new BigDecimal("3.5"));
        order.setAreaWidth(new BigDecimal("8.0"));
        order.setTotalArea(new BigDecimal("24.5"));
        order.setTotalLaborCost(new BigDecimal("254.56"));
        order.setTotalMaterialCost(new BigDecimal("450.99"));
        order.setTotalTax(new BigDecimal("125.20"));
        order.setTotalCost(new BigDecimal("1023.73"));

        service.createOrder(order);

        Order fromDb = service.getOrderById(order.getOrderId());

        assertEquals(fromDb, order);

        service.removeOrder(fromDb.getOrderId());

        assertNull(service.getOrderById(fromDb.getOrderId()));
    }

    @Test
    public void testUpdateOrder() {
        Product product = new Product();
        product.setProductName("Wood");
        product.setCostPerSquareFoot(new BigDecimal("2.75"));
        product.setLaborCostPerSquareFoot(new BigDecimal("1.99"));

        productDao.addProduct(product);

        Tax tax = new Tax();
        tax.setStateName("Ohio");
        tax.setStateTax(new BigDecimal("1.75"));

        taxDao.addTax(tax);

        Order order = new Order();
        order.setFirstName("Clayton");
        order.setLastName("Gilbert");
        order.setOrderDate(LocalDate.now());
        order.setProduct(product);
        order.setTax(tax);
        order.setAreaLength(new BigDecimal("3.5"));
        order.setAreaWidth(new BigDecimal("8.0"));
        order.setTotalArea(new BigDecimal("24.5"));
        order.setTotalLaborCost(new BigDecimal("254.56"));
        order.setTotalMaterialCost(new BigDecimal("450.99"));
        order.setTotalTax(new BigDecimal("125.20"));
        order.setTotalCost(new BigDecimal("1023.73"));

        service.createOrder(order);

        Order fromDb = service.getOrderById(order.getOrderId());

        assertEquals(fromDb, order);
        
        order.setFirstName("Destinee");
        
        service.updateOrder(order);
        
        Order fromDbUpdated = service.getOrderById(fromDb.getOrderId());
        
        assertEquals("Destinee", fromDbUpdated.getFirstName());
    }

    @Test
    public void testGetAllOrders() {
        Product product = new Product();
        product.setProductName("Wood");
        product.setCostPerSquareFoot(new BigDecimal("2.75"));
        product.setLaborCostPerSquareFoot(new BigDecimal("1.99"));

        productDao.addProduct(product);

        Tax tax = new Tax();
        tax.setStateName("Ohio");
        tax.setStateTax(new BigDecimal("1.75"));

        taxDao.addTax(tax);

        Order order = new Order();
        order.setFirstName("Clayton");
        order.setLastName("Gilbert");
        order.setOrderDate(LocalDate.now());
        order.setProduct(product);
        order.setTax(tax);
        order.setAreaLength(new BigDecimal("3.5"));
        order.setAreaWidth(new BigDecimal("8.0"));
        order.setTotalArea(new BigDecimal("24.5"));
        order.setTotalLaborCost(new BigDecimal("254.56"));
        order.setTotalMaterialCost(new BigDecimal("450.99"));
        order.setTotalTax(new BigDecimal("125.20"));
        order.setTotalCost(new BigDecimal("1023.73"));

        service.createOrder(order);

        Product product2 = new Product();
        product2.setProductName("Tile");
        product2.setCostPerSquareFoot(new BigDecimal("2.75"));
        product2.setLaborCostPerSquareFoot(new BigDecimal("1.99"));

        productDao.addProduct(product2);

        Tax tax2 = new Tax();
        tax2.setStateName("Florida");
        tax2.setStateTax(new BigDecimal("1.75"));

        taxDao.addTax(tax2);

        Order order2 = new Order();
        order2.setFirstName("Jacob");
        order2.setLastName("Williams");
        order2.setOrderDate(LocalDate.now());
        order2.setProduct(product);
        order2.setTax(tax);
        order2.setAreaLength(new BigDecimal("3.5"));
        order2.setAreaWidth(new BigDecimal("8.0"));
        order2.setTotalArea(new BigDecimal("24.5"));
        order2.setTotalLaborCost(new BigDecimal("254.56"));
        order2.setTotalMaterialCost(new BigDecimal("450.99"));
        order2.setTotalTax(new BigDecimal("125.20"));
        order2.setTotalCost(new BigDecimal("1023.73"));

        service.createOrder(order2);

        List<Order> allOrders = service.getAllOrders();

        assertEquals(2, allOrders.size());

    }

}
