/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.unitTests;

import com.sg.flooringmastery.dao.TaxDao;
import com.sg.flooringmastery.dto.Tax;
import java.math.BigDecimal;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author cjsm12
 */
public class TaxDaoUnitTests {

    TaxDao taxDao;

    public TaxDaoUnitTests() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        ApplicationContext ctx
                = new ClassPathXmlApplicationContext("test-applicationContext.xml");

        taxDao = ctx.getBean("taxDao", TaxDao.class);
        
        List<Tax> allTaxes = taxDao.getAllTaxes();
        for (Tax tax : allTaxes) {
            taxDao.removeTax(tax.getStateName());
        }

    }

    @After
    public void tearDown() {
    }

    @Test
    public void testAddGetTax() {
        Tax tax = new Tax();
        tax.setStateName("Ohio");
        tax.setStateTax(new BigDecimal("1.75"));

        taxDao.addTax(tax);

        Tax matchingTax = taxDao.getTaxByName("Ohio");
        assertEquals(tax, matchingTax);
    }

    @Test
    public void testRemoveTax() {
        Tax tax = new Tax();
        tax.setStateName("Ohio");
        tax.setStateTax(new BigDecimal("1.75"));

        taxDao.addTax(tax);

        Tax matchingTax = taxDao.getTaxByName("Ohio");
        assertEquals(tax, matchingTax);
        
        taxDao.removeTax(tax.getStateName());
        assertEquals(0, taxDao.getAllTaxes().size());
    }

    @Test
    public void testUpdateTax() {
        Tax tax = new Tax();
        tax.setStateName("Ohio");
        tax.setStateTax(new BigDecimal("1.75"));

        taxDao.addTax(tax);

        Tax matchingTax = taxDao.getTaxByName("Ohio");
        assertEquals(tax, matchingTax);
        
        tax.setStateName("Indiana");
        
        taxDao.updateTax(tax);
        
        assertNull(taxDao.getTaxByName("Ohio"));
        
        assertEquals("Indiana", taxDao.getTaxByName("Indiana").getStateName());
    }

    @Test
    public void testGetAllTaxes() {
        Tax tax = new Tax();
        tax.setStateName("Ohio");
        tax.setStateTax(new BigDecimal("1.75"));

        taxDao.addTax(tax);

        Tax tax2 = new Tax();
        tax2.setStateName("Texas");
        tax2.setStateTax(new BigDecimal("2.50"));

        taxDao.addTax(tax2);

        List<Tax> allTaxes = taxDao.getAllTaxes();
        assertEquals(2, allTaxes.size());

    }

}
