/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flooringmastery.unitTests;

import com.sg.flooringmastery.dao.UserDao;
import com.sg.flooringmastery.dto.User;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author cjsm12
 */
public class UserDaoUnitTests {

    UserDao userDao;

    public UserDaoUnitTests() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        ApplicationContext ctx
                = new ClassPathXmlApplicationContext("test-applicationContext.xml");

        userDao = ctx.getBean("userDao", UserDao.class);

        List<User> allUsers = userDao.getAllUsers();
        for (User user : allUsers) {
            userDao.removeUser(user.getUsername());
        }

    }

    @After
    public void tearDown() {
    }

    @Test
    public void testAddGetUser() {
        User user = new User();
        user.setUsername("cjsm12");
        user.setPassword("password");
        String authority1 = "ROLE_ADMIN";
        String authority2 = "ROLE_USER";
        List<String> roles = new ArrayList<>();
        roles.add(authority1);
        roles.add(authority2);
        user.setAuthorities(roles);

        userDao.addUser(user);

        List<User> usersFromDb = userDao.getAllUsers();
        assertEquals(1, usersFromDb.size());
    }

    @Test
    public void testRemoveUser() {
        User user = new User();
        user.setUsername("cjsm12");
        user.setPassword("password");
        String authority1 = "ROLE_ADMIN";
        String authority2 = "ROLE_USER";
        List<String> roles = new ArrayList<>();
        roles.add(authority1);
        roles.add(authority2);
        user.setAuthorities(roles);

        userDao.addUser(user);

        userDao.removeUser(user.getUsername());

        assertEquals(0, userDao.getAllUsers().size());
    }

    @Test
    public void testGetAllUsers() {
        User user = new User();
        user.setUsername("cjsm12");
        user.setPassword("password");
        String authority1 = "ROLE_ADMIN";
        String authority2 = "ROLE_USER";
        List<String> roles = new ArrayList<>();
        roles.add(authority1);
        roles.add(authority2);
        user.setAuthorities(roles);

        userDao.addUser(user);

        User user2 = new User();
        user2.setUsername("softwareGuy");
        user2.setPassword("password");
        String authority3 = "ROLE_USER";
        List<String> roles2 = new ArrayList<>();
        roles2.add(authority3);
        user.setAuthorities(roles2);

        userDao.addUser(user2);
        
        assertEquals(2, userDao.getAllUsers().size());
    }
}
