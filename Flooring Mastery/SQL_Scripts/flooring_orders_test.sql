DROP DATABASE IF EXISTS flooring_orders_test;

CREATE DATABASE IF NOT EXISTS flooring_orders_test;

USE flooring_orders_test;

CREATE TABLE IF NOT EXISTS products
(
product_id int(32) NOT NULL auto_increment,
product_name varchar(30) NOT NULL,
cost_per_sq_ft varchar(10) NOT NULL,
labor_cost_per_sq_ft varchar(10) NOT NULL,
PRIMARY KEY(product_id)
);

INSERT INTO products(product_id, product_name, cost_per_sq_ft, labor_cost_per_sq_ft) VALUES
(1,'Stained Wood','3.54','2.75'),
(2,'Ceramic Tile','1.50','2.00'),
(3,'Glass','4.10','3.55'),
(4,'Treated PlyWood','1.55','2.25'),
(5,'Plastic','2.99','1.25');

CREATE TABLE IF NOT EXISTS taxes
(
tax_id int(32) NOT NULL auto_increment,
state_name varchar(20) NOT NULL,
tax_rate varchar(10) NOT NULL,
PRIMARY KEY(tax_id)
);

INSERT INTO taxes(tax_id, state_name, tax_rate) VALUES
(1,'Alabama','3.54'),
(2,'Alaska','4.50'),
(3,'Arizona','3.10'),
(4,'Arkansas','2.55'),
(5,'California','5.99'),
(6,'Colorado','4.54'),
(7,'Conneticut','5.50'),
(8,'Delaware','3.10'),
(9,'Florida','3.55'),
(10,'Georgia','6.99'),
(11,'Hawaii','5.54'),
(12,'Idaho','4.50'),
(13,'Illinois','6.10'),
(14,'Indiana','3.55'),
(15,'Iowa','4.99'),
(16,'Kansas','6.54'),
(17,'Kentucky','4.50'),
(18,'Louisiana','5.10'),
(19,'Maine','7.55'),
(20,'Maryland','3.99'),
(21,'Massachusetts','4.54'),
(22,'Michigan','6.50'),
(23,'Minnesota','5.10'),
(24,'Mississippi','3.55'),
(25,'Missouri','5.99'),
(26,'Montana','4.54'),
(27,'Nebraska','5.50'),
(28,'Nevada','3.10'),
(29,'New Hampshire','5.55'),
(30,'New Jersey','4.99'),
(31,'New Mexico','6.99'),
(32,'New York','4.54'),
(33,'North Carolina','3.10'),
(34,'North Dakota','5.55'),
(35,'Ohio','4.69'),
(36,'Oklahoma','4.54'),
(37,'Oregon','3.50'),
(38,'Pennsylvania','4.10'),
(39,'Rhode Island','6.55'),
(40,'South Carolina','5.99'),
(41,'South Dakota','4.54'),
(42,'Tennessee','6.50'),
(43,'Texas','4.10'),
(44,'Utah','3.55'),
(45,'Vermont','5.99'),
(46,'Virginia','4.54'),
(47,'Washington','3.50'),
(48,'West Virginia','4.10'),
(49,'Wisconsin','5.55'),
(50,'Wyoming','6.99');

CREATE TABLE IF NOT EXISTS orders
(
order_id int(32) NOT NULL auto_increment,
first_name varchar(20) NOT NULL,
last_name varchar(20) NOT NULL,
order_date DATETIME NOT NULL,
product_id int(32) NOT NULL,
tax_id int(32) NOT NULL,
area_length varchar(10) NOT NULL,
area_width varchar(10) NOT NULL,
total_area varchar(10) NOT NULL,
total_material_cost varchar(10) NOT NULL,
total_labor_cost varchar(10) NOT NULL,
total_tax varchar(10) NOT NULL,
total_cost varchar(10) NOT NULL,
PRIMARY KEY(order_id)
);

ALTER TABLE orders ADD CONSTRAINT ibfk_orders_products 
FOREIGN KEY(product_id) REFERENCES products(product_id);

ALTER TABLE orders ADD CONSTRAINT ibfk_orders_taxes
FOREIGN KEY(tax_id) REFERENCES taxes(tax_id);

CREATE TABLE IF NOT EXISTS users
(
user_id int(32) NOT NULL auto_increment,
username varchar(30) NOT NULL,
user_password varchar(30) NOT NULL,
enabled tinyint(1) NOT NULL,
PRIMARY KEY(user_id),
KEY username(username)
);

INSERT INTO users(user_id, username, user_password, enabled) VALUES
(1,'user','password', 1),
(2, 'admin','password', 1);

CREATE TABLE IF NOT EXISTS authorities
(
username varchar(30) NOT NULL,
authority varchar(30) NOT NULL,
KEY username (username)
);

INSERT INTO authorities(username, authority) VALUES
('user','ROLE_USER'),
('admin','ROLE_USER'),
('admin','ROLE_ADMIN');

ALTER TABLE authorities 
ADD CONSTRAINT ibfk_authorities_username FOREIGN KEY(username) REFERENCES users(username) ON DELETE CASCADE;

