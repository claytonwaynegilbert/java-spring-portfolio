/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.factorizor;

import java.util.Scanner;

/**
 *
 * @author cjsm12
 */
public class Factorizor {

    public static void main(String[] args) {

        /*
        In this lab, you will write a program that calculates all the factors of a number
        entered by the user. The program must print out the original number along with 
        each of its factors. The program must also tell the user if the number is a 
        perfect number. A perfect number is a number where all the factors of the number 
        add up to that number; for example, the first perfect number is 6 because its 
        factors 1, 2, and 3 add up to 6. Additionally, the program must tell the user 
        whether or not the number is a prime number. A prime number is defined as a
        number that has only itself and 1 as factors; for example, 3 is a prime number.

        Your program must have the following features:

        This program must be a Java Console Application called Factorizor.
        This program must ask the user the number for which he/she wants to factor.
        The program must print out the original number.
        The program must print out the total number of factors for the number.
        The program must print out each factor of the number (not including the number 
        itself).
        The program must print out whether or not the number is perfect.
        The program must print out whether or not the number is prime
         */
        
        
        Scanner sc = new Scanner(System.in);

        //Welcome user
        System.out.println("Welcome to Facotrizor!!!");
        System.out.println("------------------------\n");

        //Ask user for a number to factor
        System.out.print("Give me a number to factorize : ");
        int numberToFactor = sc.nextInt();

        //Declare variables
        int factorSum = 0;
        boolean primeCheck;
        boolean perfectCheck;

        System.out.println("You asked to factor " + numberToFactor);
        System.out.println("These are the factors of " + numberToFactor + ": ");
        
        //for loop to check for factors of user number and adding each factor found 
        //to a variable for checking if number is perfect later
        for(int i = 1; i < numberToFactor; i++){
            if(numberToFactor % i == 0){
                factorSum += i;
                //Print out factor
                System.out.println(i + " is a factor of " + numberToFactor);
            }
        }
        
        //Checking if number is prime and or perfect by method calls
        primeCheck = isPrime(numberToFactor);
        perfectCheck = isPerfect(numberToFactor, factorSum);
        
        //Informing user if number is perfect or prime
        System.out.println(numberToFactor + " is prime = " + primeCheck);
        System.out.println(numberToFactor + " is perfect = " + perfectCheck);

    }

    static boolean isPrime(int n) {
        //check if n is a multiple of 2
        if (n % 2 == 0) {
            return false;
        }
        //if not, then just check the odds
        for (int i = 3; i * i <= n; i += 2) {
            if (n % i == 0) {
                return false;
            }
        }
        //Is passes all conditions above, number is prime
        return true;
    }
    
    static boolean isPerfect(int numberToFactor, int factorSum){
        //Checking to see if factors total sum added up is equal to the original number
        //because by definition that is what a perfect number is
        if(factorSum == numberToFactor){
            return true;
        }else{
            return false;
        }
    }

}
