package com.cg.Recipe.Book;

import javax.transaction.Transactional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional //We can use the transactional annotation on all of our tests so that we don't have to make sure the database is in good standing after
              //each test, instead the transactional annotation rolls back after each test so no changes are really permanently made
//@SpringApplicationConfiguration(classes = RecipeBookApplication.class) //This annotation is if we had a application context configuration file
@ActiveProfiles("test") //We can use this annotation so that all beans marked with profile of test will be used
public class RecipeBookApplicationTests {

	@Test
	public void contextLoads() {
	}

}
