/*This file will be picked up by spring boot when our h2 in memory database is used in the situation where no active profile is set.
Since the H2 database is a create-drop type database, meaning a new database schema is created each time the program is restarted, 
we want Spring to pick this sql file up because it can use this file to prepopulate the database tables with tables and relevant data.
The naming format is either schema.sql or data- followed by the database, so h2. We wouldn't want this type of behavior for a mysql type 
database because the data is stored permanently and if this would be ran each time the application started, there would be a bunch of
duplicate data in our database tables
*/

-- Here is where we create our tables and insert data into them

-- INSERT INTO cateogries (description) VALUES ('AMERICAN');
-- INSERT INTO cateogries (description) VALUES ('ITALIAN');
-- INSERT INTO cateogries (description) VALUES ('MEXICAN');
-- INSERT INTO cateogries (description) VALUES ('FAST-FOOD');



