/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import domain.Category;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author cjsm12
 */

//We use JPA's CrudRepository object to get basic CRUD operations for us...
public interface CategoryRepository extends CrudRepository<Category, Integer> {  
    
    //Here we can define dynamic finder methods and Spring Data will create the SQL code behind the scenes using
    //the name of the method and data passed in to use againast the database tables, so the findByDescription name is important
    //and the findBy is used to find one object and findAll is used to get a collection of objects.
    //Also Optional is a Java 8 wrapper object that is good to use if you have the posiibility of null being returned as it handles it
    //better without any exceptions being thrown, we can then use the .get() method on the Optional to get the specific object, which in this
    //case is Category.
    Optional<Category> findByDescription(String description);
}
