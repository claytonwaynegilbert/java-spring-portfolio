/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositories;

import domain.Recipe;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author cjsm12
 */
public interface RecipeRepository extends CrudRepository<Recipe, Integer> {
}
