/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commands;

import java.util.Set;

/**
 *
 * @author cjsm12
 */

public class CategoryCommand {
    
    private Integer categoryId;
    private String description;
    private Set<RecipeCommand> recipes;
    
    public CategoryCommand(){
        
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<RecipeCommand> getRecipes() {
        return recipes;
    }

    public void setRecipes(Set<RecipeCommand> recipes) {
        this.recipes = recipes;
    }
    
    
    
}
