/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import commands.RecipeCommand;
import converters.RecipeCommandToRecipe;
import converters.RecipeToRecipeCommand;
import domain.Recipe;
import exceptions.NullDataException;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repositories.RecipeRepository;

/**
 *
 * @author cjsm12
 */
@Service
public class RecipeServiceImpl implements RecipeService {

    private final RecipeRepository recipeRepository;
    private final RecipeToRecipeCommand recipeToRecipeCommandConverter;
    private final RecipeCommandToRecipe recipeCommandToRecipeConverter;

    @Autowired
    public RecipeServiceImpl(RecipeRepository recipeRepository, RecipeToRecipeCommand recipeToRecipeCommandConverter, RecipeCommandToRecipe recipeCommandToRecipeConverter) {
        this.recipeRepository = recipeRepository;
        this.recipeToRecipeCommandConverter = recipeToRecipeCommandConverter;
        this.recipeCommandToRecipeConverter = recipeCommandToRecipeConverter;
    }

    @Override
    public Set<Recipe> getRecipes() {
        Set<Recipe> recipeSet = new HashSet<>();
        recipeRepository.findAll().iterator().forEachRemaining(recipeSet::add); //Here we find all recipes, iterate through them and for each one,
                                                                                //call the add method on the recipeSet variable created above.
        return recipeSet;
    }

    @Override
    public RecipeCommand saveRecipeCommand(RecipeCommand recipeCommand) {
        Recipe recipeToSave = recipeCommandToRecipeConverter.convert(recipeCommand); //First we convert from our command object to real domain object so that
                                                                                     //we can persist to the database.
                                                                                     
        Recipe fromDb = recipeRepository.save(recipeToSave); //Save it to the database, from which we get a copy back of the newly created recipe...

        return recipeToRecipeCommandConverter.convert(fromDb); //Convert the recipe back to the recipe command object for use on the fornt end
    }

    @Transactional
    @Override
    public Recipe findById(int id) throws NullDataException {

        Optional<Recipe> recipeOptional = recipeRepository.findById(id);

        if (!recipeOptional.isPresent()) {
            throw new NullDataException("Recipe with ID of " + id + " was not found.");
        }

        return recipeOptional.get();
    }

    @Transactional
    @Override
    public RecipeCommand findCommandById(int id) throws NullDataException {
        return recipeToRecipeCommandConverter.convert(findById(id)); //Use the original findById method along with a converter to convert the found domain
                                                                     //recipe to the recipe command object to be sent back to the user interface
    }

    @Override
    public void deleteById(int id) {
        recipeRepository.deleteById(id);
    }

}
