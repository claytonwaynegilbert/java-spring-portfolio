/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import commands.IngredientCommand;
import converters.IngredientCommandToIngredient;
import converters.IngredientToIngredientCommand;
import domain.Ingredient;
import domain.Recipe;
import exceptions.NullDataException;
import java.util.Optional;
import org.springframework.stereotype.Service;
import repositories.RecipeRepository;
import repositories.UnitOfMeasureRepository;

/**
 *
 * @author cjsm12
 */
@Service
public class IngredientServiceImpl implements IngredientService {

    private final RecipeRepository recipeRepository;
    private final IngredientToIngredientCommand ingredientConverter;
    private final IngredientCommandToIngredient ingredientConverterTwo;
    private final UnitOfMeasureRepository uomRepository;

    public IngredientServiceImpl(RecipeRepository recipeRepository, IngredientToIngredientCommand ingredientConverter, IngredientCommandToIngredient ingredientConverterTwo, UnitOfMeasureRepository uomRepository) {
        this.recipeRepository = recipeRepository;
        this.ingredientConverter = ingredientConverter;
        this.ingredientConverterTwo = ingredientConverterTwo;
        this.uomRepository = uomRepository;
    }

    //This method is used to view a specific ingredient of a specific recipe...
    @Override
    public IngredientCommand findByRecipeIdAndIngredientId(int recipeId, int ingredientId) throws NullDataException {
        //First we find the recipe
        Optional<Recipe> recipeOptional = recipeRepository.findById(recipeId);

        //If nothing there, throw error...
        if (!recipeOptional.isPresent()) {
            throw new NullDataException("Recipe with ID of " + recipeId + " could not be found");
        }

        //Otherwise get the actual recipe object out of the Optional
        Recipe recipe = recipeOptional.get();

        //Now to find the specific ingredient, we get all of the ingredients from the recipe, then through streams, go through and filter all
        //the ingredients in the set and find the one who's id matches the one passed in, once found, use the .map function which converts a type
        //to another type and convert it from an original ingredient to the ingredient command we use to pass back to the front end.
        Optional<IngredientCommand> ingredientCommand = recipe.getIngredients().stream().filter(ingredient -> ingredient.getIngredientId() == ingredientId)
                .map(ingredient -> ingredientConverter.convert(ingredient)).findFirst();

        //If we didn't find any ingredients matching the id passed in, then it doens't exists and we throw an error...
        if (!recipeOptional.isPresent()) {
            throw new NullDataException("Ingredient with ID of " + ingredientId + " could not be found");
        }

        //Return the ingredient command object if everthing goes smoothly
        return ingredientCommand.get();

    }

    @Override
    public IngredientCommand createOrUpdateIngredientCommand(IngredientCommand command) throws NullDataException {
        //Get the belonging recipe using the recipeId on the ingredient object...
        Optional<Recipe> recipeOptional = recipeRepository.findById(command.getRecipeId());

        //Make sure the ingredient belongs to a recipe...
        if (!recipeOptional.isPresent()) {
            //Otherwise just return a new one
            return new IngredientCommand();
        } else {
            //Get the recipe out of the optional..
            Recipe recipe = recipeOptional.get();

            //Attempt to find the ingredient we are passing, which will tell hibernate to perform either an update or a creation...
            Optional<Ingredient> ingredientOptional = recipe.getIngredients().stream()
                    .filter(ingredient -> ingredient.getIngredientId().equals(command.getIngredientId()))
                    .findFirst();

            //If we find it, that means we are doing an update..
            if (ingredientOptional.isPresent()) {
                //Get the ingredient out of the optional...
                Ingredient ingredient = ingredientOptional.get();
                //Set the new description of the ingredient...
                ingredient.setDescription(command.getDescription());
                //Also set the new unit of measure on the ingredient by using the uom id we have on our updated ingredient and search for it in the database...
                ingredient.setUom(uomRepository.findById(command.getUom().getUnitOfMeasureId())
                        .orElseThrow(() -> new NullDataException("No unit of measure by id of " + command.getUom().getUnitOfMeasureId() + " found.")));
            } else {
                //If we don't find the ingredient, it means we are creating a new one...
                Ingredient ingredient = ingredientConverterTwo.convert(command);
                //Set the recipe it belongs to onto it...
                ingredient.setRecipe(recipe);
                //Add the ingredient to the matching recipe...
                recipe.addIngredient(ingredient);
            }

            //Save the recipe with the added ingredient to the database...
            Recipe savedRecipe = recipeRepository.save(recipe);

            //Make sure the ingredient we just created is now on the saved database recipe...
            Optional<Ingredient> savedIngredientOptional = savedRecipe.getIngredients().stream()
                    .filter(recipeIngredients -> recipeIngredients.getIngredientId().equals(command.getIngredientId()))
                    .findFirst();

            //Return back the ingredient as an ingredientcommand object by using the converter...
            return ingredientConverter.convert(savedIngredientOptional.get());

        }
    }

    @Override
    public void deleteIngredient(int recipeId, int ingredientId) {
        //We find the matching recipe the ingredient belongs to...
        Optional<Recipe> matchingRecipe = recipeRepository.findById(recipeId);
        
        //if we find the recipe...
        if(matchingRecipe.isPresent()){
            //Get the recipe out of the optional...
            Recipe recipe = matchingRecipe.get();
            
            //Look through all the ingredients of the recipe and find the matching one we are trying to delete...
            Optional<Ingredient> ingredientOptional = recipe.getIngredients().stream()
                                                                             .filter(ing -> ing.getIngredientId().equals(ingredientId))
                                                                             .findFirst();
            //If we find the ingredient...
            if(ingredientOptional.isPresent()){
                //Get the ingredient out of the optional...
                Ingredient ingToDelete = ingredientOptional.get();
                //Set the recipe on the ingredient to null since we don't want it associated with any recipe anymore...
                ingToDelete.setRecipe(null);
                //Also remove the ingredient from the recipe it was previously on...
                recipe.getIngredients().remove(ingToDelete);
                //Resave the recipe which will perform an update due to the recipe having an id
                recipeRepository.save(recipe);
            }else{
                
            }
        }
    }
}
