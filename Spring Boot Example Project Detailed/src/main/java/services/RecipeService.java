/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import commands.RecipeCommand;
import domain.Recipe;
import exceptions.NullDataException;
import java.util.Set;

/**
 *
 * @author cjsm12
 */
public interface RecipeService {
    
    Set<Recipe> getRecipes();
    
    Recipe findById(int id) throws NullDataException;
    
    RecipeCommand findCommandById(int id) throws NullDataException;
    
    RecipeCommand saveRecipeCommand(RecipeCommand recipeCommand);
    
    void deleteById(int recipeId);
    
}
