/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import commands.UnitOfMeasureCommand;
import java.util.Set;

/**
 *
 * @author cjsm12
 */
public interface UnitOfMeasureService {
    
    Set<UnitOfMeasureCommand> allUnitOfMeasures();
    
}
