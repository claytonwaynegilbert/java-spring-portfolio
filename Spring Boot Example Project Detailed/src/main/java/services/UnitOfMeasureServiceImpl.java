/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import commands.UnitOfMeasureCommand;
import converters.UnitOfMeasureToUnitOfMeasureCommand;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.springframework.stereotype.Component;
import repositories.UnitOfMeasureRepository;

/**
 *
 * @author cjsm12
 */
@Component
public class UnitOfMeasureServiceImpl implements UnitOfMeasureService {

    private UnitOfMeasureRepository uomRepository;
    private UnitOfMeasureToUnitOfMeasureCommand uomConverter;

    public UnitOfMeasureServiceImpl(UnitOfMeasureRepository uomRepository, UnitOfMeasureToUnitOfMeasureCommand uomConverter) {
        this.uomRepository = uomRepository;
        this.uomConverter = uomConverter;
    }
    
    @Override
    public Set<UnitOfMeasureCommand> allUnitOfMeasures() {
        return StreamSupport.stream(uomRepository.findAll() //Since the JPA returns an iterator object, an easy way to convert that iterator into something
                                                            //we can stream against is to use StreamSupport object.
                            .spliterator(), false)
                            .map(uomConverter::convert)
                            .collect(Collectors.toSet());
                            

    }
    
}
