/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import commands.IngredientCommand;
import exceptions.NullDataException;

/**
 *
 * @author cjsm12
 */
public interface IngredientService{
    
    IngredientCommand findByRecipeIdAndIngredientId(int recipeId, int ingredientId) throws NullDataException;
    
    IngredientCommand createOrUpdateIngredientCommand(IngredientCommand command) throws NullDataException;
    
    void deleteIngredient(int recipeId, int ingredientId);
    
}
