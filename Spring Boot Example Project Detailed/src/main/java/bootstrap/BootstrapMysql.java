
import domain.Category;
import domain.UnitOfMeasure;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import repositories.CategoryRepository;
import repositories.UnitOfMeasureRepository;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author cjsm12
 */
@Component
@Profile({"dev","prod"})
public class BootstrapMysql implements ApplicationListener<ContextRefreshedEvent>{

    private CategoryRepository categoryRepo;
    private UnitOfMeasureRepository uomRepo;

    @Autowired
    public BootstrapMysql(CategoryRepository cateogryRepo, UnitOfMeasureRepository uomRepo) {
        this.categoryRepo = cateogryRepo;
        this.uomRepo = uomRepo;
    }
    
    public void onApplicationEvent(ContextRefreshedEvent e) {
        
        if(categoryRepo.count() == 0){
            loadCategories();
        }
        
        if(uomRepo.count() == 0){
            loadUnitOfMeasures();
        }  
    }
    
    private void loadCategories(){
        Category category1 = new Category();
        category1.setDescription("American");
        categoryRepo.save(category1);
        
        Category category2 = new Category();
        category2.setDescription("Italian");
        categoryRepo.save(category2);
        
        Category category3 = new Category();
        category3.setDescription("Mexico");
        categoryRepo.save(category3);
    }
    
    private void loadUnitOfMeasures(){
        UnitOfMeasure uom1 = new UnitOfMeasure();
        uom1.setUnitOfMeasure("Cup");
        uomRepo.save(uom1);
        
        UnitOfMeasure uom2 = new UnitOfMeasure();
        uom2.setUnitOfMeasure("Tablespoon");
        uomRepo.save(uom2);
        
        UnitOfMeasure uom3 = new UnitOfMeasure();
        uom3.setUnitOfMeasure("1/2 Cup");
        uomRepo.save(uom3);
        
        
    }
    
    
}
