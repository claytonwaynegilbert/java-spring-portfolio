/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package converters;

import commands.NotesCommand;
import domain.Notes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

/**
 *
 * @author cjsm12
 */

@Component
public class NotesToNotesCommand implements Converter<Notes, NotesCommand> {

    private final RecipeToRecipeCommand recipeConverter;

    @Autowired
    public NotesToNotesCommand(RecipeToRecipeCommand recipeConverter) {
        this.recipeConverter = recipeConverter;
    }
    
    @Nullable
    @Override
    public NotesCommand convert(Notes notes) {
        if(notes == null){
            return null;
        }
        
        final NotesCommand nc = new NotesCommand();
        nc.setNotesId(notes.getNotesId());
        nc.setRecipeNotes(notes.getRecipeNotes());
        nc.setRecipe(recipeConverter.convert(notes.getRecipe()));
        
        return nc;
    }

}
