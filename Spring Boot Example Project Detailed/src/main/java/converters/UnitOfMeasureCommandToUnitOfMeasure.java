/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package converters;

import commands.UnitOfMeasureCommand;
import domain.UnitOfMeasure;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

/**
 *
 * @author cjsm12
 */
@Component
public class UnitOfMeasureCommandToUnitOfMeasure implements Converter<UnitOfMeasureCommand, UnitOfMeasure> {

    @Nullable
    @Override
    public UnitOfMeasure convert(UnitOfMeasureCommand command) {
        if(command == null){
            return null;
        }
        
        UnitOfMeasure uom = new UnitOfMeasure();
        uom.setUnitOfMeasureId(command.getUnitOfMeasureId());
        uom.setUnitOfMeasure(command.getUnitOfMeasure());

        return uom;
    }

}
