/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package converters;

import commands.IngredientCommand;
import domain.Ingredient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

/**
 *
 * @author cjsm12
 */
@Component
public class IngredientCommandToIngredient implements Converter<IngredientCommand, Ingredient> {

    private final UnitOfMeasureCommandToUnitOfMeasure uomConverter;
    private final RecipeCommandToRecipe recipeConverter;

    @Autowired
    public IngredientCommandToIngredient(UnitOfMeasureCommandToUnitOfMeasure ufmConverter, RecipeCommandToRecipe recipeConverter) {
        this.uomConverter = ufmConverter;
        this.recipeConverter = recipeConverter;
    }
    
    @Nullable
    @Override
    public Ingredient convert(IngredientCommand ic) {
        if(ic == null){
            return null;
        }
        
        final Ingredient ingredient = new Ingredient();
        ingredient.setIngredientId(ic.getIngredientId());
        ingredient.setDescription(ic.getDescription());
        ingredient.setAmount(ic.getAmount());
        ingredient.setUom(uomConverter.convert(ic.getUom()));
        ingredient.setRecipe(recipeConverter.convert(ic.getRecipe()));
        
        return ingredient;
    }
    
}
