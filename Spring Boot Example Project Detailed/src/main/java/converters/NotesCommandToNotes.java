/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package converters;

import commands.NotesCommand;
import domain.Notes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

/**
 *
 * @author cjsm12
 */
@Component
public class NotesCommandToNotes implements Converter<NotesCommand, Notes> {

    private final RecipeCommandToRecipe recipeConverter;

    @Autowired
    public NotesCommandToNotes(RecipeCommandToRecipe recipeConverter) {
        this.recipeConverter = recipeConverter;
    }

    @Nullable
    @Override
    public Notes convert(NotesCommand notesCommand) {
        if(notesCommand == null) {
            return null;
        }
        
        final Notes notes = new Notes();
        notes.setNotesId(notesCommand.getNotesId());
        notes.setRecipeNotes(notesCommand.getRecipeNotes());
        notes.setRecipe(recipeConverter.convert(notesCommand.getRecipe()));

        return notes;
    }

}
