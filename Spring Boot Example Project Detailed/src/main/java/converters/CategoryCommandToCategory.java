/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package converters;

import commands.CategoryCommand;
import domain.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

/**
 *
 * @author cjsm12
 */

@Component
public class CategoryCommandToCategory implements Converter<CategoryCommand, Category> {

    private final RecipeCommandToRecipe recipeConverter;

    @Autowired
    public CategoryCommandToCategory(RecipeCommandToRecipe recipeConverter) {
        this.recipeConverter = recipeConverter;
    }
    
    @Nullable
    @Override
    public Category convert(CategoryCommand cmd) {
        if(cmd == null){
            return null;
        }
        
        final Category category = new Category();
        category.setCategoryId(cmd.getCategoryId());
        category.setDescription(cmd.getDescription());

        if(cmd.getRecipes() !=null && cmd.getRecipes().size() > 0){
            cmd.getRecipes().forEach(recipe -> category.getRecipes().add(recipeConverter.convert(recipe)));
        }
        
        return category;
    }
    
}
