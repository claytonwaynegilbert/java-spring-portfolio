/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package converters;

import commands.IngredientCommand;
import domain.Ingredient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

/**
 *
 * @author cjsm12
 */
@Component
public class IngredientToIngredientCommand implements Converter<Ingredient, IngredientCommand> {

    private final UnitOfMeasureToUnitOfMeasureCommand uomConverter;
    private final RecipeToRecipeCommand recipeConverter;

    @Autowired
    public IngredientToIngredientCommand(UnitOfMeasureToUnitOfMeasureCommand uomConverter, RecipeToRecipeCommand recipeConverter) {
        this.uomConverter = uomConverter;
        this.recipeConverter = recipeConverter;
    }

    @Nullable
    @Override
    public IngredientCommand convert(Ingredient ingredient) {
        if(ingredient == null){
            return null;
        }
        
        final IngredientCommand ic = new IngredientCommand();
        ic.setIngredientId(ingredient.getIngredientId());
        if (ingredient.getRecipe() != null) {
            ic.setRecipeId(ingredient.getRecipe().getRecipeId());
        }
        ic.setDescription(ingredient.getDescription());
        ic.setAmount(ingredient.getAmount());
        ic.setUom(uomConverter.convert(ingredient.getUom()));
        ic.setRecipe(recipeConverter.convert(ingredient.getRecipe()));

        return ic;
    }

}
