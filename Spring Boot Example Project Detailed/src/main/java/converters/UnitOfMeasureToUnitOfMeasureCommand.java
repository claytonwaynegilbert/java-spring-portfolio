/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package converters;

import commands.UnitOfMeasureCommand;
import domain.UnitOfMeasure;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

/**
 *
 * @author cjsm12
 */
@Component
public class UnitOfMeasureToUnitOfMeasureCommand implements Converter<UnitOfMeasure, UnitOfMeasureCommand> {

    @Nullable
    @Override
    public UnitOfMeasureCommand convert(UnitOfMeasure unitOfMeasure) {
        if(unitOfMeasure == null){
            return null;
        }
        
        UnitOfMeasureCommand command = new UnitOfMeasureCommand();
        command.setUnitOfMeasureId(unitOfMeasure.getUnitOfMeasureId());
        command.setUnitOfMeasure(unitOfMeasure.getUnitOfMeasure());
        
        return command;
    }
    
}
