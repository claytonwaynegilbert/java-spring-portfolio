/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package converters;

import commands.RecipeCommand;
import domain.Difficulty;
import domain.Recipe;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

/**
 *
 * @author cjsm12
 */
@Component
public class RecipeCommandToRecipe implements Converter<RecipeCommand, Recipe> {

    private final IngredientCommandToIngredient ingredientsConverter;
    private final NotesCommandToNotes notesConverter;
    private final CategoryCommandToCategory categoriesConverter;

    public RecipeCommandToRecipe(IngredientCommandToIngredient ingredientsConverter, NotesCommandToNotes notesConverter, CategoryCommandToCategory categoriesConverter) {
        this.ingredientsConverter = ingredientsConverter;
        this.notesConverter = notesConverter;
        this.categoriesConverter = categoriesConverter;
    }

    @Nullable //@Nullable means this method can return null without error
    @Override
    public Recipe convert(RecipeCommand rc) {
        if(rc == null){
            return null;
        }
        
        final Recipe recipe = new Recipe();
        recipe.setRecipeId(rc.getRecipeId());
        recipe.setDescription(rc.getDescription());
        recipe.setPrepTime(rc.getPrepTime());
        recipe.setCookTime(rc.getCookTime());
        recipe.setServings(rc.getServings());
        recipe.setSource(rc.getSource());
        recipe.setUrl(rc.getUrl());
        recipe.setDirections(rc.getDirections());
        recipe.setDifficulty(rc.getDifficulty());
        recipe.setImage(rc.getImage());
        recipe.setNotes(notesConverter.convert(rc.getNotes()));

        //Here we are doing conversions for multiple ingredients and categories...we check if both are not null and have a size greater than 0, meaning
        //there are at least one object in the sets, then we for each object in the set, add it to the object we are returning by calling the
        //specific getter needed and as well as the add function on the set itself, before passing in the appropriate converter to convert the object from
        //the old to the new.
        if (rc.getIngredients() != null && rc.getIngredients().size() > 0) {
            rc.getIngredients().forEach(ingredient -> recipe.getIngredients().add(ingredientsConverter.convert(ingredient)));
        }

        if (rc.getCategories() != null && rc.getCategories().size() > 0) {
            rc.getCategories().forEach(category -> recipe.getCategories().add(categoriesConverter.convert(category)));
        }
        
        return recipe;
    }

}
