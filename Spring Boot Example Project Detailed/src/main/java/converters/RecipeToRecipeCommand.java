/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package converters;

import commands.RecipeCommand;
import domain.Recipe;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

/**
 *
 * @author cjsm12
 */
@Component
public class RecipeToRecipeCommand implements Converter<Recipe, RecipeCommand> {

    private final IngredientToIngredientCommand ingredientsConverter;
    private final NotesToNotesCommand notesConverter;
    private final CategoryToCategoryCommand categoriesConverter;

    public RecipeToRecipeCommand(IngredientToIngredientCommand ingredientsConverter, NotesToNotesCommand notesConverter, CategoryToCategoryCommand categoriesConverter) {
        this.ingredientsConverter = ingredientsConverter;
        this.notesConverter = notesConverter;
        this.categoriesConverter = categoriesConverter;
    }
    
    @Nullable
    @Override
    public RecipeCommand convert(Recipe recipe) {
        if(recipe == null){
            return null;
        }
        
        final RecipeCommand rc = new RecipeCommand();
        rc.setRecipeId(recipe.getRecipeId());
        rc.setDescription(recipe.getDescription());
        rc.setPrepTime(recipe.getPrepTime());
        rc.setCookTime(recipe.getCookTime());
        rc.setServings(recipe.getServings());
        rc.setSource(recipe.getSource());
        rc.setUrl(recipe.getUrl());
        rc.setDirections(recipe.getDirections());
        rc.setDifficulty(recipe.getDifficulty());
        rc.setImage(recipe.getImage());
        rc.setNotes(notesConverter.convert(recipe.getNotes()));

        //Here we are doing conversions for multiple ingredients and categories...we check if both are not null and have a size greater than 0, meaning
        //there are at least one object in the sets, then we for each object in the set, add it to the object we are returning by calling the
        //specific getter needed and as well as the add function on the set itself, before passing in the appropriate converter to convert the object from
        //the old to the new.
        
        if (recipe.getIngredients() != null && recipe.getIngredients().size() > 0) {
            recipe.getIngredients().forEach(ingredient -> rc.getIngredients().add(ingredientsConverter.convert(ingredient)));
        }

        if (recipe.getCategories() != null && recipe.getCategories().size() > 0) {
            recipe.getCategories().forEach(category -> rc.getCategories().add(categoriesConverter.convert(category)));
        }
        
        return rc;
    }

}
