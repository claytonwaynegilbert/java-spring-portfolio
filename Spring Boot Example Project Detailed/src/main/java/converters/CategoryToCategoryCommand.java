/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package converters;

import commands.CategoryCommand;
import domain.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

/**
 *
 * @author cjsm12
 */

@Component
public class CategoryToCategoryCommand implements Converter<Category, CategoryCommand> {

    private final RecipeToRecipeCommand recipeConverter;

    @Autowired
    public CategoryToCategoryCommand(RecipeToRecipeCommand recipeConverter) {
        this.recipeConverter = recipeConverter;
    }
    
    @Nullable
    @Override
    public CategoryCommand convert(Category category) {
        if(category == null){
            return null;
        }
        
        final CategoryCommand cmd = new CategoryCommand();
        cmd.setCategoryId(category.getCategoryId());
        cmd.setDescription(category.getDescription());

        if(category.getRecipes() !=null && category.getRecipes().size() > 0){
            category.getRecipes().forEach(recipe -> cmd.getRecipes().add(recipeConverter.convert(recipe)));
        }
        
        return cmd;
        
    }
    
}
