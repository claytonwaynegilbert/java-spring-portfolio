/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import commands.RecipeCommand;
import domain.Recipe;
import exceptions.NullDataException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import services.RecipeService;

/**
 *
 * @author cjsm12
 */
@Controller
public class RecipeController {
    
    private RecipeService recipeService;

    @Autowired
    public RecipeController(RecipeService recipeService) {
        this.recipeService = recipeService;
    }
     

    @RequestMapping(value = "/recipe/{id}/show", method = RequestMethod.GET)
    public String showRecipe(@PathVariable(name = "id") String id, Model model) throws NullDataException {
        Integer recipeId = Integer.parseInt(id);

        Recipe foundRecipe = recipeService.findById(recipeId);

        model.addAttribute("recipe", foundRecipe);

        return "recipe/showRecipe";
    }
    
    @RequestMapping("recipe/new")
    public String showCreateRecipeForm(Model model){
        RecipeCommand rc = new RecipeCommand(); //We use the command object because that is what is used on the client sidem and then we convert it to
                                                //our actual domain recipe object when persisting to the database
        
        model.addAttribute("recipe", rc);
        
        return "recipe/recipeForm";
    }
    
    
    @RequestMapping("recipe/{id}/update")
    public String showUpdateForm(@PathVariable("id") String id, Model model) throws NullDataException{
        Integer recipeId = Integer.parseInt(id);
        
        model.addAttribute("recipe", recipeService.findById(recipeId)); //We go and find the recipe we want to update and add it to the model so that
                                                                        //it will be automatically loaded on the update from
        return "recicpe/recipeForm";
    }
    
    @RequestMapping(name="/recipe", method=RequestMethod.POST)
    public String saveOrUpdateRecipe(@ModelAttribute("recipe") RecipeCommand recipe){ //This method can do both creating and updating objects as JPA know
                                                                                      //which operation to perform depending on if an id is present or not
        RecipeCommand savedRecipe = recipeService.saveRecipeCommand(recipe);
        
        return "redirect:/recipe/" + savedRecipe.getRecipeId() + "/show"; //We then redirect to our other controller method to show the new either created
                                                                          //or updated recipe
    }
    
    @RequestMapping(value="/recipe/{id}/delete", method=RequestMethod.DELETE)
    public String deleteRecipeById(@PathVariable("id") String id){
        Integer recipeId = Integer.parseInt(id);
        
        recipeService.deleteById(recipeId);
        
        return "redirect:/";
    }
    

}
