/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import commands.IngredientCommand;
import commands.RecipeCommand;
import commands.UnitOfMeasureCommand;
import exceptions.NullDataException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import services.IngredientService;
import services.RecipeService;
import services.UnitOfMeasureService;

/**
 *
 * @author cjsm12
 */
@Controller
public class IngredientController {
    
    private RecipeService recipeService;
    private IngredientService ingredientService;
    private UnitOfMeasureService uomService;

    @Autowired
    public IngredientController(RecipeService recipeService, IngredientService ingredientService, UnitOfMeasureService uomService) {
        this.recipeService = recipeService;
        this.ingredientService = ingredientService;
        this.uomService = uomService;
    }
    
    //This controller method is used to display all the ingredients of a recipe
    @RequestMapping(name="/recipe/{recipeId}/ingredients", method=RequestMethod.GET)
    public String listIngredients(@PathVariable("recipeId") String id, Model model) throws NullDataException{
        Integer recipeId = Integer.parseInt(id);
        
        model.addAttribute("recipe", recipeService.findById(recipeId)); //We find the recipe because the recipe is what stores the ingredients we want to view
        
        return "ingredients/list";
    }
    
    //This controller method is used to display an ingredient only
    @RequestMapping(value="/recipe/{recipeId}/ingredient/{ingredientId}/show", method=RequestMethod.GET)
    public String displayRecipeIngredient(@PathVariable("recipeId") String recipeId, 
                                                  @PathVariable("ingredientId") String ingredientId, 
                                                  Model model) throws NullDataException{
        
        model.addAttribute("ingredient", ingredientService.findByRecipeIdAndIngredientId(Integer.parseInt(recipeId), Integer.parseInt(ingredientId)));
        
        return "/ingredients/list"; 
    }
    
    //This controller method is used to display the edit form of a single ingredient of a recipe
    @RequestMapping(value="/recipe/{recipeId}/ingredient/{ingredientId}/update", method=RequestMethod.GET)
    public String displayEditIngredientForm(@PathVariable("recipeId") String recipeId, 
                                                  @PathVariable("ingredientId") String ingredientId, 
                                                  Model model) throws NullDataException{
        model.addAttribute("ingredient", ingredientService.findByRecipeIdAndIngredientId(Integer.parseInt(recipeId), Integer.parseInt(ingredientId)));
        
        model.addAttribute("uomList", uomService.allUnitOfMeasures());
        
        return "/ingredients/ingredientEditForm"; 
    }
    
    //This controller method is used for either creating or updating a specific ingredient of a recipe
    @RequestMapping(value="/recipe/{recipeId}/ingredient", method=RequestMethod.POST)
    public String saveOrUpdateIngredient(@ModelAttribute(name="ingredientCommand") IngredientCommand command) throws NullDataException{
        IngredientCommand savedIngredient = ingredientService.createOrUpdateIngredientCommand(command);
        
        return "redirect:/recipe/" + savedIngredient.getRecipeId() + "/ingredient/" + savedIngredient.getIngredientId() + "/show";
    }
    
    //This controller is for taking the user to the ingredient form and since we are creating an ingredient, the page will be blank
    @RequestMapping(value="/recipe/{recipeId}/ingredient/new", method=RequestMethod.POST)
    public String createNewIngredient(@PathVariable("recipeId") String recipeId, Model model) throws NullDataException{
        Integer id = Integer.parseInt(recipeId);
        
        RecipeCommand command = recipeService.findCommandById(id);
        
        IngredientCommand ingCommand = new IngredientCommand();
        ingCommand.setRecipeId(command.getRecipeId());
        ingCommand.setUom(new UnitOfMeasureCommand());
        
        model.addAttribute("ingredient", ingCommand);
        
        model.addAttribute("uomList", uomService.allUnitOfMeasures());
        
        return "/ingredients/ingredientEditForm";
    }
    
    @RequestMapping(value="/recipe/{recipeId}/ingredient/{ingredientId}/delete", method=RequestMethod.DELETE)
    public String deleteIngredient(@PathVariable("recipeId") String recipeId, 
                                   @PathVariable("ingredientId") String ingredientId){
        
        ingredientService.deleteIngredient(Integer.parseInt(recipeId), Integer.parseInt(ingredientId));
        
        return "/recipe/" + Integer.parseInt(recipeId) + "/ingredients";
        
    }
}
