/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import services.RecipeService;

/**
 *
 * @author cjsm12
 */
@Controller
public class HomeController {
    
    private RecipeService recipeService;

    @Autowired
    public HomeController(RecipeService recipeService) {
        this.recipeService = recipeService;
    }
     
    @RequestMapping({"","/","/index"}) //Can use multiple url's to check against
    public String getHomePage(Model model){
        
        model.addAttribute("recipes", recipeService.getRecipes());
        
        return "home";
    }
    
}
