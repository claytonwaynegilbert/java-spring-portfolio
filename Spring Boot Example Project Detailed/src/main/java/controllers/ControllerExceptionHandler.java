/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import exceptions.NullDataException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author cjsm12
 */

//This controller advice annotation tells Spring to use this controller any time the specific exception described below is thrown inside the application...
@ControllerAdvice
public class ControllerExceptionHandler {

    //ExceptionHandler annotation tells Spring what excpetion we want to target...while responseStatus allows us to send a code back to the client...
    @ExceptionHandler(NullDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ModelAndView handleNullDataException(Exception exception) { //Get access to our exception that was thrown...
        
        ModelAndView modelAndView = new ModelAndView();
        
        //Set the page you want Spring to redirect to...
        modelAndView.setViewName("400error");
        //Add the exception to the model for displaying on the error page
        modelAndView.addObject("exception", exception);
        
        return modelAndView;
    }

}
