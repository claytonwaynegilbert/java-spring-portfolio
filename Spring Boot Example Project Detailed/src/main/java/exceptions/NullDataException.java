/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author cjsm12
 */
public class NullDataException extends Exception {

    /**
     * Creates a new instance of <code>NullDataException</code> without detail
     * message.
     */
    public NullDataException() {
    }

    /**
     * Constructs an instance of <code>NullDataException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public NullDataException(String msg) {
        super(msg);
    }

    public NullDataException(String message, Throwable cause) {
        super(message, cause);
    }
    
}
