/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.bigdecimal;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 *
 * @author savannahg
 */
public class BigDecimalPractice {
    
    public static void main(String[] args) {
        
        //Creating a BigDecimal using the String constructor
        BigDecimal a = new BigDecimal("22.75");
        System.out.println(a.toString());
        
        //Setting the scale of the bigdecimal above to 1, meaning  there is only one number allowed to the right
        //of the decimal point, but to be able to do that, you also need to tell the BigDecimal how to handle the
        //other numbers that will be cut off, which we do by setting the RoundingMode which rounds the number either
        //up or down depending on how you set it. We assign this result to another BigDecimal b.
        BigDecimal b = a.setScale(1, RoundingMode.HALF_UP);
        System.out.println(b.toString());
        
        //Dividing two numbers with various rounding modes and scales applied
        BigDecimal op1 = new BigDecimal("10");
        BigDecimal op2 = new BigDecimal("6");
        
        System.out.println("Different Round Mode results: ");

        BigDecimal result = op1.divide(op2, 2, RoundingMode.HALF_UP);        
        System.out.println("HALF UP w/ scale = 2: " + result.toString());
        
        result = op1.divide(op2, 1, RoundingMode.HALF_UP);
        System.out.println("HALF UP w/ scale = 1: " + result.toString());
        
        result = op1.divide(op2, 2, RoundingMode.CEILING);
        System.out.println("CEILING: " + result.toString());
        
        result = op1.divide(op2, 2, RoundingMode.HALF_DOWN);
        System.out.println("HALF_DOWN: " + result.toString());
        
        result = op1.divide(op2, 2, RoundingMode.HALF_EVEN);
        System.out.println("HALF_EVEN: " + result.toString());
        
        
        
    }
    
}
