/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.statecapitalsv2;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

/**
 *
 * @author cjsm12
 */
public class StateCapitalsV2 {

    public static void main(String[] args) {

        /*
        It will be a Java Console Application called StateCapitals2.
        It will have a class called Capital with the following properties:
        Name
        Population
        Square mileage
        It will have a HashMap that holds the names of all the states and their 
        corresponding Capital objects.  (State name is the key, Capital object is the 
        value.)
        Print the name, population, and square mileage for each capital along with its 
        corresponding state name to the screen.  (Hint: Use the key set to get each 
        Capital object out of the map one by one and then print each field of the Capital 
        object to the screen.)
        Print the states with capitals that have a population over a given value, which is 
        typed in by the user of the program.  (Hint: You will have to add code to ask the 
        user for a minimum population.  Once you have this value, go through each 
        state/capital pair as you did for the last step, but only print the information for 
        capitals that have a population above the limit.)
        */
        
        //Create HashMap instance 
        Map<String, Capital> stateCapitals = new HashMap<>();
        
        //Inserting all States and appropriate Capital object
        stateCapitals.put("Alabama", new Capital("Montgomery", 100000, 234000));  //Montgomery
        stateCapitals.put("Alaska", new Capital("Juneau", 10300, 40000));       //Juneau
        stateCapitals.put("Arizona", new Capital("Phoenix", 1210340, 230000));     //Phoenix
        stateCapitals.put("Arkansas", new Capital("Little Rock", 1343000, 543000));  //Little Rock
        stateCapitals.put("California", new Capital("Sacremento", 4431000, 2212000));  //Sacremento
        stateCapitals.put("Colorado", new Capital("Denver", 134000, 645040));    //Denver
        stateCapitals.put("Conneticut", new Capital("Hartford", 1432000, 7665400));  //Hartford
        stateCapitals.put("Deleware", new Capital("Dover", 1430900, 2430030));     //Dover
        stateCapitals.put("Florida", new Capital("Tallahassee", 990000, 53400));    //Tallahassee
        stateCapitals.put("Georgia", new Capital("Atlanta", 1000000, 135210));     //Atlanta
        stateCapitals.put("Hawaii", new Capital("Honolulu", 20004500, 80000));    //Honolulu
        stateCapitals.put("Idaho", new Capital("Boise", 546000, 364600));          //Boise
        stateCapitals.put("Illinois", new Capital("Springfield", 454000, 232500));    //Springfield
        stateCapitals.put("Indiana", new Capital("Indianapolis", 7574000, 243200));     //Indianapolis
        stateCapitals.put("Iowa", new Capital("Desmoines", 43000, 630000));          //Desmoines
        stateCapitals.put("Kansas", new Capital("Topeka", 53000, 2532000));              //Topeka
        stateCapitals.put("Kentucky", new Capital("Frankfort", 2450600, 1212000));            //Frankfort
        stateCapitals.put("Louisiana", new Capital("Baton Rouge", 4363100, 205300));            //Baton Rouge
        stateCapitals.put("Maine", new Capital("Augusta", 6311300, 224100));             //Augusta
        stateCapitals.put("Maryland", new Capital("Annapolis", 543000, 242000));          //Annapolis
        stateCapitals.put("Massachusettes", new Capital("Boston", 622400, 54400));          //Boston
        stateCapitals.put("Michigan", new Capital("Lansing", 2563200, 2355340));            //Lansing
        stateCapitals.put("Minnesota", new Capital("St. Paul", 5333000, 543300));          //St. Paul
        stateCapitals.put("Mississippi", new Capital("Jackson", 1232100, 20000));        //Jackson
        stateCapitals.put("Missouri", new Capital("Jefferson City", 12322000, 20000));    //Jefferson City
        stateCapitals.put("Montana", new Capital("Helena", 62433000, 2241200));            //Helena
        stateCapitals.put("Nebraska", new Capital("Lincoln", 223553000, 332100));             //Lincoln
        stateCapitals.put("Nevada", new Capital("Caron City", 434000, 5730000));            //Caron City
        stateCapitals.put("New Hamphsire", new Capital("Concord", 3435000, 264320));     //Concord
        stateCapitals.put("New Jersey", new Capital("Trenton", 344300, 4542200));        //Trenton
        stateCapitals.put("New Mexico", new Capital("Santa Fe", 34335000, 65600));           //Santa Fe
        stateCapitals.put("New York", new Capital("Albany", 144525000, 356300));            //Albany
        stateCapitals.put("North Carolina", new Capital("Raleigh", 1343200, 205300));          //Raleigh
        stateCapitals.put("North Dakota", new Capital("Bismark", 1235000, 20000));              //Bismark
        stateCapitals.put("Oklahoma", new Capital("Oklahoma City", 1545300, 674300));             //Oklahoma City
        stateCapitals.put("Ohio", new Capital("Columbus", 13556000, 544400));             //Columbus
        stateCapitals.put("Oregon", new Capital("Salem", 544000, 255540));              //Salem
        stateCapitals.put("Pennsylvania", new Capital("Harrisburg", 1455000, 445005));           //Harrisburg
        stateCapitals.put("Rhode Island", new Capital("Providence", 4544000, 20330));             //Providence
        stateCapitals.put("South Carolina", new Capital("Columbia", 144000, 343300));       //Columbia
        stateCapitals.put("South Dakota", new Capital("Pierre", 1000, 2043450));          //Pierre
        stateCapitals.put("Tennessee", new Capital("Nashville", 5554000, 5454200));          //Nashville
        stateCapitals.put("Texas", new Capital("Austin", 15454000, 454600));                 //Austin
        stateCapitals.put("Utah", new Capital("Salt Lake City", 4545400, 2454400));              //Salt Lake City
        stateCapitals.put("Vermont", new Capital("Montpieler", 44000, 2055005));              //Montpieler
        stateCapitals.put("Virginia", new Capital("Richmond", 1444500, 890455));               //Richmond
        stateCapitals.put("Washington", new Capital("Olympia", 1664000, 3356000));             //Olympia
        stateCapitals.put("West Virgina", new Capital("Charleston", 44000, 743556));                //Charleston
        stateCapitals.put("Wisconsin", new Capital("Madison", 100000, 453667));                  //Madison
        stateCapitals.put("Wyoming", new Capital("Cheyenne", 76000, 334000));                 //Cheyenne
        
        //Displaying Capital information
        System.out.println("---CAPITAL INFORMATION---");
        System.out.println("=========================");
        
        //1st Method / ONLY DISPLAYING CAPITAL
        //Get the collection of values from Map
        Collection<Capital> capitals = stateCapitals.values();
        
        //Loop through each value printing out appropriate information of each capital
        for(Capital currentCapital : capitals){
            System.out.println("Name: " + currentCapital.getName() + " | Population: " + currentCapital.getPopulation() + " | Square Mileage: " + currentCapital.getSquareMileage());
        }
        
        //2nd Method DISPLAYING STATE ALONG WITH CAPITAL
        //Get all the keys from the Map
        Set<String> states = stateCapitals.keySet();
        
        //Loop through all Keys getting ahold of each state as it is accessed by the loop
        //Using the .get() method on the Map to get each capital by inserting
        //the appropriate state as the key, which allows us to then access all properties
        //of the capital for display purposes
        for(String currentState : states){
            System.out.println("State name: " + currentState + " | Capital name: " + stateCapitals.get(currentState).getName() + "| Population: " + stateCapitals.get(currentState).getPopulation() + " | Square Mileage: " + stateCapitals.get(currentState).getSquareMileage());
        }
        
        //Using user input to get only Capitals with a specific population
        System.out.println("\n---STATES with a CAPITAL over specific POPULATION---");
        System.out.println("====================================================");
        
        //Scanner for user input and variable to store user input inside
        Scanner userInput = new Scanner(System.in);
        int userPopulation = 0;
        
        //Ask user what population they want Capitals population to be above
        System.out.print("Let me find all states that have a Capital population over your specified input: ");
        userPopulation = userInput.nextInt();
        
        //Storing users input in another final variable for use in Lambda below
        final int userInputFinal = userPopulation;
        
        //Getting all keys from Map
        Set<String> stateList = stateCapitals.keySet();
        
        //Looping through all keys and doing a check with each key(State) that checks
        //to see if the current States population is greater than the one specified by
        //the user. If so, print out current state's Capital and appropriate information
        for(String currentState : stateList){
            if(stateCapitals.get(currentState).getPopulation() > userPopulation){
                System.out.println(
                        "State: " + currentState + 
                        " | Capital: " + stateCapitals.get(currentState).getName() + 
                        " | Population: " + stateCapitals.get(currentState).getPopulation());
            }
        }
        
        
        //Solving problem more efficiently and quicker using lambda a filter
        System.out.println("--- LAMBDA Version ---");
        System.out.println("======================");
        List<Capital> matchingCapitals = 
                stateCapitals.values().stream()
                              .filter(c -> c.getPopulation() > userInputFinal)
                              .collect(Collectors.toList());
        
        //Print out all matching States from lambda
        for(Capital current : matchingCapitals){
            System.out.println(current.getName() + " | " + current.getPopulation());
        }
        

    }
}
