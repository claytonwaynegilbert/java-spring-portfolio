/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.shapesandperimeters;

/**
 *
 * @author cjsm12
 */
public class Rectangle extends Shape {
    
    private double length;
    private double height;
    
    public Rectangle(String color, double length, double height){
        super(color);
        this.length = length;
        this.height = height;
    }

    @Override
    public double area() {
        return length * height;
    }

    @Override
    public double perimiter() {
        return (length * 2) + (height * 2);
    }
    
    
}
