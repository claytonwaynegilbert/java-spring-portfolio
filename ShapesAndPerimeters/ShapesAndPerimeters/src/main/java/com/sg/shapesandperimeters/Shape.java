/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.shapesandperimeters;

/**
 *
 * @author cjsm12
 */
public abstract class Shape {
    
    /*
    Create a set of classes to represent a square, rectangle, triangle, and circle.  
    Have these classes inherit from a base class called Shape.  Each class will implement
    at least two methods: one called area(), which will return the area of the shape; 
    and another called perimeter(), which will return the perimeter of the shape.
    
    The base class — Shape — will have a property called color and the two methods area()
    and perimeter(), but they will be empty.  They are designed to be overridden by 
    inherited shapes, so make sure that any shape that inherits from the base class 
    implements their own versions of area() and perimeter() based on the type of shape 
    it is.  It is suggested you start with a square because this should be the easiest 
    to implement.  Create a Shape base class, inherit a square from it, and override the 
    two methods.  If you have done this correctly, it should give you the idea for the
    others.
    */
    
    protected String color;
    
    public Shape(String color){
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
    
    public abstract double area();
    
    public abstract double perimiter();
    
}
