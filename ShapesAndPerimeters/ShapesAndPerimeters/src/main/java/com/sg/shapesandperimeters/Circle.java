/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.shapesandperimeters;

/**
 *
 * @author cjsm12
 */
public class Circle extends Shape {
    
    private double perimeter;
    private double radius;
    private static final double PI = 3.14;
    
    public Circle(String color, double perimeter, double radius){
        super(color);
        this.perimeter = perimeter;
        this.radius = radius;
    }

    @Override
    public double area() {
        return PI * (radius * radius);
    }

    @Override
    public double perimiter() {
        return (2 * PI * radius);
    }
    
}
