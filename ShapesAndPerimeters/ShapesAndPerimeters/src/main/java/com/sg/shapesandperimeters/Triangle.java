/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.shapesandperimeters;

/**
 *
 * @author cjsm12
 */
public class Triangle extends Shape {
    
    private double sideA;
    private double sideB;
    private double base;
    private double height;

    public Triangle(String color, double sideA, double sideB, double base, double height) {
        super(color);
        this.sideA = sideA;
        this.sideB = sideB; 
        this.base = base;
        this.height = height;
    }

    @Override
    public double area() {
        return (base * height) / 2;
    }

    @Override
    public double perimiter() {
        return sideA + sideB + base;
    }
    
}
