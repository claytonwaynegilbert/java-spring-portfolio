/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.shapesandperimeters;

/**
 *
 * @author cjsm12
 */
public class Square extends Shape{
    
    private double length;
    private double width;
    
    public Square(String color, double length, double width){
        super(color);
        this.length = length;
        this.width = width;
    }

    @Override
    public double area() {
        return length * width;
    }

    @Override
    public double perimiter() {
        return (length * 2) + (width * 2);
    }
    
}
