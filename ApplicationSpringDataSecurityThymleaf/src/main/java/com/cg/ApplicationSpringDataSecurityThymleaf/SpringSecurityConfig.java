package com.cg.ApplicationSpringDataSecurityThymleaf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;


@Configuration
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter{
	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {		
		auth.inMemoryAuthentication()
	.withUser("john").password("{noop}test123").roles("EMPLOYEE")
		.and()
	.withUser("sally").password("{noop}test123").roles("EMPLOYEE", "MANAGER")
		.and()
	.withUser("rita").password("{noop}test123").roles("EMPLOYEE", "ADMIN");
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
		.authorizeRequests()
		.antMatchers("/resources/**").permitAll()	
		.antMatchers("/employees/**").hasRole("EMPLOYEE")
		.antMatchers("/employees/show*").hasAnyRole("MANAGER","ADMIN")
		.antMatchers("/employees/delete").hasRole("ADMIN")		
		.and()
	.formLogin()
		.loginPage("/login")
		.loginProcessingUrl("/authenticateTheUser")
		.permitAll()
	.and()
		.logout().permitAll()
		.and()
		.exceptionHandling().accessDeniedPage("/access-denied");
	}
	
	
}
