package com.cg.ApplicationSpringDataSecurityThymleaf.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cg.ApplicationSpringDataSecurityThymleaf.entity.Employee;
import com.cg.ApplicationSpringDataSecurityThymleaf.repository.EmployeeDAOSpringData;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeDAOSpringData repo;
	
	@Override
	public List<Employee> findAllEmployees() {
		return repo.findAll();
	}

	@Override
	public Employee getEmployeeById(int EmployeeId) {
		Optional<Employee> result = repo.findById(EmployeeId);
		Employee theEmployee = null;
		if(result != null) {
			theEmployee = result.get();
		}else {
			throw new RuntimeException("ERROR: No one in DB with ID of " + EmployeeId);
		}
		return theEmployee;
	}

	@Override
	public Employee saveEmployee(Employee employee) {
		Employee savedEmployee = repo.save(employee);
		return savedEmployee;
	}

	@Override
	public void deleteEmployee(int EmployeeId) {
		repo.deleteById(EmployeeId);
	}

	@Override
	public Employee updateEmployee(Employee employee) {
		Employee savedEmployee = repo.save(employee);
		
		return savedEmployee;
	}
	

}
