package com.cg.ApplicationSpringDataSecurityThymleaf.service;

import java.util.List;

import com.cg.ApplicationSpringDataSecurityThymleaf.entity.Employee;

public interface EmployeeService {
	
	List<Employee> findAllEmployees();
	
	Employee getEmployeeById(int employeeId);
	
	Employee saveEmployee(Employee employee);
	
	void deleteEmployee(int employeeId);
	
	Employee updateEmployee(Employee employee);

}
