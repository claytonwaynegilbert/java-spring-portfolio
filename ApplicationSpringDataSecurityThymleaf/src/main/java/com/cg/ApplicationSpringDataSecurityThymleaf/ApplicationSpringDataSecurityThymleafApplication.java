package com.cg.ApplicationSpringDataSecurityThymleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApplicationSpringDataSecurityThymleafApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApplicationSpringDataSecurityThymleafApplication.class, args);
	}

}
