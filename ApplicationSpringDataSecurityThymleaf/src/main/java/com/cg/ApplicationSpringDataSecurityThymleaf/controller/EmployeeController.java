package com.cg.ApplicationSpringDataSecurityThymleaf.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.cg.ApplicationSpringDataSecurityThymleaf.entity.Employee;
import com.cg.ApplicationSpringDataSecurityThymleaf.service.EmployeeService;

@Controller
@RequestMapping("/employees")
public class EmployeeController {

	@Autowired
	private EmployeeService service;
	
	@GetMapping("/list")
	public String showAllEmployees(Model model) {
		model.addAttribute("employees", service.findAllEmployees());
		
		return "employees/list-employees";
	}
	
	@GetMapping("/showAddForm")
	public String showAddEmployeeForm(Model model) {
		Employee emp = new Employee();
		model.addAttribute("employee", emp);
		
		return "employees/add-employee-form";
	}
	
	@GetMapping("/delete")
	public String removeEmployee(@RequestParam("employeeId") int employeeId) {
		service.deleteEmployee(employeeId);
		
		return "redirect:/employees/list";
	}
	
	@PostMapping("/save")
	public String addEmployee(@Valid @ModelAttribute("employee") Employee employee, BindingResult results) {
		 if(results.hasErrors()){
			 return "employees/add-employee-form";
		 }
		service.saveEmployee(employee);

		return "redirect:/employees/list";
	}
	
	@GetMapping("/showFormUpdate")
	public String showUpdateForm(@RequestParam("employeeId") int employeeId, Model model) {
		Employee employee = service.getEmployeeById(employeeId);
		model.addAttribute("employee", employee);
		
		return "/employees/update-form";
	}
	
	@PostMapping("/update")
	public String updateEmployee(@Valid @ModelAttribute("employee") Employee employee, BindingResult results) {
		if(results.hasErrors()){
			 return "employees/update-form";
		 }
		
		service.updateEmployee(employee);
		
		return "redirect:/employees/list";
	}
	
	
	
}
