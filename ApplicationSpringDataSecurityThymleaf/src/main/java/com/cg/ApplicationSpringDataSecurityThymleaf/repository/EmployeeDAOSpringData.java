package com.cg.ApplicationSpringDataSecurityThymleaf.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.cg.ApplicationSpringDataSecurityThymleaf.entity.Employee;

//Spring Data JPA
//This annotation lets you create a rest DAO that your application can receive JSON data from to work with any front end
//@RepositoryRestResource(path="members")
//You can control what the endpoint name is specifically, so its now /members instead of /employees to get list of employees
//@RepositoryRestResource(path="members")
public interface EmployeeDAOSpringData extends JpaRepository<Employee, Integer> {

	//The following are examples of accessing and sorting through data given by Spring Data using specialized endpoints
	//Pagination
	//http://localhost:8080/members?page=0
	//http://localhost:8080/members?page=1
	
	//Sorting
	//http://localhost:8080/members?sort=lastName
	//http://localhost:8080/members?sort=firstName,desc
	//http://localhost:8080/members?sort=lastname, firstName, asc

}
