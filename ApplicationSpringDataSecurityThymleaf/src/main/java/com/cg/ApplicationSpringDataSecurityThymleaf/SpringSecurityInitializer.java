package com.cg.ApplicationSpringDataSecurityThymleaf;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;


//Registers Spring Security Filters
public class SpringSecurityInitializer extends AbstractSecurityWebApplicationInitializer {

}
