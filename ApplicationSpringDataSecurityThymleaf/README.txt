RELEASE 1.0

Spring Boot employee CRUD application featuring Spring Security and in memory user authentication, alongside Thymeleaf Templating. Connected to mysql database with configuration setup through properties file. SQL scripts are provided within project as well. 

Users are as follows:

1) user: john | pass: test123 ROLES{EMPLOYEE}

2) user: sally | pass: test123 ROLES{EMPLOYEE, MANAGER}

3) user: rita | pass: test123 ROLES{EMPLOYEE, MANAGER, ADMIN}

* EMPLOYEE is basic level security needed to view employees.

* MANAGER is needed to add or update employees.

* ADMIN is needed to remove employees from database.

-------------------------------------------------------------------------------------------------------------------------------------

RELEASE 2.0 PLANS - Add Mockito unit tests as well as configure with h2 in memory database as well.

RELEASE 3.0 PLANS - User Registration form and encrypted passwords stored in database.

RELEASE 40 FINAL -  Create REST API and incorporate Angular front-end.
