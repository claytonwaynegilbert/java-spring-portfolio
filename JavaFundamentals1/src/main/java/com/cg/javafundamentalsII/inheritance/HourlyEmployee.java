/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cg.javafundamentalsII.inheritance;

import java.math.BigDecimal;

/**
 *
 * @author cjsm12
 */
public class HourlyEmployee extends Employee {

    private double wage;
    private double hours;

    public HourlyEmployee(String firstName, String lastName, String socialSecurityNumber, double wage, double hours) {
        super(firstName, lastName, socialSecurityNumber);
        
        if (wage < 0.0 || hours < 0.0) {
            throw new IllegalArgumentException("Both wage and hours must be above 0");
        }
        this.wage = wage;
        this.hours = hours;
    }

    public double getWage() {
        return wage;
    }

    public void setWage(double wage) {
        if (wage < 0.0) {
            throw new IllegalArgumentException("Wage must be above 0");
        }
        this.wage = wage;
    }

    public double getHours() {
        return hours;
    }

    public void setHours(double hours) {
        if (getHours() < 0.0) {
            throw new IllegalArgumentException("Hours must be above 0");
        }
        this.hours = hours;
    }

    @Override
    public double earnings() {
        if(hours <= 40){
            return getWage() * getHours();
        }else{
            return 40 * getWage() + (getHours() - 40) * getWage() * 1.5;
        }
    }

}
