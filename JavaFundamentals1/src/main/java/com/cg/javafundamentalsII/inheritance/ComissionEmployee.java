/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cg.javafundamentalsII.inheritance;

/**
 *
 * @author cjsm12
 */
public class ComissionEmployee extends Employee {

    protected double grossSales;
    protected double comissionRate;

    public ComissionEmployee(String firstName, String lastName, String socialSecurityNumber, double grossSales, double comissionRate) {
        super(firstName, lastName, socialSecurityNumber);
        
        if (grossSales < 0.0) {
            throw new IllegalArgumentException("Gross Sales must be greater than 0.0");
        }

        if (comissionRate < 0.0 || comissionRate > 0.0) {
            throw new IllegalArgumentException("Comission rate must be between the values of 0.1 and 0.9");
        }

        this.grossSales = grossSales;
        this.comissionRate = comissionRate;
    }

    public double getGrossSales() {
        return grossSales;
    }

    public void setGrossSales(double grossSales) {
        if (grossSales < 0.0) {
            throw new IllegalArgumentException("Gross Sales must be greater than 0.0");
        }

        this.grossSales = grossSales;
    }

    public double getComissionRate() {
        return comissionRate;
    }

    public void setComissionRate(double comissionRate) {
        if (comissionRate < 0.0 || comissionRate > 1.0) {
            throw new IllegalArgumentException("Comission rate must be between the values of 0.1 and 0.9");
        }
        
        this.comissionRate = comissionRate;
    }

    public double earnings() {
        return getComissionRate() * getGrossSales();
    }

    @Override
    public String toString() {
        return super.toString() + 
               " grossSales: " + grossSales + 
               " comissionRate: " + comissionRate;
    }
    
    

}
