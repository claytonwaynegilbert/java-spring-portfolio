/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cg.javafundamentalsII.inheritance;

/**
 *
 * @author cjsm12
 */
public class PayrollSystemPolymorphismTest {

    public static void main(String[] args) {

        //Creating 4 different employee type objects...
        ComissionEmployee comissEmployee = new ComissionEmployee("John","Riverdale","111-11-1111",1200.43, 0.7);

        BasePlusComissionEmployee  basePlusComissEmployee = new BasePlusComissionEmployee("John","Riverdale","111-11-1111",1300.0, 0.5, 400.0);
        
        HourlyEmployee hourlyEmployee = new HourlyEmployee("John","Riverdale","111-11-1111",22.50, 40);
        
        SalariedEmployee salariedEmployee = new SalariedEmployee("John","Riverdale","111-11-1111", 3000);
        
        //Creating the superclass employee array to store all objects into, this can be done since all employee types extend employee
        Employee[] employees = new Employee[4];
        employees[0] = comissEmployee;
        employees[1] = basePlusComissEmployee;
        employees[2] = hourlyEmployee;
        employees[3] = salariedEmployee;
        
        //Loop through all employee types
        for(Employee currentEmployee : employees){
            
            //See if current employee we are on is of type BasePlus...
            if(currentEmployee instanceof BasePlusComissionEmployee){
                                
                //If it is of this type, cast the employee type to the BasePlusComissionEmployee type 
                //so that we can access the appropriate base salary field of this class
                BasePlusComissionEmployee employee = 
                        (BasePlusComissionEmployee) currentEmployee;
                
                //Print old information of this employee's salary
                System.out.println("Old salary for employee : " + employee.getBaseSalary());
                
                //Change employee's salary
                employee.setBaseSalary(1.10 * employee.getBaseSalary());
                
                //Print out new salary of employee
                System.out.println("New salary for employee : " + employee.getBaseSalary());
            }
            
            //If it wasn't the employee we wanted to manipulate, just print out their earnings as well
            System.out.println("Current employee salary : " + currentEmployee.earnings());
        }

    }
}
