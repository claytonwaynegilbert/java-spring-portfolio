/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cg.javafundamentalsII.inheritance;

import java.math.BigDecimal;

/**
 *
 * @author cjsm12
 */
public abstract class Employee {
    
    private final String firstName;
    private final String lastName;
    private final String socialSecurityNumber;

    public Employee(String firstName, String lastName, String socialSecurityNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getFristName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }
    
    public abstract double earnings();

    @Override
    public String toString() {
        return "FirstName: " + firstName + " LastName: " + lastName + " SocialSecurityNumber: " + socialSecurityNumber;
                
    }
    
    
    
    
}
