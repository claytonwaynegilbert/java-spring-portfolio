/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cg.javafundamentalsII;

/**
 *
 * @author cjsm12
 */
public enum Books {
    
    //Enum Constants with title and year
    AWIT("A Wrinkle In Time","1975"),
    GOTFAI("Game Of Thrones: Fire and Ice","2008"),
    TCON("The Chronicles Of Narnia","2006"),
    HPATPS("Harry Potter and The Philosophers Stone","2004"),
    PJATLT("Percy Jackson and The Lightning Thief","2013");
    
    //Fields of the enum
    private String title;
    private String year;

    //Setting the title and year within the enum when creating the enums above
    Books(String title, String year){
        this.title = title;
        this.year = year;
    }

    //Getters for getting access to each enums data
    public String getTitle() {
        return title;
    }

    public String getYear() {
        return year;
    }

    //Custom toString method for displaying each enum out
    @Override
    public String toString() {
        return title + " came out in " + year;
    }
    
    
    
}
