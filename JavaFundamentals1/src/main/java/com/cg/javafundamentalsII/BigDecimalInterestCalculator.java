/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cg.javafundamentalsII;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 *
 * @author cjsm12
 */
public class BigDecimalInterestCalculator {

    public static void main(String[] args) {

        BigDecimal amount;
        BigDecimal principal = new BigDecimal("1000.0");
        BigDecimal interest = new BigDecimal("0.05");

        for (int year = 1; year <= 10; year++) {
            amount = principal.multiply(interest.add(BigDecimal.ONE).pow(year));

            System.out.printf("%4d%20s%n", year, "$" + amount.setScale(2, RoundingMode.HALF_UP));
            
        }

    }

}
