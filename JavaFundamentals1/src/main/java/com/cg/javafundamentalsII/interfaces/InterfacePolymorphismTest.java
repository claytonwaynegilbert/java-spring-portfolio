/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cg.javafundamentalsII.interfaces;

/**
 *
 * @author cjsm12
 */
public class InterfacePolymorphismTest {
    
    public static void main(String[] args) {
        
        //Creating a Payable array, which is an interface
        Payable[] payables = new Payable[4];
        //Pointing all the payable variable references to either a invoice object or employee object,
        //this can be done because the invoice directly implements the payable interface and salariedemployee 
        //extends employee, which implements the payable interface, so both objects can be seen as an Payable type.
        payables[0] = new Invoice("12345","Silver Spoon",30,4.75);
        payables[1] = new Invoice("12345678","Ceramic Plate",11,9.99);
        payables[2] = new SalariedEmployee("John","Jones","222-22-2222",34.50);
        payables[3] = new SalariedEmployee("Jacob","Ronald","333-33-3333",17.89);
        
        //Looping through all objects within the array of payables and calling each objects toString() method
        for(Payable payable : payables){
            System.out.println(payable.toString());
        }
        
    }
    
}
