/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cg.javafundamentalsII.interfaces;

/**
 *
 * @author cjsm12
 */
public abstract class Employee implements Payable{
    //Employee class doesn't have to give implementation of the Payable interface since the class is abstract already
    //however all derived classes of type employee must implement the payable interface method in order to work
    
    private final String firstName;
    private final String lastName;
    private final String socialSecurityNumber;

    public Employee(String firstName, String lastName, String socialSecurityNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getFristName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }
    
    public abstract double earnings();

    @Override
    public String toString() {
        return "FirstName: " + firstName + " LastName: " + lastName + " SocialSecurityNumber: " + socialSecurityNumber;
                
    }
    
    
    
    
}
