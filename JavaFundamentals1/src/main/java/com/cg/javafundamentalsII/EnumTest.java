/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cg.javafundamentalsII;

import java.util.EnumSet;

/**
 *
 * @author cjsm12
 */
public class EnumTest {
    
    public static void main(String[] args) {
        
        //Printing out all books from the book enum 
        System.out.println("All books...");
        for(Books book : Books.values()){
            System.out.println(book);
        }
        
        System.out.println("\n\n");
        
        //Printing out a specified range of books
        System.out.println("Books within specified range...");
        for(Books book : EnumSet.range(Books.AWIT, Books.TCON)){
            System.out.println(book);
        }
        
        
        
    }
    
}
