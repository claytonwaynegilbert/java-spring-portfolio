/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cg.javafundamentalsII.intstreamoperations;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 *
 * @author cjsm12
 */
public class StreamsAndStrings {

    public static void main(String[] args) {

        String[] fruits = {"Apple", "Orange", "Banana", "Kiwi", "Strawberry"};

        System.out.printf("Fruits in all uppercase : %s%n",
                Arrays.asList(fruits).stream()
                        .map(s -> s.toUpperCase())
                        .sorted()
                        .collect(Collectors.toList()));

        Arrays.asList(fruits).stream()
                .filter(s -> s.compareToIgnoreCase("m") > 0)
                .sorted(String.CASE_INSENSITIVE_ORDER)
                .collect(Collectors.toList());
                                 
        
    }
    
}
