/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cg.javafundamentalsII.intstreamoperations;

import java.util.stream.IntStream;

/**
 *
 * @author cjsm12
 */
public class IntStreamOperations {

    public static void main(String[] args) {

        //Creating an array that we will use with the IntStream type to create
        //streams with
        int[] values = {3, 10, 12, 5, 7, 22, 11, 32};

        System.out.print("Original Values: ");

        //Using the foreach terminal operation to print out all values of the array
        IntStream.of(values)
                .forEach(value -> System.out.println(value));

        //Getting max number
        int max = IntStream.of(values)
                .max()
                .getAsInt();

        System.out.println(max);

        //Getting the minimum number
        int min = IntStream.of(values)
                .min()
                .getAsInt();

        System.out.println(min);

        //Getting the average number
        double average = IntStream.of(values)
                .average()
                .getAsDouble();

        System.out.println(average);

        //Displaying the total number of elements in the array
        System.out.println(IntStream.of(values).count());

        //Here we filter out all the numbers and find only the even ones, put them into a new stream,
        //and then sort them before printing all of them out
        IntStream.of(values)
                .filter(number -> number % 2 == 0)
                .sorted()
                .forEach(number -> System.out.print("Value is : " + number));

        
        //Here we find all the numbers that are odd, and then call the .map function, which
        //takes a number and does something to that number, whether it be convert it to another type
        //or in this case, multiply the number by 10
        IntStream.of(values)
                .filter(number -> number % 2 != 0)
                .map(number -> number * 10)
                .sorted()
                .forEach(number -> System.out.print("Value is : " + number));
    }

}
