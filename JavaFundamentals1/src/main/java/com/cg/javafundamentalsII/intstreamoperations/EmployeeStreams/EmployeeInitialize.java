/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cg.javafundamentalsII.intstreamoperations.EmployeeStreams;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 *
 * @author cjsm12
 */
public class EmployeeInitialize {

    public static void main(String[] args) {

        Employee[] employees = {
            new Employee("John", "Jacobs", 3250.54, "IT"),
            new Employee("Jacob", "Lee", 10250.54, "Sales"),
            new Employee("Bruce", "Hoble", 5350.54, "Media"),
            new Employee("Jason", "Maven", 4250.54, "Marketing"),
            new Employee("Westin", "Wayne", 250.54, "IT"),};

        //Create a list of employees based off the array above
        List<Employee> employeeList = Arrays.asList(employees);

        //Creating a predicate that returns a true or false value to use in filter operations
        Predicate<Employee> fourToSixThousand = e -> (e.getSalary() >= 4000 && e.getSalary() <= 6000);

        //using the predicate above to filter out all employees out of salary range, and then
        //sorting the employees lowest to highest salary using the comparator function that
        //takes as an argument how you want to compare employees with.
        employeeList.stream()
                .filter(fourToSixThousand)
                .sorted(Comparator.comparing(e -> e.getSalary()))
                .collect(Collectors.toList());

        //Here we are filtering just the same but instead are finding the first 
        //instance of the employee that matches our description and are returning that
        employeeList.stream()
                .filter(fourToSixThousand)
                .findFirst()
                .orElse(null);

        //Creating two function lambdas and storing in variables, function interface
        //is used with the comparator lambda where we compare two employee objects first
        //using their last name, then their first name
        Function<Employee, String> byFirstName = e -> e.getFirstName();
        Function<Employee, String> byLastName = e -> e.getLastName();

        //Creating a custom comparator using the two functions above
        Comparator<Employee> lastThenFirst
                = Comparator.comparing(byLastName).thenComparing(byFirstName);

        //Sort out all employees by their last name, then print out all employees
        employeeList.stream()
                .sorted(lastThenFirst)
                .forEach(e -> System.out.print(e));

        //Same as above, but in reverse order
        employeeList.stream()
                .sorted(lastThenFirst.reversed())
                .forEach(e -> System.out.print(e));

        //We call the map function in order to convert the stream from employee objects
        //to a new stream of just last names Strings, we then remove any duplicates
        //before we sort them in ascending order by default and print out each one.
        employeeList.stream()
                .map(e -> e.getLastName())
                .distinct()
                .sorted()
                .forEach(e -> System.out.print(e));

        employeeList.stream()
                .sorted(lastThenFirst)
                .map(e -> e.getLastName())
                .forEach(e -> System.out.print(e));

        //Converting employees to a double, then adding up all the values in the stream
        employeeList.stream()
                .mapToDouble(e -> e.getSalary())
                .sum();

        //Same as above, however we are getting the average of all the salaries
        employeeList.stream()
                .mapToDouble(e -> e.getSalary())
                .average()
                .getAsDouble();
        
        //Filtering out all employees that don't match criteria, before we 
        //collect them in a way that each department is returns as the key and
        //a list of employees belonging to that department is returned as the value
        employeeList.stream()
                    .filter(e -> e.getSalary() <= 2000.50)
                    .sorted(lastThenFirst)
                    .collect(Collectors.groupingBy(e -> e.getDepartment()));
        
                    
                    

    }

}
