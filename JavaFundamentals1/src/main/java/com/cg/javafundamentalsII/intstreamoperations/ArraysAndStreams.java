/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cg.javafundamentalsII.intstreamoperations;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author cjsm12
 */
public class ArraysAndStreams {

    public static void main(String[] args) {

        Integer[] values = {3, 10, 12, 5, 7, 22, 11, 32};

        System.out.printf("Sorted values : %s%n", 
                           Arrays.asList(values).stream()
                                                .sorted()
                                                .collect(Collectors.toList()));
        
        List<Integer> greaterThan4 =
                Arrays.stream(values)
                      .filter(number -> number > 4)
                      .sorted()
                      .collect(Collectors.toList());
        
        System.out.println(greaterThan4);
        
        
        
        
        
        
    }

}
