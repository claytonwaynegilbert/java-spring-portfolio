/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cg.javafundamentalsII.StringMethods;

/**
 *
 * @author cjsm12
 */
public class StringReplaceTrimToCharArray {
    
    public static void main(String[] args) {
        
        String hello = "hello";
        String spaces = "    spaces    ";
        String convertToArray = "array";
        
        //The .replace() method looks at all the characters of the first argument and replaces them
        //with all with the second argument
        System.out.println(hello.replace("l", "L"));
        
        //The .trim method removes all whitespaces from both beginning and end of a string
        System.out.println(spaces.trim());
        
        //toCharArray converts a String to an Character array
        char[] array = convertToArray.toCharArray();
        
        //Printing each element of the new array out
        for(char letter : array){
            System.out.println(letter);
        }
        
        
        
        
    }
    
}
