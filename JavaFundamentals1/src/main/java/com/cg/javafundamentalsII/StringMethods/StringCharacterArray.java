/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cg.javafundamentalsII.StringMethods;

/**
 *
 * @author cjsm12
 */
public class StringCharacterArray {
    
    public static void main(String[] args) {
        
        //Declare String assigned to a String literal
        String hello = "Hello";
        
        //Declare String array that is not filled with any String objects
        char[] charArray = new char[5];
        
        //The .length method on the String class lets you see how many characters make up the String
        System.out.println("The length of the String hello above is " + hello.length());

        //The getChars method on the String array copies contents from a String to an array, the first paramater
        //specifies where to start the index on the String you are copying, the second paramater specifies where to stop 
        //the copying of the String, the third is the array you want to copy to, and lastly is where in the array you 
        //want to start copying, which is index 0.
        hello.getChars(0, 5, charArray, 0);
        
        //For loop to print out all characters of array
        for(char character : charArray){
            System.out.println(character);
        }
        
        
    }
    
}
