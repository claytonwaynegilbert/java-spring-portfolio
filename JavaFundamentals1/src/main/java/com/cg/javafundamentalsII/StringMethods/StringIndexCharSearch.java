/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cg.javafundamentalsII.StringMethods;

/**
 *
 * @author cjsm12
 */
public class StringIndexCharSearch {

    public static void main(String[] args) {

        String letters = "abcdefghijklmabcdefghijklm";

        //Prints out the index position of the letter in question
        System.out.println("a is located in the letters String at index " + letters.indexOf("a"));

        //Prints out the index position of the letter in question but doesn't start searching until after index 20
        System.out.println("Finding the second a occurence in the letters String by specifiying where to begin searching: " + letters.indexOf("a", 20));

        //Prints out the index position of the last occurence of the letter in question
        System.out.println("The last occurence of the letter b is : " + letters.lastIndexOf("b"));

        //Prints out the index position of the last known occurence of the word in question
        System.out.println("The last occurence of the word def in the String is : " + letters.lastIndexOf("def"));

    }

}
