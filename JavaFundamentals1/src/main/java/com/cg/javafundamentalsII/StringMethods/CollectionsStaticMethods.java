/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cg.javafundamentalsII.StringMethods;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author cjsm12
 */
public class CollectionsStaticMethods {
    
    public static void main(String[] args) {
        
        String[] suits = {"Hearts","Diamonds","Spades","Clubs"};
        
        List<String> list = Arrays.asList(suits);
        
        //Sorting the list above but in reverse order, if we wanted default sorting
        //we would just pass the list we want sorted by itself with no other arguments
        Collections.sort(list, Collections.reverseOrder());
        
        //The max and min methods return the maximum value and minimum value from the list
        //passed as an argument, keep in mind that you need to pass in types that are comparable
        //which we did as Strings are comparable, if we don't pass in types that are not comparable
        //we must also pass in a comparator that specifies how to compare the types you are passing in
        Collections.max(list);
        Collections.min(list);
        
        //Copying one list to another
        List<String> listToBeCopiedInto = new ArrayList<>();
        Collections.copy(listToBeCopiedInto, list);
        
        //The .fill method fills the entire list passed in with the specified value passed in
        //as the second argument
        Collections.fill(list, "Cool");
        
        //binarysearch searches through the list passed in for the item passed in as the second
        //argument, and if found, returns the index that that item can be found. Otherwise it returns
        //a negative value
        int indexFound = Collections.binarySearch(list, "Diamonds");
        
        //Printing out results of binary search
        if(indexFound >= 0){
            System.out.println("Diamonds was found at index position " + indexFound);
        }else{
            System.out.println("Diamonds was not found");
        }
        
        //Same as  copy above
        List<String> listToAddAllTo = new ArrayList<>();
        Collections.addAll(listToAddAllTo, suits);
        
        //Frequency returns the result of how many times the specified value appears
        //in the specified list passed in
        int result = Collections.frequency(list, "Diamonds");
        System.out.println("Diamonds was found in the list " + result + " times.");
        
        
        
        
        
        
        
        
        
    }
    
}
