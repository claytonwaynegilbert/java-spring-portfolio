/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cg.javafundamentalsII.StringMethods;

/**
 *
 * @author cjsm12
 */
public class StringCompare {

    public static void main(String[] args) {

        String s1 = new String("Hello");
        String s2 = "goodbye";
        String s3 = "Happy Birthday";
        String s4 = "happy birthday";

        //.equals compares the actual content of the two strings, so this would equate to true
        if (s1.equals("Hello")) {
            System.out.println("They're Equal");
        } else {
            System.out.println("They're not equal");
        }

        //The double equals operand sign compares the references of the tqo Strings, so this would equate to false since these 
        //two objects are pointing to different areas of memory
        if (s1 == "Hello") {
            System.out.println("They're equal");
        } else {
            System.out.println("They're not equal");
        }

        if (s3.equalsIgnoreCase(s4)) {
            System.out.println("They're equal");
        } else {
            System.out.println("They're not equal");
        }
        
        //The .compareTo method compares the first letter of the first String to the first letter of the second string
        //and see if that letter in the alphabet comes before the other Strings letter, and if so, return a negative
        //value equal to how far apart the letters are in the alphabet, the same is true for the opposite(letter comes after another)
        //and lastly, if letters are the same, a 0 is returned.
        System.out.println(s1.compareTo(s2));

        
    }

}
