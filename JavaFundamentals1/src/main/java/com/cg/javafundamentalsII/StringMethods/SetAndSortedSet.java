/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cg.javafundamentalsII.StringMethods;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 *
 * @author cjsm12
 */
public class SetAndSortedSet {

    public static void main(String[] args) {

        String[] setValues = {"one", "two", "three", "one"};

        //A set doesn't allow duplicates, so when you pass the array into the constructor,
        //the set will automatically get rid of all of the duplicate strings for you,
        //also the set interface doesn't keep track of insertion order. So printing the
        //items from the set back out will result in an random order.
        Set<String> genericSet = new HashSet<>(Arrays.asList(setValues));

        for (String number : genericSet) {
            System.out.println(number);
        }

        //The sortedset also doesn't allow duplicates, but it maintains insertion order,
        //also treeset is a specific implementation of the sortedset interface
        SortedSet<String> sortedSet = new TreeSet<>(Arrays.asList(setValues));

        for (String number : sortedSet) {
            System.out.println(number);
        }
        
    }

}
