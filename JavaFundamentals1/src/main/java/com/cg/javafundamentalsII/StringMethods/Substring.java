/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cg.javafundamentalsII.StringMethods;

/**
 *
 * @author cjsm12
 */
public class Substring {

    public static void main(String[] args) {

        String letters = "abcdefghijklmabcdefghijklm";

        //When all you do is put an index into the paramaters of the substring method, the method will
        //return you all the remaining letters starting after that index position
        System.out.println("Printing out all letters after the index 20: " + letters.substring(20));

        //When you specify a biginning and ending index, the new word includes the beginning index but doesn't include the last index.
        System.out.println("Printing out the letters starting from the specified beginning index and up to but not"
              + " including the last index : " + letters.substring(3, 6));

    }

}
