/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cg.javafundamentalsII.StringMethods;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 *
 * @author cjsm12
 */
public class MapInterface {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        Map<String, Integer> values = new HashMap<>();

        System.out.println("Enter a String: ");
        //Grab the entire sentence the user types...
        String sentence = sc.nextLine();

        //Split the users sentence into a bunch of individual words by splitting by the space
        String[] tokens = sentence.split(" ");
        //Looping through all the split up words...
        for (String word : tokens) {
            //and check if the map already contains that word, if so...
            if (values.containsKey(word)) {
                //Get the value of that word...
                int value = values.get(word);
                //And reinsert the word along with the value of the word + 1
                values.put(word, value + 1);
            }else{
                //Otherwise put the word into the map
                values.put(word, 1);
            }
        }
        
        //Get all the keys from the map
        Set<String> keySet = values.keySet();
        
        //Create a sortedset for the keys so that we maintain the order
        SortedSet<String> keys = new TreeSet<>(keySet);
        
        //Loop through all the keys and get the matching value of the keys and print them out
        for(String key : keys){
            System.out.println(key + " : " + values.get(key));
        }

    }

}
