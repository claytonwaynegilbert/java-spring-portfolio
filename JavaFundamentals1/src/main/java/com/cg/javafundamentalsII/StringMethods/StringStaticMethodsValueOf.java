/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cg.javafundamentalsII.StringMethods;

/**
 *
 * @author cjsm12
 */
public class StringStaticMethodsValueOf {

    public static void main(String[] args) {

        //The .valueOf method converts whatever passed into the paramaters into a String and returns it back
        String convertedNumber = String.valueOf(34);
        //Here we check to see if the String given back from the .valueOf method is equal to the string literal 34.
        if (convertedNumber.equals("34")) {
            System.out.println(convertedNumber + " was converted to a String");
        }else{
            System.out.println("The number was not converted to a String");
        }
        
    }

}
