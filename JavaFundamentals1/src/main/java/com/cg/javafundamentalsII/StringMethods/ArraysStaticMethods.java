/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cg.javafundamentalsII.StringMethods;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author cjsm12
 */
public class ArraysStaticMethods {
    
    public static void main(String[] args) {
        
        //Create a fruit array
        String[] fruit = {"Apple","Banana","Orange"};
        
        //Creating a linked list and copying all the elements of the fruit array above to
        //the linkedlist by calling the asList method on the Arrays class. This creates a 
        //read only copied list inside the LinkedList, so deletion would not be an option.
        LinkedList<String> fruitList = new LinkedList<>(Arrays.asList(fruit));
        
        //Using various LinkedList methods to add elements to the list
        fruitList.addFirst("Grape");
        fruitList.addLast("Kiwi");
        fruitList.add("Peach");
        fruitList.add(3, "Paire");
        
        //Here we convert the linkedlist back into an array by calling the .toArray
        //method on the linked list and passing in a new array of the appropriate type
        //as well as initializing the size of it by calling the .size() method on the 
        //linkedlist
        fruit = fruitList.toArray(new String[fruitList.size()]);        
        
        
    }
    
}
