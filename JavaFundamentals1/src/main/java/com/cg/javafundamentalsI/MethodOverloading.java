/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cg.javafundamentalsI;

/**
 *
 * @author cjsm12
 */
public class MethodOverloading {

    public static void main(String[] args) {

        System.out.printf("Calling the integer square method %d%n", square(7));

        System.out.printf("Calling the double square method %.2f%n", square(7.5));

    }

    //Two methods with same name but different argument types, which is key in making a method overloaded.
    public static int square(int number) {
        return number * number;
    }

    public static double square(double number) {
        return number * number;
    }

}
