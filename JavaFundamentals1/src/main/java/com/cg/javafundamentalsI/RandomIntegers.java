/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cg.javafundamentalsI;

import java.security.SecureRandom;

/**
 *
 * @author cjsm12
 */
public class RandomIntegers {
    
    
    public static void main(String[] args) {
        
        //Declare type SecureRandom which is safer to use in security and games as its output is true random of nature compared to just the random,
        //which can be manipulated to have the output predicted.
        SecureRandom random = new SecureRandom();
        
        //Loop to print out 20 random numbers
        for(int counter = 1; counter <= 20; counter++){
            
            //Get a random number between 1 and 6.
            int result = random.nextInt(6) + 1;
            
            //Print out number
            System.out.print(result);
            
            //Check to see if 5 numbers have been printed in a line, and if so, create a new line for display purposes
            if(counter % 5 == 0){
                System.out.println("");
            }
        }
        
        
        
    }
    
}
