/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cg.javafundamentalsI;

import java.security.SecureRandom;

/**
 *
 * @author cjsm12
 */
public class Craps {

    //Declare random class for generating true random values
    private static final SecureRandom random = new SecureRandom();

    //Declaring enum for use of putting names to magic hard coded numbers
    private enum GAMESTATUS {
        WON, LOST, CONTINUE
    };

    //Magic number constants that signify winning, losing, and continue playing
    private static final int SNAKE_EYES = 2;
    private static final int TREY = 3;
    private static final int SEVEN = 7;
    private static final int YO_LEVEN = 11;
    private static final int BOX_CARS = 12;

    public static void main(String[] args) {

        System.out.print("======Welcome to Craps!=======");

        //Declaring variables for keeping track of current point if rolled
        int myPoint = 0;
        //GameStatus variable for keeping track of players current status
        GAMESTATUS status;
            
            //Calling static method to roll the dice and return value of two 6 sided dice.
            int sumOfDice = rollDice();

            //Do a switch statement on that result
            switch (sumOfDice) {
                case SEVEN:
                case YO_LEVEN:
                    status = GAMESTATUS.WON;
                    break;
                case SNAKE_EYES:
                case TREY:
                case BOX_CARS:
                    status = GAMESTATUS.LOST;
                    break;
                default:
                    //If the roll was neither a straight win or loss, then we need to keep rolling until a win or lose condition is met
                    status = GAMESTATUS.CONTINUE;
                    //Keep track of current roll
                    myPoint = sumOfDice;
                    //Display point total to user
                    System.out.printf("The point is %d ", myPoint);
                    break;
            }

        //If the users status is currently on continue after the switch statment...
        while (status == GAMESTATUS.CONTINUE) {
            //Roll the dice again...
            sumOfDice = rollDice();
            
            //Do one last check to see if we rolled a winning roll, otherwise we lost
            if(sumOfDice == myPoint){
                status = GAMESTATUS.WON;
            }else{
                status = GAMESTATUS.LOST;
            }
        }
        
        //Display appropriate win or lose message
        if(status == GAMESTATUS.WON){
            System.out.println("Player WON");
        }else{
            System.out.println("Player LOSES");
        }

    }

    //Method to roll dice...
    private static int rollDice() {

        //Generate random number between 1 and 6 for both dice...
        int dice1 = random.nextInt(6) + 1;
        int dice2 = random.nextInt(6) + 1;

        //Calculate the sum of the dice
        int sum = dice1 + dice2;

        //Print out the sum to the user
        System.out.printf("The user rolled a %d ", sum);

        //return sum to caller
        return sum;
    }

}
