/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cg.javafundamentalsI.DeckOfCards;

import java.security.SecureRandom;

/**
 *
 * @author cjsm12
 */
public class Deck {
    
    private Card[] deck;
    private int currentCard;
    private static final int NUMBER_OF_CARDS = 52;
    private SecureRandom random = new SecureRandom();
    
    public Deck(){
        
        String[] faces = {"Ace","Deuce","Three","Four","Five","Six","Seven","Eight","Nine","Ten","Jack","Queen","King"};
        String[] suits = {"Hearts","Diamonds","Clubs","Spades"};
        currentCard = 0;
        deck = new Card[NUMBER_OF_CARDS];
        
        for(int count = 0; count < deck.length; count++){
            deck[count] = new Card(faces[count % 13], suits[count / 13]);
        }
    }
    
    public void shuffleDeck(){
        currentCard = 0;
        
        for(int firstCard = 0; firstCard < deck.length; firstCard++){
            
            int secondCard = random.nextInt(NUMBER_OF_CARDS);
            
            Card temp = deck[firstCard];
            deck[firstCard] = deck[secondCard];
            deck[secondCard] = temp;
            
        }
    }
    
    public Card dealCard(){
        
        if(currentCard < deck.length){
            Card card = deck[currentCard];
            currentCard++;
            return card;
        }else{
            return null;
        }
    }
    
    
    
}
