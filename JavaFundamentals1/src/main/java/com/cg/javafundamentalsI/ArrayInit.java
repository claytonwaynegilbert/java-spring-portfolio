/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cg.javafundamentalsI;

/**
 *
 * @author cjsm12
 */
public class ArrayInit {
    
    public static void main(String[] args) {
        
        //Creating an integer array with 10 elements
        int[] intArray = new int[10];
        
        //Looping through all elements of array and dynamically inserting 10 elements
        for(int i = 0; i < intArray.length; i++){
            intArray[i] = i + 1;
        }
        

        //Creating and intitalizing an String array
        String[] fruitList = {"Apple", "Banana", "Orange", "Strawberry", "Kiwi"};
        
        //Looping through all elements of the fruit array and printing them out
        for(int i = 0; i < fruitList.length; i++){
            System.out.println(fruitList[i]);
        }
        
        //Using enhanced for loop to get sum of all elements of an array
        int[] anotherIntArray = {1,4,10,23,7,8,12,11,9};
        int total = 0;
        //Enhanced for loop
        for(int number: anotherIntArray){
            //Keep adding each element to running total
            total += number;
        }
        //Display total
        System.out.printf("%d", total);
        
        
        
    }
    
}
