$(document).ready(function () {

    loadItems();

    $('#make-purchase-button').click(function (event) {
        var productId = $('#item-display-box').val();
        var moneyToPay = $('#money').text();

        if (productId.length < 0 || (productId < 1 || productId > 9)) {
            $('#messages').empty();
            $('#messages').append('<p style="text-align: center; font-size: 10px">Error: Please enter a product # or make sure the product # is within range.</p>');
        }
        
        if(moneyToPay.length == 0){
            $('#messages').empty();
            $('#messages').append('<p style="text-align: center; font-size: 11px">Error: Please enter money into the machine.</p>');
        }

        $.ajax({
            type: 'GET',
            url: 'http://localhost:8080/money/' + moneyToPay + '/item/' + productId,
            success: function(changeObject){
                $('#messages').text('Thank You!');
                $('#change').text(changeObject.quarters + ' Quarters ' + changeObject.dimes + ' Dimes ' + changeObject.nickels + ' Nickels ' + changeObject.pennies + ' Pennies ');
                var initialQuantity = $.trim($('#quantity' + productId).text());
                var initialQuantityNumber = parseInt(initialQuantity, 10);
                var newQuantity = initialQuantityNumber - 1;
                $('#quantity' + productId).text(newQuantity);
                $('#money').text('');
                $('#item-display-box').val('');
                
            },
            error: function(statusCode){
                $('#messages').text('');   
                var errorMessage = $.parseJSON(statusCode.responseText);
                console.log(errorMessage.message);
                $('#messages').append('<p style="text-align: center; font-size: 16px; margin-top: 8px">' + errorMessage.message + '</p>');    
            }
        });
    });

    $('#get-change-button').click(function (event) {
        $('#change').empty();
        $('#messages').empty();
        var change = getCurrentMoneyInputed();
        var penniesTotal = change * 100;
        var quarter = parseInt("25");
        var dime = parseInt("10");
        var nickel = parseInt("5");
        var penny = parseInt("1");

        var quarterCount = penniesTotal / quarter;
        penniesTotal = penniesTotal %= quarter;
        var dimeCount = penniesTotal / dime;
        penniesTotal = penniesTotal %= dime;
        var nickelCount = penniesTotal / nickel;
        penniesTotal = penniesTotal %= nickel;
        var pennyCount = penniesTotal / penny;
        penniesTotal = penniesTotal %= penny;
        var totalCount = quarterCount + dimeCount + nickelCount + pennyCount;

        if (totalCount <= 0) {
            $('#messages').append('<p style="text-align: center; font-size: 16px; margin-top: 8px">No change to return</p>');
            return false;
        }

        $('#change').text(quarterCount.toFixed(0) + ' Quarters ' + dimeCount.toFixed(0) + ' Dimes ' + nickelCount.toFixed(0) + ' Nickels ' + pennyCount.toFixed(0) + ' Pennies ');
        $('#money').text(''); 
    });
});

function loadItems() {
    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/items',
        success: function (itemsArray, status) {
            for (var i = 0; i < itemsArray.length; i++) {
                $('#id' + (i + 1)).text(itemsArray[i].id);
                $('#name' + (i + 1)).text(itemsArray[i].name);
                $('#price' + (i + 1)).text('$' + itemsArray[i].price.toFixed(2));
                $('#quantity' + (i + 1)).text(itemsArray[i].quantity);
            }
        },
        error: function () {
            for (var i = 1; i <= 9; i++) {
                $('#img' + i).hide();
                $('#quantity-header' + i).hide();
            }
            $('#messages').append('<p style="text-align: center; margin-top: 6px; font-size: 13px">Error: Vending Machine is down at the moment.</p>');
        }
    });
}

function addMoneyToMachine(amount) {
    $('#change').empty();
    var currentTotal = parseFloat($('#money').text()) || 0;
    var currentTotalInPennies = currentTotal * 100;
    var amountComingInPennies = parseFloat(amount.toString());
    var centTotal = currentTotalInPennies + amountComingInPennies;
    var centsRepresentedInDollars = centTotal / 100;

    $('#money').text(centsRepresentedInDollars.toFixed(2));
}

function getCurrentMoneyInputed() {
    var currentTotal = parseFloat($('#money').text()) || 0;
    return currentTotal;
}

function stageProductForPurchase(id) {
    $('#messages').empty();
    $('#change').empty();
    $('#item-display-box').val(id);
}
