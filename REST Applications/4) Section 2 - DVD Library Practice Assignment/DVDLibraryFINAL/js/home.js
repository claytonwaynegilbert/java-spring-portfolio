$(document).ready(function(){
   
    loadDvds();
    
    $('#createDvdButton').on('click', function(event){
        $('#searchForm').hide();
        $('#dvdTable').hide();
        $('#createDvdHeader').show();
        $('#createDvdForm').show();
    });
       
    
    
    
    
});

function loadDvds(){
    clearDvdTable();
    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/dvds',
        success: function(dvdList){
            $.each(dvdList, function(index, dvd){
                var dvdTitle = dvd.title;
                var releaseDate = dvd.realeaseYear;
                var director = dvd.director;
                var rating = dvd.rating;
                
                var tableRow = '<tr>';
                    tableRow += '<td style="text-align: center">' + dvdTitle + '</td>';
                    tableRow += '<td style="text-align: center">' + releaseDate + '</td>';
                    tableRow += '<td style="text-align: center">' + director + '</td>';
                    tableRow += '<td style="text-align: center">' + rating + '</td>';
                    tableRow += '<td style="text-align: center">' + '<a>Edit</a>' +  ' | ' + '<a>Delete</a>';
                    tableRow += '</tr>';
                
                $('#tableBody').append(tableRow);
            });
        },
        error: function(){
            $('#errorMessages').append($('<li>').attr({class:'list-group-item list-group-item-danger'}).text('Error: Problem connecting to web service.'));
        }   
    }); 
}

function clearDvdTable(){
    $('#tableBody').empty();
}