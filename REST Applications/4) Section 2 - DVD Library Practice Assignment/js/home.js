$(document).ready(function () {

    loadDvds();

    $('#create-dvd-navbar-button').click(function (event) {
        clearErrorMessages();
        $('.navbar').hide();
        $('#dvd-list-table').hide();
        $('#create-dvd-title').show();
        $('#create-dvd-page').show();
    });

    $('#create-dvd-button').click(function (event) {
        var hasValidationErrors = checkAndDisplayValidationErrors($('#create-dvd-form').find('input'));
        
        if(hasValidationErrors){
            $('error-messages').show();
            return false;
        }
        
        $.ajax({
            type: 'POST',
            url: 'http://localhost:8080/dvd',
            data: JSON.stringify({
                title: $('#create-dvd-movie-title').val(),
                realeaseYear: $('#create-dvd-release-year').val(),
                director: $('#create-dvd-director').val(),
                rating: $('#create-dvd-rating-select').val(),
                notes: $('#create-dvd-notes').val()
            }),
            headers: {
                'Accept': "application/json",
                'Content-Type': "application/json"
            },
            'dataType': "json",
            success: function () {
                clearErrorMessages();
                hideCreateDvdPage();
                loadDvds();
            },
            error: function () {
                $('#error-messages')
                    .append($('<li>'))
                    .attr({
                        class: 'list-group-item list-group-item-danger'
                    })
                    .text('Error: Problem calling web service. Please try again');
            }
        });
    });

    $('#save-changes-button').click(function (event) {
        var hasValidationErrors = checkAndDisplayValidationErrors($('#edit-dvd-form').find('input'));
        
        if(hasValidationErrors){
            $('error-messages').show();
            return false;
        }
        
        $.ajax({
            type: 'PUT',
            url: 'http://localhost:8080/dvd/' + $('#edit-dvd-id').val(),
            data: JSON.stringify({
                dvdId: $('#edit-dvd-id').val(),
                title: $('#edit-dvd-movie-title').val(),
                realeaseYear: $('#edit-release-year').val(),
                director: $('#edit-director').val(),
                rating: $('#edit-rating-select').val(),
                notes: $('#edit-notes').val()
            }),
            headers: {
                'Accept': "application/json",
                'Content-Type': "application/json"
            },
            'dataType': "json",
            success: function () {
                clearErrorMessages();
                hideEditDvdPage();
                loadDvds();
            },
            error: function () {
                $('#error-messages')
                    .append($('<li>'))
                    .attr({
                        class: 'list-group-item list-group-item-danger'
                    })
                    .text('Error: Problem calling web service. Please try again');
            }
        });
    });

    $('#search-button').click(function (event) {
        var hasValidationErrors = checkAndDisplayValidationErrors($('#top-row').find('input'));
        
        if(hasValidationErrors){
            $('error-messages').show();
            return false;
        }
        
        var searchRequest = $('#search-bar').val();
        var searchCategory = $('#search-category option:selected').val();
        var table = $('#dvd-list-content-body');

        if (searchCategory === "Title") {
            $.ajax({
                type: 'GET',
                url: 'http://localhost:8080/dvds/title/' + searchRequest,
                success: function (dvdList) {
                    clearErrorMessages();
                    clearDvdTable();
                    $.each(dvdList, function (index, dvd) {
                        var title = dvd.title;
                        var releaseYear = dvd.realeaseYear;
                        var director = dvd.director;
                        var rating = dvd.rating;
                        var id = dvd.dvdId;

                        var tableRow = '<tr>';
                        tableRow += '<td style="text-align: center"><a onclick="showDvdInfo(' + id + ')">' + title + '</a></td>';
                        tableRow += '<td style="text-align: center">' + releaseYear + '</td>';
                        tableRow += '<td style="text-align: center">' + director + '</td>';
                        tableRow += '<td style="text-align: center">' + rating + '</td>';
                        tableRow += '<td style="text-align: center"><a onclick="showEditPage(' + id + ')">Edit</a> | <a onclick="deleteDvd(' + id + ')">Delete</a></td>';
                        tableRow += '</tr>';

                        table.append(tableRow);
                    });
                },
                error: function () {
                    $('#error-messages')
                        .append($('<li>'))
                        .attr({
                            class: 'list-group-item list-group-item-danger'
                        })
                        .text('Error: Problem calling web service. Please try again');
                }
            });

        } else if (searchCategory === "Release Year") {
            $.ajax({
                type: 'GET',
                url: 'http://localhost:8080/dvds/year/' + searchRequest,
                success: function (dvdList) {
                    clearDvdTable();
                    clearErrorMessages();
                    $.each(dvdList, function (index, dvd) {
                        var title = dvd.title;
                        var releaseYear = dvd.realeaseYear;
                        var director = dvd.director;
                        var rating = dvd.rating;
                        var id = dvd.dvdId;

                        var tableRow = '<tr>';
                        tableRow += '<td style="text-align: center"><a onclick="showDvdInfo(' + id + ')">' + title + '</a></td>';
                        tableRow += '<td style="text-align: center">' + releaseYear + '</td>';
                        tableRow += '<td style="text-align: center">' + director + '</td>';
                        tableRow += '<td style="text-align: center">' + rating + '</td>';
                        tableRow += '<td style="text-align: center"><a onclick="showEditPage(' + id + ')">Edit</a> | <a onclick="deleteDvd(' + id + ')">Delete</a></td>';
                        tableRow += '</tr>';

                        table.append(tableRow);
                    });
                },
                error: function () {
                    $('#error-messages')
                        .append($('<li>'))
                        .attr({
                            class: 'list-group-item list-group-item-danger'
                        })
                        .text('Error: Problem calling web service. Please try again');
                }
            });

        } else if (searchCategory === "Director Name") {
            $.ajax({
                type: 'GET',
                url: 'http://localhost:8080/dvds/director/' + searchRequest,
                success: function (dvdList) {
                    clearDvdTable();
                    clearErrorMessages();
                    $.each(dvdList, function (index, dvd) {
                        var title = dvd.title;
                        var releaseYear = dvd.realeaseYear;
                        var director = dvd.director;
                        var rating = dvd.rating;
                        var id = dvd.dvdId;

                        var tableRow = '<tr>';
                        tableRow += '<td style="text-align: center"><a onclick="showDvdInfo(' + id + ')">' + title + '</a></td>';
                        tableRow += '<td style="text-align: center">' + releaseYear + '</td>';
                        tableRow += '<td style="text-align: center">' + director + '</td>';
                        tableRow += '<td style="text-align: center">' + rating + '</td>';
                        tableRow += '<td style="text-align: center"><a onclick="showEditPage(' + id + ')">Edit</a> | <a onclick="deleteDvd(' + id + ')">Delete</a></td>';
                        tableRow += '</tr>';

                        table.append(tableRow);
                    });
                },
                error: function () {
                    $('#error-messages')
                        .append($('<li>'))
                        .attr({
                            class: 'list-group-item list-group-item-danger'
                        })
                        .text('Error: Problem calling web service. Please try again');
                }
            });

        } else if (searchCategory === "Rating") {
            $.ajax({
                type: 'GET',
                url: 'http://localhost:8080/dvds/rating/' + searchRequest,
                success: function (dvdList) {
                    clearDvdTable();
                    clearErrorMessages();
                    $.each(dvdList, function (index, dvd) {
                        var title = dvd.title;
                        var releaseYear = dvd.realeaseYear;
                        var director = dvd.director;
                        var rating = dvd.rating;
                        var id = dvd.dvdId;

                        var tableRow = '<tr>';
                        tableRow += '<td style="text-align: center"><a onclick="showDvdInfo(' + id + ')">' + title + '</a></td>';
                        tableRow += '<td style="text-align: center">' + releaseYear + '</td>';
                        tableRow += '<td style="text-align: center">' + director + '</td>';
                        tableRow += '<td style="text-align: center">' + rating + '</td>';
                        tableRow += '<td style="text-align: center"><a onclick="showEditPage(' + id + ')">Edit</a> | <a onclick="deleteDvd(' + id + ')">Delete</a></td>';
                        tableRow += '</tr>';

                        table.append(tableRow);
                    });
                },
                error: function () {
                    $('#error-messages')
                        .append($('<li>'))
                        .attr({
                            class: 'list-group-item list-group-item-danger'
                        })
                        .text('Error: Problem calling web service. Please try again');
                }
            });
        }
    });
});

function loadDvds() {
    clearDvdTable();
    var table = $('#dvd-list-content-body');

    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/dvds',
        success: function (dvdList) {
            $.each(dvdList, function (index, dvd) {
                clearErrorMessages();
                var title = dvd.title;
                var releaseYear = dvd.realeaseYear;
                var director = dvd.director;
                var rating = dvd.rating;
                var id = dvd.dvdId;

                var tableRow = '<tr>';
                tableRow += '<td style="text-align: center"><a onclick="showDvdInfo(' + id + ')">' + title + '</a></td>';
                tableRow += '<td style="text-align: center">' + releaseYear + '</td>';
                tableRow += '<td style="text-align: center">' + director + '</td>';
                tableRow += '<td style="text-align: center">' + rating + '</td>';
                tableRow += '<td style="text-align: center"><a onclick="showEditPage(' + id + ')">Edit</a> | <a onclick="deleteDvd(' + id + ')">Delete</a></td>';
                tableRow += '</tr>';

                table.append(tableRow);

            });
        },
        error: function () {
            $('#error-messages')
                .append($('<li>'))
                .attr({
                    class: 'list-group-item list-group-item-danger'
                })
                .text('Error: Problem calling web service. Please try again');
        }
    });
}

function clearDvdTable() {
    $('#dvd-list-content-body').empty();
}

function clearErrorMessages() {
    $('#error-messages').empty();
}

function hideCreateDvdPage() {
    $('#create-dvd-movie-title').val('');
    $('#create-dvd-release-year').val('');
    $('#create-dvd-director').val('');
    $('#create-dvd-rating-select').val('');
    $('#create-dvd-notes').val('');
    $('#create-dvd-title').hide();
    $('#create-dvd-page').hide();
    $('.navbar').show();
    $('#dvd-list-table').show();
}

function hideEditDvdPage() {
    $('#edit-dvd-movie-title').val('');
    $('#edit-dvd-release-year').val(''),
        $('#edit-dvd-director').val('');
    $('#edit-dvd-rating-select').val('');
    $('#edit-dvd-notes').val('');
    $('#edit-dvd-title').hide();
    $('#edit-dvd-page').hide();
    $('.navbar').show();
    $('#dvd-list-table').show();
}

function showEditPage(dvdId) {
    $('.navbar').hide();
    $('#dvd-list-table').hide();
    $('#edit-dvd-page').show();
    $('#edit-dvd-title').show();

    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/dvd/' + dvdId,
        success: function (dvd) {
            clearErrorMessages();
            $('#edit-dvd-id').val(dvd.dvdId);
            $('#edit-dvd-movie-title').val(dvd.title);
            $('#edit-release-year').val(dvd.realeaseYear);
            $('#edit-director').val(dvd.director);
            $('#edit-rating-select option[value=' + dvd.rating + ']').prop('selected', 'selected');
            $('#edit-notes').val(dvd.notes);
        },
        error: function () {
            $('#error-messages')
                .append($('<li>'))
                .attr({
                    class: 'list-group-item list-group-item-danger'
                })
                .text('Error: Problem calling web service. Please try again');
        }
    });
}

function deleteDvd(dvdId) {
    $.ajax({
        type: 'DELETE',
        url: 'http://localhost:8080/dvd/' + dvdId,
        success: function () {
            clearErrorMessages();
            loadDvds();
        }
    });
}

function showDvdInfo(dvdId) {
    $.ajax({
        type: 'GET',
        url: 'http://localhost:8080/dvd/' + dvdId,
        success: function (dvd) {
            clearErrorMessages();
            $('.navbar').hide();
            $('#dvd-list-table').hide();
            $('#display-dvd-title-header').append(dvd.title);
            $('#display-dvd-title').show();
            $('#display-dvd-information-page').show();
            $('#display-dvd-release-year').append(dvd.realeaseYear);
            $('#display-dvd-director').append(dvd.director);
            $('#display-dvd-rating').append(dvd.rating);
            $('#display-dvd-notes').append(dvd.notes);
        },
        error: function () {
            $('#error-messages')
                .append($('<li>'))
                .attr({
                    class: 'list-group-item list-group-item-danger'
                })
                .text('Error: Problem calling web service. Please try again');
        }
    });
}

function hideDvdDisplay() {
    $('#display-dvd-title').hide();
    $('#display-dvd-information-page').hide();
    $('.navbar').show();
    $('#dvd-list-table').show();
    loadDvds();
}

function checkAndDisplayValidationErrors(input) {
    // clear displayed error message if there are any
    $('#error-messages').empty();
    // check for HTML5 validation errors and process/display appropriately
    // a place to hold error messages
    var errorMessages = [];

    // loop through each input and check for validation errors
    input.each(function () {
        // Use the HTML5 validation API to find the validation errors
        if (!this.validity.valid) {
            var errorField = $('label[for=' + this.id + ']').text();
            errorMessages.push(errorField + ' ' + this.validationMessage);
        }
    });

    // put any error messages in the errorMessages div
    if (errorMessages.length > 0) {
        $.each(errorMessages, function (index, message) {
            $('#error-messages').append($('<li>').attr({
                class: 'list-group-item list-group-item-danger'
            }).text(message));
        });
        // return true, indicating that there were errors
        return true;
    } else {
        // return false, indicating that there were no errors
        return false;
    }
}
