package App;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@SpringBootApplication
@EnableWebSecurity //This annotation allows us to override Spring Boot auto configuration when it comes to security

//We extend this class so that we can provide our own customized configuration of Spring Security
//and how it behaves.
public class SpringBootSecurityDemoApplication extends WebSecurityConfigurerAdapter {

    //We override this method from the above extended class so we can tell Spring to require authorization on every http request AND
    //instead of using the default user and password given by Spring boot, use the setup we have in the application properties file
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().anyRequest().authenticated().and().httpBasic();
        //This switches the login to a web based form instead of a popup
        http.authorizeRequests().anyRequest().authenticated().and().formLogin();
    }

    public static void main(String[] args) {
        SpringApplication.run(SpringBootSecurityDemoApplication.class, args);
    }
}
