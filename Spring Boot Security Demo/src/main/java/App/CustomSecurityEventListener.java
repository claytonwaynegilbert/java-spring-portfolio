/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App;

import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AbstractAuthenticationFailureEvent;
import org.springframework.stereotype.Component;

/**
 *
 * @author cjsm12
 */

//This class is used to demonstrate the ability to catch events thrown by Spring Security when they happen, you can
//catch these events using a ApplicationListener and passing in the event you want to catch when thrown by Spring Security.
@Component
public class CustomSecurityEventListener implements ApplicationListener<AbstractAuthenticationFailureEvent> {

    @Override
    public void onApplicationEvent(AbstractAuthenticationFailureEvent event) {
        System.out.println(event.getException().getMessage());
    }
    
}
