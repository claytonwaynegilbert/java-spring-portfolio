/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.daysoftheweek;

import com.sg.daysoftheweek.DaysOfWeek.DaysOfTheWeek;
import java.util.Scanner;

/**
 *
 * @author savannahg
 */
public class App {

    public static void main(String[] args) {
        
        //Static method below returns an enum value
        DaysOfTheWeek day = askForInput();

        //Use a switch statement on the enum to display appropriate String.
        switch (day) {
            case MONDAY:
                System.out.print("There are 4 more days until Friday!");
            case TUESDAY:
                System.out.print("There are 3 more days until Friday!");
            case WEDNESDAY:
                System.out.print("There are 2 more days until Friday!");
            case THURSDAY:
                System.out.print("There are 1 more days until Friday!");
            case FRIDAY:
                System.out.print("It's Friday silly!");
            case SATURDAY:
                System.out.print("There are 6 more days until Friday!");
            case SUNDAY:
                System.out.print("There are 5 more days until Friday!");
            default:
                System.out.print("You need to specify a valid day of the week!");
        }
    }
    
    private static DaysOfTheWeek askForInput(){
        //Get users input using the Scanner type
        Scanner userInput = new Scanner(System.in);
        System.out.print("Enter the day of the week: ");
        String usersChoice = userInput.next();
        //Convert users choice to upper case and then passing upper case String into 
        //valueOf method of enum that will convert String into an enum value, which gets returned.
        String usersChoiceUpperCase = usersChoice.toUpperCase();
        return DaysOfTheWeek.valueOf(usersChoiceUpperCase);
    }

}
