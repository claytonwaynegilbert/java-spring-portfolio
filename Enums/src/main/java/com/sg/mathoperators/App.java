/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.mathoperators;

import java.util.Scanner;

/**
 *
 * @author savannahg
 */
public class App {
    
    public static void main(String[] args) {
        //Declare type Scanner for user input
        Scanner sc = new Scanner(System.in);
        //Get numbers from user
        System.out.print("Enter number one: ");
        double numberOne = sc.nextDouble();
        System.out.print("Enter number two: ");
        double numberTwo = sc.nextDouble();
        //Get the operation the user wants to perform on the two numbers
        System.out.print("What operation do you want to perform[ADD, SUBTRACT, MULTIPLY, DIVIDE]: ");
        String usersChoice = sc.next();
        //Convert users String to uppercase in case they typed in a lower case version of the enum
        String usersChoiceUpperCase = usersChoice.toUpperCase();
        //Convert users String into an enum value by calling the valueOf method on the enum itself
        MathOperators operation = MathOperators.valueOf(usersChoiceUpperCase);
        //Call the static method below which takes the enum, the two numbers, and returns the result back
        //as a double before finally displaying the result.
        double result = calculate(operation, numberOne, numberTwo);
        System.out.print("The answer is " + result);
        
    }
    
    private static double calculate(MathOperators operation, double operand1, double operand2){
        //Uses a switch on the enum that gets passed in to perform appropriate logic
        switch(operation){
            case ADD:
                return operand1 + operand2;
            case SUBTRACT:
                return operand1 - operand2;
            case MULTIPLY:
                return operand1 * operand2;
            case DIVIDE:
                return operand1 / operand2;
            default:
                System.out.println("ERROR");
        }
        return 0;
    }
    
}
