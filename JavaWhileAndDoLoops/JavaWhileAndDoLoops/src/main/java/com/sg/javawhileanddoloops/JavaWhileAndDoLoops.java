/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.javawhileanddoloops;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author cjsm12
 */
public class JavaWhileAndDoLoops {

    public static void main(String[] args) {

        /*
        Filename: StayPositive.java
        Write a while loop that asks the user for a number and counts down from it, 
        printing out each number until it reaches zero, and then stops. 
        (Stop at zero because the numbers after, they just get too NEGATIVE to deal with!)

        BONUS: Print out 10 numbers per line, and then start a new line ... 
         */
        
        //Welcome user
        System.out.println("Welcome to StayPositive!");
        System.out.println("------------------------");

        //Declare type Scanner for user input
        Scanner sc = new Scanner(System.in);
        //Declare variable to store users input
        int userChoice;

        //Ask user to pick a number and store in variable
        System.out.print("Pick a positive number to count down from: ");
        userChoice = sc.nextInt();
        System.out.println("Here goes nothing!");

        //While loop to count down users number until 0
        while (userChoice > -1) {
            //Print users choice and each consecutive number down to zero
            System.out.print(userChoice + " ");
            //Decrament users number
            userChoice--;
            //Break out of loop when number huts 0
            if (userChoice == -1) {
                break;
            }
        }

        //Printing ending statement
        System.out.println("\nWhew! Better stop there.");

        /*
        Filename: LovesMe.java
        Write a program that picks petals off a daisy (ordinarily 34 petals!) and prints 
        out "It LOVES me NOT!" or "It LOVES me!" for every other petal until it runs out 
        of petals. Feel free to print a happy message at the end too … since the result 
        should ALWAYS be the same.

        Did you write it in a while, or do/while loop? Why? (answer in a comment)
         */
        //Welcome user
        System.out.println("Welcome to LovesMe!");
        System.out.println("-------------------\n");

        //initialize variale for current peddal count on flower
        int peddalCount = 0;
        System.out.println("Well here goes nothing...");

        //Using a while loop to check for the count of the peddals and keep executing 
        //loop until the peddal count reaches the total number of peddals at 34
        while (peddalCount <= 34) {
            System.out.println("It LOVES me NOT!");
            //Increase peddal count each time one is plucked
            peddalCount++;
            System.out.println("It LOVES me!");
            peddalCount++;
        }

        //Ending message to user 
        System.out.println("WHEW! I knew It LOVED ME!");

        /*
        Filename: MaybeItLovesMe.java
        Change your daisy-prediction program from before, to have a RANDOM number of 
        petals for the daisy (range is usually from 13 - 89 petals).
        If the last petal ends on "it loves me NOT" print "Awwww, bummer." otherwise print
        "Oh, wow! It really LOVES me!"
         */
        
        //Welcome user
        System.out.println("Welcome to MaybeItLovesMe!");
        System.out.println("--------------------------");

        //Declare type Random for the random daisy peddal number
        Random rn = new Random();
        //Creating random number of range between 13 and 89
        int numberOfPeddals = rn.nextInt(89) + 13;
        //Keeping track if how many peddals have been plucked
        int peddalCurrentCount = 0;

        //Using a do while loop to pick the peddals
        do {
            System.out.println("It LOVES me NOT!");
            //Update peddal count
            peddalCurrentCount++;
            //Condition check to see if peddal count is greater than the random number 
            //created by the random object
            if (peddalCurrentCount >= numberOfPeddals) {
                System.out.println("Awwww, bummer!");
            }
            System.out.println("It LOVES me!");
            peddalCurrentCount++;
            if (peddalCurrentCount >= numberOfPeddals) {
                System.out.println("Oh, wow! It really LOVES ME!");
            }
        } while (peddalCurrentCount <= numberOfPeddals);

        /*
        Filename: LazyTeenager.java
        Write a program that simulates a lazy teenager who won't clean their room until 
        their parent tells them repeatedly to do it.

        You should write this in a do/while loop, which continues to execute until the 
        room is clean.

        Every time the loop executes, the parent should tell the teenager to clean their
        room. Each time the teenager is told to clean their room, it increases the chance 
        the teenager will clean the room by 5%. However, by the 15th time, they're going 
        to get grounded and have their xbox confiscated. 
        (Note: Use a break, not a compound condition to stop the loop in that case.)

        So the first time they're told, there is only a 5% chance they'll clean it.
        The second time, there's a 10% chance. Use Random to calculate this “chance” 
        check.
         */
        
        //Welcome user
        System.out.println("GuessMeFinally");
        System.out.println("---------------");

        //Declaring variables
        int cleanRoom = 0;
        int chanceToCleanRoom = 0;
        int timesToldToCleanRoom = 0;
        boolean willCleanRoom = false;

        //Using do while loop
        do {
            //Update times told to clean room variable
            timesToldToCleanRoom++;
            //Inform user how many times they have been told to clean room
            System.out.println("Clean Your Room!" + "(X" + timesToldToCleanRoom + ")");
            //Increase chance of cleaning room by 5%
            chanceToCleanRoom += 5;
            //See if current chance to clean room actually happens
            willCleanRoom = rn.nextInt(100) <= chanceToCleanRoom;
            //Seeing if parent told teen less than 14 times to clean room and
            //if probability that teen will cleean his room evaluates to true
            if (timesToldToCleanRoom <= 14 && willCleanRoom) {
                cleanRoom = 1;
                System.out.println("Alright FINE! I'll clean my room...");
                //Checking to see if parent told teen to clean room more than 15 times
            } else if (timesToldToCleanRoom >= 15) {
                //Break out of loop if told more than 15 times to clean room
                cleanRoom = 1;
                break;
            }
        } while (cleanRoom != 1);

        //Print angry parent statement
        System.out.println("FINE I'LL DO IT! NO MORE PS4 FOR TWO WEEKS!");
    }

}
