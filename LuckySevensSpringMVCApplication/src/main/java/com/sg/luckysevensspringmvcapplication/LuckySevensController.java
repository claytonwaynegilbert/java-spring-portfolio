/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.luckysevensspringmvcapplication;

import java.util.Map;
import java.util.Random;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author cjsm12
 */
@Controller
public class LuckySevensController {

    @RequestMapping(value = "/rollDice", method = RequestMethod.POST)
    public String playLuckySevens(HttpServletRequest request, Map<String, Object> model) {

        String userMoney = request.getParameter("moneyToBet");
        int userMoneyAmount = Integer.parseInt(userMoney);
        int maxMoneyHeld = userMoneyAmount;
        Random rn = new Random();
        int diceRollCount = 0;
        int diceRollAtMaxMoneyHeld = 0;

        while (userMoneyAmount > 0) {
            int diceOne = rn.nextInt(7) + 1;
            int diceTwo = rn.nextInt(7) + 1;
            int diceSum = diceOne + diceTwo;

            if (diceSum == 7) {
                diceRollCount++;
                userMoneyAmount += 4;
                if (userMoneyAmount > maxMoneyHeld) {
                    maxMoneyHeld = userMoneyAmount;
                    diceRollAtMaxMoneyHeld = diceRollCount;
                }
            } else {
                diceRollCount++;
                userMoneyAmount -= 1;
            }
        }

        model.put("initialBalance", userMoney);
        model.put("rollCount", diceRollCount);
        model.put("diceRollWithHighestMoneyHeld", diceRollAtMaxMoneyHeld);
        model.put("maxMoneyHeld", maxMoneyHeld);

        return "results";
    }
}
