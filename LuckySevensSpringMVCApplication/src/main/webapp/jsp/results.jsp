<%-- 
    Document   : results
    Created on : Sep 18, 2017, 5:38:23 AM
    Author     : cjsm12
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lucky Sevens Results</title>
        <style>
            #results{
                font-weight: bold;
            }
        </style>
    </head>
    <body>
        <h1>Results:</h1>
        <br>
        <p id="results">
            You bet: $${initialBalance}<br><br>
            You are broke after: ${rollCount} rolls<br><br>
            You should have quit after ${diceRollWithHighestMoneyHeld} rolls when you had $${maxMoneyHeld}<br><br>
        </p>

        <a href="index.jsp"><strong>Play again</strong></a>
    </body>
</html>
