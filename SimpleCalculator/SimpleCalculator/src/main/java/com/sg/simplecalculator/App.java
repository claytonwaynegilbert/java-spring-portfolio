/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.simplecalculator;

import java.util.Scanner;

/**
 *
 * @author cjsm12
 */
public class App {

    public static void main(String[] args) {

        //Declare type Scanner for user input and SimpleCalculator for calculations
        Scanner sc = new Scanner(System.in);
        SimpleCalculator calculator = new SimpleCalculator();
        
        //Declare other variables  for loop and results
        boolean calculatorOn = true;
        double result = 0;

        //Welcome user
        System.out.println("Welcome to SimpleCalculator!");
        System.out.println("----------------------------");

        //Loop to keep executing program until user exits
        while (calculatorOn) {

            //Ask user for which operation to perform
            System.out.print("What would you like to do: [add, subtract, multiply, divide, exit]: ");
            String usersChoice = sc.nextLine();
            
            //Check if user chose to exit, if so, break out of loop
            if(usersChoice.equalsIgnoreCase("exit")){
                break;
            }

            //Ask user for first number
            System.out.print("Give me your first number: ");
            double firstNumber = sc.nextDouble();
            sc.nextLine();

            //Ask user for second number
            System.out.print("Give me your second number: ");
            double secondNumber = sc.nextDouble();
            sc.nextLine();

            //Using a switch statement on users choice and calling appropriate 
            //method on SimpleCalculator type and assigning result to variable
            switch (usersChoice) {
                case "add":
                    result = calculator.add(firstNumber, secondNumber);
                    break;
                case "subtract":
                    result = calculator.subtract(firstNumber, secondNumber);
                    break;
                case "multiply":
                    result = calculator.multiply(firstNumber, secondNumber);
                    break;
                case "divide":
                    result = calculator.division(firstNumber, secondNumber);
                    break;
                default:
                    System.out.println("You didn't enter one of the four options listed.");
                    break;
            }
            
            //Displaying result to user
            System.out.println("Your result = " + result);
        }
        
        //Exit message for when user exits calculator
        System.out.println("Thank you and goodbye!");
    }
}
