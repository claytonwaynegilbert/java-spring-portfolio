/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.simplecalculator;

/**
 *
 * @author cjsm12
 */
public class SimpleCalculator {
    
    public double add(double operand1, double operand2){
        return operand1 + operand2;
    }
    
    public double subtract(double operand1, double operand2){
        return operand1 - operand2;
    }
    
    public double division(double operand1, double operand2){
        return operand1 / operand2;
    }
    
    public double multiply(double operand1, double operand2){
        return operand1 * operand2;
    }
    
}
