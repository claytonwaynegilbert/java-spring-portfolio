/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.contactlistspringmvcapplication.controller;

import com.sg.contactlistspringmvcapplication.dao.ContactListDao;
import com.sg.contactlistspringmvcapplication.dao.SearchTerm;
import com.sg.contactlistspringmvcapplication.model.Contact;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author cjsm12
 */
@CrossOrigin
@Controller
public class SearchController {
    
    private ContactListDao dao;
    
    @Inject
    public SearchController(ContactListDao dao){
        this.dao = dao;
    }
    
    @RequestMapping(value="/displaySearchPage", method=RequestMethod.GET)
    public String displaySearchPage(HttpServletRequest request, Map<String, Object> model){ 
        return "search";
    }
    
    @RequestMapping(value="/search/contacts", method=RequestMethod.POST)
    public List<Contact> searchContacts(@RequestBody Map<String, String> searchMap){
         Map<SearchTerm, String> searchCriteria = new HashMap<>();
         
         String currentTerm = searchMap.get("firstName");
         if(currentTerm != null && !currentTerm.isEmpty()){
             searchCriteria.put(SearchTerm.FIRST_NAME, currentTerm);
         }
         
         currentTerm = searchMap.get("lastName");
         if(currentTerm != null && !currentTerm.isEmpty()){
             searchCriteria.put(SearchTerm.LAST_NAME, currentTerm);
         }
         
         currentTerm = searchMap.get("company");
         if(currentTerm != null && !currentTerm.isEmpty()){
             searchCriteria.put(SearchTerm.COMPANY, currentTerm);
         }
         
         currentTerm = searchMap.get("phone");
         if(currentTerm != null && !currentTerm.isEmpty()){
             searchCriteria.put(SearchTerm.PHONE, currentTerm);
         }
         
         currentTerm = searchMap.get("email");
         if(currentTerm != null && !currentTerm.isEmpty()){
             searchCriteria.put(SearchTerm.EMAIL, currentTerm);
         }
         
         return dao.searchContacts(searchCriteria);
    }
}
