/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.contactlistspringmvcapplication.controller;

import com.sg.contactlistspringmvcapplication.dao.ContactListDao;
import com.sg.contactlistspringmvcapplication.model.Contact;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author cjsm12
 */
@Controller
public class ContactController {
    
    private ContactListDao dao;
    
    @Inject
    public ContactController(ContactListDao dao){
        this.dao = dao;
    }
    
    @RequestMapping(value="/displayContactsPage", method=RequestMethod.GET)
    public String displayContactsPage(HttpServletRequest request, Model model){
        
        List<Contact> allContacts = dao.getAllContacts();
        
        model.addAttribute("contacts", allContacts);
        
        return "contacts";
    }
    
    @RequestMapping(value="/createContact", method=RequestMethod.POST)
    public String createContact(HttpServletRequest request, Model model){
        
        Contact newContact = new Contact();
        
        newContact.setFirstName(request.getParameter("firstName"));
        newContact.setLastName(request.getParameter("lastName"));
        newContact.setCompany(request.getParameter("company"));
        newContact.setPhone(request.getParameter("phone"));
        newContact.setEmail(request.getParameter("email"));
        
        dao.addContact(newContact);
        
        return "redirect: displayContactsPage";
    }
    
    @RequestMapping(value="/displayContactDetails", method=RequestMethod.GET)
    public String displayContactDetails(HttpServletRequest request, Model model){
        
        String id = request.getParameter("contactId");
        long contactId = Long.parseLong(id);
        Contact foundContact = dao.getContactById(contactId);
        
        model.addAttribute("contact", foundContact);
        
        return "contactDetails";
    }
    
    @RequestMapping(value="/deleteContact", method=RequestMethod.GET)
    public String deleteContact(HttpServletRequest request, Model model){
        
        String id = request.getParameter("contactId");
        long contactId = Long.parseLong(id);       
        dao.deleteContact(contactId);

        return "redirect: displayContactsPage";
    }
    
    @RequestMapping(value="/displayEditContactForm", method=RequestMethod.GET)
    public String displayEditContactPage(HttpServletRequest request, Model model){
        
        String id = request.getParameter("contactId");
        long contactId = Long.parseLong(id);
        Contact matchingContact = dao.getContactById(contactId);
        model.addAttribute("contact", matchingContact);
        
        return "editContactForm";
    }
    
    @RequestMapping(value="/editContact", method=RequestMethod.POST)
    public String editContact(@Valid @ModelAttribute("contact") Contact contact, BindingResult results){
        
        if(results.hasErrors()){
            return "editContactForm";
        }
        
        dao.updateContact(contact);
        
        return "redirect: displayContactsPage";
    }
    
    
}
