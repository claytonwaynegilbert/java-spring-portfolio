/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.contactlistspringmvcapplication.controller;

/**
 *
 * @author cjsm12
 */
public class UpdateIntegrityException extends Exception {

    /**
     * Creates a new instance of <code>UpdateIntegrityException</code> without
     * detail message.
     */
    public UpdateIntegrityException() {
    }

    /**
     * Constructs an instance of <code>UpdateIntegrityException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public UpdateIntegrityException(String msg) {
        super(msg);
    }

    public UpdateIntegrityException(String message, Throwable cause) {
        super(message, cause);
    }
    
    
    
    
}
