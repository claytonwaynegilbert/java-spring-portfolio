/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.contactlistspringmvcapplication.dao;

import com.sg.contactlistspringmvcapplication.model.Contact;
import java.util.List;
import java.util.Map;

/**
 *
 * @author cjsm12
 */
public interface ContactListDao {
    
    Contact addContact(Contact contact);
    
    void deleteContact(long contactId);
    
    void updateContact(Contact contact);
    
    List<Contact> getAllContacts();
    
    Contact getContactById(long contactId);
    
    List<Contact> searchContacts(Map<SearchTerm, String> searchMap);
  
}
