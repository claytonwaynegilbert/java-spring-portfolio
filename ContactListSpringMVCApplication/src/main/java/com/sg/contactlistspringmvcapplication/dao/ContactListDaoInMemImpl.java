/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.contactlistspringmvcapplication.dao;

import com.sg.contactlistspringmvcapplication.model.Contact;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 *
 * @author cjsm12
 */
public class ContactListDaoInMemImpl implements ContactListDao {
    
    private Map<Long, Contact> contactMap = new HashMap<>();
    
    private static long contactID = 0;

    @Override
    public Contact addContact(Contact contact) {
        contact.setContactId(contactID);
        contactMap.put(contact.getContactId(), contact);
        contactID++;
        return contact;
    }

    @Override
    public void deleteContact(long contactId) {
        contactMap.remove(contactId);
    }

    @Override
    public void updateContact(Contact contact) {
        contactMap.put(contact.getContactId(), contact);
    }

    @Override
    public List<Contact> getAllContacts() {
        return new ArrayList<>(contactMap.values());
    }

    @Override
    public Contact getContactById(long contactId) {
        return contactMap.get(contactId);
    }

    @Override
    public List<Contact> searchContacts(Map<SearchTerm, String> searchMap) {
        
        String firstNameSearchCriteria = searchMap.get(SearchTerm.FIRST_NAME);
        String lastNameSearchCriteria = searchMap.get(SearchTerm.LAST_NAME);
        String companySearchCriteria = searchMap.get(SearchTerm.COMPANY);
        String phoneSearchCriteria = searchMap.get(SearchTerm.PHONE);
        String emailSearchCriteria = searchMap.get(SearchTerm.EMAIL);
        
        Predicate<Contact> firstNameMatchPredicate;
        Predicate<Contact> lastNameMatchPredicate;
        Predicate<Contact> companyMatchPredicate;
        Predicate<Contact> phoneMatchPredicate;
        Predicate<Contact> emailMatchPredicate;
        
        //Used for when search field is empty
        //because if all fields are empty then
        //we just want to return all contacts
        Predicate<Contact> truePredicate = (c) -> {
            return true;
        };
        
        if(firstNameSearchCriteria == null || firstNameSearchCriteria.isEmpty()){
            firstNameMatchPredicate = truePredicate;
        }else{
            firstNameMatchPredicate = c -> c.getFirstName().equalsIgnoreCase(firstNameSearchCriteria);
        }
        
        if(lastNameSearchCriteria == null || lastNameSearchCriteria.isEmpty()){
            lastNameMatchPredicate = truePredicate;
        }else{
            lastNameMatchPredicate = c -> c.getLastName().equalsIgnoreCase(lastNameSearchCriteria);
        }
        
        if(companySearchCriteria == null || companySearchCriteria.isEmpty()){
            companyMatchPredicate = truePredicate;
        }else{
            companyMatchPredicate = c -> c.getCompany().equalsIgnoreCase(companySearchCriteria);
        }
        
        if(phoneSearchCriteria == null || phoneSearchCriteria.isEmpty()){
            phoneMatchPredicate = truePredicate;
        }else{
            phoneMatchPredicate = c -> c.getPhone().equalsIgnoreCase(phoneSearchCriteria);
        }
        
        if(emailSearchCriteria == null || emailSearchCriteria.isEmpty()){
            emailMatchPredicate = truePredicate;
        }else{
            emailMatchPredicate = c -> c.getEmail().equalsIgnoreCase(emailSearchCriteria);
        }
        
        return contactMap.values().stream()
                                  .filter(firstNameMatchPredicate
                                  .and(lastNameMatchPredicate)
                                  .and(companyMatchPredicate)
                                  .and(phoneMatchPredicate)
                                  .and(emailMatchPredicate))
                                  .collect(Collectors.toList());
        
        
    }
    
    
    
}
