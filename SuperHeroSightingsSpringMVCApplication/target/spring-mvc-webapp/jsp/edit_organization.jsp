<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Edit Sighting</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/edit-organization.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <h1>Edit Organization</h1>
            <hr>
            <div class="row">
                <div id="description-box" class="col-md-8">
                    <p id="description"> 
                        Edit everything about your Organization!<br>
                        Change the name, description, location, members, and more<br>
                        It's a simple as filling out the information and clicking edit...<br>
                    </p>
                </div>  
            </div>
            <div class="row">
                <div class="col-md-4" id="edit-organization-image">
                    <img src="https://image.ibb.co/hfRL86/Fantastic_Four_logo.png"/>
                </div>
            </div>
            <sf:form id="edit-organization-form" 
                     class="form form-horizontal"
                     role="form"
                     action="editOrganization"
                     method="POST"
                     modelAttribute="organization">
                <div class="form-group" id="name-row">
                    <label for="organization-name" class="control-label col-md-4">Name:</label>
                    <div class="col-md-5">
                        <sf:input type="text" id="organization-name" class="form-control" path="name"/>
                        <sf:errors path="name" cssClass="error"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group" id="description-row">
                        <label for="organization-description" class="control-label col-md-4">Description:</label>
                        <div class="col-md-5">
                        <sf:input type="text" id="organization-description" class="form-control" path="description"/>
                        <sf:errors path="description" cssClass="error"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group" id="location-row">
                        <label for="organization-location" class="control-label col-md-4">Location Name:</label>
                        <div class="col-md-5">
                        <sf:select id="sighting-location" class="form-control" path="location.name">
                            <sf:options items="${locations}" itemValue="name" itemLabel="name"/>
                        </sf:select>
                        <sf:errors path="location.name" cssClass="error"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group" id="contact-row">
                        <label for="organization-contact" class="control-label col-md-4">Contact Number:</label>
                        <div class="col-md-5">
                        <sf:input type="text" id="organization-contact" class="form-control" path="contactNumber"/>
                        <sf:errors path="contactNumber" cssClass="error"></sf:errors>
                        <sf:hidden path="organizationId"/>
                    </div>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" id="edit-organization-button" value="Edit"/>
                    <a href="displayMainPage">
                        <button type="button" id="cancel-button" class="btn btn-primary">Cancel</button>
                    </a>
                </div>
            </sf:form>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>
