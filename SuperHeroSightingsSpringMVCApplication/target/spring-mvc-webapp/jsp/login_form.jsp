<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
        <title>Login Form</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">        
    </head>
    <body>
        <div class="navbar">
            <ul class="nav nav-tabs">
                <li role="presentation">
                    <a href="${pageContext.request.contextPath}/home">
                        Home
                    </a>
                </li>
            </ul>    
        </div>
        <div class="container">
            <h1>Login</h1>
            <hr>
            
            <c:if test="${param.login_error == 1}">
                <h3>Wrong username or password!</h3>
            </c:if>
                
            <form class="form form-horizontal"
                  role="form"
                  action="j_spring_security_check"
                  method="POST">
                <div class="form-group">
                    <label for="userName" class="control-label col-md-4">
                        Username:</label>
                    <div class="col-md-6">
                        <input type="text" 
                               id="userName" 
                               class="form-control"
                               name="j_username"
                               placeholder="Username"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="userName" class="control-label col-md-4">
                        Password:</label>
                    <div class="col-md-6">
                        <input type="password" 
                               id="password" 
                               class="form-control"
                               name="j_password"
                               placeholder="Password"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8">
                        <input type="submit" 
                               class="btn btn-primary"
                               value="Login"/>
                    </div>
                </div>
            </form>
        </div>
    </body>
</html>
