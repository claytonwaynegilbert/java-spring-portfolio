<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>${organization.name} Details</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">        
        <link href="${pageContext.request.contextPath}/css/organization-details.css" rel="stylesheet">
    </head>
    <body>
        <h1>${organization.name}</h1>
        <hr>
        <div class="container">
            <div class="row"> 
                <p><strong><span class="large-text">Name:</span></strong> <c:out value="${organization.name}"/></p><br>
                <p><strong><span class="large-text">Description:</span></strong> <c:out value="${organization.description}"/></p><br>
                <p><strong><span class="large-text">location:</span></strong> <c:out value="${organization.location.name}"/></p><br>
                <p><strong><span class="large-text">Contact Number:</span></strong> <c:out value="${organization.contactNumber}"/></p><br>
                <p>
                    <strong><span class="large-text">Members:</span></strong> 
                    <c:forEach var="fighter" items="${fighters}">
                        <c:out value="${fighter.name}"/><br>
                    </c:forEach>
                </p>
                <br><br>
                <a href="displayMainPage">
                    <button id="back-button" class="btn btn-primary">Back</button>
                </a>
            </div>
        </div>

        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    </body>
</html>
