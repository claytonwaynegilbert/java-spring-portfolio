<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>${location.name} Details</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">        
        <link href="${pageContext.request.contextPath}/css/location-details.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/upload-image-page.css" rel="stylesheet">
    </head>
    <body>
        <h1>Upload Image</h1>
        <hr>
        <div class="container">
            <div class="row"> 
                <div class="navbar">
                    <ul class="nav nav-tabs">
                        <li role="presentation"><a href="${pageContext.request.contextPath}/home">Home</a></li>
                        <li role="presentation"><a href="${pageContext.request.contextPath}/main/displayMainPage">Manager</a></li>
                        <sec:authorize access="hasRole('ROLE_USER')">
                        <li role="presentation" class="active">
                                <a href="${pageContext.request.contextPath}/upload/viewUploadForm">
                                    Upload
                                </a>
                            </li>
                        </sec:authorize>
                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                            <li role="presentation">
                                <a href="${pageContext.request.contextPath}/displayUserList">
                                    Admin(Admin Access Only)
                                </a>
                            </li>
                        </sec:authorize>
                    </ul>    
                </div>

                <c:if test="${pageContext.request.userPrincipal.name != null}">
                    <p>Hello : ${pageContext.request.userPrincipal.name}
                        | <a href="<c:url value="/j_spring_security_logout" />" > Logout</a>
                    </p>
                </c:if>

                <div>
                    <h3>${errorMsg}</h3>
                </div>

                <div id="description-box" class="col-md-8">
                    <p id="description"> 
                        Welcome to the upload form for Superhero Sightings! Here you can upload photos of your Supers!<br>
                        Whether it be heroes/villains you made up for fun, or it be The Incredible Hulk!<br>
                        These photos will show up when you click to see the details of your Sightings back in the Manager.<br>
                        So why are you just sitting there reading text on a screen...GET UPLOADING!<br>
                    </p>
                </div>

                <div class="col-md-6">
                    <form role="form" 
                          method="POST" 
                          action="uploadImage" 
                          enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="displayTitle">Display Title:</label>
                            <input type="text" 
                                   id="displayTitle" 
                                   name="displayTitle"
                                   placeholder="Image name and Display Title must both match name of Hero/Villain in the database"
                                   class="form-control"/>
                        </div>
                        <div class="form-group">
                            <label for="picture">Upload File:</label> 
                            <input type="file" 
                                   id="picture" 
                                   name="picture"/>
                        </div>
                        <input type="submit" value="Upload Picture" class="btn btn-primary"/>
                    </form>
                </div>
            </div>
        </div>

        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    </body>
</html>
