<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>${fighter.name} Details</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">        
        <link href="${pageContext.request.contextPath}/css/fighter-details.css" rel="stylesheet">
    </head>
    <body>
        <h1>${fighter.name} Details</h1>
        <hr>
        <div class="container">
            <div class="row"> 
                <p><strong><span class="large-text">Name:</span></strong> <c:out value="${fighter.name}"/></p><br>
                <p><strong><span class="large-text">Description:</span></strong> <c:out value="${fighter.description}"/></p><br>
                <p><strong><span class="large-text">Super Powers:</span></strong> <c:out value="${fighter.superPower}"/></p><br>
                <p><strong><span class="large-text">Alliance:</span></strong> <c:out value="${fighter.alliance}"/></p><br>
                <p>
                    <strong><span class="large-text">Organizations:</span></strong>
                    <c:forEach var="org" items="${fighter.organizationsBelongedTo}">
                        <c:out value="${org.name}"/>
                    </c:forEach>
                </p>
                <br><br>
                <a href="displayMainPage">
                    <button id="back-button" class="btn btn-primary">Back</button>
                </a>
            </div>
        </div>

        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    </body>
</html>
