/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
//    var sightingCount = document.getElementById("sighting-count").value;
//    var nameArray = document.getElementById("location-name").value;
//    var nameHalf = nameArray.replace("[", "");
//    var nameOtherHalf = nameHalf.replace("]", "");
//    var string = nameOtherHalf.toString();
//    var array = string.split(',');
//
//    for (var i = 0; i < sightingCount; i++) {
//        var first = array[i];
//        var address = first.toString();
//
//        jQuery(function ($) {
//            var q = encodeURIComponent(address);
//            $('#map' + i).attr('src',
//                    'https://www.google.com/maps/embed/v1/place?key=AIzaSyAWs0wLkQVVvuBvvXO-F1tERLsHZz3pdqM&q=' + q);
//        }
//      );
//    }
});







function getLatAndLongitude(address) {
    geocoder = new google.maps.Geocoder();
    var address = address;
    var array = [];
    geocoder.geocode({'address': address}, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            var lat = results[0].geometry.location.lat();
            var lon = results[0].geometry.location.lng();
            array.push(lat);
            array.push(lon);
            console.log(array)
        } else {
            alert("Geocode was not successful for the following reason: " + status);
        }
    });
    return array;
}

function createGoogleMap(mapElement, address) {
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            var mapOptions = {
                zoom: 14,
                center: results[0].geometry.location,
                disableDefaultUI: true
            };
            var map = new google.maps.Map(document.getElementById(mapElement), mapOptions);
            var marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location
            });
        } else {
            alert("Geocode was not successful for the following reason: " + status);
        }
    });
}
