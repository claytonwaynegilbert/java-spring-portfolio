DROP DATABASE IF EXISTS super_hero_database_v2;

CREATE DATABASE super_hero_database_v2;

USE super_hero_database_v2;

CREATE TABLE IF NOT EXISTS fighters
(
fighter_id int(11) NOT NULL auto_increment,
fighter_name varchar(25) NOT NULL,
description varchar(200) NOT NULL,
super_power varchar(100) NOT NULL,
alliance varchar(10) NOT NULL,
PRIMARY KEY(fighter_id)
)Engine=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS locations
(
location_id int(11) NOT NULL auto_increment,
location_name varchar(25) NOT NULL,
description varchar(200) NOT NULL,
address varchar(60) NOT NULL,
latitude varchar(15) NULL,
longitude varchar(15) NULL,
PRIMARY KEY(location_id)
)Engine=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS organizations
(
organization_id int(11) NOT NULL auto_increment,
organization_name varchar(25) NOT NULL,
description varchar(200) NOT NULL,
location_id int(11) NOT NULL,
contact_number varchar(25) NULL,
PRIMARY KEY(organization_id)
)Engine=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS sightings
(
sighting_id int(11) NOT NULL auto_increment,
fighter_id int(11) NOT NULL,
location_id int(11) NOT NULL,
sighting_date datetime NOT NULL,
PRIMARY KEY(sighting_id)
)Engine=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS fighter_organizations
(
fighter_id int(11) NOT NULL,
organization_id int(11) NOT NULL,
PRIMARY KEY(fighter_id, organization_id)
)Engine=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;


CREATE TABLE IF NOT EXISTS picture (
picture_id int(11) NOT NULL auto_increment,
display_title varchar(30) NOT NULL UNIQUE,
PRIMARY KEY(picture_id)
) Engine=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=23;


CREATE TABLE IF NOT EXISTS `users` (
 `user_id` int(11) NOT NULL AUTO_INCREMENT,
 `username` varchar(20) NOT NULL,
 `password` varchar(100) NOT NULL,
 `enabled` tinyint(1) NOT NULL,
 PRIMARY KEY (`user_id`),
 KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;
--
-- Dumping data for table `users`
--
INSERT INTO `users` (`user_id`, `username`, `password`, `enabled`) VALUES
(1, 'admin', 'password', 1),
(2, 'sidekick', 'password', 1);
--
-- Table structure for table `authorities`
--
CREATE TABLE IF NOT EXISTS `authorities` (
 `username` varchar(20) NOT NULL,
 `authority` varchar(20) NOT NULL,
 KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Dumping data for table `authorities`
--
INSERT INTO `authorities` (`username`, `authority`) VALUES
('admin', 'ROLE_ADMIN'),
('admin', 'ROLE_USER'),
('sidekick', 'ROLE_USER');


UPDATE `super_hero_database_v2`.`users`
SET
`password` = '$2a$10$fFdmGDLOjPd0mjV8EkufNewNSQ2dZ1d9M5k/QqgdhdCASfh3Q97Hm'
WHERE `user_id` = 1;
SELECT * FROM `super_hero_database_v2`.`users`;

UPDATE `super_hero_database_v2`.`users`
SET
`password` = '$2a$10$fFdmGDLOjPd0mjV8EkufNewNSQ2dZ1d9M5k/QqgdhdCASfh3Q97Hm'
WHERE `user_id` = 2;
SELECT * FROM `super_hero_database_v2`.`users`;

--
-- Constraints for table `authorities`
--
ALTER TABLE `authorities`
 ADD CONSTRAINT `authorities_ibfk_1` FOREIGN KEY (`username`) REFERENCES `users` (`username`) ON UPDATE CASCADE;
 
ALTER TABLE organizations ADD CONSTRAINT organizations_ibfk_1 FOREIGN KEY(location_id) 
REFERENCES locations(location_id) ON DELETE NO ACTION;

ALTER TABLE sightings ADD CONSTRAINT sightings_ibfk_1 FOREIGN KEY(fighter_id) 
REFERENCES fighters(fighter_id) ON DELETE NO ACTION;

ALTER TABLE sightings ADD CONSTRAINT sightings_ibfk_2 FOREIGN KEY(location_id) 
REFERENCES locations(location_id) ON DELETE NO ACTION;

ALTER TABLE fighter_organizations ADD CONSTRAINT fighter_organizations_ibfk_1 FOREIGN KEY(fighter_id) 
REFERENCES fighters(fighter_id) ON DELETE NO ACTION;

ALTER TABLE fighter_organizations ADD CONSTRAINT fighter_organizations_ibfk_2 FOREIGN KEY(organization_id) 
REFERENCES organizations(organization_id) ON DELETE NO ACTION;
