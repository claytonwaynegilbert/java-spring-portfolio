/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dto;

import java.time.LocalDate;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author cjsm12
 */
public class Sighting {

    private int sightingId;
    @NotNull(message="A sighting requires a Hero/Villain. If none exists please go back and creat one before continuing")
    private Fighter fighter;
    @NotNull(message="A sighting requires a Location. If none exists please go back and creat one before continuing")
    private Location location;
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    @NotNull(message="A sighting date is required")
    private LocalDate sightingOccurence;

    public int getSightingId() {
        return sightingId;
    }

    public void setSightingId(int sightingId) {
        this.sightingId = sightingId;
    }

    public Fighter getFighter() {
        return fighter;
    }

    public void setFighter(Fighter fighter) {
        this.fighter = fighter;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public LocalDate getSightingOccurence() {
        return sightingOccurence;
    }

    public void setSightingOccurence(LocalDate sightingOccurence) {
        this.sightingOccurence = sightingOccurence;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + this.sightingId;
        hash = 29 * hash + Objects.hashCode(this.fighter);
        hash = 29 * hash + Objects.hashCode(this.location);
        hash = 29 * hash + Objects.hashCode(this.sightingOccurence);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Sighting other = (Sighting) obj;
        if (this.sightingId != other.sightingId) {
            return false;
        }
        if (!Objects.equals(this.fighter, other.fighter)) {
            return false;
        }
        if (!Objects.equals(this.location, other.location)) {
            return false;
        }
        if (!Objects.equals(this.sightingOccurence, other.sightingOccurence)) {
            return false;
        }
        return true;
    }

}
