/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dto;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author cjsm12
 */
public class Fighter {

    private int fighterId;
    @NotEmpty(message = "This field must have a value")
    @Length(max = 50, message = "This field must be no more than 50 characters long")
    private String name;
    @NotEmpty(message = "This field must have a value")
    @Length(max = 200, message = "This field must be no more than 200 characters long")
    private String description;
    @NotEmpty(message = "This field must have a value")
    @Length(max = 75, message = "This field must be no more than 75 characters long")
    private String superPower;
    @NotEmpty(message = "This field must have a value")
    @Length(max = 10, message = "This field must be no more than 10 characters long")
    private String alliance;
    //@NotEmpty(message="An Organization needs to be selected. If there is none available, please go back and create one")
    String[] organizationNames;
    private List<Organization> organizationsBelongedTo;

    public int getFighterId() {
        return fighterId;
    }

    public void setFighterId(int fighterId) {
        this.fighterId = fighterId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSuperPower() {
        return superPower;
    }

    public void setSuperPower(String superPower) {
        this.superPower = superPower;
    }

    public String getAlliance() {
        return alliance;
    }

    public void setAlliance(String alliance) {
        this.alliance = alliance;
    }

    public String[] getOrganizationNames() {
        return organizationNames;
    }

    public void setOrganizationNames(String[] organizationNames) {
        this.organizationNames = organizationNames;
    }

    public List<Organization> getOrganizationsBelongedTo() {
        return organizationsBelongedTo;
    }

    public void setOrganizationsBelongedTo(List<Organization> organizationsBelongedTo) {
        this.organizationsBelongedTo = organizationsBelongedTo;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 73 * hash + this.fighterId;
        hash = 73 * hash + Objects.hashCode(this.name);
        hash = 73 * hash + Objects.hashCode(this.description);
        hash = 73 * hash + Objects.hashCode(this.superPower);
        hash = 73 * hash + Objects.hashCode(this.alliance);
        hash = 73 * hash + Arrays.deepHashCode(this.organizationNames);
        hash = 73 * hash + Objects.hashCode(this.organizationsBelongedTo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Fighter other = (Fighter) obj;
        if (this.fighterId != other.fighterId) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.superPower, other.superPower)) {
            return false;
        }
        if (!Objects.equals(this.alliance, other.alliance)) {
            return false;
        }
        if (!Arrays.deepEquals(this.organizationNames, other.organizationNames)) {
            return false;
        }
        if (!Objects.equals(this.organizationsBelongedTo, other.organizationsBelongedTo)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "Fighter{" + "fighterId=" + fighterId + ", name=" + name + ", description=" + description + ", superPower=" + superPower + ", alliance=" + alliance + ", organizationNames=" + organizationNames + ", organizationsBelongedTo=" + organizationsBelongedTo + '}';
    }

}
