/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author cjsm12
 */
@Controller
public class LoginController {
    
    @RequestMapping("/login")
    public String displayLoginForm(Model model){
        
        return "login_form";
    }
    
}
