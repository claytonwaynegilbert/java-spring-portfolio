/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.controller;

import com.sg.dao.PictureDao;
import com.sg.dto.Picture;
import java.io.File;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author cjsm12
 */
@Controller
@RequestMapping("/upload")
public class UploadController {

    private PictureDao dao;

    @Inject
    public UploadController(PictureDao dao) {
        this.dao = dao;
    }

    @RequestMapping(value = "/viewUploadForm", method = RequestMethod.GET)
    public String showUploadImageForm() {

        return "upload_image_form";
    }

    @RequestMapping(value = "/uploadImage", method = RequestMethod.POST)
    public String uploadImage(HttpServletRequest request,
            Model model,
            @RequestParam(value = "picture") MultipartFile pictureFile) {

        String displayTitle = request.getParameter("displayTitle");

        // only save the pictureFile if the user actually uploaded something
        if (!pictureFile.isEmpty()) {
            try {
                String contentType = pictureFile.getContentType();
                if (!pictureFile.getContentType().equalsIgnoreCase(contentType)) {
                    model.addAttribute("errorMsg", "Images must be of type png only.");
                    return "upload_image_form";
                }
                // we want to put the uploaded image into the 
                // <pictureFolder> folder of our application. getRealPath
                // returns the full path to the directory under Tomcat
                // where we can save files.
                String pictureFolder = "/pictureFolder/";
                String savePath = request
                        .getSession()
                        .getServletContext()
                        .getRealPath(pictureFolder);
                File dir = new File(savePath);
                // if <pictureFolder> directory is not there, 
                // go ahead and create it
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                // get the filename of the uploaded file - we'll use the
                // same name on the server.
                String fileName = pictureFile.getOriginalFilename();
                // transfer the contents of the uploaded pictureFile to 
                // the server
                String filePath = savePath + fileName;
                File dest = new File(filePath);
                pictureFile.transferTo(dest);

                // we successfully saved the pictureFile, now save a 
                // Picture to the DAO
                Picture picture = new Picture();
                picture.setDisplayTitle(displayTitle);
                picture.setFileName(pictureFolder + fileName);

                Picture titleCheckPicture = dao.getPictureByName(displayTitle);
                if (titleCheckPicture != null) {
                    model.addAttribute("errorMsg", "Can't upload picture with duplicate title name");
                    return "upload_image_form";
                }

                dao.addPicture(picture);
                // redirect to home page to redisplay the entire album
                return "redirect:/main/displayMainPage";

            } catch (Exception e) {
                // if we encounter an exception, add the error message 
                // to the model and return back to the pictureFile upload 
                // form page
                model.addAttribute("errorMsg", "File upload failed: "
                        + e.getMessage());

                return "upload_image_form";
            }
        } else {
            // if the user didn't upload anything, add the error 
            // message to the model and return back to the pictureFile 
            // upload form page
            model.addAttribute("errorMsg",
                    "Please specify a non-empty file.");

            return "upload_image_form";
        }
    }
}
