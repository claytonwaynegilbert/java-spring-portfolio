/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.controller;

import com.sg.dao.PictureDao;
import com.sg.service.SuperHeroServiceLayer;
import com.sg.dto.Fighter;
import com.sg.dto.Location;
import com.sg.dto.Organization;
import com.sg.dto.Picture;
import com.sg.dto.Sighting;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author cjsm12
 */
@Controller
@RequestMapping("/main")
public class ManagerController {

    private SuperHeroServiceLayer service;
    private PictureDao pictureDao;

    @Inject
    public ManagerController(SuperHeroServiceLayer service, PictureDao pictureDao) {
        this.service = service;
        this.pictureDao = pictureDao;
    }

    @RequestMapping(value = "/displayMainPage", method = RequestMethod.GET)
    public String displayMainPage(Model model) {
        List<Fighter> allFighters = service.selectAllFighters();
        List<Location> allLocations = service.selectAllLocations();
        List<Organization> allOrganizations = service.selectAllOrganizations();
        List<Sighting> allSightings = service.selectAllSightings();
        List<Sighting> sightingsFeed = service.selectLast10Sightings();

        List<String> nameList = new ArrayList<>();
        for (Sighting current : sightingsFeed) {
            nameList.add(current.getLocation().getName());
        }

        model.addAttribute("fighters", allFighters);
        model.addAttribute("organizations", allOrganizations);
        model.addAttribute("locations", allLocations);
        model.addAttribute("sightings", allSightings);
        model.addAttribute("sightingsFeed", sightingsFeed);
        model.addAttribute("locationNames", nameList);

        return "main_page";
    }

    @RequestMapping(value = "/addFighterPage", method = RequestMethod.GET)
    public String displayAddFighterPage(Model model) {
        Fighter fighter = new Fighter();
        List<Organization> organizations = service.selectAllOrganizations();
        List<String> organizationNames = new ArrayList<>();

        for (Organization currentOrg : organizations) {
            organizationNames.add(currentOrg.getName());
        }

        model.addAttribute("fighter", fighter);
        model.addAttribute("organizations", organizationNames);

        return "create_fighter";
    }

    @RequestMapping(value = "/addLocationPage", method = RequestMethod.GET)
    public String displayAddLocationPage(Model model) {
        Location location = new Location();

        model.addAttribute("location", location);

        return "create_location";
    }

    @RequestMapping(value = "/addOrganizationPage", method = RequestMethod.GET)
    public String displayAddOrganizationPage(Model model) {
        Organization organization = new Organization();
        List<Location> allLocations = service.selectAllLocations();

        model.addAttribute("locations", allLocations);
        model.addAttribute("organization", organization);

        return "create_organization";
    }

    @RequestMapping(value = "/addSightingPage", method = RequestMethod.GET)
    public String displayAddSightingPage(Model model) {
        Sighting sighting = new Sighting();
        List<Fighter> allFighters = service.selectAllFighters();
        List<Location> allLocations = service.selectAllLocations();

        model.addAttribute("sighting", sighting);
        model.addAttribute("locations", allLocations);
        model.addAttribute("fighters", allFighters);

        return "create_sighting";
    }

    @RequestMapping(value = "/createFighter", method = RequestMethod.POST)
    public String createFighter(@Valid @ModelAttribute Fighter fighter, BindingResult result, Model model) {

        if (result.hasErrors()) {
            List<Organization> allOrganizations = service.selectAllOrganizations();
            model.addAttribute("organizations", allOrganizations);

            return "create_fighter";
        }

        service.addFighter(fighter);

        return "redirect: displayMainPage";
    }

    @RequestMapping(value = "/createLocation", method = RequestMethod.POST)
    public String createLocation(@Valid @ModelAttribute Location location, BindingResult result) {

        if (result.hasErrors()) {
            return "create_location";
        }

        service.addLocation(location);

        return "redirect: displayMainPage";
    }

    @RequestMapping(value = "/createOrganization", method = RequestMethod.POST)
    public String createOrganization(@Valid @ModelAttribute Organization organization, BindingResult result, Model model) {

        if (result.hasErrors()) {
            List<Location> allLocations = service.selectAllLocations();
            model.addAttribute("locations", allLocations);
            model.addAttribute("errMessage", "A location is needed for an Organization.");
            return "create_organization";
        }
        
        service.addOrganization(organization);

        return "redirect: displayMainPage";
    }

    @RequestMapping(value = "/createSighting", method = RequestMethod.POST)
    public String createSighting(@Valid @ModelAttribute Sighting sighting, BindingResult result, Model model) {

        if (result.hasErrors()) {
            
            if (sighting.getSightingOccurence() == null) {
                List<Fighter> allFighters = service.selectAllFighters();
                List<Location> allLocations = service.selectAllLocations();
                model.addAttribute("fighters", allFighters);
                model.addAttribute("locations", allLocations);
                model.addAttribute("dateError", 
                "There was an error formatting your date. Please make sure your date format is of type mm/dd/yyyy and the date is a valid date.");
            }

            if (sighting.getLocation() == null || sighting.getFighter() == null) {
                List<Fighter> allFighters = service.selectAllFighters();
                List<Location> allLocations = service.selectAllLocations();
                model.addAttribute("fighters", allFighters);
                model.addAttribute("locations", allLocations);
                model.addAttribute("errMsg", 
                "Sighting needs both a Hero/Villain and Location. Please create one/both");
            }

            return "create_sighting";
        }

        service.addSighting(sighting);

        return "redirect: displayMainPage";
    }

    @RequestMapping(value = "/displayEditFighterForm", method = RequestMethod.GET)
    public String displayEditFighterPage(HttpServletRequest request, Model model) {
        String id = request.getParameter("fighterId");
        int fighterId = Integer.parseInt(id);

        Fighter matchingFighter = service.selectFighterById(fighterId);
        List<Organization> allOrganizations = service.selectAllOrganizations();
        List<String> organizationNames = new ArrayList<>();
        
        for(Organization org : allOrganizations){
            organizationNames.add(org.getName());
        }
        
        String[] names = organizationNames.toArray(new String[organizationNames.size()]);
        matchingFighter.setOrganizationNames(names);
        
        
        model.addAttribute("names", names);
        model.addAttribute("fighter", matchingFighter);
        model.addAttribute("organizations", allOrganizations);

        return "edit_fighter";
    }

    @RequestMapping(value = "/editFighter", method = RequestMethod.POST)
    public String EditFighter(@Valid @ModelAttribute Fighter fighter,
            BindingResult results, Model model) {

        if (results.hasErrors()) {
            List<Organization> allOrganizations = service.selectAllOrganizations();
            model.addAttribute("organizations", allOrganizations);
            return "edit_fighter";
        }

        service.updateFighter(fighter);

        return "redirect: displayMainPage";
    }

    @RequestMapping(value = "/displayEditLocationForm", method = RequestMethod.GET)
    public String displayEditLocationPage(HttpServletRequest request, Model model) {
        String id = request.getParameter("locationId");
        int locationId = Integer.parseInt(id);

        Location matchingLocation = service.selectLocationById(locationId);

        model.addAttribute("location", matchingLocation);

        return "edit_location";
    }

    @RequestMapping(value = "/editLocation", method = RequestMethod.POST)
    public String EditLocation(@Valid @ModelAttribute Location location,
            BindingResult results) {

        if (results.hasErrors()) {
            return "edit_location";
        }

        service.updateLocation(location);

        return "redirect: displayMainPage";
    }

    @RequestMapping(value = "/displayEditOrganizationForm", method = RequestMethod.GET)
    public String displayEditOrganizationPage(HttpServletRequest request, Model model) {
        String id = request.getParameter("organizationId");
        int organizationId = Integer.parseInt(id);

        Organization matchingOrganization = service.selectOrganizationById(organizationId);
        List<Location> allLocations = service.selectAllLocations();

        model.addAttribute("organization", matchingOrganization);
        model.addAttribute("locations", allLocations);

        return "edit_organization";
    }

    @RequestMapping(value = "/editOrganization", method = RequestMethod.POST)
    public String EditOrganization(@Valid @ModelAttribute Organization organization, BindingResult results, Model model) {

        if (results.hasErrors()) {
            List<Location> allLocations = service.selectAllLocations();
            model.addAttribute("locations", allLocations);
            return "edit_organization";
        }

        service.updateOrganization(organization);

        return "redirect: displayMainPage";
    }

    @RequestMapping(value = "/displayEditSightingForm", method = RequestMethod.GET)
    public String displayEditSightingForm(HttpServletRequest request, Model model) {
        String id = request.getParameter("sightingId");
        Integer sightingId = Integer.parseInt(id);

        Sighting matchingSighting = service.selectSightingById(sightingId);
        List<Fighter> allFighters = service.selectAllFighters();
        List<Location> allLocations = service.selectAllLocations();

        model.addAttribute("sighting", matchingSighting);
        model.addAttribute("fighters", allFighters);
        model.addAttribute("locations", allLocations);

        return "edit_sighting";
    }

    @RequestMapping(value = "/editSighting", method = RequestMethod.POST)
    public String EditSighting(@ModelAttribute Sighting sighting,
            BindingResult results, Model model) {

        if (results.hasErrors()) {
            List<Fighter> allFighters = service.selectAllFighters();
            List<Location> allLocations = service.selectAllLocations();
            model.addAttribute("fighters", allFighters);
            model.addAttribute("locations", allLocations);

            return "edit_sighting";
        }

        service.updateSighting(sighting);

        return "redirect: displayMainPage";
    }

    @RequestMapping(value = "/displayFighterDetails", method = RequestMethod.GET)
    public String displayFighterDetails(HttpServletRequest request, Model model) {
        String id = request.getParameter("fighterId");
        int fighterId = Integer.parseInt(id);

        Fighter matchingFighter = service.selectFighterById(fighterId);

        model.addAttribute("fighter", matchingFighter);

        return "fighter_details";
    }

    @RequestMapping(value = "/displayLocationDetails", method = RequestMethod.GET)
    public String displayLocationDetails(HttpServletRequest request, Model model) {
        String id = request.getParameter("locationId");
        int locationId = Integer.parseInt(id);

        Location matchingLocation = service.selectLocationById(locationId);

        model.addAttribute("location", matchingLocation);

        return "location_details";
    }

    @RequestMapping(value = "/displayOrganizationDetails", method = RequestMethod.GET)
    public String displayOrganizationDetails(HttpServletRequest request, Model model) {
        String id = request.getParameter("organizationId");
        int organizationId = Integer.parseInt(id);

        Organization matchingOrganization = service.selectOrganizationById(organizationId);
        List<Fighter> matchingFighters = service.selectAllFightersByOrganizationName(matchingOrganization.getName());

        model.addAttribute("fighters", matchingFighters);
        model.addAttribute("organization", matchingOrganization);

        return "organization_details";
    }

    @RequestMapping(value = "/displaySightingDetails", method = RequestMethod.GET)
    public String displaySightingDetails(HttpServletRequest request, Model model) {
        String id = request.getParameter("sightingId");
        int sightingId = Integer.parseInt(id);

        Sighting matchingSighting = service.selectSightingById(sightingId);
        Picture matchingPicture = pictureDao.getPictureByName(matchingSighting.getFighter().getName());

        if (matchingPicture != null) {
            model.addAttribute("superName", matchingPicture.getDisplayTitle());
            model.addAttribute("sighting", matchingSighting);
            return "sighting_details";
        }

        model.addAttribute("sighting", matchingSighting);
        return "sighting_details";
    }

    @RequestMapping(value = "/deleteFighter", method = RequestMethod.GET)
    public String deleteFighter(HttpServletRequest request) {
        String id = request.getParameter("fighterId");
        int fighterId = Integer.parseInt(id);

        service.removeFighter(fighterId);

        return "redirect: displayMainPage";
    }

    @RequestMapping(value = "/deleteLocation", method = RequestMethod.GET)
    public String deleteLocation(HttpServletRequest request) {
        String id = request.getParameter("locationId");
        int locationId = Integer.parseInt(id);

        service.removeLocation(locationId);

        return "redirect: displayMainPage";
    }

    @RequestMapping(value = "/deleteOrganization", method = RequestMethod.GET)
    public String deleteOrganization(HttpServletRequest request) {
        String id = request.getParameter("organizationId");
        int organizationId = Integer.parseInt(id);

        service.removeOrganization(organizationId);

        return "redirect: displayMainPage";
    }

    @RequestMapping(value = "/deleteSighting", method = RequestMethod.GET)
    public String deleteSighting(HttpServletRequest request) {
        String id = request.getParameter("sightingId");
        int sightingId = Integer.parseInt(id);

        service.removeSighting(sightingId);

        return "redirect: displayMainPage";
    }

}
