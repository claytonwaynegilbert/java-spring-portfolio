/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.controller;

import com.sg.dao.UserDao;
import com.sg.dto.User;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author cjsm12
 */
@Controller
public class UserController {
    
    private UserDao userDao;
    private PasswordEncoder passwordEncoder;
    
    @Inject
    public UserController(UserDao userDao, PasswordEncoder passwordEncoder){
        this.userDao = userDao;
        this.passwordEncoder = passwordEncoder;
    }
    
    @RequestMapping(value="/displayUserList", method=RequestMethod.GET)
    public String displayUserList(Model model){
        List<User> allUsers = userDao.getAllUsers();
        model.addAttribute("users", allUsers);
        
        return "display_users";
    }
    
    @RequestMapping(value="/displayUserForm", method=RequestMethod.GET)
    public String displayUserForm(){
        
        return "add_user_form";
    }
    
    @RequestMapping(value="/addUser", method=RequestMethod.POST)
    public String addUser(HttpServletRequest request, Model model){
        if(request.getParameter("userName").equalsIgnoreCase("admin")){
            model.addAttribute("error", "Sorry! User admin already exists..");
            return "add_user_form";
        }
        User user = new User();
        user.setUserName(request.getParameter("userName"));
        String clearPw = request.getParameter("password");
        String hashPw = passwordEncoder.encode(clearPw);
        user.setPassword(hashPw);
        user.addAuthority("ROLE_USER");
        
        if(request.getParameter("isAdmin") != null){
            user.addAuthority("ROLE_ADMIN");
        }
        
        userDao.addUser(user);
        
        return "redirect:/displayUserList";
    }
    
    @RequestMapping(value="/deleteUser", method=RequestMethod.GET)
    public String deleteUser(Model model, HttpServletRequest request){
        String userName = request.getParameter("userName");
        
        if(userName.equalsIgnoreCase("admin")){
            model.addAttribute("error", "Cannot delete admin!");
            return "display_users";
        }
        
        userDao.deleteUser(userName);
        
        return "redirect:/displayUserList";
    }
    
    
}
