/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dao;

import com.sg.dto.User;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author cjsm12
 */
//@Component("userDaoDbImpl")
public class UserDaoDbImpl implements UserDao {

    //@Autowired() //Annotations
    private JdbcTemplate jdbcTemplate;

    //XML configuration
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private static final String SQL_INSERT_USER
            = "insert into users(username, password, enabled) "
            + "values(?, ?, 1)";

    private static final String SQL_INSERT_AUTHORITY
            = "insert into authorities(username, authority) "
            + "values(?, ?)";

    private static final String SQL_DELETE_USER
            = "delete from users where username = ?";

    private static final String SQL_DELETE_AUTHORITY
            = "delete from authorities where username = ?";

    private static final String SQL_UPDATE_USER
            = "update users set "
            + "username = ?, "
            + "password = ?, "
            + "enabled = ? "
            + "where user_id = ?";

    private static final String SQL_SELECT_ALL_USERS
            = "select * from users";

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public User addUser(User user) {
        jdbcTemplate.update(SQL_INSERT_USER, user.getUserName(), user.getPassword());
        
        Integer userId = jdbcTemplate.queryForObject("select LAST_INSERT_ID()", Integer.class);
        user.setUserId(userId);

        insertUserAuthority(user);

        return user;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void deleteUser(String userName) {
        jdbcTemplate.update(SQL_DELETE_AUTHORITY, userName);
        jdbcTemplate.update(SQL_DELETE_USER, userName);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void updateUser(User newUser) {
        jdbcTemplate.update(SQL_UPDATE_USER, 
                newUser.getUserName(),
                newUser.getPassword(),
                1,
                newUser.getUserId());
        
        deleteAuthoritiesFromUser(newUser);
        insertUserAuthority(newUser);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public List<User> getAllUsers() {
        List<User> allUsers = jdbcTemplate.query(SQL_SELECT_ALL_USERS, new UserMapper());
        return allUsers;
    }

    private void insertUserAuthority(User user) {
        List<String> allAuthorities = user.getAuthorities();
        for (String authority : allAuthorities) {
            jdbcTemplate.update(SQL_INSERT_AUTHORITY, user.getUserName(), authority);
        }
    }
    
    private void deleteAuthoritiesFromUser(User user){
        jdbcTemplate.update(SQL_DELETE_AUTHORITY, user.getUserName());
    }

    private static class UserMapper implements RowMapper<User> {
        @Override
        public User mapRow(ResultSet rs, int i) throws SQLException {
            User user = new User();
            user.setUserId(rs.getInt("user_id"));
            user.setUserName(rs.getString("username"));
            user.setPassword(rs.getString("password"));

            return user;
        }
    }

}
