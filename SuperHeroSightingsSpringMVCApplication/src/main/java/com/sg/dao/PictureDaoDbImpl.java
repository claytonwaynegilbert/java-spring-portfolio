/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dao;

import com.sg.dto.Picture;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author cjsm12
 */

//@Component("pictureDaoDbImpl")
public class PictureDaoDbImpl implements PictureDao {

    //@Autowired() //Annotations
    private JdbcTemplate jdbcTemplate;

    //XML config file
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private static final String SQL_INSERT_PHOTO
            = "insert into picture (display_title) "
            + "values(?)";

    private static final String SQL_DELETE_PHOTO
            = "delete from picture where display_title = ?";

    private static final String SQL_UPDATE_PHOTO
            = "update picture set "
            + "display_title = ? "
            + "where picture_id = ?";

    private static final String SQL_SELECT_PHOTO_BY_TITLE
            = "select * from picture where display_title = ?";

    private static final String SQL_SELECT_ALL_PHOTOS
            = "select * from picture";

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Picture addPicture(Picture picture) {
        jdbcTemplate.update(SQL_INSERT_PHOTO,
                picture.getDisplayTitle());
        
        Integer pictureId = jdbcTemplate.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);
        picture.setPictureId(pictureId);

        return picture;
    }

    @Override
    public void deletePicture(String displayTitle) {
        jdbcTemplate.update(SQL_DELETE_PHOTO, displayTitle);
    }

    @Override
    public void updatePicture(Picture newPicture) {
        jdbcTemplate.update(SQL_UPDATE_PHOTO,
                newPicture.getDisplayTitle(),
                newPicture.getPictureId());
    }

    @Override
    public Picture getPictureByName(String displayTitle) {
        try {
            return jdbcTemplate.queryForObject(SQL_SELECT_PHOTO_BY_TITLE, new PictureMapper(), displayTitle);
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    @Override
    public List<Picture> getAllPictures() {
        return jdbcTemplate.query(SQL_SELECT_ALL_PHOTOS, new PictureMapper());
    }

    public static final class PictureMapper implements RowMapper<Picture> {

        @Override
        public Picture mapRow(ResultSet rs, int i) throws SQLException {
            Picture picture = new Picture();
            picture.setPictureId(rs.getInt("picture_id"));
            picture.setDisplayTitle(rs.getString("display_title"));

            return picture;
        }

    }

}
