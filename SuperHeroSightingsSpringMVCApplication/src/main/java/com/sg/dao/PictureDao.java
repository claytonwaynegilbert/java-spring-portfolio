/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dao;

import com.sg.dto.Picture;
import java.util.List;

/**
 *
 * @author cjsm12
 */
public interface PictureDao {
    
    Picture addPicture(Picture picture);
    
    void deletePicture(String displayTitle);
    
    void updatePicture(Picture newPicture);
    
    Picture getPictureByName(String displayTitle);
    
    List<Picture> getAllPictures();
}
