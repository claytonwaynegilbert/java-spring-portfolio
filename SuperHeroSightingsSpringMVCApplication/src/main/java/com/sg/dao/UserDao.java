/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dao;

import com.sg.dto.User;
import java.util.List;

/**
 *
 * @author cjsm12
 */
public interface UserDao {

    User addUser(User user);

    void deleteUser(String userName);

    void updateUser(User newUser);

    List<User> getAllUsers();
}
