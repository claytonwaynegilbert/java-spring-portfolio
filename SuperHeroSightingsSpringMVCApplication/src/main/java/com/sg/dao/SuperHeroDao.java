/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dao;

import com.sg.dto.Fighter;
import com.sg.dto.Location;
import com.sg.dto.Organization;
import com.sg.dto.Sighting;
import java.time.LocalDate;
import java.util.List;

/**
 *
 * @author cjsm12
 */
public interface SuperHeroDao {

    Fighter addFighter(Fighter fighter);

    void removeFighter(int fighterId);

    void updateFighter(Fighter fighter);

    Fighter selectFighterById(int fighterId);
    
    Fighter selectFighterByName(String fighterName);

    List<Fighter> selectAllFighters();
    
    List<Fighter> selectAllFightersSeenAtLocation(int locationId);
    
    List<Fighter> selectAllFightersByOrganization(int organizationId);
    
    List<Fighter> selectAllFighterByOrganizationName(String organizationName);

    Location addLocation(Location loction);

    void removeLocation(int locationId);

    void updateLocation(Location location);

    Location selectLocationById(int locationId);
    
    Location selectLocationByName(String locationName);

    List<Location> selectAllLocations();
    
    List<Location> selectAllLocationsFighterHasBeen(int fighterId);
    
    List<Location> selectAllLocationsFighterHasBeen(String fighterName);

    Organization addOrganization(Organization organization);

    void removeOrganization(int organizationId);

    void updateOrganization(Organization organization);

    Organization selectOrganizationById(int organizationId);
    
    Organization selectOrganizationByName(String organizationName);

    List<Organization> selectAllOrganizations();
    
    List<Organization> selectAllOrganizationsJoinedByFighter(int fighterId);
    
    List<Organization> selectAllOrganizationsJoinedByFighter(String fighterName);
    
    Sighting addSighting(Sighting sighting);
    
    void removeSighting(int sightingId);
    
    void updateSighting(Sighting sighting);
    
    Sighting selectSightingById(int sightingId);
    
    List<Sighting> selectAllSightings();
    
    List<Sighting> selectAllSightingsByDate(LocalDate date);
    
    List<Sighting> selectLast10Sightings();


}
