/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.dao;

import com.sg.dto.Fighter;
import com.sg.dto.Location;
import com.sg.dto.Organization;
import com.sg.dto.Sighting;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import javax.inject.Qualifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author cjsm12
 */

//@Component("daoDbImpl")
public class SuperHeroDaoDbImpl implements SuperHeroDao {

    //@Autowired() //Showing both ways, annotation and...
    private JdbcTemplate jdbcTemplate;

    //...setter injection via configuation file
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private static final String SQL_INSERT_FIGHTER
            = "insert into fighters (fighter_name, description, super_power, alliance) "
            + "values (?, ?, ?, ?)";

    private static final String SQL_REMOVE_FIGHTER
            = "delete from fighters where fighter_id = ?";

    private static final String SQL_UPDATE_FIGHTER
            = "update fighters set "
            + "fighter_name = ?, "
            + "description = ?, "
            + "super_power = ?, "
            + "alliance = ? "
            + "where fighter_id = ?";

    private static final String SQL_SELECT_FIGHTER_BY_ID
            = "select * from fighters where fighter_id = ?";

    private static final String SQL_SELECT_FIGHTER_BY_NAME
            = "select * from fighters where UPPER(fighter_name) = UPPER(?)";

    private static final String SQL_SELECT_ALL_FIGHTERS
            = "select * from fighters";

    private static final String SQL_SELECT_ALL_FIGHTERS_SEEN_AT_LOCATION
            = "select * from fighters f join sightings s on "
            + "f.fighter_id = s.fighter_id "
            + "where s.location_id = ?";

    private static final String SQL_SELECT_ALL_FIGHTERS_PART_OF_ORGANIZATION
            = "select * from fighters f join fighter_organizations fo on "
            + "f.fighter_id = fo.fighter_id "
            + "where fo.organization_id = ?";

    private static final String SQL_SELECT_ALL_FIGHTERS_BY_ORGANIZATION_NAME
            = "select * from fighters f "
            + "join fighter_organizations fo on f.fighter_id = fo.fighter_id "
            + "join organizations o on o.organization_id = fo.organization_id where "
            + "UPPER(o.organization_name) = UPPER(?)";

    private static final String SQL_SELECT_FIGHTER_BY_SIGHTING_ID
            = "select * from fighters f join sightings s on "
            + "f.fighter_id = s.fighter_id where s.sighting_id = ?";

    private static final String SQL_INSERT_LOCATION
            = "insert into locations (location_name, description, address, latitude, longitude) "
            + "values (?, ?, ?, ?, ?)";

    private static final String SQL_REMOVE_LOCATION
            = "delete from locations where location_id = ?";

    private static final String SQL_UPDATE_LOCATION
            = "update locations set "
            + "location_name = ?, "
            + "description = ?, "
            + "address = ?, "
            + "latitude = ?, "
            + "longitude = ? "
            + "where location_id = ?";

    private static final String SQL_SELECT_LOCATION_BY_ID
            = "select * from locations where location_id = ?";

    private static final String SQL_SELECT_ALL_LOCATIONS
            = "select * from locations";

    private static final String SQL_SELECT_ALL_LOCATIONS_VISITED_BY_FIGHTER
            = "select * from locations l join sightings s on "
            + "l.location_id = s.location_id "
            + "where s.fighter_id = ?";

    private static final String SQL_SELECT_ALL_LOCATIONS_VISITED_BY_FIGHTER_NAME
            = "select * from locations l "
            + "join sightings s on l.location_id = s.location_id "
            + "join fighters f on f.fighter_id = s.fighter_id "
            + "where UPPER(f.name) = UPPER(?)";

    private static final String SQL_SELECT_LOCATION_BY_ORGANIZATION
            = "select * from locations l join organizations o on "
            + "l.location_id = o.location_id where o.organization_id = ?";

    private static final String SQL_SELECT_LOCATION_BY_SIGHTING_ID
            = "select * from locations l join sightings s on "
            + "l.location_id = s.location_id where s.sighting_id = ?";

    private static final String SQL_SELECT_LOCATION_BY_NAME
            = "select * from locations where UPPER(location_name) = UPPER(?)";

    private static final String SQL_INSERT_ORGANIZATION
            = "insert into organizations (organization_name, description, location_id, contact_number) "
            + "values(?, ?, ?, ?)";

    private static final String SQL_INSERT_INTO_FIGHTER_ORGANIZATIONS_TABLE
            = "insert into fighter_organizations (fighter_id, organization_id) "
            + "values(?, ?)";

    private static final String SQL_REMOVE_ORGANIZATION
            = "delete from organizations where organization_id = ?";

    private static final String SQL_REMOVE_FROM_FIGHTER_ORGANIZATIONS_TABLE_BY_ORGANIZATION
            = "delete from fighter_organizations where organization_id = ?";

    private static final String SQL_REMOVE_ORGANIZATIONS_JOINED_BY_FIGHTER
            = "delete from fighter_organizations where fighter_id = ?";

    private static final String SQL_REMOVE_ORGANIZATION_BY_LOCATION
            = "delete from organizations where location_id = ?";

    private static final String SQL_UPDATE_ORGANIZATION
            = "update organizations set "
            + "organization_name = ?, "
            + "description = ?, "
            + "location_id = ?, "
            + "contact_number = ? "
            + "where organization_id = ?";

    private static final String SQL_SELECT_ORGANIZATION_BY_ID
            = "select * from organizations where organization_id = ?";

    private static final String SQL_SELECT_ORGANIZATION_BY_NAME
            = "select * from organizations where UPPER(organization_name) = UPPER(?)";

    private static final String SQL_SELECT_ALL_ORGANIZATIONS
            = "select * from organizations";

    private static final String SQL_SELECT_ALL_ORGANIZATIONS_JOINED_BY_FIGHTER
            = "select * from organizations o join fighter_organizations fo on "
            + "o.organization_id = fo.organization_id "
            + "where fo.fighter_id = ?";

    private static final String SQL_SELECT_ALL_ORGANIZATIONS_JOINED_BY_FIGHTER_NAME
            = "select * from organizations o "
            + "join fighter_organizations fo on o.organization_id = fo.organization_id "
            + "join fighters f on f.fighter_id = fo.fighter_id "
            + "where UPPER(f.fighter_name) = UPPER(?)";

    private static final String SQL_INSERT_SIGHTING
            = "insert into sightings (fighter_id, location_id, sighting_date) "
            + "values(?, ?, ?)";

    private static final String SQL_REMOVE_SIGHTING
            = "delete from sightings where sighting_id = ?";

    private static final String SQL_REMOVE_SIGHTING_BY_FIGHTER
            = "delete from sightings where fighter_id = ?";

    private static final String SQL_REMOVE_SIGHTING_BY_LOCATION
            = "delete from sightings where location_id = ?";

    private static final String SQL_UPDATE_SIGHTING
            = "update sightings set "
            + "fighter_id = ?, "
            + "location_id = ?, "
            + "sighting_date = ? "
            + "where sighting_id = ?";

    private static final String SQL_SELECT_SIGHTING_BY_ID
            = "select * from sightings where sighting_id = ?";

    private static final String SQL_SELECT_ALL_SIGHTINGS
            = "select * from sightings";

    private static final String SQL_SELECT_LAST_10_SIGHTINGS
            = "select * from sightings LIMIT 10";

    private static final String SQL_SELECT_ALL_SIGHTINGS_ON_DATE
            = "select * from sightings where sighting_date = ?";

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Fighter addFighter(Fighter fighter) {
        jdbcTemplate.update(SQL_INSERT_FIGHTER,
                fighter.getName(),
                fighter.getDescription(),
                fighter.getSuperPower(),
                fighter.getAlliance());

        int fighter_id = jdbcTemplate.queryForObject("select LAST_INSERT_ID()", Integer.class);
        fighter.setFighterId(fighter_id);

        if (fighter.getOrganizationsBelongedTo() != null) {
            addToFighterOrganizationsTable(fighter);
        }

        return fighter;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void removeFighter(int fighterId) {
        jdbcTemplate.update(SQL_REMOVE_ORGANIZATIONS_JOINED_BY_FIGHTER, fighterId);
        jdbcTemplate.update(SQL_REMOVE_SIGHTING_BY_FIGHTER, fighterId);
        jdbcTemplate.update(SQL_REMOVE_FIGHTER, fighterId);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void updateFighter(Fighter fighter) {
        jdbcTemplate.update(SQL_UPDATE_FIGHTER,
                fighter.getName(),
                fighter.getDescription(),
                fighter.getSuperPower(),
                fighter.getAlliance(),
                fighter.getFighterId());

        jdbcTemplate.update(SQL_REMOVE_ORGANIZATIONS_JOINED_BY_FIGHTER, fighter.getFighterId());
        
        if (fighter.getOrganizationsBelongedTo() != null) {
            addToFighterOrganizationsTable(fighter);
        }

    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Fighter selectFighterById(int fighterId) {
        Fighter fighter;
        try {
            fighter = jdbcTemplate.queryForObject(SQL_SELECT_FIGHTER_BY_ID,
                    new FighterMapper(), fighterId);
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }

        setOrganizationsToFighter(fighter);
        return fighter;
    }

    @Override
    public Fighter selectFighterByName(String fighterName) {
        Fighter fighter;
        try {
            fighter = jdbcTemplate.queryForObject(SQL_SELECT_FIGHTER_BY_NAME, new FighterMapper(), fighterName);
            setOrganizationsToFighter(fighter);
            return fighter;
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public List<Fighter> selectAllFighters() {
        List<Fighter> allFighters = jdbcTemplate.query(SQL_SELECT_ALL_FIGHTERS, new FighterMapper());
        setOrganizationsToFighterCollection(allFighters);

        return allFighters;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public List<Fighter> selectAllFightersSeenAtLocation(int locationId) {
        List<Fighter> allFighters = jdbcTemplate.query(SQL_SELECT_ALL_FIGHTERS_SEEN_AT_LOCATION, new FighterMapper(), locationId);
        setOrganizationsToFighterCollection(allFighters);

        return allFighters;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public List<Fighter> selectAllFightersByOrganization(int organizationId) {
        List<Fighter> allFighters = jdbcTemplate.query(SQL_SELECT_ALL_FIGHTERS_PART_OF_ORGANIZATION, new FighterMapper(), organizationId);
        setOrganizationsToFighterCollection(allFighters);

        return allFighters;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public List<Fighter> selectAllFighterByOrganizationName(String organizationName) {
        List<Fighter> allFighters = jdbcTemplate.query(
                SQL_SELECT_ALL_FIGHTERS_BY_ORGANIZATION_NAME,
                new FighterMapper(),
                organizationName);
        
        setOrganizationsToFighterCollection(allFighters);
        return allFighters;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Location addLocation(Location location) {
        jdbcTemplate.update(SQL_INSERT_LOCATION,
                location.getName(),
                location.getDescription(),
                location.getAddress(),
                location.getLatitude(),
                location.getLongitude());

        int locationId = jdbcTemplate.queryForObject("select LAST_INSERT_ID()", Integer.class);
        location.setLocationId(locationId);
        return location;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void removeLocation(int locationId) {
        jdbcTemplate.update(SQL_REMOVE_SIGHTING_BY_LOCATION, locationId);
        jdbcTemplate.update(SQL_REMOVE_ORGANIZATION_BY_LOCATION, locationId);
        jdbcTemplate.update(SQL_REMOVE_LOCATION, locationId);
    }

    @Override
    public void updateLocation(Location location) {
        jdbcTemplate.update(SQL_UPDATE_LOCATION,
                location.getName(),
                location.getDescription(),
                location.getAddress(),
                location.getLatitude(),
                location.getLongitude(),
                location.getLocationId());
    }

    @Override
    public Location selectLocationById(int locationId) {
        try {
            return jdbcTemplate.queryForObject(SQL_SELECT_LOCATION_BY_ID,
                    new LocationMapper(),
                    locationId);
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    @Override
    public Location selectLocationByName(String locationName) {
        try {
            return jdbcTemplate.queryForObject(SQL_SELECT_LOCATION_BY_NAME, new LocationMapper(), locationName);
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }

    }

    @Override
    public List<Location> selectAllLocations() {
        return jdbcTemplate.query(SQL_SELECT_ALL_LOCATIONS, new LocationMapper());
    }

    @Override
    public List<Location> selectAllLocationsFighterHasBeen(int fighterId) {
        return jdbcTemplate.query(SQL_SELECT_ALL_LOCATIONS_VISITED_BY_FIGHTER,
                new LocationMapper(),
                fighterId);
    }

    @Override
    public List<Location> selectAllLocationsFighterHasBeen(String fighterName) {
        return jdbcTemplate.query(SQL_SELECT_ALL_LOCATIONS_VISITED_BY_FIGHTER_NAME,
                new LocationMapper(), fighterName);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Organization addOrganization(Organization organization) {
        jdbcTemplate.update(SQL_INSERT_ORGANIZATION,
                organization.getName(),
                organization.getDescription(),
                organization.getLocation().getLocationId(),
                organization.getContactNumber());

        int organizationId = jdbcTemplate.queryForObject("select LAST_INSERT_ID()",
                Integer.class);
        organization.setOrganizationId(organizationId);

        return organization;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void removeOrganization(int organizationId) {
        jdbcTemplate.update(SQL_REMOVE_FROM_FIGHTER_ORGANIZATIONS_TABLE_BY_ORGANIZATION, organizationId);
        jdbcTemplate.update(SQL_REMOVE_ORGANIZATION, organizationId);
    }

    @Override
    public void updateOrganization(Organization organization) {
        jdbcTemplate.update(SQL_UPDATE_ORGANIZATION,
                organization.getName(),
                organization.getDescription(),
                organization.getLocation().getLocationId(),
                organization.getContactNumber(),
                organization.getOrganizationId());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Organization selectOrganizationById(int organizationId) {
        Organization organization;
        try {
            organization
                    = jdbcTemplate.queryForObject(
                            SQL_SELECT_ORGANIZATION_BY_ID,
                            new OrganizationMapper(),
                            organizationId);
            setLocationToOrganizationObject(organization);

            return organization;
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }

    }

    @Override
    public Organization selectOrganizationByName(String organizationName) {
        Organization organization;
        try {
            organization = jdbcTemplate.queryForObject(SQL_SELECT_ORGANIZATION_BY_NAME, new OrganizationMapper(), organizationName);
            setLocationToOrganizationObject(organization);
            return organization;
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public List<Organization> selectAllOrganizations() {
        List<Organization> allOrganizations
                = jdbcTemplate.query(SQL_SELECT_ALL_ORGANIZATIONS, new OrganizationMapper());
        setLocationToCollectionOfOrganizations(allOrganizations);

        return allOrganizations;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public List<Organization> selectAllOrganizationsJoinedByFighter(int fighterId) {
        List<Organization> organizations = jdbcTemplate.query(
                SQL_SELECT_ALL_ORGANIZATIONS_JOINED_BY_FIGHTER,
                new OrganizationMapper(),
                fighterId);
        setLocationToCollectionOfOrganizations(organizations);
        return organizations;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public List<Organization> selectAllOrganizationsJoinedByFighter(String fighterName) {
        List<Organization> organizations
                = jdbcTemplate.query(SQL_SELECT_ALL_ORGANIZATIONS_JOINED_BY_FIGHTER_NAME,
                        new OrganizationMapper(),
                        fighterName);
        setLocationToCollectionOfOrganizations(organizations);
        return organizations;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Sighting addSighting(Sighting sighting) {
        jdbcTemplate.update(SQL_INSERT_SIGHTING,
                sighting.getFighter().getFighterId(),
                sighting.getLocation().getLocationId(),
                sighting.getSightingOccurence().toString());

        int sightingId = jdbcTemplate.queryForObject("select LAST_INSERT_ID()",
                Integer.class);
        sighting.setSightingId(sightingId);

        return sighting;
    }

    @Override
    public void removeSighting(int sightingId) {
        jdbcTemplate.update(SQL_REMOVE_SIGHTING, sightingId);
    }

    @Override
    public void updateSighting(Sighting sighting) {
        jdbcTemplate.update(SQL_UPDATE_SIGHTING,
                sighting.getFighter().getFighterId(),
                sighting.getLocation().getLocationId(),
                sighting.getSightingOccurence().toString(),
                sighting.getSightingId());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Sighting selectSightingById(int sightingId) {
        Sighting sighting;
        try {
            sighting = jdbcTemplate.queryForObject(
                    SQL_SELECT_SIGHTING_BY_ID,
                    new SightingMapper(),
                    sightingId);
            setFighterToSightingObject(sighting);
            setLocationToSightingObject(sighting);
            return sighting;

        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public List<Sighting> selectAllSightings() {
        List<Sighting> allSightings
                = jdbcTemplate.query(SQL_SELECT_ALL_SIGHTINGS,
                        new SightingMapper());
        setFighterAndLocationToSightingCollection(allSightings);

        return allSightings;
    }

    @Override
    public List<Sighting> selectLast10Sightings() {
        List<Sighting> allSightings
                = jdbcTemplate.query(SQL_SELECT_LAST_10_SIGHTINGS,
                        new SightingMapper());

        setFighterAndLocationToSightingCollection(allSightings);

        return allSightings;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public List<Sighting> selectAllSightingsByDate(LocalDate date) {
        List<Sighting> allSightings = jdbcTemplate.query(
                SQL_SELECT_ALL_SIGHTINGS_ON_DATE,
                new SightingMapper(),
                date.toString());
        setFighterAndLocationToSightingCollection(allSightings);
        return allSightings;
    }

    private void addToFighterOrganizationsTable(Fighter fighter) {
        final int fighterId = fighter.getFighterId();
        List<Organization> allOrganizations = fighter.getOrganizationsBelongedTo();
        for (Organization org : allOrganizations) {
            jdbcTemplate.update(SQL_INSERT_INTO_FIGHTER_ORGANIZATIONS_TABLE,
                    fighterId,
                    org.getOrganizationId());
        }
    }

    private void setOrganizationsToFighter(Fighter fighter) {
        final int fighterId = fighter.getFighterId();
        Fighter fighterSelected = fighter;
        List<Organization> allOrganizations
                = jdbcTemplate.query(SQL_SELECT_ALL_ORGANIZATIONS_JOINED_BY_FIGHTER,
                        new OrganizationMapper(),
                        fighterId);
        setLocationToCollectionOfOrganizations(allOrganizations);

        fighterSelected.setOrganizationsBelongedTo(allOrganizations);
    }

    private void setOrganizationsToFighterCollection(List<Fighter> fighterCollection) {
        List<Fighter> allFighters = fighterCollection;
        for (Fighter fighter : allFighters) {
            setOrganizationsToFighter(fighter);
        }
    }

    private void setLocationToOrganizationObject(Organization organization) {
        final int organizationId = organization.getOrganizationId();
        Location location
                = jdbcTemplate.queryForObject(SQL_SELECT_LOCATION_BY_ORGANIZATION, new LocationMapper(),
                        organizationId);

        organization.setLocation(location);
    }

    private void setLocationToCollectionOfOrganizations(List<Organization> organizations) {
        List<Organization> allOrganizations = organizations;

        for (Organization organization : allOrganizations) {
            setLocationToOrganizationObject(organization);
        }
    }

    private void setFighterToSightingObject(Sighting sighting) {
        Sighting selectedSighting = sighting;
        final int sightingId = sighting.getSightingId();
        Fighter fighter = jdbcTemplate.queryForObject(
                SQL_SELECT_FIGHTER_BY_SIGHTING_ID,
                new FighterMapper(),
                sightingId);
        setOrganizationsToFighter(fighter);
        selectedSighting.setFighter(fighter);
    }

    private void setLocationToSightingObject(Sighting sighting) {
        Sighting selectedSighting = sighting;
        final int sightingId = sighting.getSightingId();
        Location location = jdbcTemplate.queryForObject(SQL_SELECT_LOCATION_BY_SIGHTING_ID,
                new LocationMapper(),
                sightingId);
        selectedSighting.setLocation(location);
    }

    private void setFighterAndLocationToSightingCollection(List<Sighting> allSightings) {
        List<Sighting> sightingList = allSightings;
        for (Sighting sighting : sightingList) {
            setFighterToSightingObject(sighting);
            setLocationToSightingObject(sighting);
        }
    }

    private static final class FighterMapper implements RowMapper<Fighter> {

        @Override
        public Fighter mapRow(ResultSet rs, int i) throws SQLException {
            Fighter fighter = new Fighter();
            fighter.setFighterId(rs.getInt("fighter_id"));
            fighter.setName(rs.getString("fighter_name"));
            fighter.setDescription(rs.getString("description"));
            fighter.setSuperPower(rs.getString("super_power"));
            fighter.setAlliance(rs.getString("alliance"));

            return fighter;
        }
    }

    private static final class LocationMapper implements RowMapper<Location> {

        @Override
        public Location mapRow(ResultSet rs, int i) throws SQLException {
            Location location = new Location();
            location.setLocationId(rs.getInt("location_id"));
            location.setName(rs.getString("location_name"));
            location.setDescription(rs.getString("description"));
            location.setAddress(rs.getString("address"));
            location.setLatitude(rs.getString("latitude"));
            location.setLongitude(rs.getString("longitude"));

            return location;
        }
    }

    private static final class OrganizationMapper implements RowMapper<Organization> {

        @Override
        public Organization mapRow(ResultSet rs, int i) throws SQLException {
            Organization organization = new Organization();
            organization.setOrganizationId(rs.getInt("organization_id"));
            organization.setName(rs.getString("organization_name"));
            organization.setDescription(rs.getString("description"));
            organization.setContactNumber(rs.getString("contact_number"));

            return organization;
        }
    }

    private static final class SightingMapper implements RowMapper<Sighting> {

        @Override
        public Sighting mapRow(ResultSet rs, int i) throws SQLException {
            Sighting sighting = new Sighting();
            sighting.setSightingId(rs.getInt("sighting_id"));
            sighting.setSightingOccurence(
                    rs.getTimestamp("sighting_date").toLocalDateTime().toLocalDate());
            return sighting;
        }
    }

}
