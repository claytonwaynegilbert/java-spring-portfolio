/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.service;

import com.sg.dao.*;
import com.sg.dto.Fighter;
import com.sg.dto.Location;
import com.sg.dto.Organization;
import com.sg.dto.Sighting;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author cjsm12
 */

public class SuperHeroServiceLayerImpl implements SuperHeroServiceLayer {

    private SuperHeroDao dao;

    //Configuration file xml
    public SuperHeroServiceLayerImpl(SuperHeroDao dao) {
        this.dao = dao;
    }

    @Override
    public void addFighter(Fighter fighter) {

        String[] organizationNameList = fighter.getOrganizationNames();

        if (organizationNameList != null) {
            List<Organization> organizations = new ArrayList<>();

            for (String organizationName : organizationNameList) {
                organizations.add(dao.selectOrganizationByName(organizationName));
            }
            fighter.setOrganizationsBelongedTo(organizations);
        }

        dao.addFighter(fighter);
    }

    @Override
    public void removeFighter(int fighterId) {
        dao.removeFighter(fighterId);
    }

    @Override
    public void updateFighter(Fighter fighter) {
        if (fighter.getOrganizationNames() == null) {
            dao.updateFighter(fighter);
            return;
        }

        String[] organizationNames = fighter.getOrganizationNames();
        List<Organization> organizations = new ArrayList<>();

        for (String name : organizationNames) {
            organizations.add(dao.selectOrganizationByName(name));
        }

        fighter.setOrganizationsBelongedTo(organizations);

        dao.updateFighter(fighter);
    }

    @Override
    public Fighter selectFighterById(int fighterId) {
        return dao.selectFighterById(fighterId);
    }

    @Override
    public Fighter selectFighterByName(String fighterName) {
        return dao.selectFighterByName(fighterName);
    }

    @Override
    public List<Fighter> selectAllFighters() {
        return dao.selectAllFighters();
    }

    @Override
    public List<Fighter> selectAllFightersSeenAtLocation(int locationId) {
        return dao.selectAllFightersSeenAtLocation(locationId);
    }

    @Override
    public List<Fighter> selectAllFightersByOrganization(int organizationId) {
        return dao.selectAllFightersByOrganization(organizationId);
    }

    @Override
    public List<Fighter> selectAllFightersByOrganizationName(String organizationName) {
        return dao.selectAllFighterByOrganizationName(organizationName);
    }

    @Override
    public void addLocation(Location loction) {
        dao.addLocation(loction);
    }

    @Override
    public void removeLocation(int locationId) {
        dao.removeLocation(locationId);
    }

    @Override
    public void updateLocation(Location location) {
        dao.updateLocation(location);
    }

    @Override
    public Location selectLocationById(int locationId) {
        return dao.selectLocationById(locationId);
    }

    @Override
    public Location selectLocationByName(String locationName) {
        return dao.selectLocationByName(locationName);
    }

    @Override
    public List<Location> selectAllLocations() {
        return dao.selectAllLocations();
    }

    @Override
    public List<Location> selectAllLocationsFighterHasBeen(int fighterId) {
        return dao.selectAllLocationsFighterHasBeen(fighterId);
    }

    @Override
    public List<Location> selectAllLocationsFighterHasBeen(String fighterName) {
        return dao.selectAllLocationsFighterHasBeen(fighterName);
    }

    @Override
    public void addOrganization(Organization organization) {
        String locationName = organization.getLocation().getName();

        Location matchingLocation = dao.selectLocationByName(locationName);

        organization.setLocation(matchingLocation);

        dao.addOrganization(organization);
    }

    @Override
    public void removeOrganization(int organizationId) {
        dao.removeOrganization(organizationId);
    }

    @Override
    public void updateOrganization(Organization organization) {
        String locationName = organization.getLocation().getName();

        Location matchingLocation = dao.selectLocationByName(locationName);
        organization.setLocation(matchingLocation);

        dao.updateOrganization(organization);
    }

    @Override
    public Organization selectOrganizationById(int organizationId) {
        return dao.selectOrganizationById(organizationId);
    }

    @Override
    public Organization selectOrganizationByName(String organizationName) {
        return dao.selectOrganizationByName(organizationName);
    }

    @Override
    public List<Organization> selectAllOrganizations() {
        return dao.selectAllOrganizations();
    }

    @Override
    public List<Organization> selectAllOrganizationsJoinedByFighter(int fighterId) {
        return dao.selectAllOrganizationsJoinedByFighter(fighterId);
    }

    @Override
    public List<Organization> selectAllOrganizationsJoinedByFighter(String fighterName) {
        return dao.selectAllOrganizationsJoinedByFighter(fighterName);
    }

    @Override
    public void addSighting(Sighting sighting) {
        String fighterName = sighting.getFighter().getName();
        String locationName = sighting.getLocation().getName();

        Fighter matchingFighter = dao.selectFighterByName(fighterName);
        Location matchingLocation = dao.selectLocationByName(locationName);
        sighting.setFighter(matchingFighter);
        sighting.setLocation(matchingLocation);

        dao.addSighting(sighting);
    }

    @Override
    public void removeSighting(int sightingId) {
        dao.removeSighting(sightingId);
    }

    @Override
    public void updateSighting(Sighting sighting) {
        Fighter matchingFighter = dao.selectFighterByName(sighting.getFighter().getName());
        Location matchingLocation = dao.selectLocationByName(sighting.getLocation().getName());

        sighting.setFighter(matchingFighter);
        sighting.setLocation(matchingLocation);
        
        dao.updateSighting(sighting);
    }

    @Override
    public Sighting selectSightingById(int sightingId) {
        return dao.selectSightingById(sightingId);
    }

    @Override
    public List<Sighting> selectAllSightings() {
        return dao.selectAllSightings();
    }

    @Override
    public List<Sighting> selectAllSightingsByDate(LocalDate date) {
        return dao.selectAllSightingsByDate(date);
    }

    @Override
    public List<Sighting> selectLast10Sightings() {
        return dao.selectLast10Sightings();
    }

}
