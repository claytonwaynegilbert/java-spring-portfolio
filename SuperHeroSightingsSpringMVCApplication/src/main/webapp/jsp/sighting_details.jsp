<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Sighting Details</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">        
        <link href="${pageContext.request.contextPath}/css/sighting-details.css" rel="stylesheet">
    </head>
    <body>
        <h1>Sighting Details</h1>
        <hr>
        <div class="container">
            <c:if test="${not empty superName}">
                <img width="150" height="250" src="<c:url value="/pictureFolder/${superName}.png"/>"/>
            </c:if>
            <div class="row"> 
                <p><strong><span class="large-text">Hero/Villain:</span></strong> <c:out value="${sighting.fighter.name}"/></p><br>
                <p><strong><span class="large-text">Location:</span></strong> <c:out value="${sighting.location.name}"/></p><br>
                <p><strong><span class="large-text">Date of Sighting:</span></strong> <c:out value="${sighting.sightingOccurence}"/></p>
                <br><br>
                <a href="displayMainPage">
                    <button id="back-button" class="btn btn-primary">Back</button>
                </a>
            </div>
        </div>
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    </body>
</html>
