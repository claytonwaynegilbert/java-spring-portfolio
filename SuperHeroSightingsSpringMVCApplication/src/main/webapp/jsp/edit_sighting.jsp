<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Edit Sighting</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/edit-sighting.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <h1>Edit Sighting</h1>
            <hr>
            <div class="row">
                <div id="description-box" class="col-md-8">
                    <p id="description"> 
                        Edit everything about the Sighting you've just created!<br>
                        Misidentify the Super?<br>
                        Had a moment where you forgot where you live?<br>
                        Put the wrong month down on the date?<br>
                        Here, edit anything you want about the existing Sighting!<br>
                    </p>
                </div>  
            </div>
            <div class="row">
                <div class="col-md-4" id="edit-sighting-image">
                    <img src="https://image.ibb.co/eRbjdw/Binoculars.png"/>
                </div>
            </div>
            <sf:form id="edit-sighting-form" 
                     class="form form-horizontal"
                     role="form"
                     action="editSighting"
                     method="POST"
                     modelAttribute="sighting">
                <div class="form-group" id="name-row">
                    <label for="sighting-name" class="control-label col-md-4">Hero/Villain Name:</label>
                    <div class="col-md-5">
                        <sf:select id="sighting-fighter-name" class="form-control" path="fighter.name">
                            <sf:options items="${fighters}" itemValue="name" itemLabel="name"/>
                        </sf:select>
                        <sf:errors path="fighter.name" cssClass="error"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group" id="location-row">
                        <label for="sighting-location" class="control-label col-md-4">Location:</label>
                        <div class="col-md-5">
                        <sf:select id="sighting-location" class="form-control" path="location.name">
                            <sf:options items="${locations}" itemValue="name" itemLabel="name"/>
                        </sf:select>
                        <sf:errors path="location.name" cssClass="error"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group" id="sighting-row">
                        <label for="sighting-date" class="control-label col-md-4">Sighting Date:</label>
                        <div class="col-md-5">
                        <sf:input type="text" id="sighting-date" class="form-control" path="sightingOccurence"/>
                        <sf:errors path="sightingOccurence" cssClass="error"></sf:errors>
                        <sf:hidden path="sightingId"/>
                    </div>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" id="edit-sighting-button" value="Edit"/>
                    <a href="displayMainPage">
                        <button type="button" id="cancel-button" class="btn btn-primary">Cancel</button>
                    </a>
                </div>
            </sf:form>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>
