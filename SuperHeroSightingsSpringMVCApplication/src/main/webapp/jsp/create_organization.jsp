<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Create Organization</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/create-organization.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <h1>Create Organization</h1>
            <div class="row">
                <div class="navbar">
                    <ul class="nav nav-tabs"> 
                        <sec:authorize access="hasRole('ROLE_USER')">
                            <li role="presentation">
                                <a href="${pageContext.request.contextPath}/main/addFighterPage">
                                    Hero/Villain
                                </a>
                            </li>
                        </sec:authorize>
                        <sec:authorize access="hasRole('ROLE_USER')">
                            <li role="presentation">
                                <a href="${pageContext.request.contextPath}/main/addLocationPage">
                                    Location
                                </a>
                            </li>
                        </sec:authorize>
                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                            <li role="presentation">
                                <a href="${pageContext.request.contextPath}/main/addOrganizationPage">
                                    Organization
                                </a>
                            </li>
                        </sec:authorize>
                        <sec:authorize access="hasRole('ROLE_USER')">
                            <li role="presentation">
                                <a href="${pageContext.request.contextPath}/main/addSightingPage">
                                    Sighting
                                </a>
                            </li>
                        </sec:authorize>
                    </ul>    
                </div>
                <hr>
                <div class="row">
                    <div id="description-box" class="col-md-8">
                        <p id="description"> 
                            Create your own Organization! Heroes can't do everything alone, they need a team!<br>
                            Give your Organization a name, a description, a location, and even add up to 5 members to the team<br>
                            Once you're done, click the Create button and you're done! You're very own Avengers team awaits...<br>
                            Make sure you have a Location already in the system before you create an Organization!<br>
                        </p>
                    </div>  
                </div>
                <h3><c:out value="${errMessage}"/></h3>
                <div class="row">
                    <div class="col-md-4" id="create-organization-image">
                        <img src="https://image.ibb.co/mOa9dw/imageedit_7_6240456737.png" id="img"/>
                    </div>
                </div>
                <sf:form id="create-organization-form" 
                         class="form form-horizontal"
                         role="form"
                         action="createOrganization"
                         method="POST"
                         modelAttribute="organization">
                    <div class="form-group" id="name-row">
                        <label for="organization-name" class="control-label col-md-4">Name:</label>
                        <div class="col-md-5">
                            <sf:input type="text" id="organization-name" class="form-control" path="name" placeholder="Avengers"/>
                            <sf:errors path="name" cssClass="error"></sf:errors>
                            </div>
                        </div>
                        <div class="form-group" id="description-row">
                            <label for="organization-description" class="control-label col-md-4">Description:</label>
                            <div class="col-md-5">
                            <sf:input type="text" id="organization-description" class="form-control" path="description" placeholder="Home to the likes of Captain America, Thor, and The Hulk..."/>
                            <sf:errors path="description" cssClass="error"></sf:errors>
                            </div>
                        </div>
                        <div class="form-group" id="location-row">
                            <label for="organization-location" class="control-label col-md-4">Location Name:</label>
                            <div class="col-md-5">
                            <c:choose>
                                <c:when test="${not empty locations}">
                                    <sf:select id="organization-location" class="form-control" path="location.name">
                                        <sf:options items="${locations}" itemValue="name" itemLabel="name"/>
                                    </sf:select>
                                    <sf:errors path="location" cssClass="error"></sf:errors>
                                </c:when>
                                <c:otherwise>
                                    <p id="empty-list-description">
                                        <c:out value="No Locations in database. Please click Create Location."/>
                                        <a href="addLocationPage">
                                            <button type="button" id="create-location-button" class="btn btn-primary">Create Location</button>
                                        </a>
                                    </p>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                    <div class="form-group" id="contact-row">
                        <label for="organization-contact" class="control-label col-md-4">Contact Number:</label>
                        <div class="col-md-5">
                            <sf:input type="text" id="organization-contact" class="form-control" path="contactNumber" placeholder="1-800-AVENGERS"/>
                            <sf:errors path="contactNumber" cssClass="error"></sf:errors>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" id="create-organization-button" value="Create Organization"/>
                            <a href="displayMainPage">
                                <button type="button" id="cancel-button" class="btn btn-primary">Cancel</button>
                            </a>
                        </div>
                </sf:form>
            </div>
            <!-- Placed at the end of the document so the pages load faster -->
            <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
            <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>



