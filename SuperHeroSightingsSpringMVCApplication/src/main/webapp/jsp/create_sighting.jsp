<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Create Sighting</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/create-sighting.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <h1>Create Sighting</h1>
            <div class="row">
                <div class="navbar">
                    <ul class="nav nav-tabs"> 
                        <sec:authorize access="hasRole('ROLE_USER')">
                            <li role="presentation">
                                <a href="${pageContext.request.contextPath}/main/addFighterPage">
                                    Hero/Villain
                                </a>
                            </li>
                        </sec:authorize>
                        <sec:authorize access="hasRole('ROLE_USER')">
                            <li role="presentation">
                                <a href="${pageContext.request.contextPath}/main/addLocationPage">
                                    Location
                                </a>
                            </li>
                        </sec:authorize>
                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                            <li role="presentation">
                                <a href="${pageContext.request.contextPath}/main/addOrganizationPage">
                                    Organization
                                </a>
                            </li>
                        </sec:authorize>
                        <sec:authorize access="hasRole('ROLE_USER')">
                            <li role="presentation">
                                <a href="${pageContext.request.contextPath}/main/addSightingPage">
                                    Sighting
                                </a>
                            </li>
                        </sec:authorize>
                    </ul>    
                </div>
                <hr>
                <div class="row">
                    <div id="description-box" class="col-md-8">
                        <p id="description"> 
                            Create your own SuperHero/Villain Sighting! Spot a Super flying around your neck of the woods?<br>
                            Reporting is as easy as filling out the form below. Give the name of the Super, your location, and the date of the Sighting<br>
                            Once you're done, click the Create button and you're done..You've just logged your very own Super Sighting!<br>
                            Make sure you have a Super and Location already in the system!
                        </p>
                    </div>  
                </div>
                <div class="row">
                    <div class="col-md-4" id="create-sighting-image">
                        <img src="https://image.ibb.co/jewhyw/imageedit_4_6762385871.png" id="img"/>
                    </div>
                </div>
                <h3><c:out value="${errMsg}"/></h3>
                <sf:form id="create-sighting-form" 
                         class="form form-horizontal"
                         role="form"
                         action="createSighting"
                         method="POST"
                         modelAttribute="sighting">
                    <div class="form-group" id="name-row">
                        <label for="sighting-super-name" class="control-label col-md-4">Hero/Villain Name:</label>
                        <div class="col-md-5">
                            <c:choose>
                                <c:when test="${not empty fighters}">
                                    <sf:select id="sighting-fighter-name" class="form-control" path="fighter.name">
                                        <sf:options items="${fighters}" itemValue="name" itemLabel="name"/>
                                    </sf:select>
                                    <sf:errors path="fighter.name" cssClass="error"></sf:errors>
                                </c:when>
                                <c:otherwise>
                                    <p id="empty-list-description">
                                        <c:out value="No Supers in database. Please click Create Super."/>
                                        <a href="addFighterPage">
                                            <button type="button" id="create-fighter-button" class="btn btn-primary">Create Super</button>
                                        </a>
                                    </p>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                    <div class="form-group" id="location-row">
                        <label for="sighting-location" class="control-label col-md-4">Location Name:</label>
                        <div class="col-md-5">
                            <c:choose>
                                <c:when test="${not empty locations}">
                                    <sf:select id="sighting-location" class="form-control" path="location.name">
                                        <sf:options items="${locations}" itemValue="name" itemLabel="name"/>
                                    </sf:select>
                                    <sf:errors path="location.name" cssClass="error"></sf:errors>
                                </c:when>
                                <c:otherwise>
                                    <p id="empty-list-description">
                                        <c:out value="No locations in database. Please click Create Location."/>
                                        <a href="addLocationPage">
                                            <button type="button" id="create-location-button" class="btn btn-primary">Create Location</button>
                                        </a>
                                    </p>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                    <div class="form-group" id="date-row">
                        <label for="sighting-date" class="control-label col-md-4">Date Of Sighting:</label>
                        <div class="col-md-5">
                            <sf:input type="text" id="sighting-date" class="form-control" path="sightingOccurence" placeholder="10/12/2017"/>
                            <p><c:out value="${dateError}"/></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary" id="create-sighting-button" value="Create"/>
                        <a href="displayMainPage">
                            <button type="button" id="cancel-button" class="btn btn-primary">Cancel</button>
                        </a>
                    </div>
                </sf:form>
            </div>
            <!-- Placed at the end of the document so the pages load faster -->
            <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
            <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>


