<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>SuperHero Sightings</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">        
        <link href="${pageContext.request.contextPath}/css/main-page.css" rel="stylesheet">        
    </head>
    <body>
        <div class="container">
            <div class="jumbotron">
                <img src="https://image.ibb.co/dvcSo6/banner_text.png" alt="banner-text" width="1000" height="90"/>
                <p id="jumbotron-text">A typical CRUD application with a Superhero twist!</p>
            </div>
            <div class="row">
                <div class="navbar">
                    <ul class="nav nav-tabs">
                        <li role="presentation"><a href="${pageContext.request.contextPath}/home">Home</a></li>
                        <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/main/displayMainPage">Manager</a></li>
                        <sec:authorize access="hasRole('ROLE_USER')">
                            <li role="presentation">
                                <a href="${pageContext.request.contextPath}/upload/viewUploadForm">
                                    Upload
                                </a>
                            </li>
                        </sec:authorize>
                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                            <li role="presentation">
                                <a href="${pageContext.request.contextPath}/displayUserList">
                                    Admin(Admin Access Only)
                                </a>
                            </li>
                        </sec:authorize>
                    </ul>    
                </div>

                <c:if test="${pageContext.request.userPrincipal.name != null}">
                    <p>Hello : ${pageContext.request.userPrincipal.name}
                        | <a href="<c:url value="/j_spring_security_logout" />" > Logout</a>
                    </p>
                </c:if>

                <div class="row">
                    <hr>
                    <h2 id="description-header">Information:</h2>
                    <div id="description-box" class="col-md-8">
                        <p id="description"> 
                            Welcome to Superhero Sightings! A Spring application built with Super Heroes in mind.<br>
                            Try and think of this as a simple CRUD application but with an exciting subject matter...SUPER HEROES!<br>
                            You can navigate this home page to create your own Supers, Locations, Organizations, and even Sightings,<br>
                            which lets you document all the Heroes or Villains you have spotted flying around your city!<br>
                            Also available is the SuperHero news feed, which lets you keep up to date on your latest Sightings!
                        </p>
                    </div>  
                </div>
                <div class="col-md-4" id="location">
                    <h3 id="location-table-header">Locations</h3>
                    <table class="table table-hover" id="location-table">
                        <thead>
                        <th width="20%">Name</th>
                        <th width="20%">Description</th>
                        <th width="25%">Address</th>
                        <th width="30%"></th>
                        </thead>
                        <tbody>
                            <c:forEach var="location" items="${locations}">
                                <tr>
                                    <td>
                                        <a href="displayLocationDetails?locationId=${location.locationId}">
                                            <c:out value="${location.name}"/>
                                        </a>
                                    </td>
                                    <td><c:out value="${location.description}"/></td>
                                    <td><c:out value="${location.address}"/></td>
                                    <td>
                                        <sec:authorize access="hasRole('ROLE_USER')">
                                            <a href="displayEditLocationForm?locationId=${location.locationId}">
                                                Edit |
                                            </a>  
                                        </sec:authorize>
                                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                                            <a href="deleteLocation?locationId=${location.locationId}">
                                                Delete
                                            </a>
                                        </sec:authorize>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    <sec:authorize access="hasRole('ROLE_USER')">
                        <a href="addLocationPage">
                            <button type="button" id="create-location" class="btn btn-primary">
                                Create Location
                            </button>
                        </a>
                    </sec:authorize>
                </div>
                <div class="col-md-4">
                    <h3 id="news-feed-header">Sightings Feed:</h3>
                    <div id="news-feed">
                        <div class="col-md-12">
                            <div id="sighting-div">
                                <input type="hidden" id="sighting-count" value="${sightingCount}"/>
                                <c:forEach var="sighting" items="${sightingsFeed}" varStatus="theCount">
                                    <strong>Fighter:</strong> <c:out value="${sighting.fighter.name}"/><br>
                                    <strong>Location:</strong> <c:out value="${sighting.location.name}"/><br>
                                    <strong>Sighting Date:</strong> <c:out value="${sighting.sightingOccurence}"/><br>
                                    <hr>
                                    <br>
                                </c:forEach> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4" id="organization">
                    <h3 id="organization-table-header">Organizations</h3>
                    <table class="table table-hover">
                        <thead>
                        <th width="20%">Name</th>
                        <th width="30%">Location</th>
                        <th width="20%">Number</th>
                        <th width="30%"></th>
                        </thead>
                        <tbody>
                            <c:forEach var="org" items="${organizations}">
                                <tr>
                                    <td>
                                        <a href="displayOrganizationDetails?organizationId=${org.organizationId}">
                                            <c:out value="${org.name}"/>
                                        </a>
                                    </td>
                                    <td><c:out value="${org.location.name}"/></td>
                                    <td><c:out value="${org.contactNumber}"/></td>
                                    <td>
                                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                                            <a href="displayEditOrganizationForm?organizationId=${org.organizationId}">
                                                Edit |
                                            </a>
                                        </sec:authorize>
                                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                                            <a href="deleteOrganization?organizationId=${org.organizationId}">
                                                Delete
                                            </a>
                                        </sec:authorize>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <a href="addOrganizationPage">
                            <button type="button" id="create-organization" class="btn btn-primary">              
                                Create Organization
                            </button>
                        </a>
                    </sec:authorize>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4" id="fighter">
                    <h3 id="fighter-table-header">Heroes/Villains</h3>
                    <table class="table table-hover">
                        <thead>
                        <th width="20%">Name</th>
                        <th width="20%">Alliance</th>
                        <th width="30%">Powers</th>
                        <th width="30%"></th>
                        </thead>
                        <tbody>
                            <c:forEach var="fighter" items="${fighters}">
                                <tr>
                                    <td>
                                        <a href="displayFighterDetails?fighterId=${fighter.fighterId}">
                                            <c:out value="${fighter.name}"/>
                                        </a>
                                    </td>
                                    <td><c:out value="${fighter.alliance}"/></td>
                                    <td><c:out value="${fighter.superPower}"/></td>
                                    <td>
                                        <sec:authorize access="hasRole('ROLE_USER')">
                                            <a href="displayEditFighterForm?fighterId=${fighter.fighterId}">
                                                Edit | 
                                            </a>
                                        </sec:authorize>
                                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                                            <a href="deleteFighter?fighterId=${fighter.fighterId}">
                                                Delete
                                            </a>  
                                        </sec:authorize>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    <sec:authorize access="hasRole('ROLE_USER')">
                        <a href="addFighterPage">
                            <button type="button" id="create-fighter" class="btn btn-primary">
                                Create Fighter
                            </button>
                        </a>
                    </sec:authorize>
                </div>
                <div class="col-md-4"></div>
                <div class="col-md-4" id="sighting">
                    <h3 id="sighting-table-header">Sightings</h3>
                    <table class="table table-hover" id="sighting-table">
                        <thead>
                        <th width="20%">Name</th>
                        <th width="25%">Location</th>
                        <th width="25%">Date</th>
                        <th width="30%"></th>
                        </thead>
                        <tbody>
                            <c:forEach var="sighting" items="${sightings}">
                                <tr>
                                    <td>
                                        <a href="displaySightingDetails?sightingId=${sighting.sightingId}">
                                            <c:out value="${sighting.fighter.name}"/>
                                        </a>
                                    </td>
                                    <td><c:out value="${sighting.location.name}"/></td>
                                    <td><c:out value="${sighting.sightingOccurence}"/></td>
                                    <td>
                                        <sec:authorize access="hasRole('ROLE_USER')">
                                            <a href="displayEditSightingForm?sightingId=${sighting.sightingId}">
                                                Edit |
                                            </a>  
                                        </sec:authorize>
                                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                                            <a href="deleteSighting?sightingId=${sighting.sightingId}">
                                                Delete
                                            </a>
                                        </sec:authorize>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    <sec:authorize access="hasRole('ROLE_USER')">
                        <a href="addSightingPage">
                            <button type="button" id="create-sighting" class="btn btn-primary">
                                Create Sighting
                            </button>
                        </a>
                    </sec:authorize>
                </div>
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/js/home.js"></script>
    </body>
</html>


