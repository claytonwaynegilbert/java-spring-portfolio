<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Superhero Sightings</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">        
    </head>
    <body>
        <div class="container">
            <h1>Add Admin/Sidekick</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation" ><a href="${pageContext.request.contextPath}/home">Home</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/main/displayMainPage">Manager</a></li>
                        <sec:authorize access="hasRole('ROLE_USER')">
                        <li role="presentation">
                            <a href="${pageContext.request.contextPath}/upload/viewUploadForm">
                                Upload
                            </a>
                        </li>
                    </sec:authorize>
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <li role="presentation">
                            <a href="${pageContext.request.contextPath}/displayUserList">
                                Admin(Admin Access Only)
                            </a>
                        </li>
                    </sec:authorize>
                </ul>    
            </div>

            <c:if test="${pageContext.request.userPrincipal.name != null}">
                <p>Hello : ${pageContext.request.userPrincipal.name}
                    | <a href="<c:url value="/j_spring_security_logout" />" > Logout</a>
                </p>
            </c:if>   

            <h2>${error}</h2>

            <br>
            <form id="add-user-form"
                  class="form form-horizontal"
                  method="POST"
                  action="addUser">

                Username: <input type="text" 
                                 class="form-control"
                                 name="userName"/><br>
                Password: <input type="password" 
                                 class="form-control"
                                 name="password"/><br>
                isAdmin: <input type="checkbox"
                                name="isAdmin"
                                value="Yes"/><br><br>

                <input type="submit" class="btn btn-primary" value="Add User"/>
                <a href="${pageContext.request.contextPath}/displayUserList">
                <input type="button" class="btn btn-primary" value="Back">
                </a>
            </form>
        </div>

        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    </body>
</html>
