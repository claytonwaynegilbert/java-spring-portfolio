<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Edit SuperHero/Villain</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/edit-fighter.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <h1>Edit SuperHero/Villain</h1>
            <hr>
            <div class="row">
                <div id="description-box" class="col-md-8">
                    <p id="description"> 
                        Edit everything about the Hero/Villain you've just created!<br>
                        Don't like the name? Change it!<br>
                        Has your Super learned a new super power? State it!<br>
                        Your Super betrayed you and changed sides? Publicize it!<br>
                        Here, you have control, not the Supers!
                    </p>
                </div>  
            </div>
            <div class="row">
                <div class="col-md-4" id="edit-fighter-image">
                    <img src="https://image.ibb.co/gUKxWG/edit_super_hero.jpg"/>
                </div>
            </div>
            <sf:form id="edit-fighter-form" 
                     class="form form-horizontal"
                     role="form"
                     action="editFighter"
                     method="POST"
                     modelAttribute="fighter">
                <div class="form-group" id="name-row">
                    <label for="fighter-name" class="control-label col-md-4">Name:</label>
                    <div class="col-md-5">
                        <sf:input type="text" class="form-control" id="fighter-name" path="name"/>
                        <sf:errors path="name" cssClass="error"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group" id="description-row">
                        <label for="fighter-description" class="control-label col-md-4">Description:</label>
                        <div class="col-md-5">
                        <sf:input type="text" class="form-control" id="fighter-description" path="description"/>
                        <sf:errors path="description" cssClass="error"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group" id="power-row">
                        <label for="super-power" class="control-label col-md-4">Super Powers:</label>
                        <div class="col-md-5">
                        <sf:textarea class="form-control" id="super-power" path="superPower" rows="3"/>
                        <sf:errors path="superPower" cssClass="error"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group" id="alliance-row">
                        <label for="fighter-alliance" class="control-label col-md-4">Alliance:</label>
                        <div class="col-md-5">
                        <sf:input type="text" class="form-control" id="fighter-alliance" path="alliance"/>
                        <sf:errors path="alliance" cssClass="error"></sf:errors>
                        <sf:hidden path="fighterId"/>
                    </div>
                </div>
                <div class="form-group" id="organization-row">
                    <label for="fighter-organizations" class="control-label col-md-4">Organizations:</label>
                    <div class="col-md-6">
                        <c:choose>
                            <c:when test="${not empty organizations}">
                                <table>
                                    <tr>
                                        <td> 
                                            <sf:checkboxes items="${names}" path="organizationNames" id="fighter-organizations" style="margin: 10px 5px 0px 10px"/>
                                        </td>
                                    </tr>
                                </table>
                            </c:when>
                            <c:otherwise>
                                <p id="empty-list-description">
                                    <c:out value="No Organizations in database"/>
                                </p>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" id="edit-fighter-button" value="Edit"/>
                    <a href="displayMainPage">
                        <button type="button" id="cancel-button" class="btn btn-primary">Cancel</button>
                    </a>
                </div>
            </sf:form>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>


