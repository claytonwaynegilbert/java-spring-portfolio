<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Edit Sighting</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/edit-location.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <h1>Edit Location</h1>
            <hr>
            <div class="row">
                <div id="description-box" class="col-md-8">
                    <p id="description"> 
                        Edit everything about the Location you've already created!<br>
                        Here you can rename the location<br>
                        Edit the description of the location<br>
                        Change the address<br>
                        Or even edit the latitude and longitude of your location!<br>
                    </p>
                </div>  
            </div>
            <div class="row">
                <div class="col-md-4" id="edit-location-image">
                    <img src="https://image.ibb.co/jE0Evb/city_buildings.png" id="img"/>
                </div>
            </div>
            <sf:form id="edit-location-form" 
                     class="form form-horizontal"
                     role="form"
                     action="editLocation"
                     method="POST"
                     modelAttribute="location">
                <div class="form-group" id="name-row">
                    <label for="location-name" class="control-label col-md-4">Location Name:</label>
                    <div class="col-md-5">
                        <sf:input type="text" id="location-name" class="form-control" path="name"/>
                        <sf:errors path="name" cssClass="error"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group" id="description-row">
                        <label for="location-description" class="control-label col-md-4">Description:</label>
                        <div class="col-md-5">
                        <sf:input type="text" id="location-description" class="form-control" path="description"/>
                        <sf:errors path="description" cssClass="error"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group" id="address-row">
                        <label for="location-address" class="control-label col-md-4">Address:</label>
                        <div class="col-md-5">
                        <sf:input type="text" id="location-address" class="form-control" path="address"/>
                        <sf:errors path="address" cssClass="error"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group" id="latitude-row">
                        <label for="location-latitude" class="control-label col-md-4">Latitude:</label>
                        <div class="col-md-5">
                        <sf:input type="text" id="location-latitude" class="form-control" path="latitude"/>
                        <sf:errors path="latitude" cssClass="error"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group" id="longitude-row">
                        <label for="location-longitude" class="control-label col-md-4">Longitude:</label>
                        <div class="col-md-5">
                        <sf:input type="text" id="location-longitude" class="form-control" path="longitude"/>
                        <sf:errors path="longitude" cssClass="error"></sf:errors>
                        <sf:hidden path="locationId"/>
                    </div>
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-primary" id="edit-location-button" value="Edit"/>
                    <a href="displayMainPage">
                        <button type="button" id="cancel-button" class="btn btn-primary">Cancel</button>
                    </a>
                </div>
            </sf:form>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>
