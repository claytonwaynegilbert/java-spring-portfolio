<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Create SuperHero/Villain</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/create-fighter.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <h1>Create SuperHero/Villain</h1>
            <div class="row">
                <div class="navbar">
                    <ul class="nav nav-tabs"> 
                        <sec:authorize access="hasRole('ROLE_USER')">
                            <li role="presentation">
                                <a href="${pageContext.request.contextPath}/main/addFighterPage">
                                    Hero/Villain
                                </a>
                            </li>
                        </sec:authorize>
                        <sec:authorize access="hasRole('ROLE_USER')">
                            <li role="presentation">
                                <a href="${pageContext.request.contextPath}/main/addLocationPage">
                                    Location
                                </a>
                            </li>
                        </sec:authorize>
                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                            <li role="presentation">
                                <a href="${pageContext.request.contextPath}/main/addOrganizationPage">
                                    Organization
                                </a>
                            </li>
                        </sec:authorize>
                        <sec:authorize access="hasRole('ROLE_USER')">
                            <li role="presentation">
                                <a href="${pageContext.request.contextPath}/main/addSightingPage">
                                    Sighting
                                </a>
                            </li>
                        </sec:authorize>
                    </ul>    
                </div>
                <hr>
                <div class="row">
                    <div id="description-box" class="col-md-8">
                        <p id="description"> 
                            Create your own Hero/Villain! Everybody wants to be a Hero...So be one!<br>
                            It's as easy as filling out the form below. Give your hero a name, a description, super powers, and more<br>
                            Once you're done, click the Create button and you're done! You've just created your very own Super!<br>
                            Your only limit is your own imagination, so let it out...<br>
                            Make sure you have an Organization in the system before creating your Hero/Villain!<br>
                        </p>
                    </div>  
                </div>
                <div class="row">
                    <div class="col-md-4" id="create-fighter-image">
                        <img src="https://image.ibb.co/cxXNyw/imageedit_1_2827119160.png"/>
                    </div>
                </div>
                <sf:form id="create-fighter-form" 
                         class="form form-horizontal"
                         role="form"
                         action="createFighter"
                         method="POST"
                         modelAttribute="fighter">
                    <div class="form-group" id="name-row">
                        <label for="fighter-name" class="control-label col-md-4">Name:</label>
                        <div class="col-md-5">
                            <sf:input type="text" id="fighter-name" class="form-control" path="name" placeholder="Superman"/>
                            <sf:errors path="name" cssClass="error"></sf:errors>
                            </div>
                        </div>
                        <div class="form-group" id="description-row">
                            <label for="fighter-description" class="control-label col-md-4">Description:</label>
                            <div class="col-md-5">
                            <sf:input type="text" id="fighter-description" class="form-control" path="description" placeholder="Leader of the Justice League"/>
                            <sf:errors path="description" cssClass="error"></sf:errors>
                            </div>
                        </div>
                        <div class="form-group" id="power-row">
                            <label for="super-power" class="control-label col-md-4">Super Powers:</label>
                            <div class="col-md-5">
                            <sf:textarea id="super-power" class="form-control" path="superPower" rows="3" placeholder="Heat-Vision and flight"/>
                            <sf:errors path="superPower" cssClass="error"></sf:errors>
                            </div>
                        </div>
                        <div class="form-group" id="alliance-row">
                            <label for="fighter-alliance" class="control-label col-md-4">Alliance:</label>
                            <div class="col-md-5">
                            <sf:input type="text" id="fighter-alliance" class="form-control" path="alliance" placeholder="Hero"/>
                            <sf:errors path="alliance" cssClass="error"></sf:errors>
                            </div>
                        </div>
                        <div class="form-group" id="organization-row">
                            <label for="fighter-organizations" class="control-label col-md-4">Organizations:</label>
                            <div class="col-md-5">
                            <c:choose>
                                <c:when test="${not empty organizations}">
                                    <sf:checkboxes items="${organizations}" path="organizationNames" id="fighter-organizations" class="form-control"/>
                                </c:when>
                                <c:otherwise>
                                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                                        <p id="empty-list-description">
                                            <c:out value="No Organizations in database. Please click Create Organization."/>
                                            <a href="addOrganizationPage">
                                                <button type="button" id="create-organization-button" class="btn btn-primary">Create Organization</button>
                                            </a>
                                        </p>
                                    </sec:authorize>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary" id="create-fighter-button" value="Create"/>
                        <a href="displayMainPage">
                            <button type="button" id="cancel-button" class="btn btn-primary">Cancel</button>
                        </a>
                    </div>
                </sf:form>
            </div>
            <!-- Placed at the end of the document so the pages load faster -->
            <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
            <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
            <script src="js/home.js"></script>
    </body>
</html>

