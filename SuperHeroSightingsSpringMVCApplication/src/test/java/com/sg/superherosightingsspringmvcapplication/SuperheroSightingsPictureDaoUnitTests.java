/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.superherosightingsspringmvcapplication;

import com.sg.dao.PictureDao;
import com.sg.dto.Picture;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author cjsm12
 */
public class SuperheroSightingsPictureDaoUnitTests {

    PictureDao dao;

    public SuperheroSightingsPictureDaoUnitTests() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        ApplicationContext ctx
                = new ClassPathXmlApplicationContext("test-applicationContext.xml");

        dao = ctx.getBean("picture_dao", PictureDao.class);

        List<Picture> allPictures = dao.getAllPictures();
        for (Picture pic : allPictures) {
            dao.deletePicture(pic.getDisplayTitle());
        }

    }

    @After
    public void tearDown() {
    }

    @Test
    public void testAddGetPicture() {
        Picture picture = new Picture();
        picture.setPictureId(1);
        picture.setDisplayTitle("Superman");
        picture.setFileName("/downloads");

        dao.addPicture(picture);

        Picture fromDao = dao.getPictureByName("Superman");

        assertNotNull(fromDao);
    }

    @Test
    public void testDeletePicture() {
        Picture picture = new Picture();
        picture.setPictureId(1);
        picture.setDisplayTitle("Superman");
        picture.setFileName("/downloads");

        dao.addPicture(picture);

        Picture fromDao = dao.getPictureByName("Superman");

        assertNotNull(fromDao);

        dao.deletePicture(fromDao.getDisplayTitle());

        assertNull(dao.getPictureByName("Superman"));
    }

    @Test
    public void testUpdatePicture() {
        Picture picture = new Picture();
        picture.setPictureId(1);
        picture.setDisplayTitle("Superman");
        picture.setFileName("/downloads");

        dao.addPicture(picture);

        Picture fromDao = dao.getPictureByName("Superman");

        assertNotNull(fromDao);
        
        fromDao.setFileName("/desktop");
        
        dao.updatePicture(fromDao);
        
        assertEquals("/desktop", fromDao.getFileName());
    }
}
