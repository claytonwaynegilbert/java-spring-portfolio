/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.superherosightingsspringmvcapplication;

import com.sg.service.SuperHeroServiceLayer;
import com.sg.dto.Fighter;
import com.sg.dto.Location;
import com.sg.dto.Organization;
import com.sg.dto.Sighting;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author cjsm12
 */
public class SuperheroSightingsDaoServiceLayerUnitTests {

    SuperHeroServiceLayer service;

    public SuperheroSightingsDaoServiceLayerUnitTests() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        ApplicationContext ctx = 
                    new ClassPathXmlApplicationContext("test-applicationContext.xml");
        service = ctx.getBean("service", SuperHeroServiceLayer.class);
        
        List<Organization> organizations = service.selectAllOrganizations();
        for(Organization org : organizations){
            service.removeOrganization(org.getOrganizationId());
        }
        
        List<Sighting> sightings = service.selectAllSightings();
        for(Sighting sighting : sightings){
            service.removeSighting(sighting.getSightingId());
        }
        
        List<Location> locations = service.selectAllLocations();
        for(Location location : locations){
            service.removeLocation(location.getLocationId());
        }
        
        List<Fighter> fighters = service.selectAllFighters();
        for(Fighter fighter : fighters){
            service.removeFighter(fighter.getFighterId());
        }
        
        
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testAddGetFighter() {

        Location location = new Location();
        location.setName("Columbus Ohio");
        location.setDescription("An upscale urban jungle where tech giants have invaded.");
        location.setAddress("124 South Point Street");

        service.addLocation(location);

        Organization organization = new Organization();
        organization.setName("Justice League");
        organization.setDescription(
                "Protectors of America known to house the likes of all time heroes "
                + "such as Superman Batman and Green Lantern");
        organization.setLocation(location);
        organization.setContactNumber("1-800-JUSTICE");

        service.addOrganization(organization);

        Fighter fighter = new Fighter();
        fighter.setName("Superman");
        fighter.setDescription("Leader of Justice League");
        fighter.setSuperPower("Laser Vision");
        fighter.setAlliance("Hero");
        List<Organization> organizations = new ArrayList<>();
        organizations.add(organization);
        fighter.setOrganizationsBelongedTo(organizations);

        service.addFighter(fighter);

        Fighter newFighter = service.selectFighterById(fighter.getFighterId());
        assertEquals(newFighter, fighter);
    }

    @Test
    public void testUpdateFighter() {
        Location location = new Location();
        location.setName("Columbus Ohio");
        location.setDescription("An upscale urban jungle where tech giants have invaded.");
        location.setAddress("124 South Point Street");

        service.addLocation(location);

        Organization organization = new Organization();
        organization.setName("Justice League");
        organization.setDescription(
                "Protectors of America known to house the likes of all time heroes "
                + "such as Superman Batman and Green Lantern");
        organization.setLocation(location);
        organization.setContactNumber("1-800-JUSTICE");

        service.addOrganization(organization);

        Fighter fighter = new Fighter();
        fighter.setName("Superman");
        fighter.setDescription("Leader of Justice League");
        fighter.setSuperPower("Laser Vision");
        fighter.setAlliance("Hero");
        List<Organization> organizations = new ArrayList<>();
        organizations.add(organization);
        fighter.setOrganizationsBelongedTo(organizations);

        service.addFighter(fighter);

        Fighter newFighter = service.selectFighterById(fighter.getFighterId());
        assertEquals(newFighter, fighter);

        newFighter.setSuperPower("Super Sonic Speed");
        service.updateFighter(newFighter);
        Fighter updatedFighter = service.selectFighterById(newFighter.getFighterId());
        assertEquals("Super Sonic Speed", updatedFighter.getSuperPower());
        assertEquals(newFighter, updatedFighter);
    }

    @Test
    public void testDeleteFighter() {
        Location location = new Location();
        location.setName("Columbus Ohio");
        location.setDescription("An upscale urban jungle where tech giants have invaded.");
        location.setAddress("124 South Point Street");

        service.addLocation(location);

        Organization organization = new Organization();
        organization.setName("Justice League");
        organization.setDescription(
                "Protectors of America known to house the likes of all time heroes "
                + "such as Superman Batman and Green Lantern");
        organization.setLocation(location);
        organization.setContactNumber("1-800-JUSTICE");

        service.addOrganization(organization);

        Fighter fighter = new Fighter();
        fighter.setName("Superman");
        fighter.setDescription("Leader of Justice League");
        fighter.setSuperPower("Laser Vision");
        fighter.setAlliance("Hero");
        List<Organization> organizations = new ArrayList<>();
        organizations.add(organization);
        fighter.setOrganizationsBelongedTo(organizations);

        service.addFighter(fighter);

        Fighter newFighter = service.selectFighterById(fighter.getFighterId());
        assertEquals(newFighter, fighter);

        service.removeFighter(fighter.getFighterId());
        Fighter deletedFighter = service.selectFighterById(fighter.getFighterId());
        assertNull(deletedFighter);
    }

    @Test
    public void testAddGetLocation() {
        Location location = new Location();
        location.setName("Columbus Ohio");
        location.setDescription("An upscale urban jungle where tech giants have invaded.");
        location.setAddress("124 South Point Street");

        service.addLocation(location);
        Location addedLocation = service.selectLocationById(location.getLocationId());
        assertEquals(addedLocation, location);
        
        Location foundLocation = service.selectLocationByName(addedLocation.getName());
        assertEquals("Columbus Ohio", foundLocation.getName());
    }

    @Test
    public void testUpdateLocation() {
        Location location = new Location();
        location.setName("Columbus Ohio");
        location.setDescription("An upscale urban jungle where tech giants have invaded.");
        location.setAddress("124 South Point Street");

        service.addLocation(location);
        Location newLocation = service.selectLocationById(location.getLocationId());
        assertEquals(newLocation, location);

        newLocation.setAddress("999 AfterMarket Venue");
        service.updateLocation(newLocation);
        Location updatedLocation = service.selectLocationById(newLocation.getLocationId());
        assertEquals("999 AfterMarket Venue", updatedLocation.getAddress());
        assertEquals(newLocation, updatedLocation);
    }

    @Test
    public void testDeleteLocation() {
        Location location = new Location();
        location.setName("Columbus Ohio");
        location.setDescription("An upscale urban jungle where tech giants have invaded.");
        location.setAddress("124 South Point Street");

        service.addLocation(location);
        Location newLocation = service.selectLocationById(location.getLocationId());
        assertEquals(newLocation, location);

        service.removeLocation(location.getLocationId());
        assertNull(service.selectLocationById(location.getLocationId()));

    }

    @Test
    public void testAddGetOrganization() {
        Location location = new Location();
        location.setName("Columbus Ohio");
        location.setDescription("An upscale urban jungle where tech giants have invaded.");
        location.setAddress("124 South Point Street");

        service.addLocation(location);

        Organization organization = new Organization();
        organization.setName("Justice League");
        organization.setDescription(
                "Protectors of America known to house the likes of all time heroes "
                + "such as Superman Batman and Green Lantern");
        organization.setLocation(location);
        organization.setContactNumber("1-800-JUSTICE");

        service.addOrganization(organization);

        Organization addedOrganization
                = service.selectOrganizationById(organization.getOrganizationId());

        assertEquals(organization, addedOrganization);

    }

    @Test
    public void testUpdateOrganization() {
        Location location = new Location();
        location.setName("Columbus Ohio");
        location.setDescription("An upscale urban jungle where tech giants have invaded.");
        location.setAddress("124 South Point Street");

        service.addLocation(location);

        Organization organization = new Organization();
        organization.setName("Justice League");
        organization.setDescription(
                "Protectors of America known to house the likes of all time heroes "
                + "such as Superman Batman and Green Lantern");
        organization.setLocation(location);
        organization.setContactNumber("1-800-JUSTICE");

        service.addOrganization(organization);

        Organization addedOrganization
                = service.selectOrganizationById(organization.getOrganizationId());

        addedOrganization.setName("Avengers");

        service.updateOrganization(addedOrganization);

        Organization updatedOrganization
                = service.selectOrganizationById(addedOrganization.getOrganizationId());
        assertEquals("Avengers", updatedOrganization.getName());
        assertEquals(addedOrganization, updatedOrganization);
    }

    @Test
    public void testDeleteOrganization() {
        Location location = new Location();
        location.setName("Columbus Ohio");
        location.setDescription("An upscale urban jungle where tech giants have invaded.");
        location.setAddress("124 South Point Street");

        service.addLocation(location);

        Organization organization = new Organization();
        organization.setName("Justice League");
        organization.setDescription(
                "Protectors of America known to house the likes of all time heroes "
                + "such as Superman Batman and Green Lantern");
        organization.setLocation(location);
        organization.setContactNumber("1-800-JUSTICE");

        service.addOrganization(organization);
        service.removeOrganization(organization.getOrganizationId());

        assertNull(service.selectOrganizationById(organization.getOrganizationId()));
    }

    @Test
    public void testAddGetSighting() {
        Location location = new Location();
        location.setName("Columbus Ohio");
        location.setDescription("An upscale urban jungle where tech giants have invaded.");
        location.setAddress("124 South Point Street");

        service.addLocation(location);

        Organization organization = new Organization();
        organization.setName("Justice League");
        organization.setDescription(
                "Protectors of America known to house the likes of all time heroes "
                + "such as Superman Batman and Green Lantern");
        organization.setLocation(location);
        organization.setContactNumber("1-800-JUSTICE");

        service.addOrganization(organization);

        Fighter fighter = new Fighter();
        fighter.setName("Superman");
        fighter.setDescription("Leader of Justice League");
        fighter.setSuperPower("Laser Vision");
        fighter.setAlliance("Hero");
        List<Organization> organizations = new ArrayList<>();
        organizations.add(organization);
        fighter.setOrganizationsBelongedTo(organizations);

        service.addFighter(fighter);

        Sighting sighting = new Sighting();
        sighting.setFighter(fighter);
        sighting.setLocation(location);
        String sightingDate = "10/04/2017";
        sighting.setSightingOccurence(LocalDate.parse(sightingDate, DateTimeFormatter.ofPattern("MM/dd/yyyy")));

        service.addSighting(sighting);

        Sighting addedSighting = service.selectSightingById(sighting.getSightingId());
        assertEquals(addedSighting, sighting);
    }

    @Test
    public void testUpdateSighting() {
        Location location = new Location();
        location.setName("Columbus Ohio");
        location.setDescription("An upscale urban jungle where tech giants have invaded.");
        location.setAddress("124 South Point Street");

        service.addLocation(location);

        Organization organization = new Organization();
        organization.setName("Justice League");
        organization.setDescription(
                "Protectors of America known to house the likes of all time heroes "
                + "such as Superman Batman and Green Lantern");
        organization.setLocation(location);
        organization.setContactNumber("1-800-JUSTICE");

        service.addOrganization(organization);

        Fighter fighter = new Fighter();
        fighter.setName("Superman");
        fighter.setDescription("Leader of Justice League");
        fighter.setSuperPower("Laser Vision");
        fighter.setAlliance("Hero");
        List<Organization> organizations = new ArrayList<>();
        organizations.add(organization);
        fighter.setOrganizationsBelongedTo(organizations);
        
        service.addFighter(fighter);
        
        Fighter fighter2 = new Fighter();
        fighter2.setName("Batman");
        fighter2.setDescription("CoLeader of Justice League");
        fighter2.setSuperPower("Preparation");
        fighter2.setAlliance("Hero");
        List<Organization> organizations2 = new ArrayList<>();
        organizations2.add(organization);
        fighter2.setOrganizationsBelongedTo(organizations2);

        service.addFighter(fighter2);

        Sighting sighting = new Sighting();
        sighting.setFighter(fighter);
        sighting.setLocation(location);
        String sightingDate = "10/04/2017";
        sighting.setSightingOccurence(LocalDate.parse(sightingDate, DateTimeFormatter.ofPattern("MM/dd/yyyy")));

        service.addSighting(sighting);

        Sighting addedSighting = service.selectSightingById(sighting.getSightingId());
        assertEquals(sighting, addedSighting);

        addedSighting.setFighter(fighter2);
        service.updateSighting(addedSighting);
        assertEquals("Batman", addedSighting.getFighter().getName());
        Sighting updatedSighting = service.selectSightingById(addedSighting.getSightingId());
        assertEquals(updatedSighting, addedSighting);
    }

    @Test
    public void testDeleteSighting() {
        Location location = new Location();
        location.setName("Columbus Ohio");
        location.setDescription("An upscale urban jungle where tech giants have invaded.");
        location.setAddress("124 South Point Street");

        service.addLocation(location);

        Organization organization = new Organization();
        organization.setName("Justice League");
        organization.setDescription(
                "Protectors of America known to house the likes of all time heroes "
                + "such as Superman Batman and Green Lantern");
        organization.setLocation(location);
        organization.setContactNumber("1-800-JUSTICE");

        service.addOrganization(organization);

        Fighter fighter = new Fighter();
        fighter.setName("Superman");
        fighter.setDescription("Leader of Justice League");
        fighter.setSuperPower("Laser Vision");
        fighter.setAlliance("Hero");
        List<Organization> organizations = new ArrayList<>();
        organizations.add(organization);
        fighter.setOrganizationsBelongedTo(organizations);
        
        service.addFighter(fighter);

        Sighting sighting = new Sighting();
        sighting.setFighter(fighter);
        sighting.setLocation(location);
        String sightingDate = "10/04/2017";
        sighting.setSightingOccurence(LocalDate.parse(sightingDate, DateTimeFormatter.ofPattern("MM/dd/yyyy")));

        service.addSighting(sighting);

        Sighting addedSighting = service.selectSightingById(sighting.getSightingId());
        assertEquals(sighting, addedSighting);
        service.removeSighting(sighting.getSightingId());
        assertNull(service.selectSightingById(sighting.getSightingId()));
    }
    
    @Test
    public void testGetAllOrganizationsByFighterName() {
        Location location = new Location();
        location.setName("Columbus Ohio");
        location.setDescription("An upscale urban jungle where tech giants have invaded.");
        location.setAddress("124 South Point Street");

        service.addLocation(location);

        Organization organization = new Organization();
        organization.setName("Justice League");
        organization.setDescription(
                "Protectors of America known to house the likes of all time heroes "
                + "such as Superman Batman and Green Lantern");
        organization.setLocation(location);
        organization.setContactNumber("1-800-JUSTICE");

        service.addOrganization(organization);

        Fighter fighter = new Fighter();
        fighter.setName("Superman");
        fighter.setDescription("Leader of Justice League");
        fighter.setSuperPower("Laser Vision");
        fighter.setAlliance("Hero");
        List<Organization> organizations = new ArrayList<>();
        organizations.add(organization);
        fighter.setOrganizationsBelongedTo(organizations);
        
        service.addFighter(fighter);

        List<Organization> list = service.selectAllOrganizationsJoinedByFighter(fighter.getName());
        assertEquals(1, list.size());
    }
}
