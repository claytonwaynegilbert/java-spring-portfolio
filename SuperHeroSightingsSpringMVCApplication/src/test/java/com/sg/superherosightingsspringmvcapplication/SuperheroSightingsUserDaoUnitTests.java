/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.superherosightingsspringmvcapplication;

import com.sg.dao.UserDao;
import com.sg.dto.User;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author cjsm12
 */
public class SuperheroSightingsUserDaoUnitTests {
    
    UserDao dao;
    
    public SuperheroSightingsUserDaoUnitTests() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        ApplicationContext ctx = 
                new ClassPathXmlApplicationContext("test-applicationContext.xml");
        
        dao = ctx.getBean("user_dao", UserDao.class);
        List<User> allUsers = dao.getAllUsers();
        
        for(User user : allUsers){
            dao.deleteUser(user.getUserName());
        }
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testAddGetUser(){
        User user = new User();
        user.setUserId(1);
        user.setUserName("cjsm12");
        user.setPassword("password");
        List<String> authorities = new ArrayList<>();
        authorities.add("admin");
        authorities.add("sidekick");
        user.setAuthorities(authorities);
        
        dao.addUser(user);
        
        List<User> fromDao = dao.getAllUsers();
        assertEquals(1, fromDao.size());
    }
    
    @Test
    public void testDeleteUser(){
        User user = new User();
        user.setUserId(1);
        user.setUserName("cjsm12");
        user.setPassword("password");
        List<String> authorities = new ArrayList<>();
        authorities.add("admin");
        authorities.add("sidekick");
        user.setAuthorities(authorities);
        
        dao.addUser(user);
        
        List<User> fromDao = dao.getAllUsers();
        assertEquals(1, fromDao.size());
        
        dao.deleteUser("cjsm12");
        
        List<User> noUsers = dao.getAllUsers();
        
        assertTrue(noUsers.isEmpty());   
    }
    
    @Test
    public void testUpdateUser(){
        User user = new User();
        user.setUserId(1);
        user.setUserName("cjsm12");
        user.setPassword("password");
        List<String> authorities = new ArrayList<>();
        authorities.add("admin");
        authorities.add("sidekick");
        user.setAuthorities(authorities);
        
        dao.addUser(user);
        
        List<User> fromDao = dao.getAllUsers();
        assertEquals(1, fromDao.size());
        
        user.setUserName("Clayton");
        
        dao.updateUser(user);
        
        assertEquals("Clayton", user.getUserName());
    }

}
