/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springcustomcreatedjsrannotations.dto;

import com.sg.springcustomcreatedjsrannotations.validation.CourseCode;

/**
 *
 * @author cjsm12
 */
public class Student {
    
    //Our custom annotation
    @CourseCode(value="LUV", message="Course code must start with LUV")
    private String courseCode;

    public String getCourseName() {
        return courseCode;
    }

    public void setCourseName(String courseName) {
        this.courseCode = courseName;
    }
    
    
}
