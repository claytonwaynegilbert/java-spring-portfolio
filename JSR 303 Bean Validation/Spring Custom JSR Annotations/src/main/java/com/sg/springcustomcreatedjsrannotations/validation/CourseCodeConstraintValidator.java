/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springcustomcreatedjsrannotations.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 *
 * @author cjsm12
 */

//This class is created to actually do the business logic of the new custom annotation
//The class implements ConstraintValidator and you pass in the CourseCode annotation,
//and as well as the type of data you want to validate this annotation against, which in
//this case, is Strings.
class CourseCodeConstraintValidator implements ConstraintValidator<CourseCode, String> {

    //This is what we will use in our isValid test below, we set it to the value
    //of the annotation value through the use of a constructor below.
    private String coursePrefix;

    @Override
    public void initialize(CourseCode courseCodeValue) {
        this.coursePrefix = courseCodeValue.value();
    }

    //This is the test that Spring will run the annotation value against at runtime.
    //It takes as paramaters the user form input, as well as an additional object
    //that will help with additional error support of need be. This test will take
    //what the user submitted and compare it to the value of the annotation to see if
    //they match up, if so, true will be returned meaning the test passed, else the test
    //will return false and the error will be on the bindingResult object.
    @Override
    public boolean isValid(String userInput, ConstraintValidatorContext additionalErrorHelp) {

        boolean result;

        if (userInput != null) {
            result = userInput.startsWith(coursePrefix);
        } else {
            return true;
        }

        return result;

    }

}
