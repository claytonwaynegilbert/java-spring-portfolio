/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.springcustomcreatedjsrannotations.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

/**
 *
 * @author cjsm12
 */

//@Constraint annotation tells Spring which helper class we are using to enforce our
//business rules regarding the annotation validation
//@Target annotation tells Spring where do we want to beable to apply this custom 
//annotation. We have it set to beable to use it on methods and fields.
//@Retention tells Spring when to run the annotations, we have it set to at runtime.
@Constraint(validatedBy=CourseCodeConstraintValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface CourseCode {

    //Value and message are what we will set on the annotation when applying it. 
    //if nothing is specified, the default values will be set shown below.
    String value() default "LUV";
 
    String message() default "Must start with LUV";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
